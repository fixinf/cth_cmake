//#include "stdafx.h"
//#include "menu.h"
#include "Star_MK.h"
#include "RW_cool.h"
#include "inputs.h"
//#include "EoS_Yak.h"
//#include "CoolClass.h"
//#include "EoS_F.h"

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


CCool_EoS EOS;


bool Init()
{
  char ff[150],ffev[150];	
//  EOS.strS.rw_c.next = 0;

  if (EOS.strS.m_mu <= 100)
  {
	  return true;
      printf("mu_0< 940\n");
	  exit(1);
    }
  strcpy(EOS.strS.mu_name,"");
  gcvt(EOS.strS.m_mu,8,EOS.strS.mu_name);
  gcvt(EOS.strS.m_m,8,EOS.strS.m_name);
	
  strcpy(ff, EOS.strS.rw_c.HOME);
  strcpy(ffev, EOS.strS.rw_c.HOME_EV);
  if(!EOS.strS.m_Qstar)
    {
      switch(EOS.Hybrid.hadron.Hmodel) {
	  case '0' :{ strcat(ff, EoS_lw);
		          strcat(ffev, EoS_lw);} break;
	  case '1' :{ strcat(ff, EoS_nlw);
		          strcat(ffev, EoS_nlw);}break;
	  case '2' :{ strcat(ff, EoS_f);
		          strcat(ffev, EoS_f);}break;
	  case '3' :{ strcat(ff, EoS_hjj);  
		  strcat(ffev, EoS_hjj);}break;
	  case '4' :{ strcat(ff, EoS_FPS);  
		          strcat(ffev, EoS_FPS);
				}
      };
  }else{ strcat(ff,"\\Q_");
         strcpy(ffev,"\\Q_");};

  strcat(ff, EOS.strS.rw_c.File_Name0);
  strcat(ffev, EOS.strS.rw_c.File_Name0);
  strcpy(EOS.strS.rw_c._FIGNAME_,ff);

  strcat(ff, EOS.strS.mu_name);
  strcat(ffev, EOS.strS.mu_name);
  strcat(ff, "-");
  strcat(ffev, "-");
  strcat(ff, EOS.strS.m_name);
  strcat(ffev, EOS.strS.m_name);
  //#ifdef _NONLW_ 
  //strcat(ff, "NL");
  //#endif
  strcpy(EOS.strS.rw_c._CONFIGNAME_,ff);
  strcpy(EOS.strS.rw_c._CONFIGNAMEEV_,ffev);
  
  //	mks.init_EoS_WGB(EOS.strS.m_mu,EOS.strS.m_kT,EOS,f);
  //
  //EOS = lEOS;
 	
  if (EOS.strS.Reset_Profiles()) 
    {
      printf("No opened file \n");
      return 1;
    }

  //	RUNING = 0;
  //delete EOS;
  EOS.strS.noinit = false;	
  return false;
}



int main_sub()
{
  int i;
  EOS.strS.rw_c.next++;
  EOS.m_apply = false;
  EOS.m_msc = false;
  EOS.strS.noinit = true;
  EOS.strS.rw_c.cont_cal_f = false;


  if(!EOS.EK.m_file&&EOS.m_mef) {
    EOS.Make_EoS_FILE(EOS.strS.m_mu,1.0,EOS.strS.m_kT);
    exit(1);
    return 0;
  };

  if(EOS.m_allst) {
    EOS.Make_Star_Config();
    exit(1);
    return 0;
  };
  printf(" Configuration with \n Central Chem.Potential : %g MeV\n", EOS.strS.m_mu);
  printf(" Central Temperature %g MeV\n", EOS.strS.m_kT);
  printf(" maximum steps during the Cooling : %d\n\n", EOS.strS.num_t);


  if(EOS.m_make_nf) EOS.Make_Star_Config();
  else EOS.OnApply();

  if(EOS.strS.Init_Profiles()) 
    {
      printf("\n");
      printf("Running the star maker\n");
      EOS.Make_Star_Config();

      EOS.strS.Init_Profiles();
      EOS.OnApply();
    };

 


  //printf("oioi\n");
  for (i = 0; i <= EOS.strS.num_t ;i++)
    {
      if(EOS.strS.Cool_Star()) return 1;
    };


  return 0;
}

int main(int argc, char* steps[])
{

//EK.main_Yak_eos();
//return 0;


  bool w,w1, muset = true;
   int mus_i = 0;

   if (argc >= 2) 
   {   
       muset = false;
       EOS.strS.m_mu = atof(steps[1]); 
     }
   if (argc >= 3)  EOS.strS.num_t = atoi(steps[2]); 
   if (argc >= 4)  EOS.strS.m_kT = atof(steps[3]); 
   EOS.strS.rw_c.next = -1;
   if(!muset) main_sub(); else
     {
	   w = false;
	   while (mus_i < EOS.Nmu0)
	   {
		   if (w) mus_i++;
		   w = w1 = EOS.strS.rw_c.lowbranch = false;
		   EOS.strS.m_m = EOS.Msetdat[mus_i];
		   EOS.strS.m_mu = EOS.musetdat[mus_i];
		   w = EOS.strS.m_mu < 100;
		   if (w) {
			   EOS.strS.m_mu = EOS.musetdat1[mus_i];
			   w1 = EOS.strS.m_mu < 100;
		   }
			  if(!w1)  main_sub();
		  if (EOS.m_IIIF&&!w)
			   {
				   EOS.strS.m_mu = EOS.musetdat1[mus_i];
				   w = EOS.strS.m_mu > 100;
				   if (w) {  EOS.strS.rw_c.lowbranch = true; main_sub();
				   }
					   				   else  w = true;				   
			   }
		   }
	   }
//	   free(EOS.musetdat);
//	   free(EOS.Msetdat);

/**/
 //// Check Hybrid-EoS
// EoS_Hybrid f;
 //double n[30];
// f.quark.m_file = true;
//  f.quark.In_file();
//  f.hadron.m_file= true;
//  f.hadron.In_file();
//  f.InitPhaseTrans('2',false,false,false,0,1,1,0.);
//  for(double muB=900; muB<=1800; muB+=1){
//    cout << muB;
//    cout <<  " "  << f.p_(muB,.01) ;
//    cout <<  " "  << f.n_(muB,.01,n) ;
//    cout <<  " "  << f.en_(muB,.01) ;
//    cout << endl;
// }

// //Save Hadron-EoS as data sets
//    EoS_Hadron fh; 
//    double nvec[20];
//    fh.m_file = false;
//    fh.Hmodel='3';
//    ofstream fmue("./tst_hadron/tst_mue.dat");
//    ofstream fmub("./tst_hadron/tst_mub.dat");
//    ofstream fp("./tst_hadron/tst_p.dat");
//    ofstream fc("./tst_hadron/tst_charge.dat");
//    ofstream fe("./tst_hadron/tst_eps.dat");
//    ofstream fs("./tst_hadron/tst_s.dat");
//    ofstream fn("./tst_hadron/tst_nbar.dat");
//    ofstream fpro("./tst_hadron/tst_n_prot.dat");
//    ofstream fneu("./tst_hadron/tst_n_neut.dat");
//    ofstream fele("./tst_hadron/tst_n_elec.dat");
//    ofstream fmuo("./tst_hadron/tst_n_muon.dat");
//    double dmue=.1;
//    for(double mue=0.;mue<=300.;dmue*=1.02){
//      mue+=dmue;
//      cout<<mue<<endl;
//      fmue<<mue<<endl;
//    }
//    double dmuB=.2;
//    for(double muB=900; muB<=2200; dmuB*=1.02){
//      muB+=dmuB;
//      fmub<<muB<<endl;
//      cout<<muB<<endl;
//    }
//    dmue=.1;
//    for(double mue=0.;mue<=300.;dmue*=1.02){
//      cout << mue<<endl;
//      dmuB=.2;
//      for(double muB=900; muB<=2200; dmuB*=1.02){
//        fp<<fh.p_(muB,mue,.01)<<" ";
//        fc<<fh.charge_(muB,mue,.01)<<" ";
//        fe<<fh.en_(muB,mue,.01)<<" ";
//        fs<<fh.s_(muB,mue,.01)<<" ";
//        fn<<fh.n_(muB,mue,.01,nvec)<<" ";
//        fpro<<nvec[1]<<" ";
//        fneu<<nvec[0]<<" ";
//        fele<<nvec[13]<<" ";
//        fmuo<<nvec[14]<<" ";
//        muB+=dmuB;
//      }
//      fp<<endl;
//      fc<<endl;
//      fe<<endl;
//      fs<<endl;
//      fn<<endl;
//      fpro<<endl;
//      fneu<<endl;
//      fele<<endl;
//      fmuo<<endl;
//      mue+=dmue;
//    }
//    fp.close();
//    fc.close();
//    fe.close();
//    fs.close();
//    fn.close();
//    fmue.close();
//    fmub.close();
//    fpro.close();
//    fneu.close();
//    fele.close();
//    fmuo.close();
//    cout<<"READY"<<endl;

// //// Save Bag-EoS as data set
//   EoS_Bag fb;
//   double nvec[20];
//   ofstream fmue("./tst_quark/tst_mue.dat");
//   ofstream fmub("./tst_quark/tst_mub.dat");
//   ofstream fp("./tst_quark/tst_p.dat");
//   ofstream fc2("./tst_quark/tst_charge.dat");
//   ofstream fs2("./tst_quark/tst_s.dat");
//   ofstream fe("./tst_quark/tst_eps.dat");
//   ofstream fn("./tst_quark/tst_nbar.dat");
//   ofstream fu("./tst_quark/tst_n_u.dat");
//   ofstream fd("./tst_quark/tst_n_d.dat");
//   ofstream fs("./tst_quark/tst_n_s.dat");
//   ofstream fc("./tst_quark/tst_n_c.dat");
//   ofstream fele("./tst_quark/tst_n_elec.dat");
//   ofstream fmuo("./tst_quark/tst_n_muon.dat");
 
//   for(double mue=0.;mue<=400.;mue+=2.){
//     fmue<<mue<<endl;
//   }

//   for(double muB=800; muB<=2500; muB+=5.){
//     fmub<<muB<<endl;
//   }
  
//   for(double mue=0.;mue<=400;mue+=2.){
//     cout << mue<<endl;
//     for(double muB=800; muB<=2500; muB+=5.){
//       double p=fb.p_( muB,mue,.01);
//       fp<<p<<" ";
//       fc2<<fb.charge_( muB,mue,.01)<<" ";
//       double s=fb.s_(muB,mue,.01);
//       fs2<<s<<" ";
//       double n=fb.n_(muB,mue,.01,nvec);
//       fn<<n<<" ";
//       fe<<n*muB+.01*s-p<<" ";
//       fu<<nvec[0];
//       fd<<nvec[1];
//       fs<<nvec[2];
//       fc<<nvec[3];
//       fele<<nvec[4];
//       fmuo<<nvec[5];
//     }
//     fp<<endl;
//     fc2<<endl;
//     fs2<<endl;
//     fe<<endl;
//     fn<<endl;
//     fu<<endl;
//     fd<<endl;
//     fs<<endl;
//     fc<<endl;
//     fele<<endl;
//     fmuo<<endl;
//   }
//   fp.close();
//   fc2.close();
//   fs2.close();
//   fn.close();
//   fmue.close();
//   fmub.close();
//   fe.close();
//   fu.close();
//   fd.close();
//   fs.close();
//   fc.close();
//   fele.close();
//   fmuo.close();
//   cout<<"READY"<<endl;
//system("pause");
  return 1;
}
