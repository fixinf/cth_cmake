#if !defined(AFX_EOS_NPE_H__40E74E28_F907_4A97_8E6E_DFDC7C65E877__INCLUDED_)
#define AFX_EOS_NPE_H__40E74E28_F907_4A97_8E6E_DFDC7C65E877__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EoS_NPE.h : header file
//
#include "EoS_LW.h"
#include "EoS_HB.h"	// Added by ClassView
/////////////////////////////////////////////////////////////////////////////
// EoS_NPE window

class EoS_NPE : public EoS_LW
{
// Construction
public:
	EoS_NPE();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(EoS_NPE)
	//}}AFX_VIRTUAL

// Implementation
public:
	EoS_HB mu_;
	EoS_HB el_;
	double 	Charge(double mu_B, double mjue, double T);

	double B2ewal(double mjuB,double t,double *mju_mtr,double *m_mtr);
	void marticer(double mjuB,double mjue,double t,double *mju_mtr,double *m_mtr);
	double s_(double mjue,double T,double *mju_mtr,double *m_mtr);
	double n_(double mjue,double T,double *mju_mtr,double *m_mtr, double *n);
   double  p_(double mjue,double T,double *mju_mtr,double *m_mtr);
   double  en_(double mjue,double T,double *mju_mtr,double *m_mtr);
   void dn_(double mjue,double T,double *mju_mtr,double *m_mtr, double *dn);
   void ds_(double mjue,double T,double *mju_mtr,double *m_mtr, double *ds);

	virtual ~EoS_NPE();

	// Generated message map functions

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EOS_NPE_H__40E74E28_F907_4A97_8E6E_DFDC7C65E877__INCLUDED_)
