// EoS_Hadron.cpp:
// verallgemeinerte Klasse zur Beschreibung
// hadronischer Zustandsgleichungen
// Vorerst nur fuer die Nutzung bei
// der Konstruktion von Phasen"uberg"angen gedacht
#include <stdio.h>
#include "EoS_NLW.h"	// nichtlineares Walecka
#include "EoS_NPE.h"    // NPE-Phase
#include "HBerg.h"      // HDD - HHJ
//#include "EoS_file.h"    // Data sets
#include "EoS_crust.h"


//globale programm"ubergreifende Parameter
extern double B;           // Bag-Konstante
extern double G_s_QUARK ;  // Phasenraumfaktor

class EoS_Hadron : public EoS_HB
{
 private:

  double gsig,gom,gro;

 public:
//	 int In_file();

	 double m_mtr[20];
     double mju_mtr[20];

	 double eps,s,nb,pr,nn[20];
	 char Hmodel;
//	 double mu_e;
	 double kT;
	 double mu_b;
  EoS_Hadron(void);  //Konstruktor
  EoS_Hadron(char);  //Konstruktor
  // kind =
  // 0: NPE
  // 1: nichtlineares Walecka
  // 2: HBerg

  //  zu verarbeitende Klassen
  //-------------------------------
  EoS_NPE Hadron_npe;
  EoS_NLW Hadron_nlw;
  HBerg   Hadron_hb;
  EoS_crust EC;
//  EoS_file Hadron_file;

//bool m_file;

  double n_(double mu, double mue, double T,double* n);
  double s_(double mu, double mue, double T);
  double en_(double mu,  double mue, double T);
  //	double p_(double mu, double mue, double T);
  //	int kind_(double mu,double mue, double T);

  // member Funktionen:
  //-------------------------------
  //teils zwei Implementierungen:
  //x_(double muB) : 
  //    ladungsneutrales Beta-GGW
  //x_(double muB, double muE) : 
  //    nur Beta-GGW
  
  //Druck
  double p_(double muB, double T);
  double mue_(double muB, double T);

  double p_(double muB, double muE, double T);
  //Ladung der Phase
  double charge_(double muB, double muE, double T);
};
