// HBerg.cpp : implementation file
//

//#include "stdafx.h"
//#include "cooling_nlw.h"
#include "HBerg.h"
#include "Zbrent.h"
#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// HBerg

#define E0 15.8 
#define S0 32 
#define n0 0.16
#define sx 0.2
#define Gamma 0.6
#define hc 197.32705
#define M_pi 3.14159265358979323846 
#define M_N 939.56563
#define M_P 938.272
#define M_MU 105.658389 
#define n_c (3*n0)
#define m_pi 140
#define m_pi4 (m_pi*(m_pi/hc)*(m_pi/hc)*(m_pi/hc))
#define _BETA_  0.0
#define _ALFA_  1.0

#define _BETTA_ 6
//5 
//6 - standart
#define _DELTA_  4
//2
//4 - standart
#define _ALPHA_  (- 0.02)
//(- 0.05)
//(- 0.02) - standart



HBerg* hb;
double nBB,muEE, muB, xxB;



HBerg::HBerg()
{
}

HBerg::~HBerg()
{
}

double u(double n)
{
double y = n/n0;
double z = _ALPHA_*exp(- powf(_BETTA_/y,_DELTA_));
return y/(1 + y*z); 
}


double Derivative_u(double n)
{
double y = n/n0;
double z = (y - u(n))/(y*u(n));
return powf(u(n)/y,2)*(1 -_DELTA_* _BETTA_*z*powf(_BETTA_/y,_DELTA_-1))/n0;
}

/////////////////////////////////////////////////////////////////////////////
// HBerg message handlers

double HBerg::eps(double n, double x)
{
double epsa = mu_n(n,x) * n * (1 - x) + mu_p(n,x) *x *n - p (n,x); 

//double epsa = n*((E0*n*(n - n0*(2 + s)))/(n0*(n0 + n*s)) + 
//     powf(n/n0,Gamma)*S0*powf(1 - 2*x,2)) + M_N*n
//	 + eps_pi(n)
;

return epsa;
}



double HBerg::xBeta(double n, double mue)
{

double x =	(1 - mue /(4* powf(fabs(u(n)),Gamma)*S0) )/2.0; 

if (muB<955) return xB(n);	

return fabs(x); 	 


}





double HBerg::mu_n(double n, double x)
{

	double mun0,mun00;
		
mun00= -((E0*n*(-2*powf(n,2)*sx + 2*powf(n0,2)*(2 + sx) + 
          n*n0*(-3 + 2*sx + powf(sx,2))))/(n0*powf(n0 + n*sx,2)))
     + powf(n/n0,Gamma)*S0*(-1 + 2*x)*(-1 + 2*x*(-1 + Gamma) - Gamma);


mun0 = S0*powf(1 - 2*x,2)*powf(u(n),Gamma) + 4*S0*(1 - 2*x)*x*powf(u(n),Gamma) + 
   (E0*u(n)*(-2 - sx + u(n)))/(1 + sx*u(n)) + 
   n*(S0*powf(1 - 2*x,2)*Gamma*powf(u(n),-1 + Gamma) + 
      (E0*(-1 + u(n))*(2 + sx + sx*u(n)))/powf(1 + sx*u(n),2))*Derivative_u(n);


if(mun0<0.0) return M_N;

return M_N + mun0;

//	  + mu_pi(n)
}

double HBerg::p(double n, double x)
{
	double ppp,p00;

//p00 = n*((E0*n*(n - n0)*(n*sx + n0*(2 + sx)))/(n0*powf(n0 + n*sx,2)) + 
//     powf(n/n0,Gamma)*S0*powf(1 - 2*x,2)*Gamma); 
 
ppp=powf(n,2)*(S0*powf(1 - 2*x,2)*Gamma*powf(u(n),-1 + Gamma) + 
     (E0*(-1 + u(n))*(2 + sx + sx*u(n)))/powf(1 + sx*u(n),2))*Derivative_u(n);
return ppp;
	 //	 + P_pi(n)	 ;
}

double HBerg::mu_p(double n, double x)
{ double mup0,mup_old,uu,du;
uu=u(n);
du = Derivative_u(n);
mup0=powf(u(n),-1 + Gamma)*(S0*(-3 - 4*(-2 + x)*x)*u(n) + 
      n*S0*powf(1 - 2*x,2)*Gamma*Derivative_u(n)) + 
   (-(E0*(2 + sx - u(n))*u(n)*(1 + sx*u(n))) + 
      E0*n*(-1 + u(n))*(2 + sx + sx*u(n))*Derivative_u(n))/powf(1 + sx*u(n),2);

//
//
//mup_old= -((E0*n*(-2*powf(n,2)*sx + 2*powf(n0,2)*(2 + sx) + 
//          n*n0*(-3 + 2*sx + powf(sx,2))))/(n0*powf(n0 + n*sx,2)))
//     + powf(n/n0,Gamma)*S0*(-1 + 2*x)*(3 + 2*x*(-1 + Gamma) - Gamma) 
//	// + mu_pi(n)
//	 ;



return M_N + mup0 ;
}


double HBerg::mu_e(double ne)
{
return powf(3*M_pi*M_pi*ne,1.0/3.0)*hc;
}

double HBerg::n_mu(double mu_e)
{ double pf;
if(mu_e >= M_MU)
{
pf =sqrt(mu_e*mu_e - M_MU*M_MU);
return 1/(3*M_pi*M_pi*hc*hc*hc)*pf*pf*pf;
}else return 0;
}


double HBerg::n_e(double mu_e)
{ double pf;
pf =mu_e;
return 1/(3*M_pi*M_pi*hc*hc*hc)*pf*pf*pf;
}


double CHBETA(double xx)
{
	return hb->Charge(nBB,xx,hb->BetaEQ_mue(nBB,xx));
}

double muB_def(double n)
{
	xxB=hb->xBeta(n,muEE);
	return muB - hb->mu_n(n,xxB); 
}

   double check1;




double HBerg::Charge(double mu_b, double mu_e)
{
    muEE = mu_e;
	muB = mu_b;
	hb=this;
 	nBB = zbrent(muB_def,0.00000001, 20*n0,1e-7);

	return Charge(nBB, xxB, mu_e);
}


double HBerg::Eps(double mu_b, double mu_e)
{
    muEE = mu_e;
	muB = mu_b;
	hb=this;
 	nBB = zbrent(muB_def,0.00000001, 20*n0,1e-8);


	return eps(nBB, xxB)+ eps_e(mu_e) + eps_mu(mu_e);
}

double HBerg::P(double mu_b, double mu_e)
{
    double EE;
#ifdef _SYMMAT_
//EE = eps_bh(mu_b);
return p_bh(mu_b);
#endif
	EE = Eps(mu_b, mu_e);
	return mu_b * nBB - mu_e*Charge(nBB, xxB, mu_e) - EE;

}

double HBerg::n_B(double mu_b, double mu_e)
{
    muEE = mu_e;
	muB = mu_b;
		hb=this;
//    xxB = zbrent(BETAeq,0.000001,1.0,1e-7);
	nBB = zbrent(muB_def,0.0, 20*n0,1e-8);

	return nBB;
}


double HBerg::n_(double mu_b, double mu_e, double *n)
{
double check;
    muEE = mu_e;
	muB = mu_b;
	hb=this;
	nBB = zbrent(muB_def,0.0000001, 20*n0,1e-8);

//    xxB = zbrent(BETAeq,0.000001,1.0,1e-8);

//check = muB_def(nBB);

//nBB=findnb(mu_b);
    n[0] = (1 - xxB) * nBB;
	n[1] = xxB * nBB;
	n[2] = n_e(mu_e);
	n[3] = n_mu(mu_e); 

//if (mu_b < 950) return 0.01*nBB;

	return nBB;
}


double HBerg::p_Frac(double mu_b, double mu_e)
{
    muEE = mu_e;
	muB = mu_b;
//    xxB = zbrent(BETAeq,0.001,1.0,1e-5);
	nBB = zbrent(muB_def,0.0000001, 20*n0,1e-8);

	return xxB;
}


double HBerg::Charge(double n, double x, double mu_e)
{
	return n*x - n_e(mu_e) - n_mu(mu_e);
}

double HBerg::BetaEQ_mue(double n, double x)
{
return mu_n(n,x) - mu_p(n,x);
}

double HBerg::xB(double n)  //with miuons
{
#ifndef _SYMMAT_ 
	nBB = n;
	hb = this;
return	zbrent(CHBETA,0.0001,1.0,1e-5);
#else
	return 0.5;
#endif
}

double HBerg::p_e(double mu_e)
{
return mu_e *(mu_e/hc)*(mu_e/hc)*(mu_e/hc)/(12*M_pi*M_pi);
//powf(3*M_pi*M_pi*ne*ne*ne*ne,1.0/3.0)*hc/4.0;
}

double HBerg::eps_e(double mu_e)
{
return 3*p_e(mu_e); 
//3* powf(3*M_pi*M_pi*ne*ne*ne*ne,1.0/3.0)*hc/4.0;
}

double HBerg::eps_mu(double mu_e)
{
	double pf;
	if(mu_e>M_MU){
	pf = sqrt(mu_e*mu_e - M_MU*M_MU);
return	(mu_e*pf*
      (-powf(M_MU,2) + 2*powf(mu_e,2)) - 
     powf(M_MU,4)*log((mu_e + pf)/M_MU))/
   (8.*powf(hc,3)*powf(M_pi,2));
//return mu_e *(mu_e/hc)*(mu_e/hc)*(mu_e/hc)/(12*M_pi*M_pi);
//powf(3*M_pi*M_pi*ne*ne*ne*ne,1.0/3.0)*hc/4.0;
	}else return 0.0;
}

double HBerg::p_mu(double mu_e)
{
	double pf;
	if(mu_e>M_MU){
	pf = sqrt(mu_e*mu_e -M_MU*M_MU);
return
	(mu_e*pf*
      (-5*powf(M_MU,2) + 2*powf(mu_e,2)) + 
     3*powf(M_MU,4)*log((mu_e + pf)/M_MU))/
   (24.0*powf(hc,3)*powf(M_pi,2));
//(x*Sqrt(-Power(m,2) + Power(x,2))*(-5*Power(m,2) + 2*Power(x,2)) + 
//     3*Power(m,4)*Log(x + Sqrt(-Power(m,2) + Power(x,2))))/
//   (24.*Power(hc,3)*Power(Pi,2))
 
//mu_e*n_e(mu_e) - eps_mu(mu_e); 
//3* powf(3*M_pi*M_pi*ne*ne*ne*ne,1.0/3.0)*hc/4.0;
	}else return 0.0;
}


double HBerg::xb_0(double n)
{
return (16 + (powf(2,0.3333333333333333)*hc*
        powf(n,0.3333333333333333)*
        powf(M_pi,0.6666666666666666)*
        (-(powf(2,0.3333333333333333)*hc*
             powf(n,0.3333333333333333)*
             powf(M_pi,0.6666666666666666)) + 
          powf(-24*powf(n/n0,(3*Gamma)/2.)*powf(S0,1.5) + 
            sqrt(2*powf(hc,3)*n*powf(M_pi,2) + 
              576*powf(n/n0,3*Gamma)*powf(S0,3)),
           0.6666666666666666)))/
      (powf(n/n0,(3*Gamma)/2.)*powf(S0,1.5)*
        powf(-24*powf(n/n0,(3*Gamma)/2.)*powf(S0,1.5) + 
          sqrt(2*powf(hc,3)*n*powf(M_pi,2) + 
            576*powf(n/n0,3*Gamma)*powf(S0,3)),0.3333333333333333)
        ))/32.;
}

//  HBerg hbeos;
double mu_ext;

double Beta_eq(double n)
{double u=mu_ext - hb->mu_n(n,hb->xB(n));
return u;
}

double HBerg::findnb(double mu_b)
{double n1;
	mu_ext = mu_b;
	hb = this;

n1 = zbrent(Beta_eq,0.0001,20*n0,1e-8);
	return n1;
}

double HBerg::p_bh(double mu_b)
{
	double nb = findnb(mu_b);
    double xxb = 0.5;
	double p_L = 0;
#ifndef _SYMMAT_
         xxb = xB(nb);
	double mue = BetaEQ_mue(nb,xxb);
	p_L=p_e(mue)+ p_mu(mue);
#endif
return p(nb,xxb) + p_L ;
}

double HBerg::n_bh(double mu_b)
{
	return  findnb(mu_b);
}

double HBerg::mue(double mu_b)
{
#ifdef _SYMMAT_
	return 0;
#else
	double nb = findnb(mu_b);
	double xxb = xB(nb);
return 	BetaEQ_mue(nb,xxb);
#endif
}

double HBerg::eps_bh(double mu_b)
{
	double nb = findnb(mu_b);
    double xxb = 0.5;
	double eps_L = 0;
#ifndef _SYMMAT_
	xxb = xB(nb);
	double mue = BetaEQ_mue(nb,xxb);
eps_L = eps_e(mue)+ eps_mu(mue);
#endif
return eps(nb,xxb) + eps_L;
}


double HBerg::eps_pi(double n)
{
//return 0.0;
	double yy = (n/n_c - 1);
	if(yy > 0)
return  - m_pi4 * _BETA_ * yy* tanh(_ALFA_*yy);
	else 
		return 0.0;
}

double HBerg::mu_pi(double n)
{
	double zz= m_pi4* _BETA_;
	double yy = (n/n_c - 1);
//return 0.0;
	if(yy > 0){
//zz=yy*yy;
//zz=  1.0/cosh(_ALFA_*yy*yy);
//zz = - m_pi4 * _BETA_ * yy*sqrt(_ALFA_)*zz*zz/n_c;
zz=	-  m_pi4 * _BETA_ *( 2* yy * _ALFA_ + sinh(2*yy))
                      /cosh(_ALFA_*yy)/cosh(_ALFA_*yy)/2.0/n_c;
return zz; 
	}else 
		return 0.0;
}

double HBerg::P_pi(double n)
{
//	return 0.0;
return  n*mu_pi(n) - eps_pi(n);
}
