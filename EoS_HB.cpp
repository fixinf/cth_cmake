//#include "stdafx.h"
// EoS_HB.cpp : implementation file
// 
 
#include "EoS_HB.h" 
#include <math.h> 
 
#ifdef _DEBUG 
#define new DEBUG_NEW 
#undef THIS_FILE 
static char THIS_FILE[] = __FILE__; 
#endif 
 
#define hc3 (197.32705*197.32705*197.32705) 
 
#define M_pi        3.14159265358979323846 
 
#define M_1_SQRTPI  0.564189583547756286948 
 
#define M_SQRT_2    0.707106781186547524401 
 
#define zero 1.e-10 
extern double qgauss64(double (*func)(double),double a,double b); 
extern double qgaus400(double (*func)(double),double a,double b); 
 
double mu,kt,Mass; 
double (*Om)(double); 
/*1/(1+exp((x-mu)/kt)))->(kt->0)->thetafunction(x-mu)  
  1./(2*kt*(1+cosh((x-mu)/kt)))->(kt->0)->deltafunction(x-mu)*/ 
 
double Efun(double x) 
{ 
	return x*sqrt(x*x-Mass*Mass)*Om(x); 
} 
 
double dfunyint(double x) 
{ 
	double ret_urn= 2.*kt*( 1+cosh((x-mu)/kt) ); 
       ret_urn=Efun(x)/ret_urn; 
	   return ret_urn; 
} 
double funyint(double x) 
{ 
	return Efun(x)/(1+exp((x-mu)/kt)); 
} 
 
double relint(double Chemical_Potential,double Temperature,double Mass_of_Particle, 
	double Degeneration_Factor,int Type_of_Integrateable_Function, 
	double (*Additional_Function_depends_on_Energy)(double)) 
{ 
	int Ftype; 
	double g,a,ret_urn,acc=15.0; 
  
	g=Degeneration_Factor; 
    Ftype=Type_of_Integrateable_Function; 
 mu=Chemical_Potential; 
 kt=Temperature;if (kt<1.e-11) kt=1.e-11;/*Why less than a 0.1K Superfuidity??*/ 
 Mass=Mass_of_Particle; 
 Om=Additional_Function_depends_on_Energy; 
 a=mu-acc*kt; if (a<Mass) a=Mass; 
  
 if (kt<zero) 
	if (mu<=Mass) ret_urn=zero*zero*zero; 
	else if (Ftype) ret_urn=Efun(mu); 
		else ret_urn=qgauss64(Efun,Mass,mu); 
 
  else  if (  ( (mu+acc*kt-Mass)<=zero )||( (mu+acc*kt-a)<zero )  ) ret_urn=zero*zero*zero; 
	else if (Ftype) ret_urn=qgauss64(dfunyint,a,mu+acc*kt);  
		else ret_urn=qgauss64(funyint,Mass,mu+acc*kt); 
 
return g/2./M_pi/M_pi*ret_urn; 
} 
 
///////////////////////////////////////////////////////////////////////////// 
// EoS_HB 
 
EoS_HB::EoS_HB() 
{ 
} 
 
EoS_HB::~EoS_HB() 
{ 
} 
 
 
///////////////////////////////////////////////////////////////////////////// 
// EoS_HB message handlers 
 
double nfun(double x)  
{ 
	return 1.; 
} 
 
double pfun(double x) 
{ 
	return (x*x-Mass*Mass)/x/3.; 
} 
 
double efun(double x)  
{ 
	return x; 
} 
 
double sfun(double x)  
{ 
	return (x*x-Mass*Mass)/x/3.*(x-mu); 
} 
 
 
double EoS_HB::n_(double mu, double T) 
{ 
return relint(mu,T,mass,g,0,nfun)/hc3+zero; 
} 
 
double EoS_HB::s_(double mu, double T) 
{ 
return relint(mu,T,mass,g,1,sfun)/T/hc3+zero; 
} 
 
double EoS_HB::p_(double mu, double T) 
{ 
return relint(mu,T,mass,g,0,pfun)/hc3+zero; 
} 
 
double EoS_HB::en_(double mu, double T) 
{ 
return relint(mu,T,mass,g,0,efun)/hc3+zero; 
} 
 
 
double dndmjufun(double x)  
{ 
	return 1.; 
} 
 
double dndmassfun(double x)  
{ 
	return Mass/x; 
} 
 
double dndtfun(double x)  
{ 
	return (x-mu); 
} 
 
 
double EoS_HB::dn_dmu(double mu, double T) 
{ 
 return relint(mu,T,mass,g,1,dndmjufun)/hc3+zero; 
} 
 
double EoS_HB::dn_dm(double mu, double T) 
{ 
 return -relint(mu,T,mass,g,1,dndmassfun)/hc3-zero; 
} 
 
double EoS_HB::dn_dT(double mu, double T) 
{ 
return 1./T*relint(mu,T,mass,g,1,dndtfun)/hc3+zero; 
} 
 
double dsdtfun(double x)  
{ 
	return (x-mu)*(x-mu); 
} 
 
double dedtfun(double x)  
{ 
	return x*(x-mu); 
} 
 
double dedmjufun(double x)  
{ 
	return x; 
} 
 
 
double EoS_HB::ds_dT(double mu, double T) 
{ 
return 1./T/T*relint(mu,T,mass,g,1,dsdtfun)/hc3+zero; 
} 
 
double EoS_HB::de_dT(double mu, double T) 
{ 
return 1./T*relint(mu,T,mass,g,1,dedtfun)/hc3+zero; 
 
} 
 
double EoS_HB::de_dmu(double mu, double T) 
{ 
return relint(mu,T,mass,g,1,dedmjufun)/hc3+zero; 
 
} 
 
 
double nsfun(double x) {return Mass/x;} 
 
double dnsdmassfun(double x)  
{ 
	return Mass*Mass/x/x; 
} 
 
double dnsdmassfun1(double x)  
{ 
	return (x*x-Mass*Mass)/x/x/x; 
} 
 
double dnsdtfun(double x) 
{ 
	return Mass*(1.-mu/x); 
} 
 
double EoS_HB::ns_(double mu, double T) 
{ 
return relint(mu,T,mass,g,0,nsfun)/hc3+zero; 
} 
 
double EoS_HB::dns_dmu(double mu, double T) 
{ 
return  - relint(mu,T,mass,g,1,dnsdmassfun)/hc3 + relint(mu,T,mass,g,0,dnsdmassfun1)/hc3 - zero; 
} 
double EoS_HB::dns_dm(double mu,double T) 
{double dns; dns= -relint(mu,T,mass,g,1,dnsdmassfun)/hc3 
                  +relint(mu,T,mass,g,0,dnsdmassfun1)/hc3-zero;return dns;} 
 
double EoS_HB::dns_dT(double mu, double T) 
{ 
return 1./T*relint(mu,T,mass,g,1,dnsdtfun)/hc3+zero; 
}    
