//#include "stdafx.h"

#include "Merson.h"
#include <stdio.h>
#include <malloc.h>
#include <math.h>
//typedef enum {true, false} Bool;
//typedef Bool  (*condt) (double *);
typedef void  (*equat)(double,double *,double *);
/*typedef struct
	{double  h,h0,acc,x,xend;
	 double *y;
	 condt phascond,funct;
	 equat equations;
	 unsigned int n,jt;
	} obj;*/
 obj Merobj;
 static int iswh;
 static double *w,*yz,*f,*a,*b;
 static double z,zz,zend,bcc,ht,s,hsv,cof,wz;
 static bool ok,cnd,endcond,pl;
 static const double rzero = 1.e-23;

  void InitMer(double statpoint,double endpoint,double *intfunction,int eqnumber,
		int test,double accuracy,double step,double minstep)
		{

		  Merobj.h=step;
		  Merobj.h0=minstep;
		  Merobj.acc=accuracy;
		  Merobj.x=statpoint;
		  Merobj.xend=endpoint;
		  Merobj.n=eqnumber-1;
		  Merobj.jt=test;
		  Merobj.y=intfunction;
		  if (Merobj.n>100){
		 puts("MERSON ERROR\n");
		 printf("%d\n",Merobj.n);
		 puts("GREATER THAN 100\n");
		};
		 return; }
void InitMem(int NUM)
		  {
		  w=(double*)malloc((NUM+2)*sizeof(double));
		  f=(double*)malloc((NUM+2)*sizeof(double));
		  a=(double*)malloc((NUM+2)*sizeof(double));
		  b=(double*)malloc((NUM+2)*sizeof(double));
		  yz=(double*)malloc((NUM+2)*sizeof(double));
			return;}
void FreeMem(void)
		  {
		  free(w);
		  free(f);
		  free(a);
		  free(b);
		  free(yz);
			return;}
/*extern void second();
extern void up();
extern void enddif();*/

      void cond1(void)
	 { int k;
     for(k=0;k<=Merobj.n;k++) w[k]=yz[k];
	 z=z-s;
	 s=cof;
	 iswh=0;
	 endcond=false;
	 return;}

      void fifty(void)
       {Merobj.h=hsv;Merobj.x=z;Merobj.y=w;
       return;}

      void up(void)
       { int k;
       z=zz; for(k=0;k<=Merobj.n;k++) w[k]=yz[k]/*=w[k]*/;
/*       fifty();*/
       return;}

      void sixty(void)
       {double as;
	cof=0.5*s;
	   as=fabs(cof);
	if (as>fabs(Merobj.h0)) cond1();
	else
	    if (Merobj.jt==0)
		     {
		       printf("%d",Merobj.jt,"%f",s,Merobj.h0,z);
		       ok=false;
		       cnd=true;
		       }
		     else
		     if (iswh==1) cnd=true;
		     else
		      {
		       s=Merobj.h0;
		       /*if (hsv<0) s= -s; *//*Dangerous changes*/
		       };
	return;}

      void pass0(void)
	  { int k;
	    zz=z;
	    for(k=0;k<=Merobj.n;k++) yz[k]=w[k];
	    cnd=true;
	    ht=s/3;
	    pl=false;
	 return;}

      void pass1(void)
	{ int k;

	  z=z+ht;
	  for (k=0;k<=Merobj.n;k++)
	  {
	  a[k]=ht*f[k];
	  w[k]=a[k]+yz[k];
	   };
	 return;}


      void pass2(void)
	{ int k;

	  for (k=0;k<=Merobj.n;k++)
	  { a[k]*=0.5;
	    w[k]=a[k]+yz[k]+0.5*ht*f[k];
	  };
	return;}
      void pass3(void)
       { int k;
	 z=z+0.5*ht;
	 for (k=0;k<=Merobj.n;k++)
	 {
	 b[k]=4.5*ht*f[k];
	 w[k]=0.75*a[k]+yz[k]+0.25*b[k];
	 };
       return;}
      void pass4(void)
       {int k;
	  z=0.5*s+z;
	  for (k=0;k<=Merobj.n;k++)
	  {
	  a[k]+=2.*ht*f[k];
	  w[k]=3.*a[k]+yz[k]-b[k];
	  };
       return;}
     void pass5(void)
      {int k;
	  for (k=0;k<=Merobj.n;k++)
	  {
	  b[k]= -0.5*ht*f[k]-b[k]+2.*a[k];
	  w[k]-=b[k];
	  a[k]=fabs(5.*bcc*w[k]);
	  b[k]=fabs(b[k]);
	  };
	return;}

     void enddif(void)
	 {    double as;
		cnd=true;
		as=fabs((zz-z)/2.);
			/*printf("\n\tzz %1.15g z %1.15g as %1.15g Merobj.h0 %1.15g\n",zz,z,as,fabs(Merobj.h0));*/
		if (as>fabs(Merobj.h0)) {s=Merobj.h0;pl=true;cnd=false;iswh=0;};
/*   	      double eps=1.0e-17;
	      double yps=1.0e-10;
		   as=fabs(w[0]-wz);
		if (as>eps)
		  {  Merobj.h=eps/(eps+fabs(f[0]));
		     if (Merobj.h>yps) pl=true;
		   };*/
	      return;}

  void DifF(void)
	{	Merobj.h=s;Merobj.x=z; /*new change dangerous */
	    if ((*Merobj.phascond)(w)==true) pl=true;
	    if ((*Merobj.funct)(w)==false)
	     {
	      if (cnd==true) wz=w[0];
	      cnd=false;
	      Merobj.equations(z,w,f);
	      }
	      else
	      enddif();
	 return;}
   void first(void)
    { int k;
      k=0;
      do {

      switch (k) {
	 case 0:  pass0();break;
	 case 1:  pass1();break;
	 case 2:  pass2();break;
	 case 3:  pass3();break;
	 case 4:  pass4();break;
	 case 5:  pass5();break;
		 };
      k++;
		/*printf("\th before DifF =%g",s);*/
       DifF();
		/*printf("\th after DifF =%g",s);*/
     if (pl==true) {k=0;up();}
     if (cnd==true) k=6;}
     while (k<6);
    return;}

    void cond(void)
     {/*cond's begin*/
       int k;
       k=0;
       do{double as;
	  endcond=true;
       as=fabs(w[k]);
	/*printf("as=w[%d] %g h=%g" ,k,as,s);*/
       if (as>rzero)
	if (b[k]>a[k])
		/*{printf("\th before sixty =%g b[%d]=%g a[%d]=%g", s,k,b[k],k,a[k]);*/
	    sixty();
		/*printf("\th after sixty=%g",s);}*/
	else
	    if (iswh==1) cnd=true;
       if (cnd==false)
	if (endcond==false)
	    endcond=true;
	else
	    if (b[k]<0.03125*a[k])
		if (k<Merobj.n){
			 endcond=false;
				k++;
			}
		else
		s+=2.*s; 
//	printf("x%g h%g end%g sub%g ac%g",  
//Merobj.x,s,Merobj.xend,Merobj.x+s-Merobj.xend,exp(-(11+log10(Merobj.acc)) ));

//        if(fabs(Merobj.x+s-Merobj.xend)<exp(-(11+log10(Merobj.acc))) )  s/=2; 

// /*For ending calculation use one of conditions othewise it will continue infinity*/

      }while (endcond==false);
     return;}/*end cond*/


    void second(void)
     {double as;double ab;
       hsv=s;
       cof=zend-z;
       as=fabs(s);
      ab=fabs(cof);
       if (as<ab)
		/*{ printf("\th before first =%g",s);*/
	   first();
		/*printf("\th after first =%g",s);}*/
      else {
	    s=cof;
	    as=fabs(cof/hsv);
	    if (as<rzero)
		cnd=true;
	    else {
		 iswh=1;
		 first();
		 };
	    };
	/*printf("\th before cond =%g",s);*/
      if (cnd==false) cond();
	/*printf("\th after cond =%g",s);*/
	Merobj.h=s;Merobj.x=z; /*new change dangerous */
    return;}



void Merson(condt phascondd,condt functt,equat equationss)
	{/*merson's begin*/
	int k;
	Merobj.phascond=phascondd;
	Merobj.funct=functt;
	Merobj.equations=equationss;
  for(k=0;k<=Merobj.n;k++) w[k]=Merobj.y[k];/*  w=y;*/
  z=Merobj.x;
  zend=Merobj.xend;
  bcc=Merobj.acc;
  s=Merobj.h;
  iswh=0;
	do
	second();
	while (cnd==false);
	fifty()        ; /*up()*/

 return; }/*merson's end*/

