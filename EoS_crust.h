#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "EoS_HB.h"
#pragma once

class EoS_crust : public EoS_HB
{
public:
	EoS_crust(void);
	~EoS_crust(void);
//	double EoS_crust::ro_mass(double n);
	double p(double mu);
	double ro(double mu);
    double eps(double mu);
    double n_(double mu,double mue, double *n);
};
