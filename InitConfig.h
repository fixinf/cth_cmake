#if !defined(AFX_INITCONFIG_H__2E8AF4E1_28F3_4BBD_8BEE_D59CD12BE345__INCLUDED_)
#define AFX_INITCONFIG_H__2E8AF4E1_28F3_4BBD_8BEE_D59CD12BE345__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InitConfig.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// InitConfig dialog
#include "RW_cool.h"

class InitConfig 
{
// Construction
public:
	bool noinit;
	int m_F_;

//bool m_Gf; 
//bool m_Lf;	
//bool m_Nf;	
	int EoS_type;
	void SetFileName();
char mu_name[10];
char m_name[10];
	RW_cool rw_c;
//	char llt_c[10];
//	char time_c[10];
	char axis[3]; 
//	int ni; 
		bool mef1, msc1;
char mw1;
	bool Reset_Profiles(); 
	int Nend; 
	int num_t; 
	int Init_Profiles(); 
	double T[Num+1]; 
	double r[Num+1]; 
	double log_Ts_K[NUMT+1]; 
	double log10_t_yr[NUMT+1]; 
	bool Cool_Star(); 

	InitConfig(/*CWnd* pParent = NULL*/);   


	double	m_mu;
	double  m_m;
	double	m_kT;

	bool	m_std_c;
	bool	m_SM;
    bool	m_cond;
	bool	m_Qstar;
	
	public:
	virtual int DoModal(char*);
	protected:

protected:


	  void OnStdC();
	  void OnCsc();
	  void OnQStar();
	  void OnQc();
	  void OnDestroy();

private:
	bool m_ok;
};



#endif // !defined(AFX_INITCONFIG_H__2E8AF4E1_28F3_4BBD_8BEE_D59CD12BE345__INCLUDED_)
