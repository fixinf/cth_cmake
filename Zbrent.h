#ifndef _ZBRENT_
#define _ZBRENT_

/*******************************************************************
 *  Be carefull with function power it's not give a right result   *
 *  when one use a not double degree in it.			   *
 *      For example:						   *
 *                  "powf(anything,2)" is not give a right value	   *
 *	     it's correct if one write "powf(anything,2.)"	   *
 * 				             the pixel^is important*
 *								   *
 *     situation is same with sqrt-correct if sqrt(2.)		   *
 *				An little advice from GP	   *
 *******************************************************************/
//extern double zbrent(double (*func)(double),double x1,double x2,double tol)
;
extern double zbrent(double (*)(double),double __x1,double __x2,double __tol);
typedef double (*__sn_fun)(double);


/* ANSI: float (*func)(float); */
/* extern void nrerror(char *);*/

#endif