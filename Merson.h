#ifndef _MERSON_
#define _MERSON_
// typedef struct  {TRUE, FALSE} bool;
  typedef bool  (*condt) (double *);
/*this is the type of functions^ which  ^ must ^   contains the conditions
for finishing the integration before reaching the endpoint*/

  typedef void  (*equat)(double,double *,double *);
/*this is the type of function^ which  ^ must ^   contains the differencial
equations                    /|\      /|\    /|\______________________
	 the first is parameter----^,next---^ is the function value,and next is
its differencial   for example void equations(x,Y,dY)*/
  typedef  struct
	  {
		  double  h,h0,acc,x,xend;
		  double *y;
		  condt phascond,funct;
		  equat equations;
		  int n,jt;
		} obj;

extern obj Merobj;
extern void InitMer(double statpoint,double endpoint,double *intfunction,int eqnumber,int test,double accuracy,double step,double minstep);
extern void InitMem(int NUM);
extern void Merson(condt phascondd,condt functt,equat equationss);
extern void FreeMem(void);
#endif
