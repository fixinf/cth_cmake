//#include "stdafx.h"
// EoS_Bag.cpp : implementation file
// 
//#include "EoS_HyB.h"  
#include "EoS_Bag.h" 
#include "Zbrent.h"  
#include <stddef.h> 
#include <stdio.h> 
#include <math.h> 

//take it out on demand:
const char* quark_file="./tst_quark/tst";
//const char* quark_file="./deb_quark/deb";

#define zero 1.e-9 
//#define B  105.0 
//#define B 140.0 

//#define B 120.0 
//#define CCC 1.02
#define CCC 1.0

/*1.*75.0 */ 
/*57.5 MeV/fm^3= 1e14 g/cm^3*/  
/*85.3 critical for maxwell construction phase transition*/ 
#define M_u_QUARK 0.0 
#define M_d_QUARK 0.0 

#define M_s_QUARK 180.0 
/*150. */ 

#define M_c_QUARK 1200. 
#define M_ELECTRON 0.510999 
#define M_mju_MESON 105.658389 
/*Space factors*/ 
#define G_u_QUARK   6. 
#define G_d_QUARK   6. 

//#define G_s_QUARK    6. 

#define G_c_QUARK 0. 
//6. 
#define G_ELECTRON 2. 
#define G_mju_MESON 2. 

/*Charges*/ 
#define Q_u_QUARK 2./3. 
#define Q_d_QUARK -1./3. 
#define Q_c_QUARK 2./3. 
#define Q_s_QUARK -1./3. 
#define Q_ELECTRON -1. 
#define Q_mju_MESON -1. 


#ifdef _DEBUG 
#define new DEBUG_NEW 
#undef THIS_FILE 
static char THIS_FILE[] = __FILE__; 
#endif 

///////////////////////////////////////////////////////////////////////////// 
// EoS_Bag 

//#ifndef _SM_
//static double  mu_c = 0.0;
//static double  mu_c_0 = 0.0;
//#endif

//#ifdef _G_F_
double B_[15];
int KK;
double mu_c;
double mu_b_c;
/*************************************************************************/

static  double  Del_GL[]={137,0.23534,-5.8674e-5,1.294e-8,-5.511e-12,0,0,0,0};

//#ifndef _NOCOND_
static  double  B_GL[]={
  68.332, + 0.29345, - 0.00306,  + 1.5219e-05,  - 4.4222e-08,  + 7.6017e-11,
  - 7.607e-14,  + 4.0953e-17, - 9.1615e-21 
  
};


static  double  mu_c_GL = 975.0;

//#else
//without condensate
static  double  B_GLn[]={
  80.166, + 0.077715, - 0.00060298,  + 2.8254e-06,  - 7.9428e-09,
  + 1.3474e-11,  - 1.3487e-14,  + 7.3225e-18,  - 1.6612e-21
};

static  double  mu_c_GLn = 1014.67;
//#endif

double  mu_b_c_GL[] ={988.2,1069.59,
#ifndef _new_
		      1005.39,1059.05
#else
		      1035.88,1448.93
#endif
}; 


static int KK_GL = 8;


/************************************************************************/
static  double  Del_G[]={137,0.23534,-5.8674e-5,1.294e-8,-5.511e-12,0,0,0,0};

//#ifndef _NOCOND_
static  double  B_G[]={
  //	65.386, //Deborah
  66.886, // 1.21 critical
  //	68.886, // 1.41 critical
  + 0.14638, - 0.001802, + 9.8125e-06, - 3.0515e-08, + 5.4888e-11,
  - 5.661e-14, + 3.1096e-17, - 7.0479e-21
};
static  double  mu_c_G = 965.0;
//#else
//without condensate
static  double  B_Gn[]={
  72.906,
  //	68.906, //1.41 critical
  //72.00,
  //		0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
  + 0.025122, - 9.1152e-05, + 1.6402e-07, - 5.9621e-12, - 5.1899e-13,
  + 9.0892e-16, - 6.5617e-19, + 1.781e-22
};
static  double  mu_c_Gn = 991.0;
//#endif

double  mu_b_c_G[] ={988.2,1069.59,
#ifndef _new_
		     1005.39,1059.05
#else
		     1035.88,1448.93
#endif
}; 

/*
  #ifndef _NONLW_
  #ifndef _NOCOND_
  988.2  //LW with condensat
  #else
  1069.59  //LW without condensat
  #endif
  #else
  #ifndef _NOCOND_
  1005.39  //NLW with condensat
  #else
  1059.05  //NLW without condensat
  #endif
  #endif
  ;
*/
static int KK_G = 8;
//#endif
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//#ifdef _L2_F_
//#ifndef _NOCOND_
static  double  B_L[]={
  75.3501, + 0.28766, - 0.0032215, + 1.6528e-05, - 4.9352e-08, + 8.7023e-11,
  - 8.9237e-14, + 4.921e-17,- 1.1275e-20
};

static  double  mu_c_L = 999;
//#else
//without condensate
static  double  B_Ln[]={
  90.2179, + 0.076973, - 0.00068728, + 3.726e-06, - 1.1862e-08, + 2.2342e-11,
  - 2.4464e-14, + 1.4374e-17, - 3.5006e-21
};

static  double mu_c_Ln = 1045;
//#endif

static  double  Del_L[]={148.89, 0.20925, -6.948e-5,6.3621e-9, -7.4085e-13,0,0,0,0};



static double  mu_b_c_L[] = {1131.39,1192.43,
#ifndef _new_
			     1149.03,1252.03
#else
			     1347.91, 1592.73
#endif
}; 
/*
  #ifndef _NONLW_
  #ifndef _NOCOND_
  1131.39 //LW with condensat
  #else
  1192.43 //LW without condensat
  #endif
  #else
  #ifndef _NOCOND_
  1149.03  //NLW with condensat
  #else
  1252.03  //NLW without condensat
  #endif
  #endif
  ;
*/
static int KK_L = 8;
//#endif
////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
//#ifdef _NJL_F_

//static  double  B_[]={111, -0.09107, -0.00029182, 4.0913e-7, -4.2046e-10,3.3396e-13,-8.3336e-17,0.0,0.0};
static  double  Del_N[]={170.86, 0.2169, -0.0003794, 1.2943e-6, -3.091e-9, 
			 4.227e-12, -3.3769e-15,1.4778e-18,-2.7528e-22,0,0};
//static  double  mu_c = 1107.0;

//#ifndef _NOCOND_
//with condensate
static  double  B_N[]={
  (84.763+0.134), + 0.28604, + 0.0010708, - 2.8157e-05, + 1.6904e-07,
  - 5.4828e-10, + 1.0896e-12, - 1.3643e-15, + 1.0518e-18, - 4.5653e-22, + 8.5443e-26
};
static  double  mu_c_N = 1030;
//#else 
//without condensate
static  double  B_Nn[]={
  110.714, + 0.30219, + 0.001282, - 4.0634e-05, + 3.0828e-07, - 1.2301e-09, + 2.9441e-12,
  - 4.3677e-15, + 3.9376e-18, - 1.9774e-21, + 4.2442e-25
};

static  double  mu_c_Nn = 1100;
//#endif

//static  double  B_[]={146.4, -0.06054, 0.001355, -4.082e-6, 6.09e-9,-4.064e-12,9.584e-16,0.0,0.0};
//static  double  Del_[]={50.41, 0.3177, -0.001173, 4.245e-6, -8.896e-9, 1.071e-11, -7.429e-15, 2.763e-18,-4.272e-22};
//static  double  mu_c = 1185.0;

static  double  mu_b_c_N[] ={1252.33,1358.35,
#ifndef _new_
			     1303.47,1455.26
#else
			     1488.95,1753.77
#endif
			     
}; 
/*
  #ifndef _NONLW_
  #ifndef _NOCOND_
  1252.33   //LW with condensat
  #else
  1358.35   //LW without condensat
  #endif
  #else
  #ifndef _NOCOND_
  1303.47   //NLW with condensat
  #else
  1455.26    //NLW without condensat
  #endif
  #endif
  ;
*/
static int KK_N = 10;
//#endif
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//static double mu_b_c_HB[]={1049.31,1388.11,1500.73,1487.9,1601.97,1747.95};

static double mu_b_c_HB[]={
  //	1055.76, //Deborah
  //	1122.94, // 1.21 critical
  0.0, // 1.41 critical
  1411.67, 1515.26,1374.36,
  //	1518.22, //Deborah
  //		1176.67, // 1.4
  0.0,
  1621.09,1763.84,1577.18};

int set_eq(double *a,double *b,int k)
{
  for (int i=0;i<=k;i++)
    a[i]=b[i];
  return k;
}

EoS_Bag::EoS_Bag() 
{ 
//  m_new = true;		
//  G_s_QUARK = 0.0; 
//  B = 0.0;
  
  ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////
  QQ[0]=2./3.; 
  QQ[1]=-1./3.; 
  QQ[2]=QQ[3]=0.; 
  QQ[4]=QQ[5]= -1.; 
  
  u_.g=G_u_QUARK; 
  d_.g=G_d_QUARK; 
  st_.g=G_s_QUARK; 
  c_.g=G_c_QUARK; 
  e_.g=G_ELECTRON; 
  mu_.g=G_mju_MESON; 
  u_.mass=M_u_QUARK; 
  d_.mass=M_d_QUARK; 
  st_.mass=M_s_QUARK; 
  c_.mass=M_c_QUARK; 
  e_.mass=M_ELECTRON; 
  mu_.mass=M_mju_MESON; 
  u_.q=Q_u_QUARK; 
  d_.q=Q_d_QUARK; 
  st_.q=Q_s_QUARK; 
  c_.q=Q_c_QUARK; 
  e_.q=Q_ELECTRON; 
  mu_.q=Q_mju_MESON; 
  
} 

EoS_Bag::~EoS_Bag() 
{ 
} 

double 	EoS_Bag::Del_dq(double mu)
{
  
  if(!m_cond) return 0.0;

  if(m_SM)
    {		Del = Del_[0];
    double u  =  1; 
    for (int k = 1; k <= KK; k++)
      {
	u*= mu - mu_c;
	Del+= Del_[k]*u;
      }
    return Del;
    }else
      return dgap;
}



double  	EoS_Bag::_B(double  mu)
{ int k;
 double BB = 0;
 if(m_SM)
   {
     double u  =  1; 
     for (k = 0; k <= KK ; k++)
       {
	 //    #ifndef _NOCOND_
	 BB+= B_[k]*u;
	 u*= mu - mu_c;
	 //	#else
	 //    	BB+= B_0[k]*u;
	 //		u*= mu - mu_c_0;
	 //	#endif
       }
     return BB;
   }else
     return B;
 
}

double  	EoS_Bag::B_prim(double  mu)
{ int k;
 double BB = 0;
 if(m_SM)
   {
     double u  =  1; 
     for (k = 1; k <= KK ; k++)
       {
	 //#ifndef _NOCOND_
	 BB+= k*B_[k]*u;
	 u*= mu - mu_c;
	 //#else
	 //BB+= k*B_0[k]*u;
	 //u*= mu - mu_c_0;
	 //#endif
       }
     return BB;
   }
 else
   return 0;
}

double  	EoS_Bag::B_prim2(double  mu)
{ 
  double BB = 0;
  if(m_SM){
    double u  =  1; 
    for (int k = 2 ; k <= KK ; k++)
      {
	BB+=k*(k-1)* B_[k]*u;
	u*= mu - mu_c;
	
      }
    return BB;
  }	
  else
    return 0;
}

///////////////////////////////////////////////////////////////////////////// 
// EoS_Bag message handlers 

double EoS_Bag::p_(double mjuB, double mjue, double t) 
{ 
  ////NEW!
  //if(m_file){
  //  return Quark_file.p_(mjuB,mjue);
  //}
  ////////
  
  double sump,mjuu,mjud,mjus,mjuc,mjumu; 
  if (mjuB <= mu_c) return 0.0;
  mjus=mjud=(mjuB+mjue)/3.; 
  mjuc=mjuu=mjud-mjue; 
  mjumu=mjue; 
  sump=u_.p_(mjuu,t)+u_.p_(-mjuu,t); 
  sump+=d_.p_(mjud,t)+d_.p_(-mjud,t); 
  sump+=st_.p_(mjus,t)+st_.p_(-mjus,t); 
  sump+=c_.p_(mjuc,t)+c_.p_(-mjuc,t); 
  sump*=CCC;
  sump+=e_.p_(mjue,t)+e_.p_(-mjue,t); 
  sump+=mu_.p_(mjumu,t)+mu_.p_(-mjumu,t); 
  
  sump -= _B(mjuB);
  if (sump >=0.0) return sump;  else return 0.0;
  
} 

double EoS_Bag::n_(double mjuB,double mjue,double t,double *n) 
{
  for(int i=0;i<=5;i++) n[i]=0;
  ////NEW!
  //if(m_file){
  //  n[0]=Quark_file.n_u_(mjuB,mjue);
  //  n[1]=Quark_file.n_d_(mjuB,mjue);
  //  n[2]=Quark_file.n_s_(mjuB,mjue);
  //  n[3]=Quark_file.n_c_(mjuB,mjue);
  //  n[4]=Quark_file.n_elec_(mjuB,mjue);
  //  n[5]=Quark_file.n_muon_(mjuB,mjue);
  //  return Quark_file.nbar_(mjuB,mjue);
  //}
  ////////


  double mjuu,mjud,mjus,mjuc,mjumu,nB; 
  if (mjuB <= mu_c) return 0.0;
 
  mjus=mjud=(mjuB+mjue)/3.; 
  mjuc=mjuu=mjud-mjue; 
  mjumu=mjue; 
  n[0]  = CCC*(u_.n_(mjuu,t)  - u_.n_(-mjuu,t))  - B_prim(mjuB); 
  n[1]  = CCC*(d_.n_(mjud,t)   - d_.n_(-mjud,t))   - 2*B_prim(mjuB); 
  n[2]  = st_.n_(mjus,t)   - st_.n_(-mjus,t); 
  n[3]  = c_.n_(mjuc,t)   - c_.n_(-mjuc,t); 
  n[4]  = e_.n_(mjue,t)   - e_.n_(-mjue,t); 
  n[5]  = mu_.n_(mjumu,t) - mu_.n_(-mjumu,t); 
  nB=(n[0]+n[1]+n[2]+n[3])/3.; 
  return nB;
} 

double EoS_Bag::s_(double mjuB,double mjue,double t) 
{
  ////NEW!
  //if(m_file){
  //  return Quark_file.s_(mjuB,mjue);
  //}
  ////////

  double mjuu,mjud,mjus,mjuc,mjumu,sums; 
  
  if (mjuB <= mu_c) return 0.0;
  
  mjus=mjud=(mjuB+mjue)/3.; 
  mjuc=mjuu=mjud-mjue; 
  mjumu=mjue; 
  sums=u_.s_(mjuu,t) + u_.s_(-mjuu,t); 
  sums+=d_.s_(mjud,t) + d_.s_(-mjud,t); 
  sums+=st_.s_(mjus,t) + st_.s_(-mjus,t); 
  sums+=c_.s_(mjuc,t) + c_.s_(-mjuc,t); 
  sums+=e_.s_(mjue,t) + e_.s_(-mjue,t); 
  sums+=mu_.s_(mjumu,t)+mu_.s_(-mjumu,t); 
  return sums;
} 

void EoS_Bag::dn_(double mjuB,double mjue,double t,double *dndmjuB,double *dndt) 
{ 
  double mjuu,mjud,mjus,mjuc,mjumu,dn[6],dt[6],dmjuedmjuB,dmjuedt,tmp; 
  mjus=mjud=(mjuB+mjue)/3.; 
  mjuc=mjuu=mjud-mjue; 
  mjumu=mjue; 
  dn[0]  = CCC*(u_.dn_dmu(mjuu,t)  + u_.dn_dmu(-mjuu,t))- 2*B_prim2(mjuB); 
  dn[1]  = CCC*(d_.dn_dmu(mjud,t)   + d_.dn_dmu(-mjud,t))- B_prim2(mjuB); 
  dn[2]  = st_.dn_dmu(mjus,t)   + st_.dn_dmu(-mjus,t); 
  dn[3]  = c_.dn_dmu(mjuc,t)   + c_.dn_dmu(-mjuc,t); 
  dn[4]  = e_.dn_dmu(mjue,t)   + e_.dn_dmu(-mjue,t); 
  dn[5]  = mu_.dn_dmu(mjumu,t) + mu_.dn_dmu(-mjumu,t); 
  tmp=u_.q*dn[0]*(-2./3) 
    +    d_.q*dn[1]*(1./3.) 
    +    st_.q*dn[2]*(1./3.) 
    +    c_.q*dn[3]*(-2./3.) 
    +    e_.q*dn[4]*(1.) 
    +   mu_.q*dn[5]*(1.); 
  
  dmjuedmjuB=-( u_.q*dn[0]*(1./3.)+d_.q*dn[1]*(1./3.)+st_.q*dn[2]*(1./3.) 
		+ c_.q*dn[3]*(1./3.)+ e_.q*dn[4]*(0.)+ mu_.q*dn[5]*(0.) )		/tmp; 
  
  *dndmjuB=dn[0]*(1./3-2./3*dmjuedmjuB)+dn[1]*(1./3+1./3*dmjuedmjuB) 
    +dn[2]*(1./3+1./3*dmjuedmjuB)+dn[3]*(1./3-2./3*dmjuedmjuB); 
  
  dt[0]  = CCC*(u_.dn_dT(mjuu,t)  - u_.dn_dT(-mjuu,t)); 
  dt[1]  = CCC*(d_.dn_dT(mjud,t)   - d_.dn_dT(-mjud,t)); 
  dt[2]  = st_.dn_dT(mjus,t)   - st_.dn_dT(-mjus,t); 
  dt[3]  = c_.dn_dT(mjuc,t)   - c_.dn_dT(-mjuc,t); 
  dt[4]  = e_.dn_dT(mjue,t)   - e_.dn_dT(-mjue,t); 
  dt[5]  = mu_.dn_dT(mjumu,t) - mu_.dn_dT(-mjumu,t); 
  dmjuedt=-(  
	    u_.q*dt[0] 
	    +d_.q*dt[1] 
	    +st_.q*dt[2] 
	    +c_.q*dt[3] 
	    +e_.q*dt[4] 
	    +mu_.q*dt[5] 
	    )/tmp; 
  *dndt=dn[0]*(-2./3.)*dmjuedt 
    +dt[0]+dn[1]*(1./3.)*dmjuedt 
    +dt[1]+dn[2]*(1./3.)*dmjuedt 
    +dt[2] 
    +dn[3]*(-2./3.)*dmjuedt 
    +dt[3]; 
  
  *dndmjuB/=3; 
  
  
  *dndt/=3; 
} 

double EoS_Bag::ds_dT(double mjuB,double mjue,double t) 
{double mjuu,mjud,mjus,mjuc,mjumu,dn[6],dt[6],dmjuedt,tmp; 
 mjus=mjud=(mjuB+mjue)/3.; 
 mjuc=mjuu=mjud-mjue; 
 mjumu=mjue; 
 dn[0]  = u_.dn_dmu(mjuu,t)  + u_.dn_dmu(-mjuu,t); 
 dn[1]  = d_.dn_dmu(mjud,t)   + d_.dn_dmu(-mjud,t); 
 dn[2]  = st_.dn_dmu(mjus,t)   + st_.dn_dmu(-mjus,t); 
 dn[3]  = c_.dn_dmu(mjuc,t)   + c_.dn_dmu(-mjuc,t); 
 dn[4]  = e_.dn_dmu(mjue,t)   + e_.dn_dmu(-mjue,t); 
 dn[5]  = mu_.dn_dmu(mjumu,t) + mu_.dn_dmu(-mjumu,t); 
 tmp=Q_u_QUARK*dn[0]*(-2./3)+Q_d_QUARK*dn[1]*(1./3.)+Q_s_QUARK*dn[2]*(1./3.) 
   +Q_c_QUARK*dn[3]*(-2./3.)+Q_ELECTRON*dn[4]*(1.)+Q_mju_MESON*dn[5]*(1.); 
 dn[0]  = u_.dn_dT(mjuu,t)  - u_.dn_dT(-mjuu,t); 
 dn[1]  = d_.dn_dT(mjud,t)   - d_.dn_dT(-mjud,t); 
 dn[2]  = st_.dn_dT(mjus,t)   - st_.dn_dT(-mjus,t); 
 dn[3]  = c_.dn_dT(mjuc,t)   - c_.dn_dT(-mjuc,t); 
 dn[4]  = e_.dn_dT(mjue,t)   - e_.dn_dT(-mjue,t); 
 dn[5]  = mu_.dn_dT(mjumu,t) - mu_.dn_dT(-mjumu,t); 
 dmjuedt=-( u_.q*dn[0]  +  d_.q*dn[1] + st_.q*dn[2]+ c_.q*dn[3] 
	    +e_.q*dn[4] + mu_.q*dn[5] )					/tmp; 
 dt[0]  = u_.ds_dT(mjuu,t)  - u_.ds_dT(-mjuu,t); 
 dt[1]  = d_.ds_dT(mjud,t)   - d_.ds_dT(-mjud,t); 
 dt[2]  = st_.ds_dT(mjus,t)   - st_.ds_dT(-mjus,t); 
 dt[3]  = c_.ds_dT(mjuc,t)   - c_.ds_dT(-mjuc,t); 
 dt[4]  = e_.ds_dT(mjue,t)   - e_.ds_dT(-mjue,t); 
 dt[5]  = mu_.ds_dT(mjumu,t) - mu_.dn_dT(-mjumu,t); 
 tmp=dn[0]*(-2./3.)*dmjuedt+dt[0]+dn[1]*(1./3.)*dmjuedt+dt[1]+dn[2]*(1./3.)*dmjuedt+dt[2] 
   +dn[3]*(-2./3.)*dmjuedt+dt[3]+dn[4]*(1.)*dmjuedt+dt[4]+dn[5]*(1.)*dmjuedt+dt[5]; 
 return tmp; 
} 


double EoS_Bag::charge_(double muB, double mue, double t)
{
  ////NEW!
  //if(m_file){
  //  return Quark_file.charge_(muB,mue);
  //}
  ////////


  double echn,mjuu,mjud,mjus,mjuc,mjumu,n[6]; 
  double x =  mue/muB;
  mjus=mjud=(1+ x)/3.*muB; 
  mjuc=mjuu=(1-2* x)/3.*muB; 
  mjumu=mue; 
  n[0]  = CCC*(u_.n_(mjuu,t)   - u_.n_(-mjuu,t)); 
  n[1]  = CCC*(d_.n_(mjud,t)   - d_.n_(-mjud,t)); 
  n[2]  = st_.n_(mjus,t)  - st_.n_(-mjus,t); 
  n[3]  = c_.n_(mjuc,t)   - c_.n_(-mjuc,t); 
  n[4]  = e_.n_(mue,t)   - e_.n_(-mue,t); 
  n[5]  = mu_.n_(mjumu,t) - mu_.n_(-mjumu,t); 
  echn= u_.q*n[0]; 
  echn+=d_.q*n[1]; 
  echn+=st_.q*n[2]; 
  echn+=c_.q*n[3]; 
  echn+=e_.q*n[4]; 
  echn+=mu_.q*n[5]; 
  return echn; 
}



double MJUB,T_EM; 
EoS_Bag* bag; 

double chne(double x) /*Electric charge neutrality*/ 
{
  return	bag->charge_(MJUB,x*MJUB,T_EM);
  
} 

double EoS_Bag::B2e(double mjuB,double t) 
{
  ////NEW!
  //if(m_file){
  //  return Quark_file.mue_(mjuB);
  //}
  ////////

double x; 
 bag = this; 
 MJUB=mjuB; 
 T_EM=t; 
 x=zbrent(chne,zero,1./2., zero);//0<mjuu & 0<mjue => 0<x<1./2. 
 return x*MJUB; 
} 


void EoS_Bag::initBfuncion()
{
  if(m_SM)
    {
      if(m_cond)
	{
	  switch (m_FF)  
	    {
	    case 0: {KK=set_eq(B_,B_G,KK_G);
	    set_eq(Del_,Del_G,KK_G);
	    mu_c = mu_c_G;
	    
	    //    	if(!m_WM){mu_b_c=mu_b_c_HB[0];}else{ 
	    //		if(m_nlw) mu_b_c=mu_b_c_G[2];else mu_b_c=mu_b_c_G[0];};
	    
	    }; break; 
	    case 1:{KK=set_eq(B_,B_L,KK_L);
	    set_eq(Del_,Del_L,KK_L);
	    mu_c = mu_c_L;
	    //		if(!m_WM){mu_b_c=mu_b_c_HB[1];}else{ 
	    //	   if(m_nlw) mu_b_c=mu_b_c_L[2];else mu_b_c=mu_b_c_L[0];};
	    } ; break; 
	    case 2: {KK=set_eq(B_,B_N,KK_N);
	    set_eq(Del_,Del_N,KK_N);
	    mu_c = mu_c_N;
	    //		if(!m_WM){mu_b_c=mu_b_c_HB[2];}else{ 
	    //	    if(m_nlw) mu_b_c=mu_b_c_N[2];else mu_b_c=mu_b_c_N[0];};
	    };  break;
	    case 3: {KK=set_eq(B_,B_GL,KK_GL);
	    set_eq(Del_,Del_GL,KK_GL);
	    mu_c = mu_c_GL;
	    //		if(!m_WM){mu_b_c=mu_b_c_HB[3];}else{ 
	    //	if(m_nlw) mu_b_c=mu_b_c_GL[2];else mu_b_c=mu_b_c_GL[0];};
	    };  
	      
	    };
	  
	}
      else
	{
	  
	  switch (m_FF)  
	    {
	    case 0: {KK=set_eq(B_,B_Gn,KK_G);
	    mu_c = mu_c_Gn;
	    //		if(!m_WM){mu_b_c=mu_b_c_HB[4];}else{ 
	    //	if(m_nlw) mu_b_c=mu_b_c_G[3];else mu_b_c=mu_b_c_G[1];};
	    }; break; 
	    case 1:{KK=set_eq(B_,B_Ln,KK_L);
	    mu_c = mu_c_Ln;
	    //		if(!m_WM){mu_b_c=mu_b_c_HB[5];}else{ 
	    //	   if(m_nlw) mu_b_c=mu_b_c_L[3];else mu_b_c=mu_b_c_L[1];};
	    } ; break; 
	    case 2: {KK=set_eq(B_,B_Nn,KK_N);
	    mu_c = mu_c_Nn;
	    //		if(!m_WM){mu_b_c=mu_b_c_HB[6];}else{ 
	    //	if(m_nlw) mu_b_c=mu_b_c_N[3];else mu_b_c=mu_b_c_N[1];};
	    }; break;  
	    case 3: {KK=set_eq(B_,B_GLn,KK_GL);
	    mu_c = mu_c_GLn;
	    //		if(!m_WM){mu_b_c=mu_b_c_HB[7];}else{ 
	    //	if(m_nlw) mu_b_c=mu_b_c_GL[3];else mu_b_c=mu_b_c_GL[1];};
	    };  
	    };
	}
      B = B_[0];  //SM3D
      //B = 68;  //SM4D
      G_s_QUARK = 0.0;
     
    }
  else{
    mu_c=0.0;
    switch(m_HM)
      {
      case '1' : { G_s_QUARK = 0.0; B= 120.0;}break;//NLW ----BAG
      case '0' : { G_s_QUARK = 6.0; B = 105.0 ; }; break; //LW ----BAG
      case '2' : { G_s_QUARK = 0; B = 0 ; m_file_B = true;}; break; //FILE including QM
	  case '3':  { G_s_QUARK = 0.0; 
//		  B = 120.0 ;
		  B = 75.0 ;
				 };//HB ----BAG
      };
  };
  m_new = false;
  st_.g=G_s_QUARK; 
}


double EoS_Bag::X_gap(char type, double mu_q)
{
  double gap0,gap, d0,d1,d2,mu,dm,x,muc,a, r;
  
  //gap0 = 0.2;
  //gap = gap0 * powf(10, 0.01*mu_q - 4);
  
  d0 = 0.0001;
  d1 = 3.0;
  d2 = 50;
  mu = 400;
  dm = 100;
  muc = 364;
  
  
  x= mu_q;
  a = 0.0 ; 
  r = 3.0;
  switch(type){
   case('0'):{ gap = a= 0;} break;
    case('1'):{ gap = 1.0;  a = 0;} break;
	case('2'): { gap = 100;  a = 0;} break;
	case('4'): { gap = 0.1;  a = -25;} break;
	case('5'):{ gap = 0.05; a = 0;} break;
    case('3'):{ gap = 0.03; a = 0;} break; 
    case('A'):{ gap = 5.0; a = -25; } break;
    case('B'):{ gap = 0.1; a = -2; }break;
    case('C'):{ gap = 1;  a = - 10; }break;
    case('D'):{ gap = 20.0;  a = - 25; };
    };
//  gap = 0.4;
//  a=-400;
double b = 0;
  double arg = (x / muc - 1);
  //if(a!=0.0) gap += a*  powf(x / muc - 1, r);
  if(a!=0.0) gap *= exp(a*arg*(1 + b*arg));
 // if(arg>0.113) gap=zero;
//printf("\n %g %g",arg,gap);  
  
  if(gap < zero) gap = zero;
  
  
  //(powf(d0,((mu - x)*(dm + mu - x))/(2.*powf(dm,2)))*
  //     powf(d1,((dm + mu - x)*(dm - mu + x))/
  //       powf(dm,2)))/
  //   powf(d2,((mu - x)*(dm - mu + x))/(2.*powf(dm,2)));
  
  return gap;
}


#undef zer0 
//#undef B 
#undef M_u_QUARK 
#undef M_d_QUARK 
#undef M_s_QUARK 
#undef M_c_QUARK 
#undef M_ELECTRON 
#undef M_mju_MESON 
#undef G_u_QUARK 
#undef G_d_QUARK 
//#undef G_s_QUARK 
#undef G_c_QUARK 
#undef G_ELECTRON 
#undef G_mju_MESON 
#undef Q_u_QUARK 
#undef Q_d_QUARK 
#undef Q_c_QUARK 
#undef Q_s_QUARK 
#undef Q_ELECTRON 
#undef Q_mju_MESON 



double EoS_Bag::Xi_q(double mu)
{
  return 0.9;
}

//int EoS_Bag::In_file()
//{ int c=0;
//  //asking for datasets.
//  //bool m_q_file 
//  //and 
//  //char const* quark_file 
//  //are defined somewhere else
//  if(m_file){
//    cerr <<"Reading quark datasets from "<<quark_file<<"*.dat ..."<<endl;
//    c = Quark_file.Init(quark_file);
//    cerr <<"done..."<<endl;
//  }
//return c;
//}
