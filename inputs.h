#ifndef _INPUTS_
#define _INPUTS_
extern double ksi_p, ksi_n, ksi_q, supp, R_n, R_p, R_q;
extern double gap_q,gap_n, gap_p,ksi;
extern int n_h_c,n_q_c;
extern void Gaps(int o);
extern void specific_heat(int o);
extern void emissivity(int o);
extern void conductivity(int o);
extern double apra(double* y, double* _x, double xc, int N);
extern int ifpicon;
extern double gap_factor;
#endif