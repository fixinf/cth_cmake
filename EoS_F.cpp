//#include "stdafx.h"
#include "EoS_F.h"
#include "Star_MK.h"

	FILE* EoS_file_n;
	FILE* EoS_mini_file;

EoS_F::EoS_F(void)
{
	mu_min = 939.603 ; // in case it is not define in the EoS file.
}

EoS_F::~EoS_F(void)
{
}


//
//
// 
//double  val_F(double _n)
//{
// 
//return apr(muf,rof,_n,Nf);
// 
//}
//
//void EoS_F(double mu,double *eps0,double *p0,double *c0,double *dedmu,double *_n)
//{
//*eps0=apr(epsf,muf,mu,Nf);
//*p0=apr(Pf,muf,mu,Nf);
//*_n=apr(rof,muf,mu,Nf);
//*c0=0;
//*dedmu=0;
//}


void EoS_F::Read_EoS_FILE_()
{
	char k;
	int i, pp, ic=-1;

//strcpy(EoS_file_name,".");
//strcpy(EoS_mini_file_name,".");
	
 strcpy(EoS_file_name,name);
 strcpy(EoS_mini_file_name,name);
 strcat(EoS_mini_file_name,".eos");
 strcat(EoS_file_name,".dat");
printf("Your EOS file name is %s\n",EoS_file_name);
 
 ;
if((EoS_file_n = fopen(EoS_file_name,"r"))==NULL) 
{
printf("%s : there is no such opened file \n",EoS_file_name);
exit(1);
};
 EoS_mini_file = fopen(EoS_mini_file_name,"w");
 fprintf(EoS_mini_file,"  # e[MeV/fm3],   pr[MeV/fm3],  n_b[1/fm3],  mu_b[MeV]\n");

  i=0;
	mu_crit = zero;
while (((k=fgetc(EoS_file_n))!=EOF) && (k!='\n'));
while(i<=peos){

/*if(i==0){
    epsf[i]=epsf[0];
    Pf[i]=Pf[0];
    rof[i]=rof[0];
    muf[i]=muf[0];
epsf[0]=Pf[0]=rof[0]=0;
//muf[0]=Mn; 
pp=1;}
else
*/

	do {
// fprintf(EoS_file,"  #1  mu_b [MeV],  pr,   e, [MeV/fm3]    n_b , #5   n_n,    n_p, [1/fm3]  m_n/M_n,    m_p/M_n,  #9    n_e,  n_mu  n_u,  n_d,  n_s[1/fm3]  mu_e [MeV]  Y_p  \n");

 
#ifndef _HYP_ 
   pp=fscanf(EoS_file_n,"  %lg  %lg  %lg  %lg  %lg  %lg  %lg  %lg  %lg  %lg  %lg  %lg  %lg  %lg %lg \n",
&muf[i],&Pf[i],&epsf[i],&rof[i],&n0f[i],&n1f[i],&m0f[i],&m1f[i],&n13f[i],&n14f[i],&n15f[i],&n16f[i],&n17f[i],&muef[i]
   ,&mumuf[i]);
#else
   pp=fscanf(EoS_file_n,"  %lg  %lg  %lg  %lg  %lg  %lg  %lg  %lg  %lg  %lg  %lg  %lg  %lg  %lg %lg %lg  %lg  %lg  %lg  %lg  %lg  %lg  %lg  %lg  %lg  %lg %lg \n",
&muf[i],&Pf[i],&epsf[i],&rof[i],
&n0f[i],&n1f[i],&m0f[i],&m1f[i],
&n13f[i],&n14f[i],&n15f[i],&n16f[i],
&n17f[i],&muef[i],&mumuf[i],
&Y_Lf[i],&Y_Smf[i],&Y_S0f[i],&Y_Spf[i],
&Y_Xmf[i],&Y_X0f[i],&m_Lf[i],&m_Smf[i],&m_S0f[i],&m_Spf[i],
&m_Xmf[i],&m_X0f[i]);
#endif

	  
	  if(i==0)mu_min = mu_crit = muf[i];
  if(n0f[i]>zero) {ic = i+1; 
  mu_crit =(mu_crit+ muf[i])/2;} else
	  if (ic == i&&pp>0) mu_crit =(mu_crit+ muf[i])/2; 
		      //mu_baryon, pr, eps/* /nb */, nb, nn[0], nn[1]/nb, 
		      //Hybrid.hadron.m_mtr[0]/M_N,Hybrid.hadron.m_mtr[1]/M_P,nn[13],nn[14],
		      //nn[15],nn[16],nn[17],mu_electron,nn[1]/nb);

	}while(Pf[i]<= 1e-9&&pp>0);

if(pp<0) goto out; 

printf("%d %g %g %g %g\n",i,epsf[i],Pf[i],rof[i],muf[i]);
 fprintf(EoS_mini_file,"%g %g %g %g\n",epsf[i],Pf[i],rof[i],muf[i]);

 i++;
};

  out: Nf=i-1;
printf(" N = %d\n",Nf);
 fclose(EoS_file_n);
 fclose(EoS_mini_file);
}

int EoS_F::Read_Mass_Radius_relation(FILE *Config, double* M,double* R,double* MB, double* MU)
{// finds the stable branch and saves in M,R,NB and MU
	bool dm;
	char k;
	int NF;
double mu_c_,Mass_,Radius_,x,xx,xxx,xxxx,xxxxx,B_Mass_;
Mmax=0.1;
//n(0)    ,pr(0)       ,e(0)      ,mu(0),     Mass ,  B_Mass,   R   ,R_qc 
long int pos = ftell(Config);
if (fgetc(Config) == '#') while (((k = fgetc(Config)) != EOF) && (k != '\n')); else fseek(Config, pos, SEEK_SET);
NF = -1;
do{
fscanf(Config," %lg %lg %lg %lg %lg %lg %lg %lg %lg",&x,&xx,&xxx,&mu_c_,&Mass_,&B_Mass_,&Radius_,&xxxx,&xxxxx);
if(Mass_> Mmax-0.0001){
Mmax=M[0]=Mass_;
MB[0]=B_Mass_;
R[0]=Radius_;
mu_c_max=MU[0]=mu_c_;} else NF=1;
}while(NF<0);
do{
M[NF]=Mass_;
MB[NF]=B_Mass_;
R[NF]=Radius_;
MU[NF]=mu_c_;
fscanf(Config," %lg %lg %lg %lg %lg %lg %lg %lg %lg",&x,&xx,&xxx,&mu_c_,&Mass_,&B_Mass_,&Radius_,&xxxx,&xxxxx);
dm = (Mass_ < M[NF]);

NF++;

}while(NF<500&&Mass_>=0.1&&dm);

return NF;
}