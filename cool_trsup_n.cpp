//#include "stdafx.h"
//#include "menu.h"
//#include "Starmks_n.h"
#include "RW_cool.h"
//#include "Photo.h"

//#include "Coolings_funcs.h"
#include "nrutil_.h"

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>

extern void tridag(
				   double a[],
				   double b[],
				   double c[],
				   double r[],
				   double u[],
				   unsigned long n
				   );

#define scan  0
#define check_file "COOLING_RUN"

FILE *ll_File, *out_File ,*out_File_L ,*check, *agrF;


#define ro_00  4.915e16
                    /* g/cm^3*/
#define eps_0 2.396e-7
#define m_0   1
#define T_0   1e+9 
//#define c_0   2.396e-16
                /*MeV K^-1 fm^-3*/
#define c_0   2.78055e-4
                /*fm^-3*/
#define k_0   1.31e+20

#define index  8
#define  t_0   3.1536e+7
                        /*yr = t_0 s*/
#define l_0  1.555e+43 
                       /*erg s^-1*/  // 4 Pi (r_0)^3
#define  min_step  1e-5
//#define  DTS  1
#define  ALFA 0
double acc1=1e-9, DTS = 0.015;
double ager = 3;
double abs1( double x){
if(x>0) return x ; else return -x;
}
/********/ 


//double *a, *b, *c, *r, *u;
double a[Num+2], b[Num+2], c[Num+2], r[Num+2], u[Num+2];

//double *XX, *YY, *ZZ, *DD, *An, *Bn,*Cn;
//double *ZZ0,*ZZ1;
double XX[Num+1], YY[Num+1], ZZ[Num+1], DD[Num+1], An[Num+1], Bn[Num+1], Cn[Num+1];
//double ZZ0[Num+1],ZZ1[Num+1];
/*void initn(int NUM)
		  {
		 
 		  An=(double*)malloc((NUM+1)*sizeof(double));
		  Bn=(double*)malloc((NUM+1)*sizeof(double));
		  Cn=(double*)malloc((NUM+1)*sizeof(double));

		        return;}
void initmemx(int NUM)
		  {
		  XX=(double*)malloc((NUM+1)*sizeof(double));
		  YY=(double*)malloc((NUM+1)*sizeof(double));
		  ZZ=(double*)malloc((NUM+1)*sizeof(double));
		  DD=(double*)malloc((NUM+1)*sizeof(double));
		        return;}
void freen(void)
		  {
                  free(An);
		  free(Bn);
		  free(Cn);
			return;}

void freememx(void)
		  {
		  free(XX);
		  free(YY);
		  free(ZZ);
		  free(DD);
			return;}

*/
/******************************************************** /
void initmem(int NUM)
		  {
		  a=(double*)malloc((NUM+2)*sizeof(double));
		  b=(double*)malloc((NUM+2)*sizeof(double));
		  c=(double*)malloc((NUM+2)*sizeof(double));
		  r=(double*)malloc((NUM+2)*sizeof(double));
		  u=(double*)malloc((NUM+2)*sizeof(double));
			return;}
void freemem(void)
		  {
		  free(a);
		  free(b);
		  free(c);
		  free(r);
		  free(u);
			return;}


*****************************************************************/



/*
Defferential Equation of the type 
        D[ZZ,t]= A D[L,m] + B ,
where 
    L= C D[ZZ,m]
*/

static double accc = 1e-10;  // for the lower limit of the Capacity

//static int on_fast_cool = 1;  // to switch on electron fraction in the core 

//static int normal_shell = 0;  // to switch off the suppression in shell

//static int normal_core = 0;  // to switch off the suppression in shell



/*****************************/
double dt;

/*





*/

/**************************** time step determination*******/
double RW_cool::dt_correc()
{

int oi;
double dtno;
double dtn;
double dtc;
double BB;
//printf(" 1dt :%d %g %g %g %g %g \n",o,dt,dtno,dtn,Bn[0],T_h);
dtno=dtn=abs1(-1/(Bn[0]+ZERO));

for (oi=1;oi<=Num;oi++){
dtc = dt;
//return dt;
//if ( dt > 1e8 ) DTS*=sqrt(dt) ; 
// else {dtc = (mass[o]-mass[o-1])*(mass[o]-mass[o-1]);
//dtc = 1/abs1((2* An[o] * Cn[o] + ZERO)); };

BB = Bn[oi];
if(oi<Num) BB = Bn[oi] ; 
     else     BB = Bn[Num] + An[Num] * PhotonL(T_hat[Num])/(mass[Num]-mass[Num-1]+ZERO);
if(dtno > abs1(-1/(BB+ZERO))) dtno = abs1(-1/(BB+ZERO)) ;
if (dtn > dtno) dtn= dtno;
if(oi<=Num)
dtno= 0.01* dt *abs1(T_hat[oi]/(T_hat[oi] - T_hat_old[oi]+ZERO));
if (dtn > dtno)
dtn= dtno;

};

dtn *= 1.0/DTS;
//printf("the recommended time_step is %g\n",dtn);
if (dtn>1e-7*dt) return dtn; else return 1e-7*dt; 
//return dtn;
}

/****************************/ 

int swon;
/****************   Thermal evolution  *********************/
/****************   ------------------ *********************/

int RW_cool::Energy_flux(double ds0)
{int i;
double d1,d2,cc,LAMDA,Bn_g,Cnn,Cnn1,dcrust;

//printf("  Ef starts \n");
LAMDA=1;
//initn(Num);

 HES(An,Bn,Cn);


if (ds0<ZERO) { /*freen;*/   return 1;}

//printf(" dt :%g  \n",ds0);

Cnn=(Cn[1]+Cn[2])/2.0;
d1=mass[1];
lum_hat[1]= Cnn*(T_hat[1]-T_hat[0])/(T_hat[0]*d1+ZERO);
ZZ[0]= 0;
XX[0]= (An[0] * Cnn *ds0 /(d1*d1+ZERO))*LAMDA;
YY[0]= 1 + XX[0];
DD[0]= log(T_hat[0]) + ds0 * Bn[0]*LAMDA ;
//DD[0]+ = (ds* Ann[0]* lum_hat[1]/d1 + ds * Bnn[0])*(1-LAMDA); 
lum_hat[0]= 0; 
cc=1;
//LAMDA=1;
for (i=1; i < Num ; i++){
        d1=mass[i]-mass[i+1];
        d2=mass[i]-mass[i-1];
//Cnn1=(Cn[i+1] + Cn[i])/2.0;
//Cnn= (Cn[i] + Cn[i-1])/2.0;                       

Cnn1= Cn[i+1];
Cnn = Cn[i];
lum_hat[i+1]= - Cnn1*(T_hat[i+1] - T_hat[i])/(T_hat[i+1]*d1+ZERO);
XX[i]= (  An[i] * Cnn1/(d1*d1+ZERO)*ds0)*LAMDA ;
ZZ[i]= (- An[i] * Cnn /(d1*d2+ZERO)*ds0)*LAMDA  ;           
YY[i]= 1 + XX[i] + ZZ[i];
DD[i]= log(T_hat[i]) + ds0 * Bn[i]*LAMDA  ;
//cc= ds *  An[i]*( lum_hat[i+1] - lum_hat[i])/(- d1+ZERO)+ ds * Bn[i] ;
//DD[i]+ = cc*(1-LAMDA); 
//if (i==10)printf("1 rr= %10.4g  A=%10.4g d1=%10.4g cc=%10.4g DD=%10.4g ds=%10.4g\n", rr[i],An[i],d1,cc, DD[i],ds);

};

//LAMDA=0.5;

/*
d1=mass[Num] - mass[Num-1];
dcrust = 0.01*d1; 
An[Num]*=1;
 Bn_g = Bn[Num] + An[Num]*PhotonL(T_hat[Num])/(dcrust);
//Cnn=(Cn[Num] + Cn[Num - 1])/2.0;
Cnn= Cn[Num];
 XX[Num] = 0; 
 ZZ[Num] = (An[Num] * Cnn /(d1*dcrust+ZERO)*ds0)*LAMDA ;
 YY[Num] = 1 + ZZ[Num];
 DD[Num]= log(T_hat[Num]) + ds0 *  Bn_g*LAMDA;
*/

// /*
double ro_cut= 0.7;  //something new
d1=mass[Num] - mass[Num-1];
dcrust = 
//0.002 new together with the change in the Photon L
0.002*
d1; 
double fff=1;
An[Num]*=fff;
 Bn_g = 
	  Bn[Num] +
	  An[Num]*PhotonL(T_hat[Num])/(dcrust)
	 ;
//Cnn=(Cn[Num] + Cn[Num - 1])/2.0;
Cnn= Cn[Num]*fff;
//*ro_cut/ro[Num]  //something new has to be understood
;
 XX[Num] = 0; 
 ZZ[Num] = (An[Num] * Cnn /(d1*dcrust+ZERO)*ds0)*LAMDA ;
 YY[Num] = 1 + ZZ[Num];
 DD[Num]= log(T_hat[Num]) + ds0 *  Bn_g*LAMDA;
// DD[Num]+ = (ds *  An[Num]*( PhotonL(T_hat_old[Num]) - lum_hat[Num])/(d1+ZERO)+ds * Bn[Num])*(1-LAMDA);
// printf("rr=%g  An =%g  Bn_g=%g  Bn=%g  DD=%g PL= %g\n", rr[Num],An[Num],Bn_g,Bn[Num], DD[Num],PhotonL(T_hat[Num]));
//printf("OK!\n");
//LAMDA=1;
//freen;
//  */
  return 0;
}


/******************************************************  TIME STEP ***********/
void RW_cool::Step_in_time(double time_step)
{//double T_s4;
int j;
//int i;

//printf(" dt ts :%g  \n",time_step);

Energy_flux(time_step);

Luminos();
//initmem(Num);

for(j=1;j<=Num+1;j++)
{
a[j]=-ZZ[j-1];
b[j]=YY[j-1];
c[j]=-XX[j-1];
r[j]=DD[j-1];

//printf(" 1 i=%d  a=%g b=%g c=%g r=%g\n",j,a[j],b[j],c[j],r[j]);
};

tridag(a, b, c, r, u, Num+1);


for(j=1;j<=Num+1;j++)
{
//	T_hat_old[j-1]=T_hat[j-1];
if (u[j]< -60) T_hat[j-1] = ZERO; else
	T_hat[j-1]=exp(u[j]);
};
//printf("Num = %d,T_hat[N]= %g\n",Num, T_hat[Num]);

//freemem();
}

/***********************************  *******   ********************************/

/*********    STEP TIME *********************/
int RW_cool::Step_time(double dalta_time)
{

int i,nnn;
//int cacc,l,ld;
double ds, _tm,_s;
_tm=0;
_s=0;
double ZZ0[Num+1],ZZ1[Num+1];

//                  ZZ0=(double*)malloc((Num+1)*sizeof(double));
//                  ZZ1=(double*)malloc((Num+1)*sizeof(double));
dt=ds= dalta_time;

ds=dt_correc();

//printf(" ds :%g  \n",ds);


nnn = (int)(log(dt/ds)/log(2.0))+1; //printf("1 # num=%d ;es_dt=%g\n", nnn ,ds);
if (nnn < 1) {nnn = 0; ds = dt;};

for (i=0;i<= Num ;i++)  ZZ0[i]=T_hat[i];

swon=0;
_tm=dt;
while (_tm >ZERO){
					_s+=ds; 
//                         for (i=0;i<= Num ;i++)  T_hat_old[i]=T_hat[i];
//if(scan) printf("2 # t=%g T(0)=%g T(R)=%g ds = %g\r", 
//                         _s/t_0,T_hat[0],T_hat[Num],ds);
  if(_tm > ds)
	Step_in_time(ds); 
  else 
  Step_in_time(_tm);
 ds=dt_correc();
if(ds<2) ds =2 ;
  _tm=(dt -_s);
 };
/*
for (i=0;i<= Num ;i++) ZZ1[i]=T_hat[i];
_s=0;
ds/=2.0;
nnn+=1;  
  while ( ds > min_step ) { 
while (_tm=(dt -_s)>ZERO){_s+=ds; if (_tm>ds)  Step_in_time(ds); 
                                      else Step_in_time(_tm);};
_s=0;_tm=0;
for(i=0;i<=Num;i++){
_tm = abs1(T_hat[i] - ZZ1[i]); //printf("%d diff=%g\n",ld,_tm);
 if (_tm >_s){ld=i;_s=_tm;};
};

if (_s<1e-5) return nnn;
_s=0;
for (i=0;i<= Num ;i++) 
{ ZZ1[i]=T_hat[i];
  T_hat[i]=ZZ0[i];
};

nnn++;
ds/=2.0; //printf("1 # num=%d; _dt=%g\n", nnn ,ds);
};
*/
swon=0;
//                  free(ZZ0);
//                  free(ZZ1);

return nnn;
}
/*******************************************************************/



bool RW_cool::Evolution(double del_tt,double pr_ft,double *lg_t,double* lg_T)
{
int j,n;
//int i,o;
double lgT_s,lgT_c,d_t,_t;
double lgT_s_r;
//double tau;

//printf("oioi 4in %d cont_cal_f %d\n",ni,cont_cal_f);

if(!cont_cal_f)
{  
if(lgnlgs)
{	ll_File=fopen(lgnlgs_file ,"w");
fprintf(ll_File," #log10(_t), lgT_s\n"); 
}
       strcpy(agr_file_,HOME_EV); 
       strcat(agr_file_,".agr");

agrF = fopen(agr_file_,"a");

 if(lowbranch) fprintf(agrF, "@    s%d legend  \"%.4g LB\" \n", next, mass[Num]);else
fprintf(agrF, "@    s%d legend  \"%.4g\" \n",next,mass[Num]);
fprintf(agrF,"@target G0.S%d \n@type xy\n",next);
out_File=fopen(evolution_file ,"w");
fprintf(out_File," #log10(_t),lum_hat(R)*l_0,lgT_c, lgT_s, lgT_m, log10(PhotonL(T_hat(R))*l_0)\n"); 
out_File_L=fopen(evol_L_file ,"w");

#ifndef _HYP_
fprintf(out_File_L, " #log10(_t)   L_PPBF,  L_pBS,  L_NPBFs, \
L_NPBFp,  \
L_MU_H_p, \
L_MU_H_n,    L_ee,   L_DU_H,  L_PU , L_Photo \n");
#else
fprintf(out_File_L," #log10(_t)   L_PPBF,  L_pBS,  L_NPBFs, \
L_NPBFp,  \
L_MU_H_p, \
L_MU_H_n,    L_ee,   L_DU_np, \
 L_DU_Lp \
	L_DU_Sn \
	L_DU_SL \
	L_DU_SS \
	L_DU_XL \
	L_DU_XS \
	L_DU_XSo \
	L_DU_XX \
L_PU , L_Photo \n"); 
#endif


} else 
{if(lgnlgs)	ll_File=fopen(lgnlgs_file ,"a");
	out_File=fopen(evolution_file ,"a");
	out_File_L=fopen(evol_L_file ,"a");
	agrF =fopen(agr_file_,"a");
}
//printf("oioi 5in %d pr_ft %g\n",ni,pr_ft);


j=0; 
_t=_time;

d_t = del_tt /pr_ft ;
//if(scan) printf("del_t= %g dt= %g sec\n",del_tt,d_t);

for(j=1;j<=pr_ft;j++){ 

//initmemx(Num);
n=Step_time(d_t);
//freememx();



_t+=d_t/t_0; //printf("time= %g yr",_t);

//rr[Num] = 12 ; //experiment ----  R =10 km
//mass[Num] = 1.4 ;//experiment ----  M =1 M_sun


//rr[Num] = 10 ; //experiment ----  R =10 km
//mass[Num] = 1.4 ;//experiment ----  M =1 M_sun


double lgT_m = log10(T_hat[Num]
//					 *exp(- phi[Num])
					 )
//-  log10(sqrt(1 - 2*r_0*mass[Num]/rr[Num]))
					 				 + 9;
//printf("T_hatNum = %g\n",T_hat[Num]);
*lg_T= lgT_s = Surface_t_T(T_hat[Num]) + 9 ; //Shell surface Temperature; in K
//printf("lg_T = %g\n",lgT_s);
lgT_c= 9 + log10(T_hat[0]*exp(-phi[0]));    //Cenrtal Temperature;


lgT_s_r = lgT_s  //T_eff  local in K
+ log10(sqrt(1 - 2*r_0*mass[Num]/rr[Num])) // Temperature infinity

; 

if(scan){printf(" lg(t[yr])=%g L =%g  lgT_c=%g  lgT_s=%g \n",
 log10(_t),PhotonL(T_hat[Num])*l_0,lgT_c,lgT_s); 
 printf("j=%d t=%g   lum_hat =%g T_hat(0)=%g T_hat(R)=%g dt=%g n=%d\n",
                           j-1, _t ,lum_hat[Num], T_hat[0], T_hat[Num],dt,n);} 
else {
 check=fopen(check_file,"w");
 fprintf(check," \n Cooling Project \n N = %d\n\n ",ni);
 fprintf(check,"del_t= %g dt= %g sec\n",del_tt,d_t);
 fprintf(check," lg(t[yr]=%g L =%g  lgT_c=%g  lgT_s=%g \n",
 *lg_t=log(_t)/log(10.0),lum_hat[Num]*l_0,lgT_c,lgT_s); 
 fprintf(check,"j=%d t=%g lum__hat =%g T_hat(0)=%g T_hat(R)=%g \n dt_est=%g dt=%g n=%d\n\n",
    j-1, _time ,lum_hat[Num], T_hat[0], T_hat[Num],dt_correc(),del_t,n); 
 fclose(check);

};


//output in evolutionary files
double age = log10(_t);
if(age > -10){
#ifdef _HYP_
	fprintf(out_File_L, " %g  %g  %g  %g %g  %g  %g  %g  %g  %g %g  %g %g  %g  %g  %g  %g  %g %g\n",
		age, log10(L_PPBF), log10(L_pBS), log10(L_NPBFs),
		log10(L_NPBFp),
		log10(L_MU_H_p),
		log10(L_MU_H_n), log10(L_ee), 
		log10(L_DU_np),
		log10(L_DU_Lp),
		log10(L_DU_Sn),
		log10(L_DU_SL),
		log10(L_DU_SS),
		log10(L_DU_XL),
		log10(L_DU_XS),
		log10(L_DU_XSo),
		log10(L_DU_XX),
				log10(L_PU), log10(PhotonL(T_hat[Num])*l_0));
#else
	fprintf(out_File_L, " %g  %g  %g  %g %g  %g  %g  %g  %g  %g %g\n",
		age, log10(L_PPBF), log10(L_pBS), log10(L_NPBFs),
		log10(L_NPBFp),
		log10(L_MU_H_p),
		log10(L_MU_H_n), log10(L_ee), log10(L_DU_H),
		log10(L_PU), log10(PhotonL(T_hat[Num])*l_0));

#endif // _HYP_


/* THE MAIN OUTPUT __________  for the cooling evolution  IN EV DATA */

fprintf(out_File," %g  %g  %g  %g %g %g\n",
    age,lum_hat[Num]*l_0,lgT_c,lgT_s_r,
	lgT_m,	log10(PhotonL(T_hat[Num])*l_0)); 

fprintf(agrF," %g  %g\n",
    age,lgT_s_r); //for plot in agr file 
/***********************************************************************/
}

if (ager > 4.5) ager = 3;
if(lgnlgs&&(
//   1.5 // for lognlogs
(ager<=age)&&(ager + 0.05>=age) // for BC
)){
fprintf(ll_File," %g  %g \n",
    age,lgT_s_r); 
ager += 0.5;
};
}
fclose(out_File);fclose(out_File_L);
if(lgnlgs) fclose(ll_File);

if(lgT_s_r>min_lgT){
fclose(agrF);
	return 0;
}else
{
fprintf(agrF,"&\n");
fclose(agrF);
	return 1;
}
}


 



