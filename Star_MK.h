// Star_MK.h: interface for the Star_MK class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STAR_MK_H__A2C046A6_9D2F_430E_9739_C1A5689619D3__INCLUDED_)
#define AFX_STAR_MK_H__A2C046A6_9D2F_430E_9739_C1A5689619D3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "cool_eos.h"
#include "RW_cool.h"

class Star_MK  
{
public:
//	CCool_EoS* pEoS;

	void init_EoS_WGB(double *muu, double tee, CCool_EoS *EoS_type,char* Fret);
	void star_maker();

	Star_MK();
	virtual ~Star_MK();
};

#endif // !defined(AFX_STAR_MK_H__A2C046A6_9D2F_430E_9739_C1A5689619D3__INCLUDED_)
