// RW_cool.h: interface for the RW_cool class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RW_COOL_H__647D75FD_A2A2_4183_BDD7_9A5BD9AB9508__INCLUDED_)
#define AFX_RW_COOL_H__647D75FD_A2A2_4183_BDD7_9A5BD9AB9508__INCLUDED_

#include "inputs.h"
#include "stdio.h"
#include "EoS_Bag.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// #define _HYP_

//#define HOME "D:/DC/CTN/CTN/Data"
//#define HOME "D:/Hovik/CTN/CTNW/Data"
//#define HOME_EV "./Data/EVdd2"
//#define HOME "./Data/3"

#define EoS_lw "\\\WL_"

#ifdef _new_
#define EoS_nlw "\\\WNL_"
#else
#define EoS_nlw "/WNLp_"
#endif

#define EoS_hjj "\\\HD_"
#define EoS_f "\\\F_"
#define EoS_SLy "\\\SLy_"
#define EoS_FPS "\\\FPS_"

#define Config_sufx  "_int.dat"
#define Config_file1  "conf.dat"


#define final_sufx "_fi.dat"
#define initial_sufx "_in.dat"
#define ev_sufx "evol"


#define Num   250
#define NUMT 300

#define r_0   1.477
#define n_0   0.16
#define ro_0  2.761e+4
                         /* MeV/fm^3       */
#define kT_0  0.08617
                         /* MeV 1e-9 K^-1  */  

#define  t_0  3.1536e+7
                         /* yr = t_0 s     */
#define ZERO  1e-15

//extern char final_file[100], initial_file [100], evolution_file [100];
#define LA 1.0

class RW_cool  
{
		EoS_Bag Qeos;

public:
	double Xi_mix_q(double mu);
	double Xi_mix;
	char _XGAP_,_FGAP_,_FGAPn_, _FGAPL_;
	double min_lgT;
	double init_eta;
	double init_T;
	int PTPk;
	bool cptp,lgnlgs;
	int NPTP;
	double ptp[50];
	double Eta(double n_nu, double T);
	double NN_nu(double eta, double T_h);
    double DYDE_nu(double eta, double T);
	
	int m_FF;
	int next;
	bool lowbranch;

bool m_cond;
bool m_SM;

bool m_WM;
	double QGap(double mu);
	double QGap_X(double mu);
FILE* test;
int DNum;

char File_Name0[150];
char final_file[150], 
     initial_file [150], 
	 lgnlgs_file [150],
	 evolution_file [150],
evol_L_file [150];
char agr_file_[150];
char HOME_EV[150];
char HOME [150];

char  _CONFIGNAMEEV_[150],
      _CONFIGNAME_[150],
      _FIGNAME_[150];
char evolution_sufx[6];
	RW_cool();
	virtual ~RW_cool();

double A_(int o);
double B_(int o);
double C_(int o);
double D2_(int o);
bool Evolution(double del_tt,double pr_ft,double *lg_t,double* lg_T);
bool EV_run(int *nii, double *lg_t,double* lg_T);

//	int Step_time(double, double);
	int Step_time(double);
//	void Step_in_time(double, double);
	void Step_in_time(double);
    void HES(double *,double *,double *); 
    int Energy_flux(double); 
//    int Energy_flux(double,double); 
//    double  dt_correc(double,double);
    double  dt_correc();



/****** The set of Star internel Structure functions *****/
 

 double  
          ro[Num+1],   // energy density          in       MeV            fm^-3
          rr[Num+1],   // distence from cenrte    in km      
        mass[Num+1],   // accumulated mass        in M_sun
        c_v_[Num+1],   // specific heat  
        eps_[Num+1],   // emmisivity  
/**/
#ifdef _HYP_
eps_DU_np[Num + 1],
eps_DU_Lp[Num + 1],
eps_DU_Sn[Num + 1],
eps_DU_SL[Num + 1],
eps_DU_SS[Num + 1],
eps_DU_XL[Num + 1],
eps_DU_XS[Num + 1],
eps_DU_XSo[Num + 1],
eps_DU_XX[Num + 1],
#endif // _HYP_
   eps_PPBF[Num+1]
 ,
  
	 eps_nnBS[Num + 1]
	 ,
	 eps_npBS[Num + 1]
	 ,
	 eps_ppBS[Num + 1]
	 ,// emmisivity  
        eps_NPBFs[Num+1]//s
 ,
		eps_NPBFp[Num+1]
 ,    
        eps_MU_H_p[Num+1]
 ,    
        eps_MU_H_n[Num+1]
 ,    
        eps_ee[Num+1]
 ,    
        eps_DU_H[Num+1]
 ,    
eps_PU[Num+1]
,
/**/
eps_MU_Q,
eps_DU_Q_n,
eps_DU_Q ,
eps_BS_Q ,
eps_JP_Q ,     
eps_gg ,
eps_QPBF,
/**/
gap_n_0,
gap_p_0,
gap_q_0,
gap_q_0_X,
/**/
       supp_[Num+1],   // suppretion coeffitient 
          k_[Num+1],   // conductivity            in       MeV K^-1 s^-1  fm^-1 
		  d_3[Num+1],
		  d_2[Num+1],
		  d_4[Num+1],
         phi[Num+1],   // gravitatioal potential
        T_in[Num+1];   // initialtemperature profile  in K

 double    Xi[Num+1],  //Quark-phase fruction
         ms_n[Num+1],  // effective mass of neurton/neutron mass
         ms_p[Num+1],  // effective mass of neurton/neutron mass

		 ms_L[Num+1],  // effective mass of neurton/neutron mass
         ms_Sp[Num+1],  // effective mass of neurton/neutron mass
         ms_S0[Num+1],  // effective mass of neurton/neutron mass
         ms_Sm[Num+1],  // effective mass of neurton/neutron mass
         ms_X0[Num+1],  // effective mass of neurton/neutron mass
         ms_Xm[Num+1],  // effective mass of neurton/neutron mass

Y_L[Num+1],  //  fruction
Y_Sm[Num+1],  //  fruction
Y_S0[Num+1],  //  fruction
Y_Sp[Num+1],  //  fruction
Y_X0[Num+1],  //  fruction
Y_Xm[Num+1],  //  fruction
		 
		 
		 
Y_e0,//electron fraction at saturation
		 Y_e[Num+1],  // electron fruction
           n_p[Num+1],  //proton fruction
           n_n[Num+1],  //neutron fruction
           n_b[Num+1],  //baryon number desity   
           mu_b[Num+1],  //baryochemical potential
            p[Num+1];  //presuure distribution     in       MeV    fm^-3

 double    T_hat[Num+1],  //temperature profile  in T_0
          T_hat_old[Num+1];  // temperature profile (t - dt) in T_0
 double    eta[Num+1],  // neutrinos chem.potetial per Temperature profile
	 Y_Le[Num+1],  // Lepton fraction profile
       lum1_hat[Num+1],
       lum2_hat[Num+1],
		  lum_hat[Num+1];  // luminosity profile  in l_0

 /*******************************************************************/

 double L_nu,dm,_time,_time0;
 char Config_file[130];
double Surface_t_T(double T_h);
double PhotonL(double T_h);
double NeutrinoL(double eta, double T_h);
double NeutrinoFlux(double eta, double T_h);

double S_t_T_Turuta(double lgT_m);
double S_t_T_Y(double lgT_m);
double S_t_T_G(double lgT_m);
double S_t_T_NULL(double Tin9);

void in_temp_profile(int j);
void critmix();

char _CRUST_;
char _GAP_;

double gap_fps;
double gap_fns;
double gap_fnp;


 bool     cont_cal_f,
			ifpicon,
			ifPI;
	bool	m_qcore;

bool on_fast_cool,
    normal_shell,
    normal_core,
    star_f;
double t_end,del_t;

int ni,o/*,num_t*/;
//double lg_T,lg_t;
/*******************************************************************/
double lgtin,lgtout,t_in,tau,pr_f;  //,end_run_time;

 int read_initial_str();
 int begin_input();
 void write_results();
protected:
	bool picon1(int);
	bool picon0(void);
	double PIF,F1,Ga,Z1, C_kn;
double Ga_pi;
double m_n,m_p;

	bool S10n,S10p,P32n;
	double ksi_p, ksi_n,
		ksi_q,
		ksi_q_X,
		supp,
		supp_X,
		R_n, R_p,
		RMp , RMn,
		R_D,R_D_h,
	 R_q,
	 R_q_X;
 double gap_q,
	 gap_q_X,
	 gap_n, 
	 gap_p,
	 gap_N,
	 ksi;
 int n_h_c,n_q_c;
 double TTc,
	 T_c,
	 T_c_X,
	 T_c_l,
	 T_c_n,
	 T_c_p;
 void Gaps(int o,double T);
 void specific_heat(int o,double T);
 void emissivity(int o,double T);
 void conductivity(int o,double T);
 void neutrinodiffusion(int o,double T);
 
double gap_factor;
double Rc(double y);
double lnRn1(double y);
double lnRn2(double y);
double lnRp2(double ,double);
double lnRp1(double ,double);
double GapApp(double *KK, double n, int Z);
double GapAV18(double *KK, double n, int Z);
double GapUSP(double *KK, double n);
double GapUSP2(double *KK, double n);

public:
	// Integral Luminosities
	double 
#ifdef _HYP_
		L_DU_np,
		L_DU_Lp,
		L_DU_Sn,
		L_DU_SL,
		L_DU_SS,
		L_DU_XL,
		L_DU_XS,
		L_DU_XSo,
		L_DU_XX,
#endif // _HYP_
		L_PPBF,
   L_pBS,    
        L_NPBFs,
		L_NPBFp,    
        L_MU_H_p,    
        L_MU_H_n,    
        L_ee,    
        L_DU_H,    
L_PU;
	void Luminos(void);
	// Experimental 3P2 gaps for neutrons
	double Gap_BG(double n);
	double GapPp(double *KK, double n);
//double GapFSn(double *KK, double n);
//double GapFPn(double *KK, double n);

	int DM_heat(int o, double T);
	double Q_DM, k_DM;
	double k_fac_DM;


};

#endif // !defined(AFX_RW_COOL_H__647D75FD_A2A2_4183_BDD7_9A5BD9AB9508__INCLUDED_)
