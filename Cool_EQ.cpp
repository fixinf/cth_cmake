//#include "stdafx.h"
//#include "menu.h"
#include "RW_cool.h"
//#include "inputs.h"
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define eps00 3.839e26
/*erg cm^-3 s^-1*/



//#define _PRINT_
//#define _PRINT_INPUTS

//#define _TEST_EM_H
//#define _TEST_EM_Q

//#define _TEST_RF
//#define _TEST_GAPS
//#define _TEST_CV
//#define _TEST_Om

#ifdef _TEST_RF
#define _TEST_ ".\\Data\\DD2\\MU_RFactors.dat"
#endif
#ifdef _TEST_Om 
#define _TEST_ "test_omega_3.0.dat"
#endif
//#define _TEST_ "test_gaps9_HDD.dat"
//#define _TEST_ "test_XgapsDD2NJL_1.47.dat"
#ifdef _TEST_EM_H
#define _TEST_ ".\\Data\\DD2\\ALL_EMISS_M2.416T0.1SC.dat"
#endif // _TEST_EM_H
#ifdef _TEST_GAPS
#define _TEST_ ".\\Data\\DD2\\ALL_Gaps_M2.416T0.1SC.dat"
#endif



/*******************************************************************/
double RW_cool::A_(int o)
{
	double m_h, ro_h, c_v_h, r_h;
	m_h = mass[o];
	ro_h = ro[o] / ro_0;
	c_v_h = c_v_[o];
	r_h = rr[o] / r_0;
	//printf(" Cap in A %g \n",c_v_h);
	return -ro_h*sqrt(1 - 2 * m_h / (r_h + ZERO)) / (c_v_h * T_hat[o] + ZERO);

}

/*******************************************************************/


double RW_cool::B_(int o)
{
	double  eps_h, c_v_h, phi_h;
	eps_h = eps_[o];
	c_v_h = c_v_[o];
	//printf(" Cap in B %g \n",c_v_h);
	phi_h = phi[o];
	return -exp(2 * phi_h)* eps_h / (c_v_h * T_hat[o] + ZERO);
}

/*******************************************************************/

double RW_cool::C_(int o)
{
	double m_h, k_h, ro_h, r_h, phi_h;
	m_h = mass[o];
	k_h = k_[o];
	ro_h = ro[o] / ro_0;
	phi_h = phi[o];
	r_h = rr[o] / r_0;
	return -k_h * T_hat[o]
		* exp(phi[o])
		* r_h * r_h* r_h* r_h* ro_h *sqrt(1 - 2 * m_h / (r_h + ZERO));
}

/*******************************************************************/

void RW_cool::HES(double *An, double *Bn, double *Cn)
{

	int o;

	//#ifdef _STD_C_
	//	normal_shell = 1;
	//#else	
	//	normal_shell = 1;

	normal_core = 0;
	//	ifpicon = 1;
	gap_factor =
		1;
	Y_e0 = apra(Y_e, n_b, n_0, Num);
	/* Main cycle in spacelike diration */
#ifndef _PRINT_
#ifdef  _PRINT_INPUTS 
	test = fopen(_TEST_, "w");
#ifdef _TEST_EM_H
	fprintf(test, " #r[km] , n_b, eps -- PPBF, NPBFp, NPBFs, MU_H_p, MU_H_n, ppBS, npBS, nnBS, DU_H\n");
#endif
#ifdef _TEST_Om
	fprintf(test, " #r[km] , n_b , mass , omega^2, Gamma, PIF, m_n, m_p \n");
#endif // _TEST_Om
#ifdef _TEST_GAPS
	fprintf(test, " #r[km] , n_b[1/fm^3] , T_c_n [10^9K], T_c_p[10^9K], gap_n_0[10^9K], gap_p_0[10^9K], gap_n(T)[10^9K], gap_p(T)[10^9K] \n");
#endif // _TEST_GAPS


#endif
	for (o = 0; o <= Num; o++) {
#else 
	test = fopen(_TEST_, "w");
	o = 1;
	n_b[o] = 2 * n_0;
	n_n[o] = 0.293657;
	n_p[o] = 0.0266966;
	ms_n[o] = 1;
	ms_p[o] = 1;
	Y_e[o] = 0.0186131;
	Xi[o] = 0;
	phi[o] = 0;
	for (int i = 0;i<300;i++) {
		T_hat[o] = exp((-2.0 + 0.01*i)*log(10.0));
#endif

#ifdef  _PRINT_INPUTS
		double for_T = 0.1; // in T9 K
		T_hat[o] = for_T*exp(phi[o]);
#endif

		Gaps(o, T_hat[o]);
		//Y_e[o]=ZERO*ZERO;
		specific_heat(o, T_hat[o]);
		conductivity(o, T_hat[o]);
		emissivity(o, T_hat[o]);

#ifdef _DM_
		DM_heat(o, T_hat[o]);
		eps_[o] += -Q_DM;

		k_[o] += k_DM;
		k_[o] *= k_fac_DM;
#endif
		//*0.13 //APR 
		//*0.265 //HHJ CasA
		//*0.106 //HHJ-SM CasA

		////k_[o]*= 0.24
		////			

		//c_v_[o]=4000;
		//k_[o]=1e-7;

		/**/
		//eps_[o] = 0 
		//   + 1.e-6*pow(T_hat[o],8)
		//;
		/*		if (ro[o]<0.005 )
		{c_v_[o]*=0.00001;
		eps_[o]*=-0.01;
		k_[o]*=100.;
		}
		*/
		/*   The main neutrino emission processes */

#ifdef _PRINT_
	};
	fclose(test);
	exit(1);
#else

		//eps_[o]=0;
		//printf(" r=%g Cv =%g eps =%g k =%g\n",rr[o],c_v_[o],eps_[o],k_[o]);

		An[o] = A_(o);

		double BB = Bn[o] = B_(o);
		if (BB > 1 / ZERO)
		{
			double s = 1;
		}
		Cn[o] = C_(o);
#ifdef _PRINT_INPUTS
		fprintf(test, " %14.7g %4.7g %4.7g %4.7g %4.7g %4.7g %4.7g %14.7g %14.7g %4.7g %4.7g\n", rr[o],
#ifdef _TEST_GAPS
			n_b[o], T_c_n, T_c_p, gap_n_0, gap_p_0, gap_n, gap_p, 0.0, 0.0, 0.0
#endif // _TEST_GAPS


#ifdef _TEST_Om
			n_b[o], mass[o], Z1, Ga, PIF, m_n, m_p, 0, 0, 0
#endif // _TEST_Om


#ifdef _TEST_EM_H
			n_b[o],
			eps_PPBF[o] * eps00, eps_NPBFp[o] * eps00, eps_NPBFs[o] * eps00,
			eps_MU_H_p[o] * eps00, eps_MU_H_n[o] * eps00,
			eps_ppBS[o] * eps00, eps_npBS[o] * eps00, eps_nnBS[o] * eps00,
			eps_DU_H[o] * eps00
#endif

#ifdef _TEST_EM_Q
			mu_b[o],
			gap_q_0_X,
			eps_DU_Q_n,
			eps_DU_Q,
			eps_MU_Q,
			eps_BS_Q,
			eps_JP_Q,
			eps_gg,
			eps_QPBF,
			0
#endif
#ifdef _TEST_RF
			n_b[o],
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
#endif


		);
#endif	
		//fprintf(test," n=%d %g %g %g %g  \n",o,rr[o],- An[o],- Bn[o],- Cn[o]);
		//printf(" n=%d %g %g %g %g  \n",o,rr[o],- An[o],- Bn[o],- Cn[o]);
	};

#ifdef _PRINT_INPUTS
	fclose(test);
	exit(1);
#endif
#endif
	//#ifdef _PRINT_
	//fclose(test);
	//exit(0);
	//#endif

}
