#if !defined(EOS_HYBRID_DEFINED)
#define EOS_HYBRID_DEFINED
#include "EoS_Bag.h"
#include "EoS_Hadron.h"
#include "Zbrent.h"

class EoS_Hybrid
{
 private:
  // 0 = NPE
  // 1 = NLW
  // 2 = HBerg
  char c_hadron;
  // = SM + 2*cond + 4*FF
  int c_quark;

  // = hyb + 2*maxwell + 4*hadronic
  int c_hybrid;
 // int hyb_hyb;
//  int hyb_max; 
//  int hyb_had;
  double c_T;

  // checks stored values
  char check_hadron();
  int check_quark();
  int check_hybrid();

  // calculates value for comparison
  // with stored value
  int check_quark(bool SM, 
		  bool cond, 
		  bool FF);
  int check_hybrid(int hyb, 
		   int maxwell, 
		   int hadronic);

  // self explaining...
  void construct_maxwell(double T);
  void construct_glendenning(double T);

  
  double crit_muB_min;
  double crit_muB_max;

  double mue_mix(double muB, double T );

 public:
  int m_maxwell;
  int sw;
  double eps,s;
  //  double *nn;
  double nn[20];
  double mu_e,mu_b,kT;
  double nb;
  double pr;
  double Xi;
  int m_hadron;
  int m_hybrid;
  
  //need general quark and hadron eos
  EoS_Bag    quark;
  EoS_Hadron hadron;

  //empty constructor
  EoS_Hybrid(){
  
   //assure that phasetransition
  //will be calculated under initialization of
  //EoS_Hybrid
  c_hadron = -1;
  c_quark  = -1;
  c_hybrid = -1;
  c_T      = -1.;

  //Limits for critical baryochemical potential
  crit_muB_min = 1000;
  crit_muB_max = 1800;  
};
  //constructor for specific Hybrid-EoS
  EoS_Hybrid(char hadron_sw, 
	     bool quark_sw_SM, 
	     bool quark_sw_cond,
	     bool quark_sw_FF,
	     int hybrid_hyb,
	     int hybrid_maxwell,
	     int hybrid_hadronic,
	     double T);

  //member function checks if EoS's or temperature
  //have changed. If so, it calculates the
  //baryochemical chritical potentials
  //where phase transition occures
  void InitPhaseTrans(char hadron_sw, 
		      bool quark_sw_SM, 
		      bool quark_sw_cond,
		      bool quark_sw_FF,
		      int hybrid_hyb,
		      int hybrid_maxwell,
		      int hybrid_hadronic,
		      double T);

  // returns ratio of hadronic to hadronic+quark matter
  // for given muB and T
  double fraction(double muB, double T);

  // EoS:
  double mue_(double muB, double T);
 // double p_(double muB, double T);
  
  double n_(double mu, double T,double* n);
  double s_(double mu, double T);
  double en_(double mu,  double T);
  double pr_(double mu, double T);
  int kind_(double mu,double T);
  
  //these are the critical baryochemical potentials
  //for hadron and quark matter...
  double muB_crit_H;
  double muB_crit_Q;
};

#endif // !defined(EOS_HYBRID_DEFINED)
