//#include "stdafx.h"
/* Av18 + Three Body Forces by Brueckner-Bethe-Goldstone approximation
   mjuB is shifted by MN inside the program so there is no necessity to do it before calling BHFeos*/
#include "BHF.h"
#include "zbrent.h"
//#include "relintgp.h"
#include <math.h>

#define zero 1.e-10
#define M_PI 3.1415926535897932384626433832795
#define hc3 (197.32705*197.32705*197.32705)
#define hc 197.32705
#define MJUON 1
#include "av18.fit"
//#include "paris.fit"

static double M_[]={939.56563, 938.272, 
		    1115.683, 1197.449, 
		    0.51099907, 
#if MJUON
		    105.658389
#else
		    0.0
#endif
};

//Space factors g=2*J+1 J-spin
//n p 
//lambda sigmaminus
//e mju
static double G_[]={2.,2.,
		    2., 2., 
		    2.,
#if MJUON
		    2.
#else
		    0.
#endif
};

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

BHF::BHF(){}

BHF::~BHF(){}

double BAsym(double nN, double *doubleAsymdnN)
{
  int cntr=1; while (nN>nNmat[cntr]&&nNmat[cntr]<2.) cntr++;
  *doubleAsymdnN=(BAsymat[cntr]-BAsymat[cntr-1])/(nNmat[cntr]-nNmat[cntr-1]);
  return BAsymat[cntr-1]+(nN-nNmat[cntr-1])*(*doubleAsymdnN);
}

double Esym(double nN, double *dEsymdnN)
{
  int cntr=1; while (nN>nNmat[cntr]&&nNmat[cntr]<2.) cntr++;
  *dEsymdnN=(Esymat[cntr]-Esymat[cntr-1])/(nNmat[cntr]-nNmat[cntr-1]);
  return Esymat[cntr-1]+(nN-nNmat[cntr-1])*(*dEsymdnN);
}

double mjunfun(double x,double xsig,double nN) 
{double doubleAsymdnN,Esympo,dEsymdnN;
 Esympo=Esym(nN,&dEsymdnN);
 return 
   BAsym(nN,&doubleAsymdnN)
   +nN*doubleAsymdnN
   +(1.-2.*x)*(1.-2.*x)*Esympo
   +nN*(1.-2.*x)*(1.-2.*x)*dEsymdnN
   +4.*x*(1.-2.*x)*Esympo
   +mjuntil(nN)*xsig
   +M_[0];
}

double _A_,_B_;

double ZEDzebr(double zed)
{return _A_*zed*zed*zed+_B_*zed*zed+1;}

double xsigfun(double mjuB,double mjue,double nN)
{double xsig,a,b,c
   //,dzi
   ;
 c=M_[3]+Usig(nN)-mjuB-mjue;

 if (c>zero) xsig = 0.0;
 else 
   {
     _A_=a=mjusigtil(nN)/c;
     _B_=b=pow(3.*M_PI*M_PI*nN,2./3.)*hc*hc/2./msig(nN)/c;
     if (27.*a*a*27.*a*a+4.*b*b*b*27.*a*a<zero){
       xsig=zbrent(ZEDzebr,zero,1.,zero);     
       return xsig*xsig*xsig;
     }
     //	dzi=pow(1./2.*(27.*a*a+2.*b*b*b+3*a*sqrt(3.*27.*a*a+4.*b*b*b*3.)),1./3.);
     //	xsig=-b/3./a-b*b/3./a/dzi-dzi/3./a;
   }
 return xsig*xsig*xsig;
}

double xfun(double mjue,double nN,double xsig)
{double	x;
 x=1./2.*( 1.-(mjue-mjuntil(nN)*xsig+mjuptil(nN)*xsig)/4./Esym(nN,&x) );
 if (x<zero) x=0.0;
 return x;
}

double nlam(double mjuB,double nN,double x,double xsig)
{double nla;
 if (M_[2]-mjuB+mjulamtil(nN)*xsig+Ulam(nN,x)>zero) nla=zero;
 else
   nla=pow(2.*mlam(nN)*(mjuB-mjulamtil(nN)*xsig-M_[2]-Ulam(nN,x)),3./2.)/3./M_PI/M_PI/hc3;
 return nla;
}

double _muE_,_muB_,_NN_;
BHF *bhf;

double mjuechargeNT_s(double mjue)
{
  return bhf->mjuechargeNT(mjue);
}

double mjuBchargeNT_s(double NB)
{
  return bhf->mjuBchargeNT(NB);
}


double BHF::mjuechargeNT(double mjue)
{
  double ret_urn,xsig,x;
  _muE_=mjue;
  //_NN_=zbrent(mjuBchargeNT,zero,2.,zero);
  xsig=xsigfun(_muB_,_muE_,_NN_);
  x=xfun(_muE_,_NN_,xsig);
  g = G_[4];
  mass = M_[4];
  ret_urn= EoS_HB::n_(_muE_,zero);
  g = G_[5];
  mass = M_[5];
  ret_urn += n_(_muE_,zero) 
    + xsig*_NN_ - x*_NN_;
  return ret_urn;
}

double BHF::mjuBchargeNT(double nN)
{
  double ret_urn,xsig,x;
  _NN_=nN;
  bhf = this;
  _muE_=zbrent(mjuechargeNT_s,zero,1500./*_muB_-M_[1]*/,zero);
  xsig=xsigfun(_muB_,_muE_,_NN_);
  x=xfun(_muE_,_NN_,xsig);
  ret_urn=_muB_- mjunfun(x,xsig,_NN_);
  return ret_urn;
}

double BHF::BHFHHintchne(double mjuB, double *nB, double *eps, double *n)
{double x, temp, nN, xsig;
 int i;
 _muB_=mjuB;
 bhf = this;
 nN=zbrent(mjuBchargeNT_s,zero,2.,zero);
 _muE_=zbrent(mjuechargeNT_s,zero,500./*_muB_-M_[1]*/,zero);
 xsig=xsigfun(_muB_,_muE_,nN);
 x=xfun(_muE_,nN,xsig);
 n[3]=xsig*nN+zero;
 n[1]=x*nN;
 n[0]=nN-n[1];
 n[2]=nlam(_muB_,nN,x,xsig);
 *nB=nN+n[2]+n[3]+zero;
 g = G_[4];
 mass = M_[4];
 n[4]=n_(_muE_,zero);
#if MJUON
 g = G_[5];
 mass = M_[5];
 n[5]=n_(_muE_,zero);
#else
 n[5]=zero;
#endif
 for(i=0;i<=5;i++) n[i]+=zero;
 *eps=( BAsym(nN,&temp)+(1.-2.*x)*(1.-2.*x)*Esym(nN,&temp) )*nN;
 *eps+=n[2]*(M_[2]/*mlam(nN)*/+Ulam(nN,x))+3.*n[2]/10./mlam(nN)*pow(3.*M_PI*M_PI*n[2],2./3.)*hc*hc;
 *eps+=n[3]*(M_[3]/*msig(nN)*/+Usig(nN))+3.*n[3]/10./msig(nN)*pow(3.*M_PI*M_PI*n[3],2./3.)*hc*hc;
 g = G_[4];
 mass = M_[4];
 *eps+=en_(_muE_,zero);
#if MJUON
 g = G_[5];
 mass = M_[5];
 *eps+=en_(_muE_,zero);
#endif
 *eps+=M_[0]*nN;
 return _muE_;
}

double mjuBmjue2nN(double nN)
{double xsig,x,ret_urn;
 xsig=xsigfun(_muB_,_muE_,nN);
 x=xfun(_muE_,nN,xsig);
 ret_urn=_muB_- mjunfun(x,xsig,nN);
 return ret_urn;
}

void BHF::BHFHHint(double mjuB, double mjue, double *nB, double *eps, double *n)
{
  double temp, nN, xsig, x;
  int i;
  _muB_=mjuB;
  _muE_=mjue;
  nN=zbrent(mjuBmjue2nN,zero,2.,zero);
  xsig=xsigfun(_muB_,_muE_,nN);
  x=xfun(_muE_,nN,xsig);
  n[3]=xsig*nN+zero;
  n[1]=x*nN;
  n[0]=nN-n[1];
  n[2]=nlam(_muB_,nN,x,xsig);
  *nB=nN+n[2]+n[3]+zero;
  g = G_[4];
  mass = M_[4];
  n[4]=n_(mjue,zero);
#if MJUON
  g = G_[5];
  mass = M_[5];
  n[5]=n_(mjue,zero);
#else
  n[5]=zero;
#endif
  for(i=0;i<=5;i++) n[i]+=zero;
  *eps=( BAsym(nN,&temp)+(1.-2.*x)*(1.-2.*x)*Esym(nN,&temp) )*nN;
  *eps+=n[2]*(mlam(nN)+Ulam(nN,x))+3.*n[2]/10./mlam(nN)*pow(3.*M_PI*M_PI*n[2],2./3.)*hc*hc;
  *eps+=n[3]*(msig(nN)+Usig(nN))+3.*n[3]/10./msig(nN)*pow(3.*M_PI*M_PI*n[3],2./3.)*hc*hc;
  g = G_[4];
  mass = M_[4];
  *eps+=en_(mjue,zero);
#if MJUON
  g = G_[5];
  mass = M_[5];
  *eps+=en_(mjue,zero);
#endif
  *eps+=M_[0]*nN;
}
#undef MJUON
#undef zero
#undef M_PI
