//#include "stdafx.h"
#include "EoS_Hybrid.h"

EoS_Hybrid* tmp_hyb;
double _T,_MUB;

#define Zero 0.000001


//TEMPORARY!!!

//EoS_Hybrid::EoS_Hybrid(void){};
EoS_Hybrid::EoS_Hybrid(char hadron_sw, 
		       bool quark_sw_SM, 
		       bool quark_sw_cond,
		       bool quark_sw_FF,
		       int hybrid_hyb,
		       int hybrid_maxwell,
		       int hybrid_hadronic,
		       double T)
{
  //assure that phasetransition
  //will be calculated under initialization of
  //EoS_Hybrid
  c_hadron = -1;
  c_quark  = -1;
  c_hybrid = -1;
  c_T      = -1.;

  //Limits for critical baryochemical potential
  crit_muB_min = 1000;
  crit_muB_max = 1800;  

  //Here we go...
  InitPhaseTrans( hadron_sw, 
		  quark_sw_SM, 
		  quark_sw_cond,
		  quark_sw_FF,
		  hybrid_hyb,
		  hybrid_maxwell,
		  hybrid_hadronic,
		  T);
}


char EoS_Hybrid::check_hadron(){
  // ask for kind of hadronic EoS
  return c_hadron;
}

int EoS_Hybrid::check_quark(){
  return quark.m_SM + 2*quark.m_cond + 4*quark.m_FF;
}

int EoS_Hybrid::check_quark(bool SM, bool cond, bool FF){
  return SM + 2*cond + 4*FF;
}

int EoS_Hybrid::check_hybrid(){
  return c_hybrid;
}

int EoS_Hybrid::check_hybrid(int hyb, int maxwell, int hadronic){
  return hyb + 2*maxwell + 4*hadronic;
}

//////////////////////////////////////////////
// all about maxwell
double hybrid_p_diff_maxwell(double muB){
  //returns had.-qu.-pressure difference for given muB in beta-eq.
  //_T has to be defined before calling this function!
  return tmp_hyb->hadron.p_(muB,_T)-tmp_hyb->quark.p_(muB,tmp_hyb->quark.B2e(muB,_T),_T); 
}

void EoS_Hybrid::construct_maxwell(double T){
  tmp_hyb=this;
  _T = T;
  muB_crit_H 
    = muB_crit_Q
    = zbrent(hybrid_p_diff_maxwell,crit_muB_min,crit_muB_max,zero);

  //checks on EoS:
  //For mu's above muB_crit: P_H<P_Q!!!
  double muB=muB_crit_H + 1 /*MeV*/;
  double p_H = hadron.p_(muB,T);
  double p_Q = quark.p_(muB,quark.B2e(muB,T),T);
  if(p_H > p_Q ){
    printf(
     "EoS_Hybrid: Maxwell construction results in non physical result.\n");
    if(m_hadron){
      printf("EoS_Hybrid: Switch to hadronic EoS\n");
      muB_crit_H= muB_crit_Q=1./zero;
    }
    else{
      printf("EoS_Hybrid: Switch to quark EoS\n");
      muB_crit_H= muB_crit_Q=zero;
    }
  }
}  

//////////////////////////////////////////////
// all about glendenning
double hybrid_p_diff_glen_hadron(double muB){
  double mue = tmp_hyb->hadron.mue_(muB,_T);
  double P_H = tmp_hyb->hadron.p_(muB,mue,_T);
  double P_Q = tmp_hyb->quark.p_(muB,mue,_T);
  return P_H-P_Q;
}

double hybrid_p_diff_glen_quark(double muB){
  double mue = tmp_hyb->quark.B2e(muB,_T);
  double P_H = tmp_hyb->hadron.p_(muB,mue,_T);
  double P_Q = tmp_hyb->quark.p_(muB,mue,_T);
  return P_H-P_Q;
}

void EoS_Hybrid::construct_glendenning(double T){
  tmp_hyb=this; 
  _T = T;
  //Step one: critical baryochemical Potential for hadronic phase
  //  crit_muB_H = zbrent(p_diff_glen_hadron,crit_muB_min,crit_muB_max,zero);
  muB_crit_H = zbrent(hybrid_p_diff_glen_hadron,crit_muB_min,crit_muB_max,zero);

  //Step two: critical baryochemical Potential for quark    phase
  //  crit_muB_Q = zbrent(p_diff_glen_quark ,crit_muB_min,crit_muB_max,zero);
  muB_crit_Q = zbrent(hybrid_p_diff_glen_quark ,crit_muB_min,crit_muB_max,zero);

  //checks on crit_muB_(H/Q) (if necessary) have to be performed now:
  //on crit_muB_H<crit_muB_Q!
  if(muB_crit_Q<=muB_crit_H){
    printf("Glendenning construction for phase transition failed:\n");
    printf("-->Hadronic EoS Nr.%s\n",hadron.Hmodel);
    printf("-->critical chemical pot.(hadrons):%e\n",muB_crit_H);
    //    printf("-->Quark EoS: m_SM=%d, m_cond=%d, m_FF=%d\n",quark.m_SM,quark.m_cond,quark.m_FF);
    printf("-->critical chemical pot.(quarks ):%e\n",muB_crit_Q);
    printf("Will do a maxwell construction instead... \n");
    construct_maxwell(T);
  }

}



void EoS_Hybrid::InitPhaseTrans(char hadron_sw, 
				bool quark_sw_SM, 
				bool quark_sw_cond,
				bool quark_sw_FF,
				int hybrid_hyb,
				int hybrid_maxwell,
				int hybrid_hadronic,
				double T)
{
  // if all stays at it is,
  // there is nothing to do: return
  if(  ( hadron_sw==check_hadron() )
       &&( check_quark(quark_sw_SM, 
		       quark_sw_cond,
		       quark_sw_FF)
	   ==check_quark()         )
       &&( check_hybrid(hybrid_hyb,
			hybrid_maxwell,
			hybrid_hadronic)
	   ==check_hybrid()        )
       &&( c_T
	   ==T                     )
       ){
    return;
  }

  ///////////////////////////////////////////
  // If there are changes,
  // we have to apply them:

  // Starting with hadronic EoS.
  if(hadron_sw!=check_hadron()){
     // change hadronic EoS
    hadron.Hmodel = hadron_sw;
    // reset check switch to actual value
    c_hadron  = hadron_sw;
//    printf("EoS_Hybrid: Switched to new hadronic EoS (Nr.%d)\n",hadron.Hmodel);
  }
  // Continuing with quark EoS.
  if(check_quark(quark_sw_SM, 
		 quark_sw_cond,
		 quark_sw_FF)
     !=check_quark()){
    // change quark EoS
    quark.m_SM  = quark_sw_SM;
    quark.m_cond= quark_sw_cond;
    quark.m_FF  = quark_sw_FF;
    // and reinitialize bag constant
    quark.initBfuncion();
    // reset check switch to actual value
    c_quark = check_quark(quark_sw_SM, 
			  quark_sw_cond,
			  quark_sw_FF);
    printf("EoS_Hybrid: Switched to new quark EoS: SM=%d cond=%d FF=%d\n",
	   quark.m_SM,
	   quark.m_cond,
	   quark.m_FF);
  }

  //Temperature?
  if(c_T!=T){
    c_T=T;
    printf("EoS_Hybrid: Switched to new temperature: T=%f\n",c_T);
  }
  if(check_hybrid(hybrid_hyb,
		  hybrid_maxwell,
		  hybrid_hadronic)
     !=check_hybrid()){
    m_hybrid = hybrid_hyb;
    m_maxwell = hybrid_maxwell;
    m_hadron = hybrid_hadronic;
    printf("EoS_Hybrid: Switched to new kind of phase transition: hyb=%d max=%d had=%d\n",m_hybrid,m_maxwell,m_hadron);
  }  
  //Now everything should be fine
  //to care about phase transition

  // phase transition at all?
  if(hybrid_hyb){
    // Maxwell or Glendenning?
    if(hybrid_maxwell){
      printf("EoS_Hybrid: Maxwell - phase transition\n");
      construct_maxwell(T);
    }
    else{
      printf("EoS_Hybrid: Glendenning - phase transition\n");
      construct_glendenning(T);
    }
  }
  // no phase transition...
  else{
    // hadronic EoS
    if(hybrid_hadronic){
      printf("EoS_Hybrid: no phase transition, hadronic matter\n");
      muB_crit_H= muB_crit_Q=1./zero;
    }
    // quark EoS
    else{
      printf("EoS_Hybrid: no phase transition, quark matter\n");
      muB_crit_H= muB_crit_Q=zero;
    }
  }

  // reset check switch to actual value 
  // (in any case...)
  c_hybrid = check_hybrid(hybrid_hyb,
			  hybrid_maxwell,
			  hybrid_hadronic);
  printf("EoS_Hybrid: muB_H^{crit}: %e\n", muB_crit_H);
  printf("EoS_Hybrid: muB_Q^{crit}: %e\n", muB_crit_Q);
  printf("\n");
}

double pressure_diff(double mue){
  double c;
  c = tmp_hyb->hadron.p_(_MUB,mue,_T);
  c-= tmp_hyb->quark.p_ (_MUB,mue,_T);
  return c;
}

double EoS_Hybrid::mue_mix(double muB, 
			   double T )
{
//  tmp_hyb=this;
//  _T   = T;
//  _MUB = muB;
//  
//  double min=0.,max=3000.;
  //if(hadron.m_file)  {
  //  min=hadron.Hadron_file.D_p.y_vec[0];
  //  max=hadron.Hadron_file.D_p.y_vec[hadron.Hadron_file.D_p.rows-1];
  //}
  //if(quark.m_file){
  //  min=quark.Quark_file.D_p.y_vec[0];
  //  max=quark.Quark_file.D_p.y_vec[quark.Quark_file.D_p.rows-1];
  //}
  //if(hadron.m_file && quark.m_file){
  //  if(hadron.Hadron_file.D_p.y_vec[0]<quark.Quark_file.D_p.y_vec[0]) 
  //    min=quark.Quark_file.D_p.y_vec[0];
  //  else
  //    min=hadron.Hadron_file.D_p.y_vec[0];
  //
  //  int ih=hadron.Hadron_file.D_p.rows-1;
  //  int iq= quark.Quark_file.D_p.rows-1;
  //  if(hadron.Hadron_file.D_p.y_vec[ih]<quark.Quark_file.D_p.y_vec[iq]) 
  //    max=hadron.Hadron_file.D_p.y_vec[ih];
  //  else
  //    max=quark.Quark_file.D_p.y_vec[iq];
  //}

  return 0;//zbrent(pressure_diff,
		//min,
		//max,
       //zero);
}

double EoS_Hybrid::fraction(double muB, double T){
  // if another phase transition is required,
  // call InitPhaseTrans(...).
  InitPhaseTrans(hadron.Hmodel,        // actual status
		 quark.m_SM,       // .
		 quark.m_cond,     // .
		 quark.m_FF,       // of EoS.
		 m_hybrid,          // given parameters
		 m_maxwell,     // .
		 m_hadron,          // of desired phase transition
		 T);               // Temperature...
  if(muB< muB_crit_H) return 0;
  if(muB> muB_crit_Q) return 1;

  //This part should be of interest only with
  //glendenning construction (for mixed phase)
  mu_e = mue_mix(muB,T);
  double charge_h= hadron.charge_(muB,mu_e,T);
  double charge_q= quark.charge_(muB,mu_e,T);
  //  printf("charges: %e %e\n", charge_h, charge_q);
  return charge_q/(charge_h-charge_q);
}

//Warning: Allways use InitPhaseTrans before using the following stuff
double EoS_Hybrid::mue_(double muB, double T){

  if(muB<=muB_crit_H) return hadron.mue_( muB, T);
  if(muB>=muB_crit_Q) return quark.B2e(muB,T);
  double chi=fraction(muB,T); //only for safety, could be done better...
  double mue= mue_mix(muB,T);
  return mue;
}

/*
double EoS_Hybrid::p_(double muB, double T){
  double p;
  //cerr<<"EoS_Hybrid::p_ : ";
  if(muB<=muB_crit_H) {
    p = hadron.p_(muB, T);
    //cerr<<"hadron : ";
    //cerr<<p<<endl;
    return p;
  }
  if(muB>=muB_crit_Q) {
    p = quark.p_ (muB,quark.B2e(muB,T), T);
    //cerr<<"quark : ";
    //cerr<<p<<endl;
    return p;
  }

  double chi=fraction(muB,T);
  double mue= mue_mix(muB,T);

  p = (1.-chi)*hadron.p_(muB,mue,T);
  p+=     chi * quark.p_(muB,mue,T);

  //cerr<<"mix : ";
  //cerr<<p<<endl;
  return p;
}
*/
int EoS_Hybrid::kind_(double mu, double T)
{ 
  mu_b =mu;
  kT = T;
  Xi = fraction(mu,T);
  if (Xi >= 1 - Zero) sw = 2;
  else 
    if (Xi <= Zero) sw = 0; 
    else sw = 1;
  return sw;
}

double EoS_Hybrid::pr_(double mu, double T)
{ 
//  if (fabs(mu-mu_b)+fabs(T-kT) > Zero) sw = kind_(mu,T);

   mu_e = mue_(mu,T);
   switch(sw) 
     {case 0:pr =hadron.p_(mu,mu_e,T);break; 
     default:pr = quark.p_(mu,mu_e,T);
     };
   return pr;
  //return p_(mu, T);
}

double EoS_Hybrid::n_(double mu, double T, double *nn_)
{
  //cerr<<"EoS_Hybrid::n_ ";
  int i;
  //  double *nh,*nq;
  double nh[15], nq[6];

//  if (fabs(mu-mu_b)+fabs(T-kT) > Zero) pr = pr_(mu,T);

  //  mu_e = this->mu_e;
  //  mu_e = mue_(mu,T);

  //  nh=(double *)malloc(15*sizeof(double));
  //  nq=(double *)malloc(6*sizeof(double));
  for(i=0;i<15;i++) nh[i]=0.;
  for(i=0;i< 6;i++) nq[i]=0.;
//hadron = this;
   switch(sw)
     {
     case 0: nb=hadron.n_(mu,mu_e,T,nh);break;
     case 1: {
       nb= (1 - Xi)*hadron.n_(mu,mu_e,T,nh) +
	 Xi*quark.n_(mu,mu_e,T,nq);
     }break;		
     case 2: 
       nb= quark.n_(mu,mu_e,T,nq);
       break;
     };

  for( i=0;i<13;i++) nn_[i]=( 1 - Xi )* nh[i]; //hadronic desities including hyperons 
  for( i=13;i<15;i++) nn_[i]=( 1 - Xi )* nh[i] + Xi * nq[i - 9]; //electrons and muons
  for( i=15;i<19;i++) nn_[i]= Xi * nq[i - 15]; // quarks up, down and strange

  //  free(nh);
  //  free(nq);
  //cerr<<nb<<endl;
  return nb;
}


double EoS_Hybrid::s_(double mu, double T)
{ 
  //cerr<<"EoS_Hybrid::s ";
//  if (fabs(mu-mu_b)+fabs(T-kT) > Zero) nb = n_(mu,T,nn);
  mu_e = this->mu_e;
  switch(sw)
    {
    case 0: s=hadron.s_(mu,mu_e,T);break;
    case 1: {
      s = (1 - Xi)*hadron.s_(mu,mu_e,T) +
	Xi*quark.s_(mu,mu_e,T);
    }break;		
    case 2: s = quark.s_(mu,mu_e,T);
    };
  //cerr<<s<<endl;
  return s;
}

double EoS_Hybrid::en_(double mu, double T)
{   
  //cerr<<"EoS_Hybrid::en_ ";
//  if (fabs(mu-mu_b)+fabs(T-kT)> Zero) 	  s = s_(mu,T);

  mu_e = this->mu_e;
/*	
  switch(sw)
    {
    case 0: eps = mu*nb + T*s - pr;break;
//    case 0: eps=hadron.en_(mu,mu_e,T);break;
    case 1: {
      s = (1 - Xi)*hadron.en_(mu,mu_e,T) +
	Xi*quark.s_(mu,mu_e,T);
    }break;		
    case 2: eps = mu*nb + T*s - pr;
    };
  //cerr<<s<<endl;
  return eps;
	
*/	
	
	//  pr = p_(mu, T);
  eps = mu*nb + T*s - pr; 
  //cerr<<eps<<endl;
  return eps;
}



