//#include "stdafx.h"
// RW_cool.cpp: implementation of the RW_cool class.
//
//////////////////////////////////////////////////////////////////////

#include "RW_cool.h"

#include "EoS_Bag.h"
#include "Zbrent.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#define l_0  1.555e+43 

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define hc3 (197.32705*197.32705*197.32705)
#define M_PI        3.14159265358979323846 


//char final_file[100], initial_file [100], evolution_file [100];
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

RW_cool::RW_cool()
{
ni=0;

DNum = Num + 1;
PTPk=0;
//NUMT;

on_fast_cool = false;
normal_shell = false;
//normal_core = 0;
//cont_cal_f = false;
star_f = false;
strcpy(HOME_EV, ".\\Data\\EVfiles");
strcpy( HOME,".\\Data\\Configs");

}

RW_cool::~RW_cool()
{
//free(ptp);
}


/****** The set of Star internel Structure functions ***** /
 char Config_file[100];
char  _CONFIGNAME_[100];

 double    ro[Num+1],   // energy density          in       MeV            fm^-3
          rr[Num+1],   // distence from cenrte    in km      
        mass[Num+1],   // accumulated mass        in M_sun
        c_v_[Num+1],   // specific heat  
        eps_[Num+1],   // emmisivity  
       supp_[Num+1],   // suppretion coefitient 
          k_[Num+1],   // condactivity            in       MeV K^-1 s^-1  fm^-1 
         phi[Num+1],   // gravitatioal potential
        T_in[Num+1];   // initial temperature profile  in K

 double    Xi[Num+1],  //Quark-phase fruction
         ms_n[Num+1],  // effective mass of neurton/neutron mass
         ms_p[Num+1],  // effective mass of neurton/neutron mass
          Y_e[Num+1],  // electron fruction
          n_p[Num+1],  //proton fruction
          n_n[Num+1],  //neutron fruction
          n_b[Num+1],  //baryon number desity
         mu_b[Num+1],  //baryochemical potential
            p[Num+1];  //presuure distribution     in       MeV    fm^-3

double    T_hat[Num+1],  //temperature profile  in T_0
          T_hat_old[Num+1],  // temperature profile (t - dt) in T_0
       lum_hat[Num+1];  // luminosity profile  in l_0
*******************************************************************/
FILE *out;

int RW_cool::read_initial_str()
{ 
FILE *fiin;
int fe,j;
//int i;
double content[30];
strcpy(Config_file,_CONFIGNAME_); 
strcat(Config_file, Config_sufx); 
strcpy(final_file,_CONFIGNAME_); 
strcpy(initial_file,_CONFIGNAME_); 
strcpy(lgnlgs_file,_FIGNAME_); 
strcat(lgnlgs_file,&_GAP_);
strcat(lgnlgs_file,"_");
strcat(lgnlgs_file,&_CRUST_);
if (m_qcore) strcat(lgnlgs_file,&_XGAP_); 
strcpy(evolution_file,_CONFIGNAMEEV_); 
strcat(evolution_file,evolution_sufx);
strcpy(evol_L_file,_CONFIGNAMEEV_); 
strcat(evol_L_file,"_L"); 
strcat(evol_L_file,evolution_sufx); 
strcat(final_file, final_sufx);
strcat(initial_file, initial_sufx); 

 if(!(fiin=fopen(Config_file, "rt")))
{
return 1;
 };
 char k;
while (((k=fgetc(fiin))!=EOF) && (k!='\n'));

for (j=0;j<=Num;j++){ 
#ifndef _HYP_

	fe=fscanf(fiin,"%lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg",
 &content[0],&content[1],&content[2],&content[3],&content[4],&content[5],
 &content[6],&content[7],&content[8],&content[9],&content[10],&content[11],
 &content[12],&content[13],&content[14],&content[15]); 
#else
fe = fscanf(fiin, "%lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg",
	&content[0], &content[1], &content[2], &content[3], &content[4], &content[5],
	&content[6], &content[7], &content[8], &content[9], &content[10],
	&content[11], &content[12], &content[13], &content[14], &content[15], &content[16],
	&content[17], &content[18], &content[19], &content[20],
	&content[21], &content[22], &content[23], &content[24], &content[25], &content[26], &content[27]);

#endif

           ro[j] = content[4]; //enrgy density
           rr[j] = content[0]; //radius in km
         mass[j] = content[2]; //mass distribution in M_solar
          n_n[j] = content[6];  // nuetron fruction
          n_p[j] = content[7];  // proton  fruction
          phi[j] = content[5]; //gravitational potential 
         ms_n[j] = content[8];  //neutron effective mass 
         ms_p[j] = content[9];  // proton effective mass 
           Xi[j] = content[11];  //Xi function 1 for quarks and 0 for hadronic phase
          n_b[j] = content[1];   //baryon number density  fm�-3   
         mu_b[j] = content[15];   //baryon chemical potential in MeV
            p[j] = content[3];   //pressure
          Y_e[j] = content[10];  //electron fruction
#ifdef _HYP_
		  Y_L[j] = content[16];  //  fruction
			  Y_Sm[j] = content[17];  //  fruction
			  Y_S0[j] = content[18];  //  fruction
			  Y_Sp[j] = content[19];  //  fruction
			  Y_X0[j] = content[20];  //  fruction
			  Y_Xm[j] = content[21];  //  fruction

		  ms_L[j] = content[22];
		   ms_Sm[j] = content[23];
		   ms_S0[j] = content[24];
		   ms_Sp[j] = content[25];
		   ms_Xm[j] = content[26];
		   ms_X0[j] = content[27];

#endif

// if(!cont_cal_f){ //printf(" Hello Guys ----> %d",cont); 

in_temp_profile(j);

T_hat_old[j] =
T_hat[j] =
//1.0
T_in[j]*exp(phi[j])
/kT_0  ;   //printf("T_hat=%g\n",T_hat[j]);

Y_Le[j] = NN_nu(eta[j],T_in[j])/(n_b[j] + ZERO) + Y_e[j];

//double check=	eta[j] - Eta((Y_L[j] - Y_e[j])*n_b[j],T_hat[j]*exp(-phi[j])*kT_0);
//if (fabs(check)>ZERO) printf("Eta does not correct\n");

T_in[j] = T_hat_old[j];

//};
 //printf("i=%d m=%g z=%g ro=%g\n", j,mass[j],ZZ[j],ro[j]); 
};

 _time0=_time=0;
 fclose(fiin);
/************************************************************** /
 if(cont_cal_f!=0)
{ if (cont_cal_f==1)fiin=fopen(final_file , "rt");
  if (cont_cal_f==2)fiin=fopen(initial_file , "rt");

for (j=0;j<=Num;j++){ 

fe=fscanf(fiin,"%lg %lg %lg %lg ",
&content[4], &content[0],&content[1],&content[2]); 

_time0=_time = content[4]; 
            rr[j] = content[0]; 
          T_in[j] = content[1]; 
   T_hat_old[j] = T_hat[j] = T_in[j] *exp(phi[j])/kT_0 ;   //printf("T_hat=%g\n",T_hat[j]);
       lum_hat[j] = content[2];  
 };

fclose(fiin);

};
***********************************************************/
char ptp_n[20];
gcvt(mass[Num],3,ptp_n);
strcat(lgnlgs_file,ptp_n); 
strcat(lgnlgs_file,".dat"); 
 return 0;

}
/*******************************************************************/


int RW_cool::begin_input()
{
//int i;


if(read_initial_str()) return 1;


//out=fopen( initial_file,"w");
//printf(" Energy_flux(ZERO) \n");

Energy_flux(0.0);

//printf(" That's ok passed Energy_flux( ZERO ) \n");

//exit(1);

//for(i=0;i<= Num;i++)fprintf(out,"%g %g %g %g\n",
//                  _time,rr[i],T_hat[i]*exp(-phi[i])*kT_0,lum_hat[i]);

//fclose(out);

return 0;
} 

/*******************************************************************/
/*******************************************************************/
void RW_cool::write_results(){

int i;
char ptp_n[5]="_";
//double L_nu;
double dm; 
int k=0;
if(_time>ptp[PTPk]&&PTPk<NPTP)
{
strcpy(final_file,_CONFIGNAME_); 
strcat(final_file,"_fi");
if(PTPk+1>10)gcvt(PTPk,4,ptp_n);  else	gcvt(PTPk,3,ptp_n);
strcat(final_file,ptp_n); 
strcat(final_file,".dat");
	   out=fopen( final_file,"w");
//L_nu=dm=0;
fprintf(out,"# r [km] ,T(r) [MeV],  for t =%g yr\n",_time);

for(i=0;i<=Num;i++){
	if(i>0)dm=-mass[i-1] + mass[i];
double TTT = T_hat[i]*exp( - phi[i])*kT_0;
fprintf(out,"%g %g\n",rr[i],TTT);
};
PTPk++;
fclose(out);
};
}

/****************Initial Temperature profile ******************************/
void RW_cool::in_temp_profile(int j)
{
//T_in[j] = 0.05*(ro[j]+ 50.0* Xi[j]); //initial temperature profile in 10^9 K
 //T_in[j] = 0.001*ro[j]/n_b[j]; 
  T_in[j] = init_T*exp(phi[0] - phi[j]); // MeV
  eta[j] = init_eta;
}

/****************Photon & crust model ******************************/


//double Surface_t_G(double T_h){
/**************************************************** /
  double RW_cool::Surface_t_T(double T_h){

  double lgT_m6,lgT_s6;
lgT_m6 = log10(T_h *exp(- phi[Num])) +  3;
lgT_s6 = log10(0.93/8) 
          + 0.25*log10(mass[Num]) 
//         - 0.5*log10(rr[Num]/10.0) 
         - log10(1 - 2*r_0*mass[Num] / rr[Num]) + 
         + 0.55 * lgT_m6 ;
//printf(" T_h=%g T_m=%g T_s=%g\n",T_h,1e6 *exp(lgT_m6*log(10.0)),1e6 *exp(lgT_s6*log(10.)));
return lgT_s6 - 3;
  }

*******************************************************/
double aa[] ={89.401, 2.87,0.534,2.203,1.349,8.328,0.01609,0.1378}; //model HZ90


double RW_cool::S_t_T_NULL(double Tin9)
{
	double q0,q1,q2; //Ts_6 = q0 (q2 * Tm_8)^q1
	q1 = 1;
	q0 = 1;
	q2 = 100;
	double 	lgT_mu = 9 + log10(Tin9) ;

double	lgT_s= log10(q0) + q1*(lgT_mu + log10(q2) - 8) + 6;
// Tsuruta law


return lgT_s;
}

double RW_cool::S_t_T_Turuta(double Tin9)
{
	double q0,q1,q2; //Ts_6 = q0 (q2 * Tm_8)^q1
	q1 = 2.0/3.0;
	q0 = 1.0e-6;
	q2 = 1.0e9;
	double 	lgT_mu = 9 + log10(Tin9) ;

double	lgT_s= log10(q0) + q1*(lgT_mu + log10(q2) - 8) + 6;
// Tsuruta law


return lgT_s;
}
double RW_cool::S_t_T_Y(double Tin9)
{

double lgT_mu, eta,gm,pppp,Tnon6,g14q,C14,Tfull6,lgTeff;
lgT_mu = 9+log10(Tin9);

g14q = aa[0]* r_0*mass[Num] /(rr[Num]*rr[Num]*sqrt(1- 2*r_0*mass[Num]/rr[Num]));
g14q = powf(g14q,0.25);


//eta = 1.0e-11;
eta = 4.0e-16;
//eta = 4.0e-8;

pppp = - aa[6]*powf(eta,-aa[7]);
gm = powf(1 + 120 *Tin9,pppp);
Tnon6 = g14q*aa[1]*powf(Tin9,aa[2]);
C14 =  powf((1+aa[3]*powf(Tin9,aa[4]))/(1+aa[5]*powf(Tin9,1.6)),0.25);
Tfull6 = 6.9*g14q*C14*powf(Tin9,0.62);
lgTeff = 0.25*log10(gm*powf(Tfull6,4)+(1-gm)*powf(Tnon6,4)) + 6; 


return lgTeff 
//- log10(sqrt(1- 2*r_0*mass[Num]/rr[Num]))
;
}

/**************************************/
double RW_cool::S_t_T_G(double Tin9)
{

double lgT_mu, eta,gm,pppp,Tnon6,g14q,C14,Tfull6,lgTeff;

lgT_mu = 9+log10(Tin9) ;

//if (lgT_m > 11 ) return lgT_m;
//printf("lg_Tm = %g Tm9 = %g\n",lgT_m,Tin9);

g14q = aa[0]* r_0*mass[Num] /(rr[Num]*rr[Num]*sqrt(1- 2*r_0*mass[Num]/rr[Num]));
g14q = pow(g14q,0.25);


//eta = 4.0e-16;

/**/
if(lgT_mu > 8.6) eta = - 8;
else{
	if(lgT_mu> 8)
eta = (  16 - 8 )/(8.6 - 8)*(lgT_mu - 8) - 16;
	else eta = -16;
}; 
eta = 4.0*pow(10.0,eta);


/*******************************************/


pppp = - aa[6]*pow(eta,-aa[7]);
gm = pow(1 + 120 *Tin9,pppp);
Tnon6 = g14q*aa[1]*pow(Tin9,aa[2]);
C14 =  pow((1+aa[3]*pow(Tin9,aa[4]))/(1+aa[5]*pow(Tin9,1.6)),0.25);
Tfull6 = 6.9*g14q*C14*pow(Tin9,0.62);
lgTeff = 0.25*log10(gm*pow(Tfull6,4)+(1-gm)*pow(Tnon6,4)) + 6; 


return lgTeff
// log10(sqrt(1- 2*r_0*mass[Num]/rr[Num]))
;

}






/**************************************/


/************************************** /

double RW_cool::S_t_T_G(double Tin9)
{

double lgT_mu, eta,gm,pppp,Tnon6,g14q,C14,Tfull6,lgTeff;
double DT;
double T_u;
double T_d;

lgT_mu = 9+log10(Tin9) ;

//if (lgT_m > 11 ) return lgT_m;
//printf("lg_Tm = %g Tm9 = %g\n",lgT_m,Tin9);

g14q = aa[0]* r_0*mass[Num] /(rr[Num]*rr[Num]*sqrt(1- 2*r_0*mass[Num]/rr[Num]));
g14q = powf(g14q,0.25);


//eta = 4.0e-16;

/*   Original values   (printed in papers)    * /
DT = 0.6;
T_d = 8;
/*               */

/*  Experimental values  * /
DT = 0.1;
T_d = 8.55;
/** /
T_u = T_d + DT;
if(lgT_mu > T_u) eta = - 8;
else{
	if(lgT_mu> T_d)
eta = (  16 - 8 )/DT*(lgT_mu - T_d) - 16;
	else eta = -16;
}; 
eta = 4.0*powf(10.0,eta);


/******************************************* /


pppp = - aa[6]*powf(eta,-aa[7]);
gm = powf(1 + 120 *Tin9,pppp);
Tnon6 = g14q*aa[1]*powf(Tin9,aa[2]);
C14 =  powf((1+aa[3]*powf(Tin9,aa[4]))/(1+aa[5]*powf(Tin9,1.6)),0.25);
Tfull6 = 6.9*g14q*C14*powf(Tin9,0.62);
lgTeff = 0.25*log10(gm*powf(Tfull6,4)+(1-gm)*powf(Tnon6,4)) + 6; 


return lgTeff
// log10(sqrt(1- 2*r_0*mass[Num]/rr[Num]))
;

}
/**************************************/
double RW_cool::Surface_t_T(double T_h)
{
double Tin9;
double lgT_s;

//printf("T_h = %g phi = %g\n",T_h,phi[Num]);


Tin9 = T_h *exp(- phi[Num])
;

switch(_CRUST_ )
{
case('N'):lgT_s = S_t_T_NULL(Tin9); break;

case('T'):lgT_s = S_t_T_Turuta(Tin9); break;

case('Y'):lgT_s = S_t_T_Y(Tin9); break;

case('G'):lgT_s = S_t_T_G(Tin9);
}
/*********************************************/
/*
lgT_s = //2.0/3.0*(lgT_m + 1.0)
6.0 + 

0.55
*(lgT_m - 6)  - 

1.2
          + 0.25*log10(mass[Num]) 
         - log10(1 - 2*r_0*mass[Num] / rr[Num]) 
          - 0.5*log10(rr[Num]/10.0) 
		  ;
*/
/******************************************** /
if (lgT_m < 8.6){ 

lgT_s = 0.8*lgT_m   - 0.44 ;

} else
lgT_s = 2.0/3.0*(lgT_m + 1.1);

***********************************************************/
//
//lgT_s = 0.3*(lgT_m - 4.18) + 4.;
//else 
//lgT_s= lgT_m ; 
//printf(" T_h=%g T_m=%g T_s=%g\n",T_h, exp(lgT_m*log(10.0)),
//1e9 *exp(lgT_s*log(10.0)));
//printf(" T_h=%g lgT_m=%g lgT_s=%g\n",T_h, lgT_m,lgT_s);

//lgT_s += log10(sqrt(1 - 2 * r_0*mass[Num] / rr[Num]));

return lgT_s - 9 ; // local temperature T_effective 

}

/* Photo luminocity ************************  the old vertion ************************ /
double RW_cool::PhotonL(double T_h){
double T_s,lgT_s,ll;
//double T_s4;
//return 0.0;


lgT_s = Surface_t_T(T_h); //printf("lgT_s - 9 = %g\n",lgT_s);
T_s = exp(lgT_s*log(10.0)); //printf(" T_s = %g\n",T_s); 
//T_s4 = exp(4*lgT_s*log(10.0)); 
ll = 2* log(rr[Num]/r_0) + 4*lgT_s*log(10.0) + 2 * phi[Num];

 return exp(ll);
// return rr[Num]*rr[Num]*T_s4/r_0/r_0 * exp(2 * phi[Num]);


}
/*******************************************************************/

/* Photo luminocity ************************************************/
double RW_cool::PhotonL(double T_h){
double T_s,lgT_s,ll;
double T_s4;

//return 0.0;
double R_c = + 0;

lgT_s = Surface_t_T(T_h); //printf("lgT_s - 9 = %g\n",lgT_s); // local T_eff in 10^9 K
T_s = exp(lgT_s*log(10.0)); //printf(" T_s = %g\n",T_s); 
//T_s4 = exp(4*lgT_s*log(10.0)); 
ll = 2* log((rr[Num] + R_c)/r_0) + 4*lgT_s*log(10.0) 
 + 2 * phi[Num]
//- log(1 - 2*r_0*mass[Num]/(rr[Num]+ R_c))
//- 2 * phi[Num]
;

return exp(ll);  //L_inf 
// return rr[Num]*rr[Num]*T_s4/r_0/r_0 * exp(2 * phi[Num]);
}

/******************************************************************* /

double RW_cool::NeutrinoL(double eta, double T_h){
double T_s,lgT_mu,ll;
//double T_s4;

//return 0.0;

lgT_mu = log(T_h - phi[Num])/log(10.0); //printf("lgT_s - 9 = %g\n",lgT_s);
//T_s = exp(lgT_s*log(10.0)); //printf(" T_s = %g\n",T_s); 
//T_s4 = exp(4*lgT_s*log(10.0)); 
ll = 2* log(rr[Num]/r_0) + 4*lgT_mu*log(10.0) 
+ 2 * phi[Num]; //Some thing should be wrong:-)
 return - exp(ll);
// return rr[Num]*rr[Num]*T_s4/r_0/r_0 * exp(2 * phi[Num]);

}

/******************************************************************* /
double RW_cool::NeutrinoFlux(double eta, double T_h){
double T_s,lgT_s,ll,nn;
//double T_s4;

//return 0.0;

//lgT_s = Surface_t_T(T_h); //printf("lgT_s - 9 = %g\n",lgT_s);
//T_s = exp(lgT_s*log(10.0)); //printf(" T_s = %g\n",T_s); 
//T_s4 = exp(4*lgT_s*log(10.0)); 
nn = NN_nu(eta,T_h);
//ll = 2* log(rr[Num]/r_0) + log(nn)*log(10.0) +  phi[Num];

// return exp(ll);
 return  0.001* rr[Num]*rr[Num]*nn/r_0/r_0 * exp(2 * phi[Num]);

}
/*******************************************************************/


bool RW_cool::EV_run(int *nii, double *lg_t,double* lg_T)
{


/********************* FOR PROFILE'S CALCULATIONS ******
printf(" end time= ");
scanf("%lg", &end_run_time);

**********************************************************/
*nii = ni;
if (ni <= NUMT)
{

del_t = t_in*exp(ni*tau)*(exp(tau) - 1)*t_0;

if(Evolution(del_t,pr_f,lg_t,lg_T)) return 1;


_time =exp(log(10.)**lg_t);
printf("del_t = %g sec, Time = %g yr; lg Ts(K)= %g  step = %d \n",del_t, _time, *lg_T, ni);
if(cptp) write_results();
cont_cal_f = true;
_time0=_time;
//_time*= exp(tau);
 ni++;
_time =  t_in*exp(ni*tau);
//if(_time>end_run_time) goto end;

};
//end:
// printf("\n Program is terminated successfully\n time =%g\n",_time);
return 0;
}


void RW_cool::critmix()
{
   for(o=1;o<=Num;o++)
   {
if((Xi[o] <1)&&(Xi[o-1]==1)) n_q_c  = o-1 ; 
if((Xi[o] >0)&&(Xi[o+1]==0)) n_h_c  = o+1 ;
   };

}



double RW_cool::QGap(double mu)
{
double gap;

Qeos.m_cond = m_cond;
Qeos.m_SM = m_SM;
Qeos.m_FF = m_FF;
//Qeos.m_WM = m_WM;
Qeos.m_nlw = true;
Qeos.m_new =false;
Qeos.m_HM = m_WM;
Qeos.initBfuncion();

gap = Qeos.Del_dq(mu);

	return gap;
}

double RW_cool::QGap_X(double mu)
{
double gap;
	//EoS_Bag Qeos;

Qeos.m_cond = m_cond;
Qeos.m_SM = m_cond;
Qeos.m_FF = m_FF;
Qeos.m_HM = m_WM;
Qeos.m_nlw = true;
Qeos.initBfuncion();

gap = Qeos.X_gap(_XGAP_,mu/3.0);

	return gap;
}


double RW_cool::NN_nu(double eta, double T)
{ double nnn,pf;

pf = eta * T; // T in MeV
nnn  =  1/(3*hc3)*(pf*pf*pf/M_PI*M_PI + pf*T*T) ;

return nnn;
}


double RW_cool::DYDE_nu(double eta, double T)
{ double nnn,pf;

pf = eta * T; // T in MeV
nnn  =  1/(3*hc3)*(3*pf*pf/M_PI*M_PI + T*T)*T ;

return nnn;
}


double TTT, NNN;

double NNN_nu(double eta)
{ double nnn,pf;

pf = eta * TTT; // T in MeV
nnn  = 1/(3*hc3)*(pf*pf*pf/M_PI*M_PI + pf*TTT*TTT);

return NNN - nnn;

}



double RW_cool::Eta(double n_nu, double T)
{
TTT=T;
NNN=n_nu;
return zbrent(NNN_nu,-30,30, 1e-15);
}



double RW_cool::Xi_mix_q(double mu)
{
double g;
	EoS_Bag Qeos;

Qeos.m_cond = m_cond;
Qeos.m_SM = m_cond;
Qeos.m_FF = m_FF;
//Qeos.m_WM = m_WM;
Qeos.m_nlw = true;
Qeos.initBfuncion();

g = Qeos.Xi_q(mu);

	return g;

}

// Integral Luminosities
void RW_cool::Luminos(void)
{ double vol;
#ifdef _HYP_
L_DU_np =
L_DU_Lp =
L_DU_Sn =
L_DU_SL =
L_DU_SS =
L_DU_XL =
L_DU_XS =
L_DU_XSo =
L_DU_XX =
#endif // _HYP_
	L_PPBF=
     L_pBS=    
        L_NPBFs=
		L_NPBFp=    
        L_MU_H_p=    
        L_MU_H_n=    
        L_ee=    
        L_DU_H=    
        L_PU  =ZERO;

for(int j=0;j<=Num-1;j++){
vol = 1
*(rr[j+1]-rr[j])*exp(2*log(rr[j]/r_0)+ 2*phi[j])/sqrt(1- 2*r_0*mass[j]/rr[j])*l_0/r_0
;
double n_ = n_b[j]/n_0;
//if(eps_PPBF[j]>ZERO)   
L_PPBF+= 
eps_PPBF[j]*
vol;
//if(eps_pBS[j]>ZERO)   
L_pBS += (eps_nnBS[j]+ eps_npBS[j] + eps_ppBS[j] )*vol;
if(n_>0.5)   
L_NPBFs+=eps_NPBFs[j]*vol;
//if(eps_NPBFp[j]>ZERO)    
L_NPBFp+=eps_NPBFp[j]*vol;    
//if(eps_MU_H_p[j]>ZERO)   
L_MU_H_p+=eps_MU_H_p[j]*vol;    
//if(eps_MU_H_n[j]>ZERO)   
L_MU_H_n+=eps_MU_H_n[j]*vol;    
//if(eps_ee[j]>ZERO)   
L_ee+=eps_ee[j]*vol;    
//if(eps_DU_H[j]>ZERO)  
L_DU_H+=eps_DU_H[j]*vol;    
//if(eps_PU[j]>ZERO)  
L_PU+= eps_PU[j]*vol;
#ifdef _HYP_
L_DU_np += eps_DU_np[j] * vol;
L_DU_Sn += eps_DU_Sn[j] * vol;
L_DU_SL += eps_DU_SL[j] * vol;
L_DU_SS += eps_DU_SS[j] * vol;
L_DU_XL += eps_DU_XL[j] * vol;
L_DU_XS += eps_DU_XS[j] * vol;
L_DU_XSo += eps_DU_XSo[j] * vol;
L_DU_XX += eps_DU_XX[j] * vol;
L_DU_Lp += eps_DU_Lp[j] * vol;

#endif // _HYP_

}
}
