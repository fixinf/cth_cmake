#if !defined(AFX_EOS_LW_H__C3668D0E_AE42_460B_9A0A_9224655336C2__INCLUDED_)
#define AFX_EOS_LW_H__C3668D0E_AE42_460B_9A0A_9224655336C2__INCLUDED_ 
 
#if _MSC_VER > 1000 
#pragma once 
#endif // _MSC_VER > 1000 
// EoS_LW.h : header file 
// 
#include "EoS_HB.h" 
///////////////////////////////////////////////////////////////////////////// 
// EoS_LW        Linear Walecka model 
 
class EoS_LW : public EoS_HB 
{ 
 
      // protected constructor used by dynamic creation 
 
// Attributes 
public: 
	EoS_LW();      
	virtual ~EoS_LW(); 
// Operations 
public: 
	double mju2n(double mju,double tu); 
	double dsdtwal(double ds,double *dn,double *dns); 
	double dndtwal(double *dn,double *dns); 
	double dndmjuwal(double *dn,double *dns); 
	void diff_n_ns_e(double mjus,double ms,double T,double *dn,double *dns,double *ds); 
	double s_(double mjus,double ms,double T); 
	double en_(double mjus,double ms,double T,double n,double ns); 
	double p_(double mjus,double ms,double T,double n,double ns); 
	double ns_(double mjus,double ms,double T); 
	double n_(double mjus,double ms,double T); 
 
 
// Implementation 
protected: 
	 
 
 
	// Generated message map functions 
protected: 
	//{{AFX_MSG(EoS_LW) 
		// NOTE - the ClassWizard will add and remove member functions here. 
	//}}AFX_MSG 
 
}; 
 
///////////////////////////////////////////////////////////////////////////// 
 
//{{AFX_INSERT_LOCATION}} 
// Microsoft Visual C++ will insert additional declarations immediately before the previous line. 
 
#endif // !defined(AFX_EOS_LW_H__C3668D0E_AE42_460B_9A0A_9224655336C2__INCLUDED_)             
