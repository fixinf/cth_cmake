//#include "stdafx.h"
// EoS_Hadron.h: s. Kommentar in EoS_Hadron.cpp
#include "EoS_Hadron.h"  //Definition der Klasse EoS_Hadron

#define H_W 0 //Hadron-Warnings(0=aus,1=an)
#define Zero 1e-10
#define M_NEUTRON 939.56563
#define M_PROTON 938.272

//TEMPORARY:
//const char* hadron_file="./tst_av18/tst";
//const char* hadron_file="./EoS_vanDalen/vanDalen";
const char* hadron_file="./tst_hadron/tst";
//const char* hadron_file="./tst_in/tst";


EoS_Hadron::EoS_Hadron(void)
{
//  if(H_W) printf("Warning: Hadronic EoS not defined! Choosing default EoS (NPE).\n");
//  EoS_Hadron('0');

//  if(m_h_file){
//    cerr <<"Reading hadronic datasets from "<<hadron_file<<"*.dat ..."<<endl;
//    Hadron_file.Init(hadron_file);
//    cerr <<"done..."<<endl;
//  }
}


EoS_Hadron::EoS_Hadron(char kind)
{
  // lege bei Initialisierung von EoS_Hadron fest,
  // welche Zustandsgleichung benutzt wird
  // Bsp: EoS_Hadron hadron(0);
  // Werte von sw entsprechen:
  // 0: NPE
  // 1: nichtlineares Walecka
  // 2: data-set
  // 3: HBerg
	//4: Potechin - fit EoS incuding crust
  //  if((Hmodel!='0')&&(Hmodel!='1')&&(Hmodel!='2')&&(Hmodel!='3')) EoS_Hadron();
}

//Druck im neutralen \beta-GGW
double EoS_Hadron::p_(double muB, double T)
{  double muE;

// if(m_file) return Hadron_file.p_(muB);

 switch(Hmodel){
 case '0':{//NPE
   muE = Hadron_npe.B2ewal(muB, T, mju_mtr, m_mtr);
   return Hadron_npe.p_(muE, T, mju_mtr, m_mtr);
   break;}
 case '1':{//NLW
   muE = Hadron_nlw.musigomro(muB,T,&gsig,&gom,&gro); 
   return Hadron_nlw.p_(muB, muE, T, gsig, gom, gro);
   break;}
 case '2':{//Data-sets
   return -1234567890;//Hadron_file.p_(muB);
   break;}
 case '3':{//HBERG
   // T = 0!
   return Hadron_hb.p_bh(muB);
   break;
		  }
 case '4':{
	 return EC.p(muB);
		 break;
		  }
 }
 return 0.;
}

// Druck im \beta-GGW
double EoS_Hadron::p_(double muB, double muE, double T)
{
  mu_b = muB;
  kT = T;

//  if(m_file)    return Hadron_file.p_(muB,muE);

  switch(Hmodel){
  case '0':{//NPE
    Hadron_npe.marticer(muB,muE,T,mju_mtr,m_mtr);
    return Hadron_npe.p_(muE, T, mju_mtr, m_mtr);
    break;}
  case '1':{//NLW
    return Hadron_nlw.p_(muB, muE, T, gsig, gom, gro);
    break;}
  case '2':{//Data-sets
    return -1234567890;//Hadron_file.p_(muB, muE);
    break;}
  case '3':{//HBERG
    // T = 0!
//    if(muB>= 950) 
    return Hadron_hb.P(muB,muE);
//    else  return Hadron_hb.p_bh(muB);
    break;}
case '4':{
	 return EC.p(muB);
		 break;
		  }
  }
  return 0.;
}

double EoS_Hadron::charge_(double muB, double muE, double T)
{
//  if(m_file) return Hadron_file.charge_(muB,muE);

	switch(Hmodel){
  case '0':{//NPE
    return Hadron_npe.Charge(muB, muE, T);
    break;}
  case '1':{//NLW
    return Hadron_nlw.Charge(muB, muE, T);
    break;}
  case '2':{//Data-sets
    return -1234567890;//Hadron_file.charge_(muB, muE);
    break;}
  case '3':{//HBERG
    // T = 0!
    return Hadron_hb.Charge(muB,muE); 
    break;}
case '4':{
	 return 0;
		 break;
		  }
  }
 return 0.;
}

double EoS_Hadron::mue_(double muB, double T){

//	if(m_file) return Hadron_file.mue_(muB);

	switch(Hmodel){
  case '0':{//NPE
    return Hadron_npe.B2ewal(muB, T, mju_mtr, m_mtr);}
    break;
  case '1':{//NLW
    return Hadron_nlw.musigomro(muB,T,&gsig,&gom,&gro);}
    break;
  case '2':{//Data-Sets
    return -1234567890;//Hadron_file.mue_(muB);
		   }
    break;
  case '3':{//HBERG
    // T = 0!
    return Hadron_hb.mue(muB);
    break;
  }
	case '4':{
	 return 0;
		 break;
		  }
}
  return 0.;
}

double EoS_Hadron::en_(double mu, double mue, double T)
{
//  if(m_file) return Hadron_file.eps_(mu,mue);

  if (fabs(mu-mu_b)+fabs(T-kT)> Zero) s = s_(mu,mue,T);

  switch(Hmodel){
  case '2': eps = -1234567890;//Hadron_file.eps_(mu,mue);
	  break;
  case '3': eps = mu*nb  - pr; break;
//  case '3': eps = Hadron_hb.Eps(mu,mue); break;
	  case '4':{
		  return EC.eps(mu);
		 break;
		  }
  default:  eps = mu*nb + T*s - pr; 
  };
  return eps;
}

double EoS_Hadron::s_(double mu, double mue, double T)
{ 
//  if(m_file) return Hadron_file.s_(mu,mue);

  switch(Hmodel){
  case '0' : 
    if (fabs(mu-mu_b)+fabs(T-kT)> Zero) nb = n_(mu,mue,T,nn);
    s=Hadron_npe.s_(mue,T,mju_mtr,m_mtr); 
    break;//printf(" kT=%g \n",kT);
  case '1' : 
    if (fabs(mu-mu_b)+fabs(T-kT)> Zero) nb = n_(mu,mue,T,nn);
    s=Hadron_nlw.s_(mu_b,mue,T,gsig, gom,gro); 
    break;//printf(" kT=%g \n",kT);
  case '2' : 
    s=-1234567890;//Hadron_file.s_(mu,mue); 
    break;
  default:  s = 0.0;
  };
  return s;
}

double EoS_Hadron::n_(double mu, double mue, double T, double *nn_){
  //if(m_file){
  //  nb=Hadron_file.nbar_(mu,mue);
  //  
  //  // is that following really necessary?
  //  for(int i=0;i<15;i++) nn_[i]=0.;
  //  nn_[0]=Hadron_file.n_neut_(mu,mue);
  //  nn_[1]=Hadron_file.n_prot_(mu,mue);
  //  nn_[13]=Hadron_file.n_elec_(mu,mue);
  //  nn_[14]=Hadron_file.n_muon_(mu,mue);
  //  m_mtr[0] = Hadron_file.m_neut_(mu,mue);
  //  m_mtr[1] = Hadron_file.m_prot_(mu,mue);
  //  for(int i=0;i<20;i++) nn[i] = nn_[i];
  //  //    for(int i=0;i<=15;i++) nn_[i]=0.;
  //  return nb;
  //}
//	char model;
//	model = Hmodel;
  switch(Hmodel){
  case '0' : {
    if (fabs(mu-mu_b)+fabs(T-kT)> Zero) pr = p_(mu,mue,T);
    nb=Hadron_npe.n_(mue, T,mju_mtr,m_mtr,nn);
    nn_[0]=nn[0];
    nn_[1]=nn[1];
    nn_[13]=nn[2];
    nn_[14]=nn[3];} 
    break;//printf(" kT=%g \n",kT);
  case '1' :  
    if (fabs(mu-mu_b)+fabs(T-kT)> Zero) pr = p_(mu,mue,T);
    nb=Hadron_nlw.n_(mu_b,mue,T,gsig,gom,gro, nn,mju_mtr,m_mtr); break;//printf(" kT=%g \n",kT);
  case '2' : {
    nb=-1234567890;//Hadron_file.nbar_(mu,mue);

    // is that following really necessary?
    nn_[0]=-1234567890;//Hadron_file.n_neut_(mu,mue);
    nn_[1]=-1234567890;//Hadron_file.n_prot_(mu,mue);
    nn_[13]=-1234567890;//Hadron_file.n_elec_(mu,mue);
    nn_[14]=-1234567890;//Hadron_file.n_muon_(mu,mue);
    m_mtr[0] =-1234567890;// Hadron_file.m_neut_(mu,mue);
    m_mtr[1] = -1234567890;//Hadron_file.m_prot_(mu,mue);
  } break;
  case '3':  { 
    if (fabs(mu-mu_b)+fabs(T-kT)> Zero) pr = p_(mu,mue,T);
//    		if(mu>950) 
    nb=Hadron_hb.n_(mu, mue, nn);
//    		else nb = Hadron_hb.n_B(mu);
    nn_[0]=nn[0];
    nn_[1]=nn[1];
    nn_[13]=nn[2];
    nn_[14]=nn[3];
    m_mtr[0] = M_NEUTRON;
    m_mtr[1] = M_PROTON;
 } break;
			 case '4':{
 //   if (fabs(mu-mu_b)+fabs(T-kT)> Zero) pr = p_(mu,mue,T);
				 mue = 0;
		//		 nb=Hadron_hb.n_(mu, mue, nn_);
				 nb = EC.n_(mu,mue,nn);
nn_[0]=nn[0];
    nn_[1]=nn[1];
    nn_[13]=nn[13];
    nn_[14]=nn[14];
  m_mtr[0] = M_NEUTRON;
    m_mtr[1] = M_PROTON;
  		} break;

  };
  for(int i=0;i<20;i++) nn[i] = 0.0;
  return nb;
}


//int EoS_Hadron::In_file()
//{ int c=0;
//  if(m_file){
//    cerr <<"Reading hadronic datasets from "<<hadron_file<<"*.dat ..."<<endl;
//    c = Hadron_file.Init(hadron_file);
//    cerr <<"done..."<<endl;
//  }
//return c;
//}
