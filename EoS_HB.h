#if !defined(AFX_EOS_HB_H__C21202E5_F833_4031_9196_9B6D98596DFD__INCLUDED_)
#define AFX_EOS_HB_H__C21202E5_F833_4031_9196_9B6D98596DFD__INCLUDED_ 
 
#if _MSC_VER > 1000 
#pragma once 
#endif // _MSC_VER > 1000 
// EoS_HB.h : header file 
// 
 
///////////////////////////////////////////////////////////////////////////// 
// EoS_HB window 
 
class EoS_HB  
{ 
// Construction 
public: 
	EoS_HB(); 
 
// Attributes 
public: 
 
// Operations 
public: 
	double g; 
	double mass; 
 
// Overrides 
	// ClassWizard generated virtual function overrides 
	//{{AFX_VIRTUAL(EoS_HB) 
	//}}AFX_VIRTUAL 
 
// Implementation 
public: 
	double q; 
	virtual double dns_dm(double mu,double T); 
	virtual double dns_dT(double mu, double T); 
	virtual double dns_dmu(double mu, double T); 
	virtual double ns_(double mu, double T); 
	virtual double de_dmu(double mu, double T); 
	virtual double de_dT(double mu, double T); 
	virtual double  ds_dT(double mu, double T); 
	virtual double dn_dT(double mu, double T); 
	virtual double dn_dm(double mu, double T); 
	virtual double dn_dmu(double mu, double T); 
	virtual double en_(double mu, double T); 
	virtual double p_(double mu, double T); 
	virtual double s_(double mu, double T); 
	virtual double n_(double mu, double T); 
	virtual ~EoS_HB(); 
 
	// Generated message map functions 
 
}; 
 
///////////////////////////////////////////////////////////////////////////// 
 
//{{AFX_INSERT_LOCATION}} 
// Microsoft Visual C++ will insert additional declarations immediately before the previous line. 
 
#endif // !defined(AFX_EOS_HB_H__C21202E5_F833_4031_9196_9B6D98596DFD__INCLUDED_)                   
