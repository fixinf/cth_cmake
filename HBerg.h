#if !defined(AFX_HBERG_H__F0CEF6C1_613C_4193_BF8B_A380F247E5E4__INCLUDED_)
#define AFX_HBERG_H__F0CEF6C1_613C_4193_BF8B_A380F247E5E4__INCLUDED_
#include "EoS_HB.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HBerg.h : header file
//

//#define _SYMMAT_

/////////////////////////////////////////////////////////////////////////////
// HBerg frame

class HBerg : public EoS_HB
{

// Attributes
public:
	HBerg();  
	 ~HBerg();
double Charge(double n, double x, double mu_e);
double BetaEQ_mue(double n, double x);
double xB(double n);  //with miuons
double n_mu(double mu_e);
double n_e(double mu_e);
double findnb(double mu_b);
double n_(double mu_b, double mu_e, double *n);
double xBeta(double n, double mue);


// Operations
public:
	double P_pi(double n);
	double mu_pi(double n);
	double eps_pi(double n);
	double mue(double mu_b);
	double p_bh(double mu_b);
	double xb_0(double n);

	double n_bh(double mu_b);
	double eps_bh(double mu_b);

	double Charge(double mu_b, double mu_e);
	double n_B(double mu_b, double mu_e);
	double Eps(double mu_b, double mu_e);
	double P(double mu_b, double mu_e);
	double p_Frac(double mu_b, double mu_e);


	double p_e(double mue);
	double mu_e(double ne);
	double eps_e(double mue);

	double p_mu(double mue);
	double eps_mu(double mue);
	double mu_p(double n, double x);
	double p(double n, double x);
	double mu_n(double n, double x);
	double eps(double n, double x);

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HBERG_H__F0CEF6C1_613C_4193_BF8B_A380F247E5E4__INCLUDED_)
