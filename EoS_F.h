#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#pragma once

#include "RW_cool.h"
//#include "cool_eos.h"
#define peos 1510


class EoS_F : public EoS_HB
{
public:
	EoS_F(void);
	~EoS_F(void);
	char EoS_file_name[500];
	char EoS_mini_file_name[520];


	double mu_crit;
double epsf[peos],Pf[peos],rof[peos],muf[peos],
 n0f[peos], n1f[peos], m0f[peos], m1f[peos], n13f[peos],
 n14f[peos], n15f[peos], n16f[peos], n17f[peos], muef[peos], mumuf[peos],
 Y_Lf[peos],Y_Smf[peos],Y_S0f[peos],Y_Spf[peos],
Y_Xmf[peos],Y_X0f[peos],m_Lf[peos],m_Smf[peos],m_S0f[peos],m_Spf[peos],
m_Xmf[peos],m_X0f[peos];
double M_M[500],M_R[500],M_MU[500],M_MB[500];
double M_M_[500], M_R_[500], M_MU_[500], M_MB_[500];

int Nf_M, Nf_M_;
double Mmax,mu_c_max, mu_min;
bool m_file;
char name[300];
int Nf;
void Read_EoS_FILE_();
int Read_Mass_Radius_relation(FILE *Config, double* M, double* R, double* MB, double* mu_c);
//void Read_Mass_Radius_relation(FILE *Config);
};
