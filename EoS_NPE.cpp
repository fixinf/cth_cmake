//#include "stdafx.h"
// EoS_NPE.cpp : implementation file
//


#include "EoS_NPE.h"

#include "Zbrent.h"
#include <math.h>
#include <malloc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// EoS_NPE
#define OM 0.000294539203
	/*14.37*4*M_PI/783./783.*/
#define SIG 0.000387584257
	/*9.33*4*M_PI/550/550*/
#define hc3 (197.32705*197.32705*197.32705)

#define M_ELECTRON 0.510999
#define G_ELECTRON 2.
#define G_NUCLON 2.
#define G_mju_MESON 2.
#define M_mju_MESON 105.658389
#define M_NEUTRON 939.56563
#define M_PROTON 938.272
#define Q_PROTON 1.
#define Q_ELECTRON -1.
#define Q_mju_MESON -1.

#define zer0 1.e-10

/*mju_mtr={mjus_neutron,mjus_proton}
 m_mtr={ms_neutron,ms_proton}
 n={n_neutron,n_proton,n_electron,n_mjuon}
mjue=mjumuon*/

double EoS_NPE::n_(double mjue,double T,double *mju_mtr,double *m_mtr, double *n)
{
 n[0]= EoS_LW::n_(mju_mtr[0],m_mtr[0],T);
 n[1]= EoS_LW::n_(mju_mtr[1],m_mtr[1],T);
 n[2]= el_.n_(mjue,T) - el_.n_(-mjue,T);
 n[3]= mu_.n_(mjue,T) - mu_.n_(-mjue,T);
return n[0] + n[1];
}/*returning the Baryon number*/

double EoS_NPE::s_(double mjue,double T,double *mju_mtr,double *m_mtr)
{double sums;
 sums = EoS_LW::s_(mju_mtr[0],m_mtr[0],T);
 sums+= EoS_LW::s_(mju_mtr[1],m_mtr[1],T);
 sums+= el_.s_(mjue,T) + el_.s_(-mjue,T) + zer0;
 sums+= mu_.s_(mjue,T) + mu_.s_(-mjue,T) + zer0;
return sums;}

double EoS_NPE::p_(double mjue,double T,double *mju_mtr,double *m_mtr)
{ double sump,nB,nsB;
 nB= EoS_LW::n_(mju_mtr[0],m_mtr[0],T)+EoS_LW::n_(mju_mtr[1],m_mtr[1],T);
 nsB=EoS_LW::ns_(mju_mtr[0],m_mtr[0],T)+EoS_LW::ns_(mju_mtr[1],m_mtr[1],T);
 sump= EoS_LW::p_(mju_mtr[0],m_mtr[0],T,nB,nsB);
 EoS_HB::mass=m_mtr[1];
 sump+=EoS_HB::p_(mju_mtr[1],T) + EoS_HB::p_(- mju_mtr[1],T);
sump+=el_.p_(mjue,T) + el_.p_(-mjue,T);
sump+=mu_.p_(mjue,T);+ mu_.p_(-mjue,T);
return sump;}

double EoS_NPE::en_(double mjue,double T,double *mju_mtr,double *m_mtr)
{ double sume,nB,nsB;
 nB= EoS_LW::n_(mju_mtr[0],m_mtr[0],T) + EoS_LW::n_(mju_mtr[1],m_mtr[1],T);
 nsB=EoS_LW::ns_(mju_mtr[0],m_mtr[0],T)+ EoS_LW::ns_(mju_mtr[1],m_mtr[1],T);
 sume= EoS_LW::en_(mju_mtr[0],m_mtr[0],T,nB,nsB);
 EoS_HB::mass=m_mtr[1];

 sume+=EoS_HB::en_(mju_mtr[1],T) + EoS_HB::en_(- mju_mtr[1],T);
 sume+=el_.en_(mjue,T) + el_.en_(-mjue,T);
 sume+=mu_.en_(mjue,T);+ mu_.en_(-mjue,T);
return sume;
}


double Msn_ST=10.,Msn_END=M_NEUTRON-.0001,Mue_ST=zer0,Mue_END=300.,REGY=1;
static double temper[]={ 1e-10 , 0.1304347827 , 0.2608695653 , 0.3913043479 , 0.5217391305 , 0.6521739131 , 
 0.7826086957 , 0.9130434784 , 1.043478261 , 1.173913044 , 1.304347826 , 1.434782609 , 
 1.565217391 , 1.695652174 , 1.826086957 , 1.956521739 , 2.086956522 , 2.217391304 , 
 2.347826087 , 2.47826087 , 2.608695652 , 2.739130435 , 2.869565217 , 3 , 
 3.130434783 , 3.260869565 , 3.391304348 , 3.521739131 , 3.652173913 , 3.782608696 , 
 3.913043478 , 4.043478261 , 4.173913044 , 4.304347826 , 4.434782609 , 4.565217391 , 
 4.695652174 , 4.826086957 , 4.956521739 , 5.086956522 , 5.217391304 , 5.347826087 , 
 5.47826087 , 5.608695652 , 5.739130435 , 5.869565217 , 6 , 6.130434783 , 
 6.260869565 , 6.391304348 , 6.52173913 , 6.652173913 , 6.782608696 , 6.913043478 , 
 7.043478261 , 7.173913044 , 7.304347826 , 7.434782609 , 7.565217391 , 7.695652174 , 
 7.826086957 , 7.956521739 , 8.086956522 , 8.217391304 , 8.347826087 , 8.47826087 , 
 8.608695652 , 8.739130435 , 8.869565217 , 9 , 9.652173913};
static double mjuph[]={ 939.5310417 , 939.5307688 , 939.5294203 , 939.52255 , 939.5164995 , 939.5084651 , 
 939.4988925 , 939.4877425 , 939.4751009 , 939.4609676 , 939.4453764 , 939.4283572 , 
 939.4099355 , 939.390134 , 939.3689674 , 939.3463546 , 939.3227923 , 939.2976249 , 
 939.2711832 , 939.2434836 , 939.2145343 , 939.1843484 , 939.1529355 , 939.1203072 , 
 939.0865319 , 939.0513944 , 939.0152968 , 938.9779507 , 938.9394014 , 938.8996798 , 
 938.8588077 , 938.8168006 , 938.7736684 , 938.7294212 , 938.6840652 , 938.6376068 , 
 938.5900658 , 938.5414223 , 938.4916952 , 938.4408985 , 938.3890159 , 938.3360764 , 
 938.2820814 , 938.2270274 , 938.1709248 , 938.1137311 , 938.0557537 , 937.9963842 , 
 937.9361449 , 937.8748927 , 937.8126358 , 937.749424 , 937.6849256 , 937.6196704 , 
 937.5533531 , 937.4860489 , 937.4177504 , 937.3484858 , 937.2781908 , 937.2069574 , 
 937.1347717 , 937.0616628 , 936.9872683 , 936.912136 , 936.8361068 , 936.7590093 , 
 936.6809894 , 936.6020208 , 936.5221054 , 936.4412421 , 936.0256494};
static double ms1[]={ 939.56562 , 939.5201213 , 939.4214998 , 939.2864601 , 939.1186939 , 938.9226701 , 
 938.7000565 , 938.4522744 , 938.1800114 , 937.8842214 , 937.5652956 , 937.2236435 , 
 936.8595521 , 936.4732351 , 936.0646311 , 935.6345062 , 935.1814065 , 934.7069665 , 
 934.2104193 , 933.691631 , 933.150524 , 932.5869162 , 932.0006143 , 931.3913797 , 
 930.7587116 , 930.1036926 , 929.4236075 , 928.719843 , 927.9917913 , 927.2389768 , 
 926.4609176 , 925.6571202 , 924.8270792 , 923.9702471 , 923.0860565 , 922.1738919 , 
 921.2318614 , 920.2616219 , 919.2612464 , 918.2298347 , 917.1666811 , 916.0706384 , 
 914.9407225 , 913.7759305 , 912.5750674 , 911.3380505 , 910.0595215 , 908.7447593 , 
 907.3865273 , 905.9846953 , 904.537297 , 903.0415931 , 901.5001882 , 899.9030287 , 
 898.252346 , 896.5423668 , 894.7713326 , 892.9331562 , 891.0297446 , 889.049142 , 
 886.9878804 , 884.8388272 , 882.6052798 , 880.2637029 , 877.7996111 , 875.2233735 , 
 872.5035539 , 869.6252995 , 866.5659461 , 863.296148 , 841.4119354};
static double ms2[]={682.3705719 , 682.3877717 , 682.4296093 , 682.4997104 , 682.5946797 , 682.7121905 , 
 682.8508411 , 683.0106736 , 683.1912505 , 683.3924588 , 683.6143095 , 683.8568067 , 
 684.1200099 , 684.4040147 , 684.7089137 , 685.0348747 , 685.3820142 , 685.7503406 , 
 686.1402919 , 686.5519441 , 686.9855192 , 687.441248 , 687.9194004 , 688.420241 , 
 688.9440246 , 689.4913568 , 690.0621152 , 690.6570094 , 691.2764239 , 691.9207699 , 
 692.5905129 , 693.2861509 , 694.0082215 , 694.7572969 , 695.5339933 , 696.3389628 , 
 697.1725836 , 698.0362187 , 698.9303593 , 699.8556569 , 700.8135857 , 701.8045318 , 
 702.8298064 , 703.890283 , 704.9874106 , 706.1229445 , 707.296322 , 708.5120406 , 
 709.7695176 , 711.0710592 , 712.4188372 , 713.8142218 , 715.261563 , 716.7603307 , 
 718.3155028 , 719.9291566 , 721.6051428 , 723.3463409 , 725.1600307 , 727.0475746 , 
 729.0156939 , 731.0705609 , 733.2243686 , 735.4787732 , 737.8415733 , 740.3364612 , 
 742.9697049 , 745.7612343 , 748.7339442 , 751.9175929 , 766.9856406};
static double mue1[]={52.00654175 , 52.01538735 , 52.02278445 , 52.04139472 , 52.0751133 , 52.12232297 , 
 52.18323606 , 52.25677358 , 52.34180429 , 52.43703236 , 52.54125593 , 52.65335362 , 
 52.77230434 , 52.89718743 , 53.02717589 , 53.16151453 , 53.29953936 , 53.44067084 , 
 53.58431558 , 53.72999829 , 53.87726547 , 54.02570358 , 54.17493712 , 54.32461417 , 
 54.47442415 , 54.62401439 , 54.7731954 , 54.92164212 , 55.0691218 , 55.21539978 , 
 55.36025821 , 55.50348739 , 55.64488513 , 55.78425696 , 55.92141325 , 56.05616836 , 
 56.18839467 , 56.3178155 , 56.4443011 , 56.5677048 , 56.68776361 , 56.80438492 , 
 56.91735134 , 57.02650664 , 57.13162416 , 57.23243825 , 57.32902559 , 57.42073772 , 
 57.50766235 , 57.58948366 , 57.66590748 , 57.73676265 , 57.80146091 , 57.86011008 , 
 57.91205119 , 57.95704179 , 57.99459624 , 58.02437208 , 58.04544167 , 58.05768331 , 
 58.06026369 , 58.05243513 , 58.0326722 , 58.00084335 , 57.95602815 , 57.89524447 , 
 57.81780756 , 57.72128183 , 57.60288138 , 57.45897593 , 56.85220978};
static double mue2[]={2.555261189 , 2.506100055 , 2.935943422 , 3.725750864 , 4.483059649 , 5.213313147 , 
 5.922035856 , 6.612816187 , 7.288517755 , 7.951031248 , 8.60207898 , 9.242964636 , 
 9.874778922 , 10.49843328 , 11.11481006 , 11.72424439 , 12.32803531 , 12.92593076 , 
 13.51873219 , 14.10687024 , 14.69070788 , 15.27059855 , 15.84686151 , 16.41979755 , 
 16.98976577 , 17.55656567 , 18.12124262 , 18.68345207 , 19.24349638 , 19.80161172 , 
 20.35801636 , 20.9129143 , 21.46649714 , 22.01895353 , 22.57046367 , 23.12120752 , 
 23.67167423 , 24.22143977 , 24.77098355 , 25.32050616 , 25.87015072 , 26.42015168 , 
 26.97069283 , 27.52195094 , 28.07413309 , 28.62720015 , 29.18221457 , 29.73799758 , 
 30.29596092 , 30.85602571 , 31.41847632 , 31.98372889 , 32.55117464 , 33.12258071 , 
 33.69737725 , 34.27646231 , 34.85997794 , 35.44880927 , 36.04243279 , 36.64268343 , 
 37.24989212 , 37.86497153 , 38.48711639 , 39.11981301 , 39.76494053 , 40.42047843 , 
 41.09083684 , 41.77772584 , 42.48391077 , 43.21298958 , 47.4967332};
void critinpe_m(double mju,double t)
{int cntr=1;double muph;Msn_ST=10.;Msn_END=M_NEUTRON-.00001;Mue_ST=zer0;Mue_END=300.;
 if (t>9.661) return;
 while (t>temper[cntr]&&cntr<71) cntr++;
 muph=mjuph[cntr-1]+(t-temper[cntr-1])/(temper[cntr]-temper[cntr-1])*(mjuph[cntr]-mjuph[cntr-1]);
 if (mju>muph)
  Msn_END=ms2[cntr-1]+(t-temper[cntr-1])/(temper[cntr]-temper[cntr-1])*(ms2[cntr]-ms2[cntr-1]);
 else
  Msn_ST=ms1[cntr-1]+(t-temper[cntr-1])/(temper[cntr]-temper[cntr-1])*(ms1[cntr]-ms1[cntr-1]);
return;}
void critinpe_e(double mju,double t)
{int cntr=1;double muph;Msn_ST=10.;Msn_END=M_NEUTRON-.00001;Mue_ST=zer0;Mue_END=300.;
 if (t>9.661) return;
 while (t>temper[cntr]&&cntr<71) cntr++;
 muph=mjuph[cntr-1]+(t-temper[cntr-1])/(temper[cntr]-temper[cntr-1])*(mjuph[cntr]-mjuph[cntr-1]);
 if (mju>muph)
   Mue_ST=mue1[cntr-1]+(t-temper[cntr-1])/(temper[cntr]-temper[cntr-1])*(mue1[cntr]-mue1[cntr-1]);
 else
   Mue_END=mue2[cntr-1]+(t-temper[cntr-1])/(temper[cntr]-temper[cntr-1])*(mue2[cntr]-mue2[cntr-1]);
return;}

EoS_LW LW;

double _MSN_,_MJUE_,_MJUB_,T_E,_NB_;
	double zbrmunpe(double mjusn)
	{double nB,ret_urn;
	nB= LW.n_(mjusn,_MSN_,T_E) + LW.n_(mjusn-_MJUE_,M_PROTON-M_NEUTRON+_MSN_,T_E);
	ret_urn=mjusn+OM*hc3*nB-_MJUB_;
	return ret_urn;}
	double zbrmnpe(double msn)
	{double nsB,mjusn,ret_urn;
	_MSN_=msn;
	mjusn=zbrent(zbrmunpe,10.,_MJUB_,zer0);
	nsB= LW.ns_(mjusn,_MSN_,T_E) + LW.ns_(mjusn-_MJUE_,M_PROTON-M_NEUTRON+_MSN_,T_E);
	ret_urn=_MSN_+SIG*hc3*nsB-M_NEUTRON;
	return ret_urn;}

	void EoS_NPE::marticer(double mjuB,double mjue,double t,double *mju_mtr,double *m_mtr)
{_MJUB_=mjuB;_MJUE_=mjue;
if (REGY) critinpe_m(mjuB,t);/*Defineing the Msn_ST and Msn_END*/
 _MSN_=m_mtr[0]=zbrent(zbrmnpe,Msn_ST,Msn_END,zer0);
 mju_mtr[0]=zbrent(zbrmunpe,10.,_MJUB_,zer0);
 mju_mtr[1]=mju_mtr[0]-_MJUE_;
 m_mtr[1]=M_PROTON-M_NEUTRON+m_mtr[0];
return;}

EoS_NPE npe;
	/*comparison of densities*/
	double chargenpe(double mjue)
	{double ne,nmju,np,*mju_mtr,*m_mtr;
	 mju_mtr=(double *)malloc(2*sizeof(double));m_mtr=(double *)malloc(2*sizeof(double));
	 npe.marticer(_MJUB_,mjue,T_E,mju_mtr,m_mtr);
	 np=LW.n_(mju_mtr[1],m_mtr[1],T_E);
	 ne=npe.el_.n_(mjue,T_E) - npe.el_.n_(-mjue,T_E) + zer0;
	 nmju=npe.mu_.n_(mjue,T_E) - npe.mu_.n_(-mjue,T_E) + zer0;
	 free(mju_mtr);free(m_mtr);
	return Q_PROTON*np+Q_ELECTRON*ne+Q_mju_MESON*nmju;
	}

double 	EoS_NPE::Charge(double mu_B, double mjue, double T)
	{double ne,nmju,np,*mju_mtr,*m_mtr;
	 mju_mtr=(double *)malloc(2*sizeof(double));
	 m_mtr=(double *)malloc(2*sizeof(double));
	 marticer(mu_B,mjue,T,mju_mtr,m_mtr);
	 np=LW.n_(mju_mtr[1],m_mtr[1],T);
	 ne=el_.n_(mjue,T) - npe.el_.n_(-mjue,T) + zer0;
	 nmju=mu_.n_(mjue,T) - npe.mu_.n_(-mjue,T) + zer0;
	 free(mju_mtr);
	 free(m_mtr);
	return Q_PROTON*np+Q_ELECTRON*ne+Q_mju_MESON*nmju;
	}


double B2ewal_regy(double mjuB,double t,double Mst,double Msend,double muest,double muend,double *mju_mtr,double *m_mtr)
{double mjue;REGY=0;
  Msn_ST=Mst;Msn_END=Msend;T_E=t;_MJUB_=mjuB;
  mjue=zbrent(chargenpe,muest,muend,zer0); 
  npe.marticer(_MJUB_,mjue,T_E,mju_mtr,m_mtr);  
return mjue;
}

double EoS_NPE::B2ewal(double mjuB,double t,double *mju_mtr,double *m_mtr)
{double mjue;REGY=1;
  critinpe_e(mjuB,t);/*Defineing the Mue_ST and Mue_END*/
  T_E=t;
  _MJUB_=mjuB;
  mjue=zbrent(chargenpe,Mue_ST,Mue_END,zer0); 
  npe.marticer(_MJUB_,mjue,T_E,mju_mtr,m_mtr);  
return mjue;
}	
	double zbrmunpe_n(double mjusn)
	{
		double nB,ret_urn;
	nB=LW.n_(mjusn,_MSN_,T_E)+LW.n_(mjusn-_MJUE_,M_PROTON-M_NEUTRON+_MSN_,T_E);
	ret_urn=_NB_-nB;
	return ret_urn;
	}
	
	double zbrmnpe_n(double msn)
	{
		double nsB,mjusn,ret_urn;
	_MSN_=msn;
	mjusn=zbrent(zbrmunpe_n,100.,M_NEUTRON+100.,zer0);
	nsB=LW.ns_(mjusn,_MSN_,T_E)+LW.ns_(mjusn-_MJUE_,M_PROTON-M_NEUTRON+_MSN_,T_E);
	ret_urn=_MSN_+SIG*hc3*nsB-M_NEUTRON;
	return ret_urn;
	}

void marticer_n(double nB,double mjue,double t,double *mju_mtr,double *m_mtr)
{_NB_=nB;_MJUE_=mjue;
 _MSN_=m_mtr[0]=zbrent(zbrmnpe_n,10.,M_NEUTRON,zer0);
 mju_mtr[0]=zbrent(zbrmunpe_n,100.,M_NEUTRON,zer0);
 mju_mtr[1]=mju_mtr[0]-_MJUE_;
 m_mtr[1]=M_PROTON-M_NEUTRON+m_mtr[0];
return;}

	/*comparison of densities*/
	double chargenpe_n(double mjue)
	{double ne,np,nmju,*mju_mtr,*m_mtr;
	 mju_mtr=(double *)malloc(2*sizeof(double));m_mtr=(double *)malloc(2*sizeof(double));
	 marticer_n(_NB_,mjue,T_E,mju_mtr,m_mtr);
	 np=LW.n_(mju_mtr[1],m_mtr[1],T_E);
	 ne=npe.el_.n_(mjue,T_E) - npe.el_.n_(-mjue,T_E) + zer0;
	 nmju=npe.mu_.n_(mjue,T_E) - npe.mu_.n_(-mjue,T_E) + zer0;
	 free(mju_mtr);
	 free(m_mtr);
	return Q_PROTON*np+Q_ELECTRON*ne+Q_mju_MESON*nmju;
	}

double B2ewal_n(double nB,double t,double *mju_mtr,double *m_mtr,double *mjuB)
{
	double mjue;
  _NB_=nB;
  T_E=t;
  mjue=zbrent(chargenpe_n,zer0,300.,zer0); 
  marticer_n(_NB_,mjue,T_E,mju_mtr,m_mtr);  
  *mjuB=mju_mtr[0]+OM*hc3*_NB_;
return mjue;
}

/*dn={dnadmjuB, dnadt}*/

void EoS_NPE::dn_(double mjue,double T,double *mju_mtr,double *m_mtr, double *dn)
{double dmjupdmjuB, dmjupdt, dmju[4], dt[4];
 double dn_n[2],dns_n[2],ds_n,dn_p[2],dns_p[2],ds_p;

 diff_n_ns_e(mju_mtr[0],m_mtr[0],T,dn_n,dns_n,&ds_n);
 diff_n_ns_e(mju_mtr[1],m_mtr[1],T,dn_p,dns_p,&ds_p);
 /*dn_N/dmju_N*/     dmju[0]  =dndmjuwal(dn_n,dns_n);
 /*dn_P/dmju_P*/     dmju[1]  =dndmjuwal(dn_p,dns_p);
 /*dn_E/dmju_E*/     dmju[2]  =el_.dn_dmu(mjue,T)+el_.dn_dmu(-mjue,T);
 /*dn_mju/dmju_mju*/ dmju[3]  =mu_.dn_dmu(mjue,T)+mu_.dn_dmu(-mjue,T);
 /*dn_N/dt*/         dt[0]=dndtwal(dn_n,dns_n);
 /*dn_P/dt*/         dt[1]=dndtwal(dn_p,dns_p);
 /*dn_E/dt*/         dt[2]=el_.dn_dT(mjue,T)-el_.dn_dT(-mjue,T);
 /*dn_mju/dt*/       dt[3]=mu_.dn_dT(mjue,T)-mu_.dn_dT(-mjue,T);
 dmjupdmjuB=(dmju[2]+dmju[3])/(dmju[1]+dmju[2]+dmju[3]);
 dmjupdt=(dt[3]+dt[2]-dt[1])/(dmju[3]+dmju[2]+dmju[1]);
 /*dndmjuB*/         dn[0]=dmju[0] + dmju[1]*dmjupdmjuB;
 /*dndt*/            dn[1]=dt[0]+dt[1]+ dmju[0]*dmjupdt;
return;}

/*ds={dsdmjuB, dsdt}*/
void EoS_NPE::ds_(double mjue,double T,double *mju_mtr,double *m_mtr, double *ds)
{
	double dmjupdmjuB, dmjupdt, dmju[4], dt[4];
    double dn_n[2],dns_n[2],ds_n,dn_p[2],dns_p[2],ds_p;

 diff_n_ns_e(mju_mtr[1],m_mtr[1],T,dn_p,dns_p,&ds_p);
 /*dn_P/dmju_P*/    dmju[1] = dndmjuwal(dn_p,dns_p);
 /*dn_E/dmju_E*/    dmju[2] = el_.dn_dmu(mjue,T)+el_.dn_dmu(-mjue,T);
 /*dn_mju/dmju_mju*/dmju[3] = mu_.dn_dmu(mjue,T)+mu_.dn_dmu(-mjue,T);
 /*dn_P/dt*/        dt[1]   = dndtwal(dn_p,dns_p);
 /*dn_E/dt*/        dt[2]   = el_.dn_dT(mjue,T)-el_.dn_dT(-mjue,T);
 /*dn_mju/dt*/      dt[3]   = mu_.dn_dT(mjue,T)-mu_.dn_dT(-mjue,T);
 dmjupdmjuB                 = (dmju[2]+dmju[3])/(dmju[1]+dmju[2]+dmju[3]);
 dmjupdt                    = (dt[3]+dt[2]-dt[1])/(dmju[3]+dmju[2]+dmju[1]);
 diff_n_ns_e(mju_mtr[0],m_mtr[0],T,dn_n,dns_n,&ds_n);
 /*ds_N/dmju_N*/    dmju[0] = dndtwal(dn_n,dns_n);
 /*ds_P/dmju_P*/    dmju[1] = dt[1];
 /*ds_E/dmju_E*/    dmju[2] = dt[2];
 /*ds_mju/dmju_mju*/dmju[3] = dt[3];
 /*ds_N/dt*/        dt[0]   = dsdtwal(ds_n,dn_n,dns_n);
 /*ds_P/dt*/        dt[1]   = dsdtwal(ds_p,dn_p,dns_p);
 /*ds_E/dt*/        dt[2]   = el_.ds_dT(mjue,T)-el_.ds_dT(-mjue,T);
 /*ds_mju/dt*/      dt[3]   = mu_.ds_dT(mjue,T)-mu_.ds_dT(-mjue,T);
/*dsdmjuB*/         ds[0]   = dmju[0]+(dmju[1]-dmju[2]-dmju[3])*dmjupdmjuB+dmju[2]+dmju[3];
/*dsdt*/ ds[1]=dt[0]+dt[1]+dt[2]+dt[3]+(dmju[1]-dmju[2]-dmju[3])*dmjupdt;
return;
}


EoS_NPE::EoS_NPE()
{
	el_.g=G_ELECTRON;
	el_.mass = M_ELECTRON;
	mu_.g = G_mju_MESON;
	mu_.mass = M_mju_MESON;
	EoS_LW::g = G_NUCLON;
	EoS_LW::mass = M_NEUTRON;
}

EoS_NPE::~EoS_NPE()
{
}


////////////////////////////////////////////////////////////////////////////
// EoS_NPE message handlers
#undef zer0
#undef M_ELECTRON
#undef M_NEUTRON
#undef M_PROTON
#undef Q_PROTON
#undef G_NUCLON
#undef M_mju_MESON
#undef G_ELECTRON
#undef G_mju_MESON
#undef Q_ELECTRON
#undef Q_mju_MESON
