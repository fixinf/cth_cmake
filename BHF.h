// BHF.h: interface for the BHF class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BHF_H__0F5315A3_6620_40B9_BF8B_67F27DD3ACD2__INCLUDED_)
#define AFX_BHF_H__0F5315A3_6620_40B9_BF8B_67F27DD3ACD2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "EoS_HB.h"


class BHF : public EoS_HB  
{
public:
	BHF();
	virtual ~BHF();

double BHF::mjuechargeNT(double mjue);
double BHF::mjuBchargeNT(double nN);
double BHF::BHFHHintchne(double mjuB, double *nB, double *eps, double *n);
void BHF::BHFHHint(double mjuB, double mjue, double *nB, double *eps, double *n);

/*n={nn,np,nlambda,nsigmaminus,ne,nmu}
second and third functions returning the mjue
mjuB must not be shifted by MN=939.56563 it is done iniside*/


};

#endif // !defined(AFX_BHF_H__0F5315A3_6620_40B9_BF8B_67F27DD3ACD2__INCLUDED_)
