// Star_MK.cpp: implementation of the Star_MK class.
//
//////////////////////////////////////////////////////////////////////

//#include "stdafx.h"
//#include "cooling_nlw.h"
#include "Star_MK.h"

#include "Merson.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <malloc.h>
#define db double

extern int NUM;


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//#define abss(x) (x>=0)?(x):(-x)
#define M_ATOMIC 931.5
#define M_NEUTRON 939.56563
#define M_PROTON 938.272

#define M_L 1115.683
#define M_Sp 1189.37
#define M_S0 1192.642 
#define M_Sm 1197.449
#define M_X0 1314.9
#define M_Xm 1321.32


#define k_B 8.617e-5
#define _FORMAT_ "  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g\n"
#define _FORMAT1_ "  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g  %.15g\n"


static db mu_out=1.e-8;
static int point_n0=17;
static int point_n= Num - 10 - 17;
static int point_n_last = 10;
static int point_n_all = 450;

static db n0=0.16;      //fm^-3 = 10^39 cm^-3 
static db kT0=0.08617;  //MeV= 10^9 K = 1/(6.2418 10^5) erg
static db cv0=2.78055e-4;


char mu_name[10];
char m_name[10];

char Name[35],Cof_Name[35];

int p_number; /*floating index of matrix*/
int sw        /*switcher of the EoS KIND*/;
double 
//#ifndef _NONLW_
//mjus_mtr[2], ms_mtr[2], 
//#else
//mjus_mtr[19], ms_mtr[19],
//#endif
mjue;
double gsig, gom,gor;

db c_v,nn[19] ,mjub,Xii;

//double *inf,dn_c,mu_end
//pr, e, dps, dpe
//;
db mu_; 
db kt_;

//db mu_out;
char *prs,
//*u,
nm;
db mu_end;    /*surface chamical potential*/;
            // temperature  
double sig,
_n;                    //baryon number density

db nu;                     // 2 * gravitational potential
db nu0;                    // 2 * gravitational potential in the center
db Mass_, Mass_B;
db Radius;
db QCRadius;
FILE *outfile;
FILE *outfile1;
char ff[100], ffc[100];

int NUM=3;				   //number of equations for the OVE
//db m = M_NEUTRON;

//const db ZERO=1.e-10;
const db c1= 1.126557e-5;
const db c2=1.4766;

CCool_EoS *pEoS;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Star_MK::Star_MK()
{

}

Star_MK::~Star_MK()
{

}


/******************************************************************/
//               Gevitational potential Lambda
/*******************************************************************/


db lambda(db r,db m)
{ return -log(1-2*c2*m/(r+ZERO));
}

/*******************************************************************/

/*******************************************************************/
//               C O N D I T I O N S
/*******************************************************************/

bool phase(db* w)
{return false;}
bool sss(db* w)
{return false;}/*w[1]>100;}*/

/*********************************************************************
			   E Q U A T I O N S
**********************************************************************/


db Dm (db r, db e)
{return c1*r*r*e;
}

/*
db Dms(db m,db r){
	db dmudr;
	dmudr=-c2*mu_*(m+c1*pr*r*r*r)/(r+ZERO)/(r-2*c2*m+ZERO);
	return dmudr;
}
*/


db Drmu(db mu,db m,db r, db pr, db e ,db n)
{
	db dmudr;
	dmudr= - c2*(pr + e)/(n + ZERO 
//		+ 0.001                    artificial crust
		)*(m+c1*pr*r*r*r)/(r+ZERO)/(r-2*c2*m+ZERO);
	return 1/(dmudr+ZERO);
}


db DN (db m, db r, db nb)
{return c1*r*r*nb*M_ATOMIC/(ZERO + sqrt(1 - 2*c2*m/(r+ZERO)));}



/*******************  The EQUATIONS of Star  STRUCTURE   *******/
/*******************  ================================   *******/

void equations(db mu,db* w,db* f)
{  
	db e, pr, r, mass, B_mass;
	
	mjub = mu;
	r = w[0];
	mass = w[1];      //Gravitational mass
	B_mass = w[2];  //Baryon mass 
	
	/******* EoS call during the calculations of the star structure*******/
	
	
	sw=pEoS->GetKind(mu,kt_);
	pr = pEoS->GetPressure(mu,kt_);
	_n = pEoS->GetDensity(mu,kt_,nn);
	sig = pEoS->GetEntropy(mu,kt_);
	e = pEoS->GetEnergy(mu,kt_);
	

	switch(sw){
	case 0: sig=0; break;  // Hadronic phase 
	case 1: sig=pEoS->Xii; break; //mixed phase 
	case 2: {sig=1; //Quark phase
		QCRadius = r;}
	};  
	
	//printf("sw %g \r    ", sig);
	/******* EoS call ends ***********************************************/
	
	f[0]=Drmu(mu, mass, r ,pr, e, _n);  //  dr/dmu
	
	f[1]=Dm(r,e)*f[0]; // dp/dmu

	f[2]=DN(mass, r, _n)*f[0]; // dm_B/dmu

	
	nu= - 2*log(mu/mu_);// nu is not complete, this is nu(r) - nu_0
	
	
	//printf(" R=%g mu=%g m=%g kt=%g n=%g  gr_p=%g  \r",r,mu,mass,kt,_n,nu); 
	return;
}



/*************************************************************************
                        B O D Y  P R O G R A M
***************************************************************************/
void inter(db mu_in,db mu_out,db kT,double *inf)
{

	double pr, e; 

	InitMer(mu_in,mu_out,inf,NUM,0,1e-8,-1e-5,1e-7);
	Merson(phase,sss,equations);// printf(" kt=%g \n",kt);
	printf("\n mu_out= %g mu_in= %g R= %g  M= %g  ",mu_out,mu_in, Merobj.y[0], Merobj.y[1]); 
	
	Mass_= inf[1]=Merobj.y[1];
	Radius= inf[0]=Merobj.y[0];
	Mass_B= inf[2]=Merobj.y[2];

	sw=pEoS->GetKind(mu_out,kt_);
	pr = pEoS->GetPressure(mu_out,kt_);
	_n = pEoS->GetDensity(mu_out,kt_,nn);
	sig = pEoS->GetEntropy(mu_out,kt_);
	e = pEoS->GetEnergy(mu_out,kt_);
	

	switch(sw){case 0: sig=0; break;  //Harons
	case 1: sig=pEoS->Xii; break; //mixed
	case 2: {sig=1;}
	};  //Quark
	
	//#ifndef _CONFIG_
	if(!pEoS->m_allst)
	{
		/*-------------  Printing the results of star stucture -------*/ 
		outfile = fopen(ff, "a"); 
		
		switch(pEoS->Hybrid.hadron.Hmodel){
		case '1' :
			fprintf(outfile,_FORMAT_,
				inf[0], _n, inf[1], pr, e, nu, nn[0]/_n, nn[1]/_n, 
				pEoS->Hybrid.hadron.m_mtr[0]/M_NEUTRON,pEoS->Hybrid.hadron.m_mtr[1]/M_PROTON,nn[13]/_n,sig,
				nn[15]/_n,nn[16]/_n,nn[17]/_n,mu_out); break;
		case '0' :
			fprintf(outfile,_FORMAT_,
				inf[0], _n, inf[1], pr, e, nu, nn[0]/_n, nn[1]/_n, 
				pEoS->Hybrid.hadron.m_mtr[0]/M_NEUTRON,pEoS->Hybrid.hadron.m_mtr[1]/M_PROTON,nn[2]/_n,sig,
				nn[4]/_n,nn[5]/_n,nn[6]/_n,mu_out); break;
case '2' :
#ifndef _HYP_
			fprintf(outfile,_FORMAT_,
			inf[0], _n, inf[1], pr, e, nu, nn[0]/_n, nn[1]/_n, 
			pEoS->Hybrid.hadron.m_mtr[0]/M_NEUTRON, pEoS->Hybrid.hadron.m_mtr[1]/M_PROTON, pEoS->nn[13]/_n,sig,
			nn[15]/_n,nn[16]/_n,nn[17]/_n,mu_out);break;
#else
fprintf(outfile,_FORMAT1_,
			inf[0], _n, inf[1], pr, e, nu, nn[0]/_n, nn[1]/_n, 
			pEoS->Hybrid.hadron.m_mtr[0]/M_NEUTRON, pEoS->Hybrid.hadron.m_mtr[1]/M_PROTON, pEoS->nn[13]/_n,sig,
			nn[15]/_n,nn[16]/_n,nn[17]/_n,mu_out,nn[2]/_n,nn[3]/_n,nn[4]/_n,nn[5]/_n,nn[6]/_n,nn[7]/_n,
			pEoS->Hybrid.hadron.m_mtr[2]/M_L, pEoS->Hybrid.hadron.m_mtr[3]/M_Sm, 
	pEoS->Hybrid.hadron.m_mtr[4]/M_S0,
			pEoS->Hybrid.hadron.m_mtr[5]/M_Sp
	, pEoS->Hybrid.hadron.m_mtr[6]/M_Xm, pEoS->Hybrid.hadron.m_mtr[7]/M_X0);break;
#endif		
		case '3' :
			fprintf(outfile,_FORMAT_,
				inf[0], _n, inf[1], pr, e, nu, nn[0]/_n, nn[1]/_n, 
				pEoS->Hybrid.hadron.m_mtr[0]/M_NEUTRON,pEoS->Hybrid.hadron.m_mtr[1]/M_PROTON,nn[13]/_n,sig,
				nn[15]/_n,nn[16]/_n,nn[17]/_n,mu_out);break;
					case '4' :
			fprintf(outfile,_FORMAT_,
				inf[0], _n, inf[1], pr, e, nu, nn[0]/_n, nn[1]/_n, 
				pEoS->Hybrid.hadron.m_mtr[0]/M_NEUTRON,pEoS->Hybrid.hadron.m_mtr[1]/M_PROTON,nn[13]/_n,sig,
				nn[15]/_n,nn[16]/_n,nn[17]/_n,mu_out);
			
		}
		fclose(outfile); 
		
		/*printf("  %g  %g  %g  %g  %g  %g  %g  %g  %g  %g  %g  %g  %g  %g  %g\r",
		inf[0], _n, inf[1], pr, e, nu, nn[0]/_n, nn[1]/_n, 
		ms_mtr[0]/M_NEUTRON,ms_mtr[1]/M_NEUTRON,nn[2]/_n,sig,
		nn[4]/_n,nn[5]/_n,nn[6]/_n);
		*/
		
		/*-> Definition of the central value of the Gravitational potential -------*/  
		
		nu0= 2*(log( mu_end / mu_)) - lambda(inf[0],inf[1]);
	}
	//#endif
	
	return;}

/**************  S T A T I C    S O L U T I O N  *******************/
/**************  ------------------------------  *******************/


void inter_static(db ktt,db mutt){
    db mumu0, 
		dn_cl,dn_cll,pr,e,
		mu_in, mu_mant;
double *inf,dn_c;

	kt_    =  ktt  /* central temperature*/ ;
	mu_out  =  mutt  /* central chemical potential*/;
	mu_end  =  pEoS->EK.mu_min /*surface chamical potential*/;
//	mu_end  =  M_ATOMIC /*surface chamical potential*/;
	mu_end  =  M_NEUTRON /*surface chamical potential*/;

	mu_mant =  945.0;
	QCRadius = 0;
	mumu0 = mutt - 2;
	
	inf=(db*)malloc(NUM*sizeof(db));
	
	InitMem(NUM);
	
	if (mu_out<mumu0)  dn_c =(  mu_end  -  mu_out )/(point_n0+point_n+1); else 
	{dn_c=( mu_mant - mumu0 )/point_n ;   // points near the surface
    dn_cl=( mumu0 - mu_out )/point_n0;   // points near the center 
	};
	dn_cll=( mu_end - mu_mant)/point_n_last;
	//dn_c=( mu_end- mu_out )/ (point_n0+point_n);
	
	inf[0]=3.6e-2;  // initial radius in km
	
	/******* EoS call for the initialization of the star structure*******/
    
	mjub=mu_;
	sw=pEoS->GetKind(mu_,kt_);
	pr = pEoS->GetPressure(mu_,kt_);
	_n = pEoS->GetDensity(mu_,kt_,nn);
	sig = pEoS->GetEntropy(mu_,kt_);
	e = pEoS->GetEnergy(mu_,kt_);
	
	//  printf("sw %d\n",sw);
	switch(sw){case 0: sig=0; break;  //Hardons
	case 1: sig=pEoS->Xii; break; //mixed
	case 2: sig=1;     };  //Quarks
		
/******* EoS call ends ***********************************************/
		
		inf[1]=c1/3.0*e*inf[0]*inf[0]*inf[0]; //initial mass in M_sun
		inf[2]=c1/3.0*_n*M_NEUTRON*inf[0]*inf[0]*inf[0]; //initial baryon mass in M_sun

		nu = 0.0;
		
/*******  Printing the results starts*****************************************/

		outfile = fopen(ff, "a"); 
		switch(pEoS->Hybrid.hadron.Hmodel)
		{

		case '1' :
			fprintf(outfile,_FORMAT_,
				inf[0], _n, inf[1], pr, e, nu, nn[0]/_n, nn[1]/_n, 
				pEoS->Hybrid.hadron.m_mtr[0]/M_NEUTRON,pEoS->Hybrid.hadron.m_mtr[1]/M_PROTON,nn[13]/_n,sig,
				nn[15]/_n,nn[16]/_n,nn[17]/_n,mu_out); break;
		case '0':
			fprintf(outfile,_FORMAT_,
				inf[0], _n, inf[1], pr, e, nu, nn[0]/_n, nn[1]/_n, 
				pEoS->Hybrid.hadron.m_mtr[0]/M_NEUTRON,pEoS->Hybrid.hadron.m_mtr[1]/M_PROTON,nn[2]/_n,sig,
				nn[4]/_n,nn[5]/_n,nn[6]/_n,mu_out); break;
		case '2':
#ifndef _HYP_
			fprintf(outfile,_FORMAT_,
			inf[0], _n, inf[1], pr, e, nu, nn[0]/_n, nn[1]/_n, 
			pEoS->Hybrid.hadron.m_mtr[0]/M_NEUTRON, pEoS->Hybrid.hadron.m_mtr[1]/M_PROTON, pEoS->nn[13]/_n,sig,
			nn[15]/_n,nn[16]/_n,nn[17]/_n,mu_out);break;
#else
fprintf(outfile,_FORMAT1_,
			inf[0], _n, inf[1], pr, e, nu, nn[0]/_n, nn[1]/_n, 
			pEoS->Hybrid.hadron.m_mtr[0]/M_NEUTRON, pEoS->Hybrid.hadron.m_mtr[1]/M_PROTON, pEoS->nn[13]/_n,sig,
			nn[15]/_n,nn[16]/_n,nn[17]/_n,mu_out,nn[2]/_n,nn[3]/_n,nn[4]/_n,nn[5]/_n,nn[6]/_n,nn[7]/_n,
	pEoS->Hybrid.hadron.m_mtr[2] / M_L, pEoS->Hybrid.hadron.m_mtr[3] / M_Sm,
	pEoS->Hybrid.hadron.m_mtr[4] / M_S0,
	pEoS->Hybrid.hadron.m_mtr[5] / M_Sp
	, pEoS->Hybrid.hadron.m_mtr[6] / M_Xm, pEoS->Hybrid.hadron.m_mtr[7] / M_X0);break;
#endif
		case '3':
			fprintf(outfile,_FORMAT_,
			inf[0], _n, inf[1], pr, e, nu, nn[0]/_n, nn[1]/_n, 
			pEoS->Hybrid.hadron.m_mtr[0]/M_NEUTRON, pEoS->Hybrid.hadron.m_mtr[1]/M_PROTON, pEoS->nn[13]/_n,sig,
			nn[15]/_n,nn[16]/_n,nn[17]/_n,mu_out);break;
case '4':
			fprintf(outfile,_FORMAT_,
			inf[0], _n, inf[1], pr, e, nu, nn[0]/_n, nn[1]/_n, 
			pEoS->Hybrid.hadron.m_mtr[0]/M_NEUTRON, pEoS->Hybrid.hadron.m_mtr[1]/M_PROTON, pEoS->nn[13]/_n,sig,
			nn[15]/_n,nn[16]/_n,nn[17]/_n,mu_out);
};		
		fclose(outfile); 
		
/*******  Calculation starts        *****************************************/
		
		for (p_number= 0; p_number < point_n + point_n0 + point_n_last;p_number++)
		{
			mu_in   =mu_out /*floating chemical potential*/; 
			
//fprintf(outfile,"\n  %g  %g  %g  " ,inf[0],inf[1],mu_in); 
			if(mu_out>mu_mant){

			if(mu_out > mumu0) mu_out= dn_cl+ mu_in ; // points are near the center
			else  mu_out= dn_c + mu_in ; // points near the surface
			}else mu_out= dn_cll + mu_in ;

			
//printf("\n muout= %g muin= %g R= %g  M= %g  \n",mu_out,mu_in, inf[0], inf[1]); 
			
			inter(mu_in,mu_out,kt_,inf);
			
		};
		outfile = fopen(ff, "a");

		printf("\n  nu_0=%g \n",nu0);
		fprintf(outfile,"\n # %g",nu0);
		fclose(outfile);

		
		FreeMem();
		free(inf);  
		
		
}

/************888*******888********888***********888********888*****************/

/**************  S T A T I C    C O N F I G U R A T I O N S  *******************/
/**************  ------------------------------------------  *******************/


void stab_config(db ktt,db mutt){
    db mumu0, pr,e,
		dn_cl,
		mu_in;
double *inf,dn_c,mu_end;

	kt_    =  ktt  /* central temperature*/ ;
	mu_out  = mutt  /* central chemical potential*/;
//	mu_end  = pEoS->EK.mu_min;// 939.603  /*surface chamical potential*/;
mu_end  = M_ATOMIC  /*surface chamical potential*/;

	mumu0 = mutt-4;
	QCRadius = 0;
	
	inf=(db*)malloc(NUM*sizeof(db));
	
	
	outfile = fopen(ff, "w"); 
	fprintf(outfile,"# n(0)    ,pr(0)       ,e(0)      ,mu(0),     Mass ,  B_Mass,   R   ,R_qc   DM/M^2 \n");
	fclose(outfile);
	
	InitMem(NUM);
	
	//if (mu_out<mumu0)  
		dn_c =(  mu_end  -  mu_out )/(point_n_all); 
//else 
//	{dn_c=( mu_end - mumu0 )/point_n ;   // points near the surface
//    dn_cl=( mumu0 - mu_out )/point_n0;// points near the center 
//	};
	
/*******  Calculation starts        *****************************************/
	
	for (p_number= 0; p_number < point_n_all;p_number++)
	{
		mu_in   = mu_out /*floating chemical potential*/; 
		
//fprintf(outfile,"\n  %g  %g  %g  " ,inf[0],inf[1],mu_in); 
		
		//if(mu_out > mumu0) mu_out= dn_cl+ mu_in ; // points are near the center
		//else  
			mu_out += dn_c  ; // points near the surface
		
//dn_c=( mu_end- mu_out )/ (point_n0+point_n);
		
		inf[0]=3.6e-2;  // initial radius in km
		
/******* EoS call for the initialization of the star structure 2 *******/
		
		mjub=mu_in;
		
		sw=pEoS->GetKind(mu_in,kt_);
		pr = pEoS->GetPressure(mu_in,kt_);
		_n = pEoS->GetDensity(mu_in,kt_,nn);
		sig = pEoS->GetEntropy(mu_in,kt_);
		e = pEoS->GetEnergy(mu_in,kt_);

	switch(sw){
		case 0: sig=0; break;  //Hadrons
		case 1: sig=pEoS->Xii; break; //mix
		case 2: sig=1;     
		};  //Bag
		
		//printf("sw %d\n", sig);
		
/******* EoS call ends ******************************************************/
		inf[1]=c1/3.0*e*inf[0]*inf[0]*inf[0]; //initial mass in M_sun
		inf[2]=c1/3.0*_n*M_ATOMIC*inf[0]*inf[0]*inf[0]; //initial baryon mass in M_sun

/******* Printing the results starts*****************************************/
		
		outfile = fopen(ff, "a"); 
		
		fprintf(outfile,"  %.10g  %.10g  %.10g  %.10g ",
			_n, pr, e, mu_in);
		
		inter(mu_in,mu_end,kt_,inf);

        fprintf(outfile,"  %.10g  %.10g  %.10g  %.10g %.10g\n",
			Mass_, Mass_B, Radius, QCRadius, (Mass_B-Mass_)/Mass_/Mass_);
		
		fclose(outfile); 
		
//printf("\n muout= %g muin= %g R= %g  M= %g  \n",mu_out,mu_in, inf[0], inf[1]); 
		
	};
	
	FreeMem();
	free(inf);  
	
	
}

/************888*******888********888***********888********888**********/

//char *prs,*u,nm;

/*************************************************************************
						M A I N   P R O G R A M
***************************************************************************/

char filename[15];

int EOSF_name()
{
	printf("Enter, please, the EOS file name:");
	scanf("%s",filename);
	return 0;
}


/*-----------------------------------*/

void Star_MK::init_EoS_WGB(double *muu, double tee, CCool_EoS *EoS_type,char* Fret)
{
//char Fret[20];
	pEoS = EoS_type;
	mu_ = *muu;
	
	if (mu_ <= pEoS->EK.mu_min)
	{printf("Please, enter the central Chem. potential : ");
	scanf("%lg",muu);
	mu_ = *muu;
    if (mu_ <= pEoS->EK.mu_min) exit(1);
	}
	printf("Config central chum. pot  = %g\n",mu_);
	
	kt_=tee;
	
	if(!pEoS->m_allst) {gcvt(mu_,8,mu_name); 
						gcvt(pEoS->strS.m_m,8,m_name); }
						else { mu_= 2800; strcpy(mu_name,"");strcpy(m_name,"!");};
	
	strcpy(ff, pEoS->strS.rw_c.HOME);
if(!pEoS->strS.m_Qstar)
	switch(pEoS->Hybrid.hadron.Hmodel) {
	case '0' :  strcat(ff, EoS_lw); break; 
	case '1' :	strcat(ff, EoS_nlw);break;
	case '2' :	strcat(ff, EoS_f);break;
	case '5' : strcat(ff, EoS_SLy);break;
		case '4' : strcat(ff, EoS_FPS);break;
		case '3' : strcat(ff, EoS_hjj);
} 
else strcat(ff,"\\Q_");
	strcat(ff, pEoS->strS.rw_c.File_Name0);

	strcpy(Fret, ff);
	strcpy(pEoS->strS.rw_c._FIGNAME_,ff);

	strcat(ff, mu_name);
	strcat(ff, "-");
	strcat(ff, m_name);
	//#ifdef _NONLW_ 
	//strcat(ff, "NL");
	//#endif
	strcpy(pEoS->strS.rw_c._CONFIGNAME_,ff);
//return Fret; 
}


/*-----------------------------------*/


void reduction()
{
	int fe,j;
	//int i;
	double content[30],mju;
	//char fd[50];
	
	char k;
	while (((k=fgetc(outfile)) != EOF) && (k!='\n'));
	
	//#if defined(_NONLW_)
	//fprintf( outfile1,"  #r ,        _n ,       m ,       pr,        e,      gr.pot,     n_n/_n,      n_p[1]/_n,   m_n/M_n,   m_p/M_n,       Y_e,   Q/H_frac,     nn[15]/_n,nn[16]/_n,nn[17]/_n, mu_B\n");
	//#else
	fprintf( outfile1,"  #r ,       _n,        m,         pr,        e,      gr.pot,      n_n/_n,        n_p/_n,    m_n/M_n,    m_p/M_n,      Y_e,    Q/H_frac ,    n_u/_n, n_d/_n,  n_s/_n, mu_B\n");
	//#endif
	
	
	//fe=fscanf(outfile,"%lc",fd);
	//printf("hallo %s ",fd);
	for (j=0;j<=Num;j++){
#ifndef _HYP_
		fe=fscanf(outfile,"%lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg", 
			&content[0],&content[1],&content[2],&content[3],&content[4],&content[5],
			&content[6],&content[7],&content[8],&content[9],&content[10],
			&content[11],&content[12],&content[13],&content[14],&mju); 
#else
		fe=fscanf(outfile,"%lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg", 
			&content[0],&content[1],&content[2],&content[3],&content[4],&content[5],
			&content[6],&content[7],&content[8],&content[9],&content[10],
			&content[11],&content[12],&content[13],&content[14], &mju,&content[16],
			&content[17],&content[18],&content[19],&content[20],
			&content[21],&content[22],&content[23],&content[24],&content[25],&content[26], &content[27]);

#endif
pEoS->strS.rw_c.ro[j] = content[4];                  //enrgy density
		pEoS->strS.rw_c.rr[j] = content[0];                  //radius
		pEoS->strS.rw_c.mass[j] = content[2];                //mass distribution
		pEoS->strS.rw_c.n_n[j] = content[6];                 // nuetron fraction
		pEoS->strS.rw_c.n_p[j] = content[7];                 // proton  fraction
		pEoS->strS.rw_c.phi[j] = content[5];                 //gravitational potential 
		pEoS->strS.rw_c.T_in[j] = content[4]+ 50* content[11]; //initial temperature profile
		pEoS->strS.rw_c.ms_n[j] = content[8];                //neutron effective mass 
		pEoS->strS.rw_c.ms_p[j] = content[9];                // proton effective mass 
		pEoS->strS.rw_c.Xi[j] = content[11];                 //Xi function 1 for quarks and 0 for hadronic phase
		pEoS->strS.rw_c.n_b[j] = content[1];                 //baryon number density   
		pEoS->strS.rw_c.mu_b[j] = content[15]=mju;			 //baryochemical potentail
		pEoS->strS.rw_c.p[j] = content[3];					 //pressure
		pEoS->strS.rw_c.Y_e[j] = content[10];                //electron fruction

#ifdef _HYP_
pEoS->strS.rw_c.Y_L[j] = content[16];                 // fruction
pEoS->strS.rw_c.Y_Sp[j] = content[17];                // fruction
pEoS->strS.rw_c.Y_S0[j] = content[18];                // fruction
pEoS->strS.rw_c.Y_Sm[j] = content[19];                // fruction
pEoS->strS.rw_c.Y_X0[j] = content[20];                // fruction
pEoS->strS.rw_c.Y_Xm[j] = content[21];                // fruction

		pEoS->strS.rw_c.ms_L[j] = content[22];                // effective mass 
		pEoS->strS.rw_c.ms_Sp[j] = content[23];                // effective mass 
		pEoS->strS.rw_c.ms_S0[j] = content[24];                //effective mass 
		pEoS->strS.rw_c.ms_Sm[j] = content[25];                // effective mass 
		pEoS->strS.rw_c.ms_X0[j] = content[26];                // effective mass 
		pEoS->strS.rw_c.ms_Xm[j] = content[27];                // effective mass 

#endif




	};
		nu0= 2*(log( mju / mu_)) - lambda(content[0],content[2]);
	for (j=0;j<=Num;j++){ 
	
		content[5]= (pEoS->strS.rw_c.phi[j] + nu0)/2.0;
        content[4]= pEoS->strS.rw_c.ro[j] ;                  //enrgy density
		content[0]= pEoS->strS.rw_c.rr[j] ;                  //radius
		content[2]= pEoS->strS.rw_c.mass[j] ;                //mass distribution
		content[6]= pEoS->strS.rw_c.n_n[j] ;                 // nuetron fraction
		content[7]= pEoS->strS.rw_c.n_p[j] ;                 // proton  fraction
 //initial temperature profile calculated during the initaialization
		content[8]= pEoS->strS.rw_c.ms_n[j] ;                //neutron effective mass 
		content[9]= pEoS->strS.rw_c.ms_p[j] ;                // proton effective mass 
		content[11]= pEoS->strS.rw_c.Xi[j];                 //Xi function 1 for quarks and 0 for hadronic phase
		content[1]= pEoS->strS.rw_c.n_b[j] ;                 //baryon number density   
		content[27]= pEoS->strS.rw_c.mu_b[j] ;			 //baryochemical potentail
		content[3]= pEoS->strS.rw_c.p[j] ;					 //pressure
		content[10]= pEoS->strS.rw_c.Y_e[j] ;                //electron fraction
		
#ifdef _HYP_
content[15]=pEoS->strS.rw_c.Y_L[j] ;                //electron fruction
content[16]=pEoS->strS.rw_c.Y_Sp[j];                //electron fruction
content[17]=pEoS->strS.rw_c.Y_S0[j];                //electron fruction
content[18]=pEoS->strS.rw_c.Y_Sm[j];                //electron fruction
content[19]=pEoS->strS.rw_c.Y_X0[j];                //electron fruction
content[20]=pEoS->strS.rw_c.Y_Xm[j];                //electron fruction

		content[21]=pEoS->strS.rw_c.ms_L[j] ;                //neutron effective mass 
		content[22]=pEoS->strS.rw_c.ms_Sp[j];                //neutron effective mass 
		content[23]=pEoS->strS.rw_c.ms_S0[j];                //neutron effective mass 
		content[24]=pEoS->strS.rw_c.ms_Sm[j];                //neutron effective mass 
		content[25]=pEoS->strS.rw_c.ms_X0[j];                //neutron effective mass 
		content[26]=pEoS->strS.rw_c.ms_Xm[j];                //neutron effective mass 

#endif

#ifndef _HYP_
		fe=fprintf(outfile1,_FORMAT_,
			content[0],content[1],content[2],content[3],content[4],content[5],
			content[6],content[7],content[8],content[9],content[10],
			content[11],content[12],content[13],content[14],content[27]); 
#else
fe=fprintf(outfile1,_FORMAT1_,
			content[0],content[1],content[2],content[3],content[4],content[5],
			content[6],content[7],content[8],content[9],content[10],
			content[11],content[12],content[13],content[14],content[27],content[15],content[16],
			content[17],content[18],content[19],content[20],
			content[21],content[22],content[23],content[24],content[25],content[26]); 

#endif
			/*
			printf("%g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\r",
			content[0],content[1],content[2],content[3],content[4],content[5],
			content[6],content[7],content[8],content[9],content[10],
			content[11],content[12],content[13],content[14],content[15]); 
		*/
		
	};
	
}
/*-----------------------------------*/
void Star_MK::star_maker()
{
	/*clrscr;*/
	
	//init_EoS_WGB();
	
	// EOSF_name();
	// init_Eos_f(&filename); 
	
	printf("\n The N A M E  of the output F I L E is  %s \n",ff);
	
	/*-----------------------------------*/ 
	
	
	//#ifndef _CONFIG_
	if(!pEoS->m_allst){
		strcpy(ffc,ff);
		strcat(ff, "_conf.dat");
		strcat(ffc, Config_sufx);
		if(!pEoS->m_make_nf)
		if(outfile=fopen(ff, "rt"))
		{ 
			outfile1 = fopen(ffc, "w");  
			
			reduction();
			
			fclose(outfile1);
			fclose(outfile); 
			
			return;
		}
}
	else 
	{
		strcat(ff, "_st.dat");
	}
	//#endif
	
		
		
		//#ifdef _CONFIG_
		if (pEoS->m_allst) stab_config(kt_,mu_);
		else {
			
			outfile = fopen(ff, "w"); 
			
//if (pEoS->Wmodel)
//{
//#if defined(_NONLW_)
//fprintf( outfile,"  #r ,       _n ,       m ,       pr, e, nu, nn[0]/_n, nn[1]/_n,   mjus_mtr[0]/M_NEUTRON,mjus_mtr[1]/M_NEUTRON,nn[13]/_n,sig, nn[15]/_n,nn[16]/_n,nn[17]/_n,mu_out\n");
//#else
//fprintf( outfile,"  #r ,       _n,        m,         pr,        e,      nu,      n_n/_n,        n_p/_n,   mu_n/M_n,        mu_p/M_n,      nn[2]/_n,sig,      nn[4]/_n,nn[5]/_n,nn[6]/_n, mu_B\n");
//#endif
//}else 
fprintf( outfile,"  #r ,       _n,        m,         pr,        e,      nu,      n_n/_n,        n_p/_n,   m_n/M_n,        m_p/M_n,      n_e/_n,sig,      n_u/_n,  n_d/_n,  n_s/_n, mu_B\n");
			
			fclose(outfile); 
			
			inter_static(kt_,mu_);
/*-----------------------------------*/ 
/*-----------------------------------*/ 
			outfile  = fopen(ff, "rt"); 
			outfile1 = fopen(ffc, "w");  
			
			reduction();
			
			fclose(outfile1);
			fclose(outfile); 
/*-----------------------------------*/
		}
//#endif
		return ;
}


