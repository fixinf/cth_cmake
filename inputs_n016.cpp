//#include "stdafx.h"
#include <math.h>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include "menu.h"
#include "RW_cool.h"
//#include "HBerg.h"

#include "EoS_Bag.h"

//#define _PRINT_Kappa_

//#define _SQGAP_

//#define _R1_

#define _HDD_3

#define hc 197.32705
#define n_0_g  2.6e14
                     /*g/cm^3*/

#define eps00 3.839e26
                 /*erg cm^-3 s^-1*/  
#define k00   8.375e27
                /* erg/cm/sec/K */
#define c00   3.839e17
                /*erg K^-1 cm^-3*/

#define accc 1e-10
#define M_PI 3.1415926535897932384626433832795
#define C_MMU 1

//const  double dgap= 100.0 /*MeV /kT_0 */; 

const  double n_g_Al=7e11/n_0_g /*g cm^-3*/;
const  double n_g_Ar=2e14/n_0_g /*g cm^-3*/;
const  double n_g_pr=4e14/n_0_g /*g cm^-3*/;
const  double n_g_Br=5e15/n_0_g /*g cm^-3*/;


const double PU = 1.5e27/eps00;
const double sint2 = 0.1;

FILE *outtt;

//int ifpicon;
//double gap_factor;

//  float hc=197.32705;//{1/(MeV*fm)}
//  float me=0.5;//{Mev}
#ifdef _HDD_3
#define nc_m 3.0
#define qw 0.2
#define C 1.6
#endif
#ifdef _HDD_2_8
#define nc_m 2.8
#define qw 0.1
#define C 1.6
#endif
#ifdef _HDD_2_5
#define nc_m 2.5
#define qw 0.4
#define C 1.6
#endif
#ifdef _HDD_2
#define nc_m 2.0
#define qw 0.99
#define C 1.6
#endif
#ifdef _HDD_1_5
const double nc_m = 1.5;
const double qw = 3.9;
#define C 1.6
#endif
double sqr(double x) { return x*x; }

/*linear approsimation (interpolation for funtion at given N points  */

double apra(double* y,double* _x,double xc,int N)
{int k=1;  
 double ap;
 if(_x[k] - _x[k - 1]>0)
while (xc<=_x[k-1]&&xc>_x[k]&&k<=N)k++;
 else while (xc <= _x[k - 1] && k <= N)k++;
ap = y[k-1]+(xc-_x[k-1])*(y[k]-y[k-1])/(_x[k]-_x[k-1]);
return ap;
}

/*********************************************/

static int ngap=31;
/*static double logro[]={12 ,12 + log(2),12 + log(3),
12 + log(4),12 + log(5),12 + log(6),12 + log(7),12 + log(8),
12 + log(9),13 ,13+log(1.5),13 + log(2),13 + log(3),13 + log(4),
13 + log(5),13 + log(6),13 + log(7),13 + log(8),13 + log(9),
14 ,14 + log(1.2),14 + log(1.7),14 + log(1.9),14 + log(2),
14 + log(3),14 + log(4),14 + log(5),14 + log(6),
15, 15 + log(2),15 + log(5) };*/

static double logro[]={27.631021,28.324168,28.729633,29.017315,
29.240459,29.422781,29.576931,29.710463,29.828246,29.933606,
30.339071,30.626753,31.032218,31.319901,31.543044,31.725366,
31.87951,32.013048,
32.130831,32.236191,32.418513,32.76682,32.878045,32.929338,
33.334804,33.622486 ,
33.845629,34.027951,34.538776,35.23192358,36.14821431};



static double GAP[]={0.03, 0.11, 0.18, 0.25, 0.30,
 0.37, 0.42, 0.5, 0.58,
0.62, 0.82, 1.0, 1.1, 1.13, 1.07, 1.01, 0.92, 0.77, 0.5,
0.45, 0.25, 0.25, 0.04, 0.02, 0.15, 0.25, 0.3, 0.45,
// 0.47, 0.5, 0.62
 0.0,0.0,0.0
};

/*      8888888888888888888888888888888888888888888888888*******************/
static double DU_Q   = 9.4e26/eps00;
static double BS_Q   = 1.0e22/eps00;
static double MU_Q   = 8.75e19/eps00;
static double MU_H_n = C_MMU*8.6e21/eps00;
static double MU_H_p = C_MMU*8.5e21/eps00;
static double DU_H   = 4.0e27/eps00;

static double NPBF   = 9.0e28/eps00;//6.6e28/eps00;
static double PPBF   = 0.875e28/eps00;
static double MPBS   = 1.0e23/eps00;
static double PnnBS   = 2* 7.4e19/eps00;
static double PnpBS = 2*1.5e20 / eps00;
static double PppBS = 2*1.7e19 / eps00;
static double QPBF   = 0.55e28/eps00;
static double JP_Q   = 0.4e20/eps00;  // PBF in Q matter

static double GG     = 1.0e29/eps00;
static double ee     = 2.8e12/eps00;

//const double Cec = 0.57e20 / c00;

static double Ce     = 0.57e20/c00;
static double CN     = 1.6e20/c00;
static double Cg     = 0.3e14/c00;
static double Cq     = 1.0e21/c00;
static double Kee    = 2.2e22/k00;
static double Kep    = 
2.5e20/k00
//2.5e22/k00
;
static double Knn    = 8.3e22/k00;
static double Knp    = 8.9e18/k00;

static double Kn     = 7.2e24/k00;

static double Kq     = 3.4e23/k00;
static double K_GB     = 3.4e33/k00;  //infinity , has to be redefined

//Phys.Rev. C66, 015802 (2002) Shovkovy, Elis eq.(25)

const double alfa   = 1;
const double beta = 0.7;
const double Gw = 1.0, Gk = 1.0;
static double m_gg   = 70;
static double N_g   = 8;
static double Kap_g = 0.6e18/k00;   //0.4 cm^2/g for K_T
static double a_PBF = 0.1;
static double N_nu  = 3;
/*      8888888888888888888888888888888888888888888888888*******************/




//double ksi_n, ksi_p, ksi_q, R_q, R_n, R_p, supp,ksi,
double m_g,t_m;
//double gap_q,gap_n,gap_p;
double n_,T_h,n_b_H,n_b_Q;
//int n_h_c,n_q_c;
double gap_L;
double z(double nn,double mm)
{
	double p_f = 1.67941*powf(nn, 1. / 3.)*hc; 
	double ms = 938. * mm + ZERO;
	return sqrt(1 + p_f*p_f / ms / ms);

}

/********** quark, neutron & proton gaps ***********/

double sqrt_gap(double t,bool p)
{ double rslt;
double /*g[] = {0,0};
if(p)
g[] = { 0.08900226757369614,0.8253968253968254 };
else*/
g[] = { 0.08900226757369614,0.8253968253968254 };

#ifdef _SQGAP_
  return sqrt(1-t);  //old vertion
#else
  rslt = sqrt(1 - t)*(1. - g[0] *sqrt(t) +  g[1]*t);
  return rslt;
#endif

}

double S_CC[] = { 1.456,0.4186,1.007,0.501,2.5,1.764};
double P_CC[] = { 1.934,0.6893,0.79,0.2824,2,1.977 };
double C_PP[] = { 0.26873,0.08984,0.002913,0.000017528380000000002,
3.047384e-7,0.022415,0.001835,5.84941e-7,0.00161 };

double vx(double tau)
{
	if (tau > 1) return 0.0; else
	return S_CC[5] * sqrt_gap(tau, 1) / tau;
}

double  RCF(double tau, double* Cx)
{
	
	double v = Cx[5] * sqrt_gap(tau,1)/tau;
	double uu = Cx[0] - sqrt(Cx[0] * Cx[0] + v*v) 
		+ Cx[4] * log(Cx[1] + sqrt(Cx[2] * Cx[2] + Cx[3] * Cx[3] * v*v))
		;
	return exp(uu);
}

double RM1(double v1, double v2)
{
	double A =
		C_PP[0] * v1* v1 + C_PP[1] * v2* v2 + C_PP[2] * v1* v1*v2* v2
		+ C_PP[3] * v1* v1*v1* v1*v1* v1 + C_PP[4] * v2* v2*v2* v2*v2* v2;
	double B = 1 + C_PP[5] * v1* v1 + C_PP[6]* v2*v2+ C_PP[7] * v1* v1*v1* v1;
	double CC = 1 + C_PP[8] * v2* v2*v2* v2;
	return CC* exp(-A / B);
}

double RMs(double v)
{
	return	exp(5.339 - sqrt(28.504921 + 4 * v*v))*
		powf(0.2414 + sqrt(0.57547396 +
			0.01737124*v*v), 7);
}

double RMa(double v)
{
	double a, b;
	a = 0.1477 + sqrt(0.72641529 +
		0.01380625* v*v);
	b = 0.1477 + sqrt(0.7264152899999999 +
		0.0168922009*v*v);
	return (exp(5.339 - sqrt(28.504921 +
		4 * v*v))*
		(powf(a, 7.5) + powf(b, 5.5))) / 2.;
}

double RBSnp(double vA)
{
	double abs = 0.9982 + sqrt(sqr(0.0018) + sqr(0.3815* vA));
	double bbs = 0.3949 + sqrt(sqr(0.6051) + sqr(0.2666 *vA));
	return 1.0 / 2.732*(abs * exp(1.306 - sqrt(sqr(1.306) + vA *vA)) +
		1.732* powf(bbs, 7) * exp(3.303 - sqrt(sqr(3.303) + 4* vA *vA)));
}

double RBSnn(double vA)
{
	double cbs = 0.1747 + sqrt(sqr(0.8253)  + sqr(0.07933* vA));
	double dbs = 0.7333 + sqrt(sqr(0.2667)  + sqr(0.1678* vA) );
	return (cbs *cbs* exp(4.228 - sqrt(sqr(4.228) + sqr(2* vA)))
		+ pow(dbs, 7.5)* exp(7.762 - sqrt(sqr(7.762) + sqr(3* vA)))) / 2.0;
}

double J(double v1, double v2) {
	double JJ;

	double u = powf(v1, 2) + powf(v2, 2);
	double w = powf(v1, 2) - powf(v2, 2);
	double I0 = (457 * powf(M_PI, 6)) / 5040.;
	double p = (12.421 + u + sqrt(45.171 + 16.35*u + powf(w, 2))) / 2.;
	double q = (12.421 + u - sqrt(45.171 + 16.35*u + powf(w, 2))) / 2.;
	double ps = (u + sqrt(6.7737 + 5524.8*u + powf(w, 2))) / 2.;
	double pe = (0.43847 + u + sqrt(491.32 + 8.368*u + powf(w, 2))) / 2.;
	double K0 = (sqrt(p - q)*(6 * powf(p, 2) + 83 * p*q + 16 * powf(q, 2))) /
		120. - (sqrt(p)*q*(4 * p + 3 * q)*
			log((sqrt(p) + sqrt(p - q)) / sqrt(q))) / 8.;
	double K1 = (powf(M_PI, 2)*sqrt(p - q)*(p + 2 * q)) / 6. -
		(sqrt(p)*powf(M_PI, 2)*q*
			log((sqrt(p) + sqrt(p - q)) / sqrt(q))) / 2.;
	double K2 = 7* (powf(M_PI, 4)*sqrt(p - q)) / 60.;

	double S = ((K0 + K1 + 0.42232*K2)*sqrt(M_PI)*powf(ps, 0.25)) /
		(sqrt(2)*exp(sqrt(pe))*I0);
	double u1 = 1.8091 + sqrt(5.051705759999999 + powf(v1, 2));
	double u2 = 1.8091 + sqrt(5.051705759999999 + powf(v2, 2));
	double DD = 1.52*exp(-u1 - u2)*powf(u1*u2, 1.5)*
		(powf(u1, 2) + powf(u2, 2));
	JJ= DD + (S*u) / (0.9163 + u);
	return JJ;
}

double Ji(double vv) {
	return powf((0.2312 + sqrt(sqr(0.7688) + sqr(0.1438*vv))), 5.5)*
		exp(3.427 - sqrt(3.427 *3.427 + vv *vv));
};




//double KS10U[] = {2.24058, -2.11406,0.943641,1.16863,-1.51686}; 
//{2.27361,-2.04576,0.955046,1.22649, -1.54992};
double KS10U2[] =	{0.66,1.3,1.03439,3.30494};


double KS10p[] = {0.0,1.117,1.241,0.1473,20.29}; 

//
double KS10n[] = {0.0,0.6,1.45,0.1,10.2}; 
//double KS10n[] = {0.0,0.6,1.45,0.1,10.2}; 

double KP32n[] = {1.0,1.961,2.755,1.3,6.461};
double KP32nAV18[] = {
0.026012, - 0.12321,
 + 1.2067, - 0.22083,
 - 0.90847, + 0.71948, - 0.23145, + 0.035082,
 - 0.0020696 
};
double KS10pAV18[] = {-0.24157, + 10.346,- 12.188, + 5.3499, 
- 0.61758, - 0.28632,+ 0.11653,- 0.016486, + 0.00085089
}; 


static double n_b_H_nc = 0.68;//  in units of n0 = 1.904e14 g/cm^3


double RW_cool::GapAV18(double *KK, double n, int Z)
{
							//n in units of n0
	double u,Tc, Tc9;
	int k;
    u  =  1; 
	Tc9 = 0;
	if (n>4.15) return Tc9; 
	for (k = 0; k <= 8 ; k++)
	{
		Tc9 += KK[k]*u;
		u*= n;
	}

Tc = Tc9; 

if(Tc < ZERO) return 0.0;
if(Z==0) return Tc/0.57;
    else return Tc/0.84;
//return Tc;
}


// Experimental 3P2 gaps for neutrons


double RW_cool::Gap_BG(double n)
{
double GG,D0,n_max,Dn,kk;

n_max = 3;
Dn = 1;
D0 = 0.08; //MeV
kk = (n-n_max)/Dn;
if (n<n_max-Dn&&n>n_max+Dn) GG = 0; else 
GG = D0
//* exp(-kk*kk)
; 
	return GG;
}


double RW_cool::GapApp(double *KK, double n, int Z)
{
							//n in units of n0
	double u1,u2, Tc,k;
	k = 1.7 * pow(n,1.0/3.0);
	u1 =  k - KK[0];
	u2 =  KK[2] - k;
if(u1*u2>ZERO)
Tc = KK[4]*(u1*u1*u2*u2)/(u1*u1+KK[1]*KK[1])/(u2*u2+KK[3]*KK[3]); 
else  Tc = 0.0;
if(Z==0) return Tc/0.57;
    else return Tc/0.84;  // in units of T9
}


double RW_cool::GapUSP(double *KK, double n)
{
							//n in units of n0
	double u1,u2,u3, D , k, K_f;
	k = 1.6795 * pow(n,1.0/3.0);
	K_f = k;
	u1 =  K_f - KK[2];
	u2 =  KK[1] - KK[3]* K_f* K_f* K_f;
	u3 = KK[0] + KK[4]* K_f;
if(K_f>ZERO&&K_f< - KK[4])	D = K_f*u3* exp(u1*u1*u2);
else  D = 0.0;
return D/kT_0;
     // in units of T9
}

double RW_cool::GapUSP2(double *KK, double n)
{
							//n in units of n0
	double u1,u2,u3, D , k, K_f;
	k = 1.6795 * pow(n,1.0/3.0); //fm^-1
	K_f = k;
//	a*k*Exp[-b*(k - k0)^2]/(1 + (k/k1)^25)
	u1 =  K_f - KK[0];
	if(K_f>ZERO&&K_f< 2){
	u3 = exp(25*log(K_f/KK[1]));
	D = KK[2]*K_f* exp( - KK[3]*u1*u1)/(1 + u3);} // MeV
else  D = 0.0;
return D/kT_0;
     // in units of T9
}

double Kn1[] = { 0.18, 0.1, 1.2, 0.6, 8.8 };//GIPSF
double Kp1[]={0.15, 0.22, 1.05, 3.8,14};//AO
double Kp2[]={0.05, 0.07, 1.05, 0.16,1.69};//BCLL
double Kp3[]={0.0, 2.9, 0.8, 0.08 ,17};//BS
double Kp4[]={0.0, 9.0, 1.3, 1.5,102};//CCDK
double Kp5[]={0.0, 5.0, 1.1, 0.5,35};//CCYms 
double Kp6[]={0.0, 5.0, 0.95, 0.3,34};//CCYps
double Kp7[]={0.0, 0.57, 1.2, 0.35,4.5};//EEHO
double Kp8[]={0.0, 6.0, 1.1, 0.6,61};//EEHOr
double Kp9[]={0.15, 2.1, 1.2, 2.8,48}; //T


double Kn2[] = { 0.20, 1.5, 1.7, 2.5,28 };//AWP2
double Kn3[] = { 0.20, 2.0, 1.4, 2.0, 50 };//AWP3
double Kn4[] = { 0.18, 4.5, 1.08, 1.1,127 };//CCDK
double Kn5[] = { 0.18, 0.06, 1.3, 0.03, 2.2 };//CLS
double Kn6[] = { 0.18, 0.1, 1.2, 0.6, 8.8 };//GIPSF
double Kn7[] = { 0.18, 0.05, 1.4, 0.1, 2.45 };//MSH  
double Kn8[] = { 0.35, 1.7, 1.67, 0.06, 4.1 };//SCLBL 
double Kn9[] = { 0.10, 4.5, 1.55, 2.5,45 };//SFB
double Kn10[] = { 0.15, 3.0, 1.4, 3.0,69 };//WAP 

 
double KL1[] = { 0.1239, 1.072, 1.398, -1.120, 14.05 }; //TT1:
double KL2[] = { 0.1383, 0.9396, 1.243, -0.9451, 8.999 };//	 TTGM :
double KL3[] = {0.2177, 0.9827, 1.305, -0.8569, 5.151};//TNNDS :
double KL4[] = { 0.1821, 1.091, 1.389, -0.9477, 9.454 };//TNEhm 
double KL5[] = {0.2276, 1.308, 1.553, -0.9694, 10.07}; //TNFGA 

int jg = 8;

double RW_cool::GapPp(double *KK, double np)
{

 double fit,kf,x1,x2,F1,F2;
 fit=0;
	kf = 1.6795 * pow(np,1.0/3.0);
x1=kf-KK[2];
x2=kf-KK[0];
if(x1*x2<0){
    F2=x1*x1/(x1*x1 + KK[3]);
    F1=x2*x2/(x2*x2 + KK[1]);
	fit= KK[4]*F1*F2/kT_0; //in T9 units
};	
	return fit;
}




void RW_cool::Gaps(int o,double T){


double gap_scale = 1.0;

double mu_B,n_n_H,n_p_H; 
n_    = n_b[o]/n_0;
mu_B =mu_b[o];
  
if((Xi[o]>0)&&(Xi[o]<1)){ n_b_H = n_b[n_h_c]/n_0;
                           n_b_Q = n_b[n_q_c]/n_0;} else
{  n_b_H = (1-Xi[o])*n_;  
   n_b_Q =  Xi[o] *n_;
};
n_n_H = n_b_H*n_n[o];
n_p_H = n_b_H*n_p[o];

  T_h   = T*exp(- phi[o]);


if (normal_core) {  R_q =  ksi_q = supp =1; T_c = gap_q =0;}
else {

if(n_b_Q > ZERO) 
{
gap_q_0 = QGap(mu_B)/kT_0;
	T_c = 0.57 *gap_q_0;

gap_q_0_X=QGap_X(mu_B)/kT_0;
	T_c_X = 0.57 *gap_q_0_X;

}else T_c_X =T_c = 0; 

Xi_mix = Xi_mix_q(mu_B);


ksi_q= supp =R_q =ksi_q_X= supp_X =R_q_X =1;
gap_q = gap_q_X =0;   
if ((T_h<T_c)&&(n_b_Q>ZERO)) 
            { 
	m_g= m_gg*sqrt( 1 - T_h / T_c);
    gap_q = gap_q_0*sqrt(1 - T_h/T_c); 
    supp= exp(- gap_q/(T_h+ZERO));
    TTc = T_h/T_c;
              if(T_h>ZERO){
//              R_q=3.2 * ( 2.5/(TTc+ZERO) -1.7 + 3.6*TTc);
               R_q=3.1 *exp(- 5*log(TTc+ZERO)/2.0);
             ksi_q =  exp(-1.76*T_c/T_h);} 
                     else {printf("T<0: Something wrong"); 
					 exit(1);
					 };
               };

if ((T_h<T_c_X)&&(n_b_Q>ZERO)) 
            { 
    gap_q_X = gap_q_0_X*sqrt(1 - T_h/T_c_X); 
    supp_X= exp(- gap_q_X/(T_h+ZERO));

              if(T_h>ZERO){
             R_q_X = 3.1 *exp(- 5*log(T_h/T_c_X+ZERO)/2.0);
             ksi_q_X =  exp(-1.76*T_c_X/T_h);} 
                     else {printf("T<0: Something wrong"); 
//					 exit(1);
					 };
               };

};

//};

S10p = S10n =P32n = false;  
   T_c_p=ZERO;
   T_c_n=ZERO;
   T_c_l = ZERO;
   ksi_n=ksi_p=ksi=1;
   R_n=R_p=1;
   gap_N = gap_p = gap_n = ZERO;
   RMp = RMn = 1;
   R_D = R_D_h = 1;

if (!normal_shell) 
{  
/************************************************************ /
HBerg HJ;

  n_b_H = exp((2.8*i-600)*log(10)/300.0); 
	double xx=HJ.xB(n_b_H*n_0);
	n_p_H = xx*n_b_H;
	n_n_H = n_b_H - n_p_H ;
***************** Define proton pairing *****************************/
#ifdef _OLDGAP_
if ((n_b_H<n_g_pr)&(n_b_H>n_g_Ar)) 
						  {  
							  S10p = true;
							  gap_p_0 = gap_scale*gap_fps*0.25/kT_0 ;
			}   else  gap_p_0 = 0;
#else

//#ifdef _AV18_
	if(_GAP_=='F'){
double GG;
switch(_FGAP_){
case '1': GG = GapPp(Kp1,n_p_H); break;
case '2': GG = GapPp(Kp2,n_p_H); break;
case '3': GG = GapPp(Kp3,n_p_H); break;
case '4': GG = GapPp(Kp4,n_p_H); break;
case '5': GG = GapPp(Kp5,n_p_H); break;
case '6': GG = GapPp(Kp6,n_p_H); break;
case '7': GG = GapPp(Kp7,n_p_H); break;
case '8': GG = GapPp(Kp8,n_p_H); break;
case '9': GG = GapPp(Kp9,n_p_H); break;
default:GG = GapPp(Kp8,n_p_H);
	};
		gap_p_0 = gap_scale*gap_fps* GG; // has to be n_p
	}
else
if(_GAP_=='A')
gap_p_0 = gap_scale*gap_fps* GapAV18(KS10pAV18,n_b_H,0);
else
if(_GAP_=='U'){
gap_p_0 = 0;
if(n_b_H<3.1)gap_p_0 = gap_scale*gap_fps* GapUSP2(KS10U2,n_p_H);
}
else 
gap_p_0 = gap_scale*gap_fps* GapApp(KS10p,n_p_H,0);

/** /
if ((n_b_H<n_g_pr)&&(n_b_H<n_g_Ar)) 
						  {  
							  S10p = true;
							  gap_p_0 = gap_factor*0.25/kT_0 ;
			}   else  gap_p_0 = 0;
**********/

	double n_L = n_b_H*Y_L[o];
	double GGL;
	switch (_FGAPL_) {
	case '1': GGL = GapApp(KL1, n_L,0); break;
	case '2': GGL = GapApp(KL2, n_L,0); break;
	case '3': GGL = GapApp(KL3, n_L,0); break;
	case '4': GGL = GapApp(KL4, n_L,0); break;
	case '5': GGL = GapApp(KL5, n_L,0); break;
	default:GGL = 0.0;
	};
	gap_L = 1.0*GGL;

	T_c_l = 0.57*gap_L;

if(	gap_p_0 > ZERO)
S10p = true;
#endif

        T_c_p= 0.57* gap_p_0;

/***************** Define neutron pairing gap **************************/

#ifdef _OLDGAP_
double xxx;
if((n_b_H<n_g_Br)&&(n_b_H>n_g_Al))
{
	if(n_b_H<=n_g_Ar)
	{
		xxx = log(n_b_H*n_0_g);
        gap_n_0 = gap_scale*gap_fns* apra(GAP,logro,xxx,ngap)/kT_0;                
		S10n = true;
	//gap_n_0 = (-13458 + 1868.4 *xxx -97.152  + 2.242 *xxx*xxx*xxx -
//0.019372*xxx*xxx*xxx*xxx)/kT_0; 

     T_c_n= 0.57* gap_n_0;
     ksi=0.5;//0.45;
    } 
	else 
	{
		gap_n_0 = gap_scale*gap_fnp* apra(GAP,logro,log(n_b_H*n_0_g+ZERO),ngap)/kT_0;
        P32n = true;
 		T_c_n= 0.84* gap_n_0;
        R_n=R_p=1;
    };

};
#else

if(n_b_H<n_b_H_nc)
{
	double GGn;
		if(_GAP_=='U')
	gap_n_0 = gap_scale*gap_fns*GapUSP2(KS10U2,n_n_H);                
		else
		{
			switch (_FGAPn_) {
			case '1': GGn = GapPp(Kn1, n_n_H); break;
			case '2': GGn = GapPp(Kn2, n_n_H); break;
			case '3': GGn = GapPp(Kn3, n_n_H); break;
			case '4': GGn = GapPp(Kn4, n_n_H); break;
			case '5': GGn = GapPp(Kn5, n_n_H); break;
			case '6': GGn = GapPp(Kn6, n_n_H); break;
			case '7': GGn = GapPp(Kn7, n_n_H); break;
			case '8': GGn = GapPp(Kn8, n_n_H); break;
			case '9': GGn = GapPp(Kn9, n_n_H); break;
			case '0': GGn = GapPp(Kn10, n_n_H); break;
			default:GGn = GapApp(KS10n, n_n_H, 0);
			}
	gap_n_0 = gap_scale*gap_fns*GGn;
		}
//		gap_n_0 = gap_scale*gap_fns*GapPp(Kn1, n_n_H, 0);



	
		T_c_n= 0.57* gap_n_0;
     ksi=0.5;//0.45;
		S10n = true;

}
else
{		
        P32n = true;
//#ifdef _AV18_	
		if(_GAP_=='A')
	gap_n_0 = gap_scale*gap_fnp*GapAV18(KP32nAV18,n_b_H,1);
else
	gap_n_0 = gap_scale*gap_fnp*
	GapApp(KP32n,n_n_H,1); // Yakovlev original functions
//Gap_BG(n_n_H);

	T_c_n= 0.84* gap_n_0;

};
#endif

/******************************************************************** /
if (P32n) fprintf(test,"%g %g %g %g\n",n_b_H,0.0,gap_n_0
				  *kT_0
				  , gap_p_0
				  *kT_0
				  );
else fprintf(test,"%g %g %g %g\n",n_b_H,gap_n_0
			 *kT_0
			 , 0.0, 
			 gap_p_0
			 *kT_0
			 );
return;

/************* R Factors **********************************/

double vn = vx(T_h / T_c_n);
double vp = vx(T_h / T_c_p);

double vl = vx(T_h / T_c_l);

if (T_h < T_c_l) R_D_h = Ji(vl);


if (vn == 0 && vp == 0) {
	RMp = RMn = 1;
}else 
if (vn == 0) { RMp = RMs(vp); RMn = RMa(vp); }else
if (vp == 0) {
	RMp = RMa(vn); RMn = RMs(vn);
}
else { RMp = RM1(vn, vp); RMn = RM1(vp, vn); }
;
#ifndef _HYP_
R_D = J(vn, vp);
#endif // !_HYP_


/***************** Define Temperature dependance of hadronic gaps ******************/

if((T_h<T_c_n)&&S10n)
	{ 
				if(T_h>ZERO) { 
		        ksi_n=exp(-1.76*T_c_n/T_h);
				R_n  = RCF(T_h/T_c_n, S_CC); //Form(18) RA
//					3.1* exp( -5*log(T_h/T_c_n)/2)*  //Should be 
//					exp(- 1.76/T_h*T_c_n);
				}
				else
                R_n   =  ksi_n  = ZERO ;
	};

if((T_h<T_c_n)&& P32n)
	{
				if (T_h>ZERO)
                  {ksi_n=exp(-1.19*T_c_n/T_h);
                  R_n   = RCF(T_h/T_c_n , P_CC); //Form(18) RB
		            //0.78* exp( -2*log(T_h/T_c_n))* //Should be Form(18) RB
					//  exp( - 1.19/T_h*T_c_n);
                   }else ksi_n=    R_n =  ZERO;
	};

if((T_h<T_c_p)&& S10p)
	{
		
					if(T_h>ZERO)
                  {
						t_m = T_h/T_c_p  ;
						ksi_p =exp(-1.76/t_m);
					R_p = RCF(t_m, S_CC);
					 // 3.1* exp( -5*log(T_h/T_c_p)/2 )*
					 // exp(- 1.76/T_h*T_c_p);
                   }else ksi_p= R_p = ZERO;
	};

if (T_h<T_c_n) gap_n =  gap_n_0*sqrt_gap(T_h/T_c_n,S10n);
if (T_h<T_c_p) gap_p =  gap_p_0*sqrt_gap(T_h/T_c_p,true);

/******************************************************* /
double gnS = GapApp(KS10n,n_n_H,0)*0.57;
double gpS = GapApp(KS10p,n_p_H,0)*0.57;
double gnP = GapApp(KP32n,n_n_H,1)*0.84;
fprintf(test,"%g %g %g %g\n",n_b_H,gnS,gnP,gpS);

*******************************************************/

//fprintf(test,"%g %g %g\n",n_b_H,T_c_n,T_c_p);
//fprintf(test,"%g %g \n",n_b_H,gap_p_0*kT_0); // output in units of MeV
		if (gap_p<gap_n) gap_N = gap_n; else gap_N = gap_p;
};

/*                 PRINTING THE GAPS                     * / 
if (o==0){
	outtt= fopen("GAPS_UDD.dat","w");
	fprintf(outtt,"# gaps : n  p  q \n");
    fclose(outtt);
}

outtt= fopen("GAPS_UDD.dat","a");
//fprintf(outtt," %g %g %g %g %g %g %g\n",rr[o],gap_n*kT_0,gap_p*kT_0,gap_q*kT_0,T_c_n*kT_0,T_c_p*kT_0,T_c*kT_0);
fprintf(outtt," %g %g %g %g \n",n_b[o]/n_0,gap_n_0*kT_0,gap_p_0*kT_0,gap_q*kT_0); // output in units of MeV
fclose(outtt);
/* */
  }



/********************************************************/
void RW_cool::specific_heat(int o,double T){
double Cgamm,Cap_e,Cap_n,Cap_p,Cap_q,Cap_q_n,c_v_h,Cglu;

 n_    = n_b[o]/n_0;
 n_b_H = (1-Xi[o])*n_;  
 n_b_Q = Xi[o] *n_;
// Y_e[o]   = Y_e[o];
 T_h   = T*exp(- phi[o]);
m_n = ms_n[o]*z(n_b_H*n_n[o], ms_n[o]);
m_p = ms_p[o]*z(n_b_H*n_p[o], ms_n[o]);
 /*********************** HEAT Capacity **************************/

Cap_e = Ce * exp( 2*log(Y_e[o]*n_+ ZERO)/3 )*T_h ;
  
if (n_b_Q < ZERO) 
Cap_q = Cap_q_n = 0; 
else {

Cap_q_n = Cq *exp( 2*log(n_b_Q+ ZERO)/3 )*T_h ;

Cap_q = Cap_q_n* ((3 - 2*Xi_mix)*supp_X*R_q_X + 2.0*Xi_mix*supp* R_q)/3.0;


};

Cap_n= CN * exp(log(n_n[o]*n_b_H)/3.0) *m_n * T_h 
//*exp( - gap_n/(T_h + ZERO));
* R_n
;
Cap_p= CN * exp(log(n_p[o]*n_b_H)/3.0) *m_p * T_h 
//*exp( - gap_p/(T_h + ZERO));
* R_p
;


//if ((T_h<T_c_p)||(T_h<T_c_n)||(Xi[o]>ZERO)) 
  
Cgamm= Cg * T_h* T_h *T_h;
Cglu= Xi[o]*N_g*Cg * T_h* T_h *T_h;

c_v_h = 0
        + Cap_e 
        + Cglu
        + Cgamm
        + Cap_n 
        + Cap_p  
        + Cap_q 
;
//fprintf(test, "%10.8g %10.8g %10.8g  \n", 9.0 + log10(T_h), R_n, R_p);

//fprintf(test, "%10.8g %10.8g %10.8g %10.8g  \n", 9.0 + log10(T_h), Cap_n, Cap_p, c_v_h);

/*if(c_v_[o] < accc) {  //printf(" fast cooling: c_v =%g\r",c_v_h);
c_v_[o] =  Cap_q_n * Xi[o] + (1-Xi[o])*(Cap_e + Cgamm + Cap_n + Cap_p);} else 
*/

c_v_[o] = c_v_h;

//printf(" %g %g %g %g %g %g %g %g\n",rr[o],c_v_h,c_v_[o],Cap_e,Cap_q,Cap_n,Cap_p,Cgamm);

  c_v_[o] = c_v_h;

}
/***********************/ 


bool RW_cool::picon0()  //medium effects
{
	double 	nbh,fs;
	double Z1min, nb_max;
// n_b_H =0.1 + 0.02*i;
Z1=1;
C_kn = 1;
Z1min = 0.15;
nb_max = 6;
if(ifpicon&&(n_b_H >= 0.8)/*&&(n_b_H <= 5.2)*/)
{ 

	if(n_b_H < nc_m){ 
nbh = n_b_H;
Z1 = exp(-qw*exp(2.4*log(nbh - 0.8 + ZERO)));
	}else
	{
nbh = nc_m;
double Z10 = exp(-qw*exp(2.4*log(nbh - 0.8 + ZERO)));
Z1 = Z10*(1+exp(- exp(0.5*log(nbh - nc_m + ZERO))))/2.;
//Z1 = (Z1min - Z10) / (nb_max - nbh)*(n_b_H - nbh) + Z10;
	};
	 

	
	Ga = 1.0/(1 + C*pow(n_b_H,1.0/3.0));
	F1 = 1 + 3 * exp(2 * log(n_b_H) / 3.0)*Gk*Gk / (4 * Ga*Ga * Gw*Gw);

 
	PIF = 0.8e3 * F1 *Gw*Gw * exp(8 * log(n_b_H) / 3.0 + 6 * log(Ga) - 2 * log(alfa*alfa*Z1) - 2 * log(alfa*alfa*Z1 / (beta*beta)));

	C_kn = exp(1.5*log(Z1) - 4*log(Ga/0.4) - log(n_b_H/0.82));

//	fprintf(test,"%g %g %g %g\n",n_b_H,Z1,Ga,C_kn);
//printf("PIF=%g\n",PIF);
}

return 0;
}


bool RW_cool::picon1(int o)  //medium effects with pion condensate
{
	double 	nbh,fs;
//n_b_H = 0.1 + 0.02*i;
	C_kn = 1;

if(ifpicon&&(n_b_H >= 0.8)&&(n_b_H <= 5.2))
{ 

	nbh = n_b_H;
fs = 1.0;
/*                         * /	
if(n_b_H < 3.0){ 
	fs = 1.0;
	}else
	{
fs= pow(n_b_H/3.0,-3.3333333);
	};
/**********/
	Ga= 1.0/(1 +  C *pow(nbh,1.0/3.0));  

	F1= 1 + 3*exp(2*log(nbh)/3.0)/(4 * Ga * Ga);

 
if(nbh<=nc_m)	Z1= exp( - qw*exp(2.4*log(nbh - 0.8 + ZERO))); 
else  Z1= exp( - qw*exp(2.4*log(5.2 - nbh + ZERO))); // Omega tilda squer

C_kn = exp(1.5*log(Z1) - 4*log(Ga/0.4) - log(nbh/0.82));

//fprintf(test,"%g %g %g %g\n",nbh,Z1,Ga,C_kn);

	PIF = 0.8e3*F1 *  exp( 8*log(nbh)/3.0 + 8*log(Ga/alfa) - 4*log(Z1));

//printf("PIF=%g\n",PIF);
}

	if((n_b_H > nc_m)&&ifpicon)
	{
//		if(gap_p<gap_n) gap_N=gap_n;else gap_N = gap_p;
		double Sint_2 = sint2*(1.0 - 3.0)/(nc_m - 5.2)*(n_b_H - nc_m) + sint2;
Ga_pi= 1.0/(1 + 1.6 *pow(n_b_H,1.0/3.0));
eps_PU[o] = PU * sint2*pow(m_n,2.0)*pow(Ga_pi,4.0)*(2.43*pow(n_b_H,1.0/3.0))*
exp( 6* log(T_h+ZERO)) * exp(- gap_N/(T_h+ZERO));
	};

return 0;

}

double T_h_;

double R_F(double gap)
{
	double jj;
//	return exp(- gap / (T_h_ + ZERO));
	jj = Ji(vx(T_h_ / (0.57*gap + ZERO)));
	return jj;
}

double R_F(double gap1, double gap2)
{
	double JJ;
	double v1 = vx(T_h / (0.57*gap1+ZERO));
	double v2 = vx(T_h / (0.57*gap2+ZERO));
	//
	//	v1 = 12.3; v2 = 0;
	//	JJ = J(v1, v2);
	//v1 = 0; v2 = 12.3;
	//JJ = J(v1, v2);
	//v1 = 12.3; v2 = 12.3;
	//JJ = J(v1, v2);
	//v1 = 3.4; v2 = 12.3;
		JJ = J(v1, v2);
		//	return exp(- gap / (T_h_ + ZERO));
	return JJ;
}



void  RW_cool::emissivity(int o,double T){
double Is,eps_h,
m_n,m_p, Y_n, Y_p,
/* baryon masses */
m_L,
m_Sm,
m_So,
m_Sp,
m_Xm,
m_Xo;

//double 
//gap_N, 
//eps_DU_H;
n_ = n_b[o] / n_0;
n_b_H = (1 - Xi[o])*n_;
n_b_Q = Xi[o] * n_;
Y_n = n_n[o];
Y_p = n_p[o];


m_n = ms_n[o] * z(n_b_H*n_n[o], ms_n[o]);
m_p = ms_p[o] * z(n_b_H*n_p[o], ms_n[o]);

#ifdef _HYP_
double mr_L2N = 1116. / 938.;
double mr_S2N = 1193. / 938.;
double mr_X2N = 1318. / 938.;

m_L= ms_L[o]*z(n_b_H*Y_L[o], ms_L[o]* mr_L2N);
m_Sm= ms_Sm[o]*z(n_b_H*Y_Sm[o], ms_Sm[o]* mr_S2N);
m_So= ms_S0[o]*z(n_b_H*Y_S0[o], ms_S0[o]* mr_S2N);
m_Sp= ms_Sp[o]*z(n_b_H*Y_Sp[o], ms_Sp[o]* mr_S2N);
m_Xm= ms_Xm[o]*z(n_b_H*Y_Xm[o], ms_Xm[o]* mr_X2N);
m_Xo= ms_X0[o]*z(n_b_H*Y_X0[o], ms_X0[o]* mr_X2N);

#endif


 T_h = T*exp(- phi[o]);


/********************** Emissivity***************************/

/************************************************************************
#ifdef _CFL_
eps_MU_Q   = MU_Q * Xi[o]* exp( 8* log(T_h+ZERO) - 2*  gap_q/(T_h+ZERO));

eps_DU_Q   = DU_Q * alfa* n_b_Q *exp( 6* log(T_h+ZERO)+ log(Y_e[o])/3
                         -  gap_q/(T_h+ZERO));

eps_DU_Q_n = DU_Q * alfa* n_b_Q *exp( 6* log(T_h+ZERO)+ log(Y_e[o])/3);

eps_BS_Q   = BS_Q * alfa* Xi[o]* exp( 8* log(T_h+ZERO) - 2*  gap_q/(T_h+ZERO));

eps_JP_Q   = JP_Q * a_PBF* N_nu *exp(2*log(n_b_Q+ZERO)/3.0 
								   + 7* log(T_h+ZERO) - 2*  gap_q/(T_h+ZERO));
#else   
************************************/
double SF2 = ((3 - 2*Xi_mix)*exp( - 2*  gap_q_X/(T_h+ZERO)) + 2*Xi_mix*exp( - 2*  gap_q/(T_h+ZERO)))/3.0;
double SF1 = ((3 - 2*Xi_mix)*exp( -  gap_q_X/(T_h+ZERO)) + 2*Xi_mix*exp( -  gap_q/(T_h+ZERO)))/3.0;

eps_MU_Q = MU_Q * Xi[o]* exp( 8* log(T_h+ZERO))* SF2;

eps_DU_Q_n=DU_Q * alfa* n_b_Q *exp( 6* log(T_h+ZERO)+ log(Y_e[o])/3);
eps_DU_Q = eps_DU_Q_n*SF1;
eps_BS_Q = BS_Q * alfa* Xi[o]* exp( 8* log(T_h+ZERO))*SF2;
eps_JP_Q = JP_Q * a_PBF* N_nu * exp(2*log(n_b_Q+ZERO)/3.0 + 7* log(T_h+ZERO))*SF2;     


eps_gg = GG * Xi[o]*exp( 7*log(m_g+ZERO)/2 + log(1 + 3* T_h*kT_0/2.0/(m_g+ZERO))
            - m_g/(T_h*kT_0+ZERO) + 3* log(T_h+ZERO)/2.0);

eps_QPBF =0;
if(T_h < T_c){
Is = exp (7* log(gap_q*kT_0)- 2*gap_q/(T_h+ZERO) 
		  + log(3.14* T_h/ 4.0 / (ZERO + gap_q))/2);
eps_QPBF = QPBF * Is* exp(log(2*Xi_mix*n_b_Q/3+ ZERO)/3);
}  
if(T_h < T_c_X){
Is = exp (7* log(gap_q_X*kT_0)- 2*gap_q_X/(T_h+ZERO) 
		  + log(3.14* T_h/ 4.0 / (ZERO + gap_q_X))/2);
eps_QPBF += QPBF * Is* exp(log((3 - 2*Xi_mix)*n_b_Q /3+ ZERO)/3);
}  

/*********************************Hadrons emissivities ************************************/

eps_DU_H[o] = eps_MU_H_p[o] = eps_MU_H_n[o] = eps_PU[o] = 0;
eps_NPBFp[o] = eps_NPBFs[o] =0;
eps_PPBF[o] = 0;
eps_nnBS[o] = eps_npBS[o] = eps_ppBS[o] = 0;
if(n_b_H > ZERO)
{
PIF = 1;
//#ifndef _PI_
if(!ifPI) picon0(); else picon1(o);


//printf("OK picon\n");

//return ;

//double gap_N;

//		if(gap_p<gap_n) gap_N=gap_n;else gap_N = gap_p;

/*                   Modified URCA                    */
double RMnExp = exp(-
	//						(gap_n + gap_p)
	//						2* gap_n // Khodel-Clark case
	(gap_n + gap_N)
	/ (T_h + ZERO));

double RMpExp = exp(-
	//						2* gap_p
	//						(gap_n + gap_p) //Khodel-Clark case
	(gap_N + gap_p)
	/ (T_h + ZERO));

eps_MU_H_n[o] = PIF* MU_H_n * Y_e[o]*n_b_H*exp(3*log(m_n+ZERO)+ log(m_p+ZERO) + 8* log(T_h+ZERO)
                        -2*log(Y_e[o]*n_b_H+ZERO*ZERO)/3)*RMn;

eps_MU_H_p[o] = PIF* MU_H_p * Y_e[o]*n_b_H*exp(3*log(m_p+ZERO)+ log(m_n+ZERO) + 8* log(T_h+ZERO)
                        -2*log(Y_e[o]*n_b_H+ZERO*ZERO)/3)*RMp;

//fprintf(test, "%10.8g %10.8g %10.8g %10.8g %10.8g %10.8g %10.8g \n",
//	9.0 + log10(T_h), eps_MU_H_n[o], eps_MU_H_p[o], RMn, RMp, RMnExp, RMpExp);


/**************************  DU PROCESS  ****************************************/
#ifndef _HYP_
double cond_DU =pow(1 - Y_p,1/3.0)-pow(Y_e[o],1/3.0)-pow( Y_p,1/3.0);

if( cond_DU<ZERO)
{
//if(gap_p<gap_n) gap_N=gap_n;else gap_N = gap_p;


	eps_DU_H[o] = DU_H* R_D* exp(log(Y_e[o] * n_b_H + ZERO) / 3.0
//	- gap_N/(T_h +ZERO) 
	+					   6 * log(T_h +ZERO) + log(m_n) + log(m_p)); 
if(fabs(Y_e[o] - Y_p)<ZERO) eps_DU_H[o]*=2;

}else 
eps_DU_H[o] = 0;
#else
	   /********************-  DU with Hyperons    *******************/

// NEW PIECE of CODE
/*=================================================*/
/* Index list */
// * Concentrations */
// Y_p protons
// Y_L Lambda
// Y_Sm Sigma^-
// Y_So Sigma^0
// Y_Sp Sigma^+
// Y_Xm Xi^-
// Y_Xo Xi^0
/***********************************/
// _np   usual DU
// _Lp   Lambda-->p l nu
// _Sn   Sigma^- --> n l nu
// _SL   Sigma^- --> Lambda l nu
// _SS   Sigma^- --> Sigma^0 l nu
// _XL   Xi^-  --> Lambda l nu
// _XS   Xi^- --> Sigma^0 l nu
// _XSo  Xi^0 --> Sigma^+ l nu
// _XX   Xi^- --> Xi^0 l nu
/*********************************/
/* parameters to be defined before */

/* gaps parameters */


double gap_Sm=0.0;
double gap_So=0.0;
double gap_Sp=0.0;
double gap_Xm=0.0;
double gap_Xo=0.0;

/*******/
double Y_n = 1 - Y_p - Y_L[o] - Y_Sm[o] - Y_S0[o] - Y_Sp[o] - Y_Xm[o] - Y_X0[o];
/*********************************/
//double DU_H   = 4.0e27; /** (erg/cm^3/sec) **/ 
//* triangle inequlities *//
double cond_DUnp1 = pow(Y_n,1/3.0) - pow(Y_e[o],1/3.0) - pow(Y_p,1/3.0);
double cond_DUnp2 = fabs(pow(Y_p,1/3.0) - pow(Y_e[o],1/3.0))- pow(Y_n,1/3.0);

double cond_DULp1 = pow(Y_L[o],1/3.0) -pow(Y_e[o],1/3.0)-pow(Y_p,1/3.0);
double cond_DULp2 = fabs(pow(Y_p,1/3.0) -pow(Y_e[o],1/3.0))-pow(Y_L[o],1/3.0);

double cond_DUSn1 = pow(Y_Sm[o],1/3.0)-pow(Y_e[o],1/3.0)-pow(Y_n,1/3.0);
double cond_DUSn2 = fabs(pow(Y_n,1/3.0)-pow(Y_e[o],1/3.0))-pow(Y_Sm[o],1/3.0);

double cond_DUSL1 = pow(Y_Sm[o],1/3.0)-pow(Y_e[o],1/3.0)-pow(Y_L[o],1/3.0);
double cond_DUSL2 = fabs(pow(Y_L[o],1/3.0)-pow(Y_e[o],1/3.0))-pow(Y_Sm[o],1/3.0);

double cond_DUSS1 = pow(Y_Sm[o],1/3.0)-pow(Y_e[o],1/3.0)-pow(Y_S0[o],1/3.0);
double cond_DUSS2 = fabs(pow(Y_S0[o],1/3.0)-pow(Y_e[o],1/3.0))-pow(Y_Sm[o],1/3.0);

double cond_DUXL1 = pow(Y_Xm[o],1/3.0)-pow(Y_e[o],1/3.0)-pow(Y_L[o],1/3.0);
double cond_DUXL2 = fabs(pow(Y_L[o],1/3.0)-pow(Y_e[o],1/3.0))-pow(Y_Xm[o],1/3.0);

double cond_DUXS1 = pow(Y_Xm[o],1/3.0)-pow(Y_e[o],1/3.0)-pow(Y_S0[o],1/3.0);
double cond_DUXS2 = fabs(pow(Y_S0[o],1/3.0)-pow(Y_e[o],1/3.0))-pow(Y_Xm[o],1/3.0);

double cond_DUXSo1= pow(Y_X0[o],1/3.0)-pow(Y_e[o],1/3.0)-pow(Y_Sp[o],1/3.0);
double cond_DUXSo2= fabs(pow(Y_Sp[o],1/3.0)-pow(Y_e[o],1/3.0))-pow(Y_X0[o],1/3.0);

double cond_DUXX1 = pow(Y_Xm[o],1/3.0)-pow(Y_e[o],1/3.0)-pow(Y_X0[o],1/3.0);
double cond_DUXX2 = - pow(Y_Xm[o],1/3.0)+fabs(pow(Y_e[o],1/3.0)-pow(Y_X0[o],1/3.0));


/* relative coefficients * /

double R_np=1;
double R_Lp=0.0394;
double R_Sn=0.0125;
double R_SL=0.2055;
double R_SS=0.6052;
double R_XL=0.0175;
double R_XS=0.0282;
double R_XSo=0.0564;
double R_XX=0.2218;

/* relative coefficients which include weak couplings and mass ratios (m_B1* m_B2)/m_n^2 */

double R_np=1;
double R_Lp=0.0394*mr_L2N;
double R_Sn=0.0125*mr_S2N;
double R_SL=0.2055*mr_S2N*mr_L2N;
double R_SS=0.6052*mr_S2N*mr_S2N;
double R_XL=0.0175*mr_X2N*mr_L2N;
double R_XS=0.0282*mr_X2N*mr_S2N;
double R_XSo=0.0564*mr_X2N*mr_S2N;
double R_XX =0.2218*mr_X2N*mr_X2N;
//************************************************************//

//* pairing effects *//
double gap_Lp,gap_Ln,gap_Sn,gap_SL,gap_SS,gap_XL,gap_XS,
gap_XSo,gap_XX;

if(gap_p<gap_n) gap_N=gap_n; else gap_N = gap_p;
T_h_ = T_h;
double 
Rp_np= 
#ifndef _R1_ 
R_F(gap_n,gap_p)
#else
//R_F(gap_N)
//exp( - gap_N/(T_h +ZERO))
ZERO
#endif
;

if(gap_p<gap_L) gap_Lp=gap_L; else gap_Lp = gap_p;
double Rp_Lp =
#ifndef _R1_ 
R_F(gap_L, gap_p)
#else
//R_F(gap_Lp)
//exp( - gap_Lp/(T_h +ZERO))
ZERO
#endif

;

if(gap_Sm<gap_n) gap_Sn=gap_n;else gap_Sn = gap_Sm;
double Rp_Sn= 
#ifndef _R1_ 
R_F(gap_n, gap_Sm)
#else
//R_F( gap_Sn)
//exp( - gap_Sn/(T_h +ZERO))
ZERO
#endif

;

if(gap_Sm<gap_L) gap_SL=gap_L;else gap_SL = gap_Sm;
double Rp_SL= 
#ifndef _R1_ 
R_F(gap_Sm, gap_L)
#else
//R_F(gap_SL)
//exp( - gap_SL/(T_h +ZERO))
ZERO
#endif

;

if(gap_Sm<gap_So) gap_SS=gap_So;else gap_SS = gap_Sm;
double Rp_SS= 
#ifndef _R1_ 
R_F(gap_Sm, gap_So)
#else
//R_F(gap_SS)
//exp( - gap_SS/(T_h +ZERO))
ZERO
#endif


;

if(gap_Xm<gap_L) gap_XL=gap_L;else gap_XL = gap_Xm;
double Rp_XL= 
#ifndef _R1_ 
R_F(gap_Xm, gap_L)
#else
//R_F( gap_XL)
//exp( - gap_XL/(T_h +ZERO))
ZERO
#endif


;

if(gap_Xm<gap_So) gap_XS=gap_So;else gap_XS = gap_Xm;
double Rp_XS= 
#ifndef _R1_ 
R_F(gap_Xm, gap_Xo)
#else
//R_F( gap_XS)
//exp( - gap_XS/(T_h +ZERO))
ZERO
#endif


;

if(gap_Xo<gap_Sp) gap_XSo=gap_Sp;else gap_XSo = gap_Xo;
double Rp_XSo= 
#ifndef _R1_ 
R_F(gap_Xo, gap_Sp)
#else
//R_F(gap_XSo)

//exp( - gap_XSo/(T_h +ZERO))
ZERO
#endif

;

if(gap_Xm<gap_Xo) gap_XX=gap_Xo;else gap_XX = gap_Xm;
double 
Rp_XX = 
#ifndef _R1_ 
R_F(gap_Xm, gap_Xo)
#else
//R_F(gap_XX)
//exp( - gap_XX/(T_h +ZERO))
ZERO

#endif

;

double zro = 0.0;

eps_DU_np[o] =
eps_DU_Lp[o] =
eps_DU_Sn[o] =
eps_DU_SL[o] =
eps_DU_SS[o] =
eps_DU_XL[o] =
eps_DU_XS[o] =
eps_DU_XSo[o] =
eps_DU_XX[o] = 0.0;
//* emissivities *//
if (cond_DUnp1 < zro && cond_DUnp2 < zro) eps_DU_np[o] = DU_H*R_np* Rp_np* exp(log(Y_e[o] * n_b_H + ZERO) / 3.0 + 6 * log(T_h + ZERO) + log(m_n) + log(m_p));
if (cond_DULp1 < zro && cond_DULp2 < zro) eps_DU_Lp[o] = DU_H*R_Lp* Rp_Lp* exp(log(Y_e[o] * n_b_H + ZERO) / 3.0 + 6 * log(T_h + ZERO) + log(m_L) + log(m_p));
if (cond_DUSn1 < zro && cond_DUSn2 < zro) eps_DU_Sn[o] = DU_H*R_Sn* Rp_Sn* exp(log(Y_e[o] * n_b_H + ZERO) / 3.0 + 6 * log(T_h + ZERO) + log(m_Sm) + log(m_n));
if (cond_DUSL1 < zro && cond_DUSL2 < zro) eps_DU_SL[o] = DU_H*R_SL* Rp_SL* exp(log(Y_e[o] * n_b_H + ZERO) / 3.0 + 6 * log(T_h + ZERO) + log(m_Sm) + log(m_L));
if (cond_DUSS1 < zro && cond_DUSS2 < zro) eps_DU_SS[o] = DU_H*R_SS* Rp_SS* exp(log(Y_e[o] * n_b_H + ZERO) / 3.0 + 6 * log(T_h + ZERO) + log(m_Sm) + log(m_So));
if (cond_DUXL1 < zro && cond_DUXL2 < zro) eps_DU_XL[o] = DU_H*R_XL* Rp_XL* exp(log(Y_e[o] * n_b_H + ZERO) / 3.0 + 6 * log(T_h + ZERO) + log(m_Xm) + log(m_L));
if (cond_DUXS1 < zro && cond_DUXS2 < zro) eps_DU_XS[o] = DU_H*R_XS* Rp_XS* exp(log(Y_e[o] * n_b_H + ZERO) / 3.0 + 6 * log(T_h + ZERO) + log(m_Xm) + log(m_So));
if (cond_DUXSo1< zro && cond_DUXSo2< zro) eps_DU_XSo[o] = DU_H*R_XSo*Rp_XSo*exp(log(Y_e[o] * n_b_H + ZERO) / 3.0 + 6 * log(T_h + ZERO) + log(m_Xo) + log(m_Sp));
if (cond_DUXX1 < zro && cond_DUXX2 < zro) eps_DU_XX[o] = DU_H*R_XX* Rp_XX* exp(log(Y_e[o] * n_b_H + ZERO) / 3.0 + 6 * log(T_h + ZERO) + log(m_Xm) + log(m_Xo));
eps_DU_H[o] = 0;

eps_DU_H[o] += eps_DU_np[o];
eps_DU_H[o] += eps_DU_Lp[o];
eps_DU_H[o] += eps_DU_Sn[o];
eps_DU_H[o] += eps_DU_SL[o];
eps_DU_H[o] += eps_DU_SS[o];
eps_DU_H[o] += eps_DU_XL[o];
eps_DU_H[o] += eps_DU_XS[o];
eps_DU_H[o] += eps_DU_XSo[o];
eps_DU_H[o] += eps_DU_XX[o];

#endif

/*************************************************************/
if(T_h < T_c_n ) {

	double eps1,Ksn;
	
Is = exp(7* log(gap_n*kT_0)- 2*gap_n/(T_h +ZERO)+ log(3.14* T_h/ 4.0 /(ZERO + gap_n))/2);
eps1= NPBF * ksi * Is* exp(    log(Y_n*n_b_H+ ZERO)/3 + log(m_n));

if(P32n) eps_NPBFp[o] =eps1; 
else
{Ksn = 0.2* exp(log(2.0*Y_n*n_b_H+ ZERO)/3.0);
eps_NPBFs[o] =eps1*Ksn;
}

}  


if(T_h < T_c_p){

double 	Y_factor = 
//0.5 * pow(Y_p*n_b_H,2.0/3.0)*
1;
 double Ksp = 0.2* exp(log(2.0*Y_p*n_b_H+ ZERO)/3.0);

Is = exp (7* log(gap_p*kT_0)- 2*gap_p/(T_h+ZERO) + log(3.14* T_h/ 4.0 /(ZERO + gap_p))/2);

eps_PPBF[o] = PPBF * Y_factor* Ksp* Is* exp( 2* log(Y_p*n_b_H+ ZERO)/3 + log(m_p));

}  
       else  eps_PPBF[o] = 0;
/**********************************************************/
//} else { eps_DU_H =  eps_MU_H_p = eps_MU_H_n = 0;
//         eps_PPBF = eps_PPBF = 0; 
//};
/*********************   BS processes   *************************************/

//double  eps_MpBS = 0;
//double eps_pBS = 0;
//double  ksip2 = exp( - 2*gap_p/(T_h+ZERO));
double  Ipp, Inp, Inn;

Ipp = Inp = Inn = 1.0;
if(ifpicon&&n_b_H>1)
{
double  Gamma_w_2 = 1.0;
double  Gamma_s_4  =  Ga*Ga*Ga*Ga;
double  omega = sqrt(Z1);
//n_b_H = 1.;

 
double GG = Gamma_w_2 *Gamma_s_4 *n_b_H *exp( - 4 * log(omega));
//Y_e0 = 0.044;
Inn = 67.6 *GG * omega *exp(- log(Y_n + ZERO) / 3);
Inp = 53.3 *GG  *exp(- log(Y_e[o]/Y_e0 + ZERO) / 3 + 2* log(n_b_H + ZERO) / 3);
Ipp = exp(log(Y_n / (Y_p/Y_e0 + ZERO)) / 3.)*Inn;

}
else  Ipp = Inp = Inn = 1.0; 
double ab_nn = 0.33;
double ab_np = 0.7;
double ab_pp = 0.77;

	eps_nnBS[o] = ab_nn * PnnBS * RBSnn(vx(T_h/T_c_n))
		*Inn 
		*exp(8 * log(ZERO + T_h)
		+ log(Y_n*n_b_H + ZERO) / 3 + 4 * log(m_n));
	eps_npBS[o] =ab_np* PnpBS * RBSnp(vx(T_h / T_c_p))*RBSnp(vx(T_h / T_c_n))
		*Inp
		*exp(8 * log(ZERO + T_h)
		+ log(Y_e[o]*n_b_H + ZERO) / 3 + 2 * log(m_n) + 2 * log(m_p));
	eps_ppBS[o] = ab_pp* PppBS * RBSnn(vx(T_h / T_c_p))
		*Ipp
		*exp(8 * log(ZERO + T_h)
		+ log(Y_p*n_b_H + ZERO) / 3 + 4 * log(m_p));

if(!ifPI) eps_PU[o] = 0;

}
/******************************   ****************************/
       

eps_ee[o] = ee * exp( log(Y_e[o]*n_+ ZERO)/3 + 6* log(T_h+ZERO)); 


eps_h = 0 
        + eps_ee[o] 
		+ eps_gg
        + eps_PU[o] 
		+ (eps_PPBF[o] + eps_NPBFp[o] + eps_NPBFs[o])
		//*0.1 
        + eps_MU_H_p[o] + eps_MU_H_n[o] 
        + eps_ppBS[o] + eps_npBS[o] + eps_nnBS[o]

        + eps_DU_H[o]
		+ eps_BS_Q 
		+ eps_DU_Q  
		+ eps_MU_Q 
		+ eps_QPBF 
		
		//+eps_JP_Q
;

//if (o==0) printf(" gaps : n  p  q \n");
//printf(" %g %g %g %g %g %g %g\n",
//rr[o],eps_ee,eps_PPBF,eps_NPBF,eps_MU_H_p,eps_MU_H_n,(eps_BS_Q + eps_DU_Q + eps_MU_Q));

/***********************/ 
supp_[o] = supp;
if (c_v_[o] < accc) 
 eps_[o] = eps_DU_Q_n + eps_ee[o] + eps_PPBF[o] + eps_NPBFp[o] + eps_NPBFs[o]+ eps_MU_H_p[o] + eps_MU_H_n[o] ; else
 eps_[o] = eps_h;

//printf("OK eps_h %g eps_DU_H %g\n",eps_h,eps_DU_H);


double c_time = c_v_[o]*T_h/eps_[o];
double c_time_DU= c_v_[o]*T_h/(eps_DU_H[o]+ZERO);
//double _PU = eps_PU*eps00;
double _DU = eps_DU_H[o]*eps00;
double M_DU= (eps_MU_H_p[o] + eps_MU_H_n[o])*eps00;
//fprintf(test,"%g %g %g\n",log10(T_h)+9,n_b_H,_c_time);
//printf("OK c_time\n");

}

 /*********************** HEAT Conductivity **************************/

double RW_cool::Rc(double y)
{
	double rr;
	static double cc0= 0.35;
	static double cc= 1.39;
	if (normal_shell||y<ZERO) return 1;
	rr= pow(0.65 + cc0 *sqrt(1 + 0.11 *(y/cc0)*(y/cc0)),1.5)
		*exp( cc*(1 - sqrt(1 + (y/cc)*(y/cc))));

return rr;
}

double RW_cool::lnRn2(double y)
{
	double rr;

	static double cc0[]= {0.38,0.012};
	static double cc[]= {3.67,7.54};

	static double cc1= 1.39;

		if (normal_shell||y<ZERO) return 0;

double ex= - 2*cc1*(1 - sqrt(1 + (y/cc1)*(y/cc1)));
double ex1 = cc[0]*(1 - sqrt(1 + 4*(y/cc[0])*(y/cc[0])));
ex += ex1;
ex1 = cc[1]*(1 - sqrt(1 + 9*(y/cc[1])*(y/cc[1]))) - ex1; 
 	rr= ex + log(0.5*pow(0.62 + cc0[0] *sqrt(1 + 0.072 *(y/cc0[0])*(y/cc0[0])),3)+
		0.5*pow(1 + cc0[1] *y*y,9.0)*exp(ex1));
return rr;
}

double RW_cool::lnRn1(double y)
{
	double rr;

	static double cc0[]= {0.053,1.35};
	static double cc[]= {0.38,0.17};

if (normal_shell||y<ZERO) return 0;
	static double cc1= 1.39;
double ex= - 2*cc1*(1 - sqrt(1 + (y/cc1)*(y/cc1)));
double ex1 = cc[0]*(1 - sqrt(1 + 4*(y/cc[0])*(y/cc[0])));
ex += ex1;
ex1 = cc[1]*(1 - sqrt(1 + 9*(y/cc[1])*(y/cc[1]))) - ex1;

	rr= ex + log(2.0/3.0*pow(0.95 + cc0[0] *sqrt(1 + 0.53 *(y/cc0[0])*(y/cc0[0])),3)
		+1.0/3.0*pow(1 + cc0[1] *y*y,2)
		*exp(ex1));

	return rr;
}
 
double RW_cool::lnRp1(double yn, double yp)
{
	double rr,ymin,ypl;
	static double aa[] = {0.78,0.48,0.11,0.05,0.085,0.013,0.22,0.35,0.22,-0.61,0.74,1.485};
	static double bb[] = {0.45,0.55,0.03,2.12,0.37,0.63,0.01,2.45,0.19,4.66};

	if (normal_shell) return 0;
	static double cc1= 1.39;
double ex= -2*cc1*(1 - sqrt(1 + (yn/cc1)*(yn/cc1)));

if(yn*yp>ZERO*ZERO){

double un = sqrt(yn*yn + aa[11]*aa[11])- aa[11];
double up = sqrt(yp*yp + aa[11]*aa[11])- aa[11];
if(yn>yp){ ymin = yp; ypl = yn;} else {ymin = yn; ypl = yp;}; 
double umin = sqrt(ymin*ymin + aa[11]*aa[11])- aa[11];
double upl = sqrt(ypl*ypl + aa[11]*aa[11])- aa[11];

	rr= ex - upl - umin + 
		log(
		(aa[0] + aa[1]*un+ aa[2]*up+ aa[3]*un*un+ aa[4]*un*up+ aa[5]*un*un*up)
	  + (aa[6] + aa[7]*upl+ aa[8]*umin+ aa[9]*un*umin+ aa[10]*up*upl)*exp(umin-upl)
	  
	 );

/***********************************************************/
	}else
	{
		if(yn>ZERO)
		{
			rr= 2.0* log(bb[0] + sqrt(bb[1]*bb[1] + bb[2]*yn*yn))
				+ ex + bb[3]*(1 - sqrt(1 +(yn/bb[3])*(yn/bb[3]))); 
		
		}else{if(yp>ZERO){
		
	rr=0.5*(bb[4] + sqrt(bb[5]*bb[5] + bb[6]*yp*yp));
	
double ex1 = bb[9]*(1 - sqrt(1 + 4*(yp/bb[9])*(yp/bb[9])))
           - bb[7]*(1 - sqrt(1 +(yp/bb[7])*(yp/bb[7])));
	
	rr+=0.5*pow(1+bb[8]*yp*yp,1.4)*exp(ex1);

		rr = ex + bb[7]*(1 - sqrt(1 +(yp/bb[7])*(yp/bb[7])))
+ log(rr);
		}else return 0;
				
		};
	};
/***************************************************/
	return rr;
}

double RW_cool::lnRp2(double yn, double yp)
{
	double rr,ymin,ypl;
static double aa[] = {1.1,0.86,0.2,0.079,0.15,0.013,-0.1,-0.23,0.062,0.75,-1.0,1.76};
static double bb[] = {0.8,0.2,0.046,2.36,-3.35,4.35,19.55,2.02,0.065,8.99,0.89,9.63};

	if (normal_shell) return 0;
	static double cc1= 1.39;

double ex= - 2*cc1*(1 - sqrt(1 + (yn/cc1)*(yn/cc1)));

if(yn*yp>ZERO*ZERO){
double un = sqrt(yn*yn + aa[11]*aa[11])- aa[11];
double up = sqrt(yp*yp + aa[11]*aa[11])- aa[11];
if(yn>yp){ ymin = yp; ypl = yn;} else {ymin = yn; ypl = yp;}; 
double umin = sqrt(ymin*ymin + aa[11]*aa[11])- aa[11];
double upl = sqrt(ypl*ypl + aa[11]*aa[11])- aa[11];

rr=
	(ex - upl-umin)+log(
		(aa[0] + aa[1]*un+ aa[2]*up+ aa[3]*un*un+ aa[4]*un*up+ aa[5]*un*un*up)
	  + (aa[6] + aa[7]*upl+ aa[8]*un*upl+ aa[9]*un*umin+ aa[10]*up*upl)
		*exp(umin - upl));
		;

/*************************************************************/
	}else
	{
		if(yn>ZERO)
		{
rr = 2.0* log(bb[0] + sqrt(bb[1]*bb[1] + bb[2]*yn*yn)) + 
     ex + bb[3]*(1 - sqrt(1 +(yn/bb[3])*(yn/bb[3]))); 
	
		}else{if(yp>ZERO){



double ex1 =  bb[9]*(1 - sqrt(1 + 1.5*(yp/bb[9])*(yp/bb[9])))-
		      bb[7]*(1 - sqrt(1 +(yp/bb[7])*(yp/bb[7])));

double ex2 = bb[11]*(1 - sqrt(1 + 9*(yp/bb[11])*(yp/bb[11])))-
			 bb[7]*(1 - sqrt(1 +(yp/bb[7])*(yp/bb[7])));


rr = log(0.044*(bb[4] + sqrt(bb[5]*bb[5] + bb[6]*yp*yp))
		 + bb[8]*exp(ex1) + bb[10]*exp(ex2));



rr += ex + bb[7]*(1 - sqrt(1 +(yp/bb[7])*(yp/bb[7])));

		}else return 0;
		};
	};
/******************************************************************/
	return rr;
}

 void RW_cool::conductivity(int o,double T){
double kappa_ee,kappa_ep,
//kappa_nn,kappa_np,
kappa_n,kappa_e,kappa_q,kappa_q_n,
kappa_q_GB,kappa_g,
//        z_n,S_kp,S_kn,
		kappa;
//double z[] = {0.1,0.5,1,1.2,1.3,2.0,2.2,3.0,3.5,4.0};
static double Knu = 3.5e17;
double ztcn,ztcp, Sn1,Sn2,Kn1,Kn2,Sp1,Sp2,Kp1,Kp2,u;

double m_n,m_p,n_e,n_e_H;

 n_    = n_b[o]/n_0;
 
n_e = n_* Y_e[o];

n_b_H = (1-Xi[o])*n_;  
n_b_Q = Xi[o] *n_;
n_e_H = n_b_H * Y_e[o];
double n_n_H= n_n[o]*n_b_H;
double n_p_H= n_p[o]*n_b_H;
m_n = ms_n[o]*z(n_n_H,ms_n[o])+ ZERO;
m_p = ms_p[o] * z(n_p_H, ms_n[o]) + ZERO;

 T_h   = T*exp(- phi[o]);

//double SSS;

static double Kee_n = 4.3e23/k00;
static double CC = 1.2;
double yn,yp;

kappa_ee = Kee_n* CC* n_e /(T_h+ZERO); 

//T_c_n=0.3; 	
//T_c_p=1; 		
//S10pairing =true;
//P32pairing =true;	

//kappa_ee = Kee* n_/4 * Y_e[o]/0.01 /(T_h+ZERO); 

//printf("OKOK \n");
	yn=yp=0;


if ((n_b_Q<=ZERO)&&(n_b_H>0.4)){

ksi_p = 1;
	
//printf("T_c_n = %g T_c_p =%g \n",T_c_n,T_c_p);

		if((T_h/(T_c_n+ZERO)<1.0)&&P32n)
			{
			ztcn=T_h/(T_c_n+ZERO);
			yn = sqrt(1 - ztcn)*(0.79  + 1.19/(ztcn+ZERO));
			};

		if((T_h/(T_c_p+ZERO)<1.0)&&S10p)
		{
		ztcp=T_h/(T_c_p+ZERO);	
		yp = sqrt(1 - ztcp)*(1.46 - 0.16/(sqrt(ztcp+ZERO)) + 1.76/(ztcp+ZERO));
	    ksi_p = exp(- yp);
//			exp(- 1.76/ztcp);
		};

		if((T_h/(T_c_n+ZERO)<1.0)&&S10p)
			{ 
			ztcn=T_h/(T_c_n+ZERO);
			yn = sqrt(1 - ztcn)*(1.46 - 0.16/(sqrt(ztcn+ZERO)) + 1.76/(ztcn+ZERO));
			};

	
//printf("n_e_H = %g  \n",n_e_H);
	
static double Kep_n = 6.3e23/k00;
	kappa_ep = Kep_n* CC/(sqrt(m_p)+ZERO)*pow(n_e_H,7.0/6.0)//
		/(T_h+ZERO)/(ksi_p+ZERO);
//	kappa_ep = Kep* exp(log(n_b_H* Y_e[o]/0.04 +ZERO)/3)* T_h /(ksi_p+ZERO);
kappa_e= kappa_ee*kappa_ep/(kappa_ee + kappa_ep + ZERO);

} else kappa_e= kappa_ee;

double C_p_f=1.6795;
double Ke0 = 8.51e21/k00;
double k_fe_sq = C_p_f*C_p_f*pow(n_e_H,2.0/3.0);
double F_el;
if(T_h<T_c_p) F_el = 2.67/(exp(1.30019*T_h/(T_c_p+ZERO))-1+ZERO); else F_el = 1;

double kappa_e_new = Ke0*k_fe_sq*F_el;

kappa_e = kappa_e_new;
//printf("OKOK 2 \n");


if (n_b_H<0.1) kappa_e*=2;

/*******************************************/

if (n_b_H>ZERO){

/********************************  Biko **************** /
z_n=log(n_n[o]*n_b_H)/3;
S_kn=0.38*exp(-3.5*z_n)+3.7*exp(0.4*z_n);
S_kp=1.83*exp(-2*z_n)+1.43*exp(2*z_n)/(0.4+exp(8*z_n));


kappa_nn = Knn* exp(-4*log(m_n))*n_b_H *n_n[o]/4 /(T_h+ZERO) /S_kn * ksi_n;
kappa_np = kappa_nn;

*******************************************************/
/************************************************************** /
kappa_np = Knp* exp(-2*log(m_n)+2*log(n_b_H *n_n[o]/4+ZERO)/3 
                     - log(n_b_H *n_p[o]/0.04+ZERO)) * T_h /S_kp * ksi_p;
*********************************************************/
/********************************************************* /
kappa_n= kappa_nn*kappa_np/(kappa_nn + kappa_np + ZERO);

********************************************************/


static double an[] ={7.9,0.22,0.2,0.17,-0.079,0.088,0.11,0.0};
static double ap[] ={0.38,102.0,53.9,-0.71,0.25,9.4,-1.59,0.8,31.3,-0.00043,26.85,0.08,-0.59,0.24,0.58,0.88};

static double bn[] ={0.49,1.1,-0.23,0.016,-0.021,0.28,0.46,0.89,-0.55,-0.062,0.04,0.21};
static double bp[] ={0.00013,1.25,0.24,0.33,0.55,-0.32,0.098,0.044,1.1,0.12,0.16,0.39,-0.3};

double k_Fn = 1.7*pow(n_n_H,1.0/3.0);
double k_Fp = 1.7*pow(n_p_H,1.0/3.0);

//printf("OKOK 3 \n");


Sn2 = an[0]*pow(k_Fn,- 2.0)*(1+ an[1]*k_Fn + an[2]*k_Fn*k_Fn)/(1 - an[3]*k_Fn);
Sn1 = an[4]*pow(k_Fn,- 2.0)*(1+ an[5]*k_Fn + an[6]*k_Fn*k_Fn)/(1 - an[7]*k_Fn);

Sp2 = ap[0]*pow(k_Fp,4.0)*pow(k_Fn,- 5.5)*(1+ ap[1]*k_Fp + ap[2]*k_Fn)
	/(1 + ap[3]*k_Fn+ ap[4]*k_Fn*k_Fn + ap[5]*k_Fp*k_Fp + ap[6]*k_Fn*k_Fp);
Sp1 = ap[7]*k_Fp*pow(k_Fn,- 2.0)*(1+ ap[8]*k_Fp + ap[9]*k_Fp*k_Fp+ ap[10]*k_Fn + ap[11]*k_Fn*k_Fn)
	/(1 + ap[12]*k_Fn+ ap[13]*k_Fn*k_Fn + ap[14]*k_Fp*k_Fp + ap[15]*k_Fn*k_Fp);

//printf("OKOK 4 k_Fn %g\n", k_Fn);

    u = k_Fn - 1.56;

//printf("ok 1 u = %g \n",u);

Kn2 = (bn[0] + bn[1]*u*u +bn[2]*u*u*u+ bn[3]*k_Fp+ bn[4]*k_Fp*k_Fp + bn[5]*u*k_Fp)/m_n/m_n;
u = k_Fn - 1.67;
//printf("ok 2 u = %g \n",u);

Kn1 = (bn[6] + bn[7]*u*u +bn[8]*u*u*u+ bn[9]*k_Fp+ bn[10]*k_Fp*k_Fp + bn[11]*u*k_Fp)/m_n/m_n;

u = k_Fn - 2.12;
//printf("ok 3 u = %g \n",u);

Kp2 = (bp[0] + bp[1]*u*u +bp[2]*u*u*u+ bp[3]*k_Fp+ bp[4]*u*k_Fp +bp[5]*u*u*u*u+ bp[6]*u*u*k_Fp)/m_p/m_p;
u = k_Fn - 2.13;
//printf("ok 4 mp = %g \n",m_p);

Kp1 = (bp[7] + bp[8]*u*u + bp[9]*u*u*u+ bp[10]*k_Fp+ bp[11]*u*k_Fp 
   +bp[12]*u*u*u*u) /m_p /m_p;


//printf("ok Kp1 %g \n",Kp1);

double lnrn1,lnrn2,lnrp1,lnrp2;

lnrn1=lnRn1(yn);
lnrn2=lnRn2(yn); 
lnrp1=lnRp1(yn,yp);
lnrp2=lnRp2(yn,yp);

//printf("%g %g %g\n",log10(T_h)+9,lnrn1,lnrn2);

//fprintf(test,"%g %g %g\n",log10(T_h)+9,lnrn1,lnrn2);


double rcy2=   Rc(yn);
rcy2*=rcy2;

if(lnrn2>60)
 kappa_n = 0;

double nu_nn = Knu * pow(m_n,3.0)*T_h*T_h 
                 *exp(lnrn2)*(Sn2 *Kn2 + 3*Sn1*Kn1*(exp(lnrn1-lnrn2) - 1));
//printf("ok 3\n");
if(lnrp2>60) kappa_n = 0;
else{

	double nu_np = Knu * m_n*pow(m_p,2.0)*T_h*T_h * exp(lnrp2)*(Sp2 *Kp2 + 0.5*Sp1*Kp1*
												 (3* exp(lnrp1 - lnrp2) - 1));

	kappa_n = Kn* (1.0/m_n)*rcy2*(1e15/(
		nu_nn  
		+ 
		nu_np
		))*n_b_H*(T_h+ZERO); 
};

if(!ifPI) picon0(); else picon1(o);

if(ifpicon) kappa_n *= 1*C_kn;

#ifdef _PRINT_Kappa_
//printf("ok 4");
fprintf(test,"%g %g \n",log10(T_h)+9, log10(kappa_e*k00 + ZERO));
#endif

/*********************************************************/

} else kappa_n = 0 ;



if (n_b_H< 0.5)
{kappa_n=0;
kappa_e*= 1e2; 
//if (n_b_H< 0.05)
//kappa_e*= 1e4;
//if (n_b_H< 0.005)
//kappa_e= 10000;

}

kappa_g= Kap_g * T_h* T_h /*T_h*/ * Xi[o];

//#ifdef _CFL_
//kappa_q = Kq* exp(- log(alfa/0.4/3.14)/2)* n_b_Q /(T_h +ZERO) * ksi_q;
//kappa_q_GB = K_GB* T_h* T_h *T_h*Xi[o];
//#else

kappa_q_n = Kq* exp(- log(alfa/0.4/3.14)/2)* n_b_Q /(T_h +ZERO);

kappa_q = kappa_q_n * ((3 - 2*Xi_mix)*ksi_q_X + 2*Xi_mix* ksi_q)/3.0;

kappa= 0
       + kappa_n 
       + kappa_e  
       + kappa_g 
       + kappa_q
//#ifdef _CFL_
//	   + kappa_q_GB
//#endif
;

//kappa= kappa * kappa_g /(kappa + kappa_g + ZERO); 
#ifdef _PRINT_
printf(" r= %g Kappa=  %g %g %g  \n",rr[o],kappa_e*k00,kappa_q*k00,kappa_g*k00);
#endif

/****************************/  
if ((c_v_[o] < accc)&&(n_b_Q>ZERO)) 
k_[o]=kappa_q_n + kappa_n + kappa_e + kappa_g ; else
k_[o]=  kappa;

/****************************   END *************************************/
}
