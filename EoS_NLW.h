// EoS_NLW.h: interface for the EoS_NLW class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EOS_NLW_H__2D17FD2D_65F6_4B94_A529_9F11D79693D7__INCLUDED_)
#define AFX_EOS_NLW_H__2D17FD2D_65F6_4B94_A529_9F11D79693D7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "EoS_HB.h"
#include "nrutil_.h"
#include "mnewt.h"
#include <math.h>
#include <malloc.h>

#define _new_

//#define NONLIN 1
#define NORHO 0
#define OMEGADELTA 0
#define HYPERON 0
#define db double

#define hc3 (197.32705*197.32705*197.32705)
#define hc 197.32705
/*1=\hbar*c=hc*MeV*fm*/
#define zero 1.e-10

/*Coupleing constants for nucleons*/


#ifdef _new_
#define OMfac 4.0249
#else //old
#define OMfac 7.15103
#endif
//#define OMfac 4.030571507756633
/*[(gom/mom)^2]=fm^2*/
//Dima (Phys Rev. C68,015803, 2003) 4.0249
//G300(K=300) Glen Nucl.Phys.A493, 1989, 521
//G300 (from weber book) 4.73277
//G300 (from weber thesis) 4.7478
//GK300B180 (from weber book) 7.15103
//B180 <-> B^{1/4}=180 MeV <-> B=136.62499 MeV/fm^3
//Glen book K=250 m/m=0.8 4.233
//glen's(1992, PhRevC 45,844) 4.11

#ifdef _new_
#define SIGfac 8.62743
#else //old
#define SIGfac 11.79197
#endif
//#define SIGfac 8.639589166566292
/*[(gsig/msig)^2]=fm^2*/
//Dima 8.62743 
//G300 (from weber book) 9.03049
//G300 (from weber thesis) 9.0592
//GK300B180 (from weber book) 11.79197
//Glen book 9.134
//glen's 8.63

#ifdef _new_
#define RHOfac 3.41835
#else //old
#define RHOfac 4.40701
#endif
//#define RHOfac 3.4231716881980354
/*[(grho/mrho)^2]=fm^2*/
//Dima 3.41835
//G300 (from weber book) 4.8246
//G300 (from weber thesis) 4.84
//GK300B180 (from weber book) 4.40701
//Glen book 4.876
//glen's 4.54

#ifdef _new_
#define Bi 0.00867497
#else //old
#define Bi 0.00295
#endif
//Dima 0.0867487
//G300 (from weber thesis) 0.003305
//G300 (from weber book) 0.003305
//GK300B180 (from weber book) 0.00295
//Glen book 0.008804

#ifdef _new_
#define C 0.00805981
#else //old
#define C -0.00107
#endif

//Dima 0.0805981
//G300 (from weber book) 0.01529
//G300 (from weber thesis) 0.01529
//GK300B180 (from weber book) -0.00107
//Glen book 0.006917 -> may be 0.016 for best fit 

/*coupling constants for hyperons are sqrt(2./3.)= 0.81649658 of nucleon's ones Geln book*/

#define GH2GN 0.81649658

//extern double* Q1_;

/*Chemical equilibrums
mju_electron=mju_muon=mjue
mjui=mjuB-Ql_i*mjue 
i->{n,p,lambda,sigma+,sigma0,sigma-,ksi0,ksi-,omega,delta++,delta+,delta0,delta-,e,mju}
 
mjus_mtr={mjus_neutron-#0,mjus_proton,mjus_lambda,-#2
	mjus_sigma_plus,mjus_sigma0,mjus_sigma_minus,-#5
	mjus_ksi0,mjus_ksi_minus,mjus_omega,-#8
	mjus_delta_plus_2,mjus_delta_plus,mjus_delta0,mjus_delta_minus-#12
	mjue,mjumju}

n={	n_neutron-#0,n_proton,n_lambda,-#2
	n_sigma_plus,n_sigma0,n_sigma_minus,-#5
	n_ksi0,n_ksi_minus,n_omega,-#8
	n_delta_plus_2,n_delta_plus,nu_delta0,n_delta_minus-#12
	n_electron,n_muon,-#14}
*/

extern    double Ql_[15];

typedef double (*_NQtp_)(double mjuB,double mjue,double t,double *n);
typedef double (*_PQtp_)(double mjuB,double mjue,double t);

class EoS_NLW : public EoS_HB  
{
public:
	double Charge(double mu_B,double mu_E, double T);
    void sigomro(double mjuB,double mjue,double t,double *gsig,double *gom,double *gro);
	double musigomro(double mjuB,double t,int mix,_NQtp_ nq,_PQtp_ pq,int qi,double *QQ,double *gsig,double *gom,double *gro);
	double musigomro(double mjuB,double t,double *gsig,double *gom,double *gro);

    void ffields(double *gmtr,double *fmtr,double **fjacmtr);
    void fields(double *gmtr,double *fmtr,double **fjacmtr);
	double DNDSHHwalnl(double mjuB,double mjue,double T,double gsig,double gom,double gro,int mix,
		_NQtp_ nq,_PQtp_ sq,int qi,double *QQ,double *DNBDMJUB,double *DNBDT,double *DSDMJUB);
	void difer(double mjuB,double mjue,double T,double gsig,double gom,double gro,double *A,double *D,double *U,double *muX,double *dndmju,double *dndmass,double *dndt,double *dnsdmass,double *dnsdt,double *dsdt);
	double p_(double mjuB, double mjue, double T, double gsig, double gom, double gro);
	double s_(double mjuB, double mjue, double T, double gsig, double gom, double gro);
	double n_(double mjuB, double mjue, double T, double gsig, double gom, double gro, double *n,double* mjus_mtr, double* ms_mtr);
	EoS_NLW();
	virtual ~EoS_NLW();


	
double _GSIG_,_GOM_,_GRO_,
_MJUE_,_MJUB_,_T_,_NB_;
_NQtp_ _NQ_;
_PQtp_ _PQ_;
double *_QQ_;
int _MIX_,_qi_,_NUMb_;

};

#endif // !defined(AFX_EOS_NLW_H__2D17FD2D_65F6_4B94_A529_9F11D79693D7__INCLUDED_)
