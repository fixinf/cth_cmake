// InitConfig.cpp : implementation file
//

//#include "stdafx.h"
//#include "cooling_nlw.h"
//#include "Star_MK.h"
#include "InitConfig.h"
//#include "Cool_EoS.h"

#include <string.h>
#include <stdlib.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// InitConfig dialog
#include <math.h>


InitConfig::InitConfig()
	{

	//	CCool_EoS* pDlg = (CCool_EoS*)pParam;
	Nend=Num;
	num_t=NUMT;
	

	m_mu  =  1183;
	m_kT  =  1.0e-10;

EoS_type = 0;
noinit = true;
rw_c.normal_shell = m_std_c;

}



bool InitConfig::Cool_Star()
{
	int i;
	double lt,lT;

if(rw_c.EV_run(&rw_c.ni,&lt,&lT)) return 1;
//printf("oioi 2in  T %g\n",lT);

//printf("oioi 2in %d\n",rw_c.ni);

//	gcvt(exp(log(10.0)*lt),3,time_c);
//		gcvt(lt,10,llt_c);
	
	log10_t_yr[rw_c.ni]=lt;
    log_Ts_K[rw_c.ni]=lT;
	for (i=0;i<=Num;i++) {
		T[i] = rw_c.T_hat[i];
		r[i] = rw_c.rr[i];
	};
	return 0;
}

int InitConfig::Init_Profiles()
{   
	int i;
	rw_c.star_f = false;
	rw_c.pr_f = 1;
//	rw_c.read_initial_str();
	if( rw_c.begin_input()) return 1;
//	rw_c.ni = 0;
	
	rw_c.lgtin =  - 10.0;
	//     lgtout = 7.0; 
//	rw_c.lgtout = 8.0; 
	rw_c.t_in = exp(rw_c.lgtin*log(10.0));
	rw_c.tau = (rw_c.lgtout - rw_c.lgtin)*log(10.0) /NUMT;
    rw_c._time = rw_c.t_in;
	
	
	rw_c.critmix();
	
	for (i=0;i<=Num;i++) {
		T[i] = rw_c.T_hat[i];
		r[i] = rw_c.rr[i];
	};
	for (i=0;i<=NUMT;i++) {
		log10_t_yr[i]  = 	rw_c.tau*i + rw_c.lgtin ;
		log_Ts_K[i] = 4.5;
	};
	//   cont_cal_f = 1;	
	return 0;
}





bool InitConfig::Reset_Profiles()
{
		int i;
//	if(NOINIT)  return 1;
	SetFileName();
	rw_c.star_f = 0;
	rw_c.pr_f = 1;

//    rw_c.cont_cal_f = false;
	rw_c.ni = 0;
	if( rw_c.begin_input()) return 1;
	
	rw_c._time = rw_c.t_in;
	rw_c.critmix();
	
	rw_c.PTPk = 0;
//	  read_initial_str();
	
	  for (i=0;i<=Num;i++) {
	  this->T[i] = rw_c.T_in[i];
	  this->r[i] = rw_c.rr[i];
	  };
	  for (i=0;i<=NUMT;i++) {
	  log10_t_yr[i]  = 	rw_c.tau*i - 10;
	  log_Ts_K[i] = rw_c.min_lgT;
	  };
	  
/*	*/
	
return 0;
}


void InitConfig::SetFileName()
{

if(!m_std_c)  
{//m_qcore = false;
strcpy(rw_c.evolution_sufx, "_ev.dat");}
else{// m_qcore = false;
strcpy(rw_c.evolution_sufx,"_ev_std.dat");}



if(!rw_c.m_qcore&&!m_Qstar) strcpy(rw_c.File_Name0,"H_");
else strcpy(rw_c.File_Name0,"");

if(rw_c.m_qcore||m_Qstar){
	if(m_SM)
{
switch (m_F_)  
	{
	case 0: strcpy(rw_c.File_Name0, "SM_"); break; 
	case 1: strcpy(rw_c.File_Name0, "SL2_") ; break; 
	case 2: strcpy(rw_c.File_Name0, "SN_"); break; 
	case 3: strcpy(rw_c.File_Name0, "SGL_");  
	};
if(!m_cond) strcat(rw_c.File_Name0, "n_");
}
else
strcat(rw_c.File_Name0, "B_");
}
rw_c.m_cond = m_cond;
rw_c.m_FF =m_F_;
rw_c.m_SM =m_SM;

rw_c.m_WM =mw1;


}





int InitConfig::DoModal(char* Warning) 
{
//	m_intalert = Warning;
//	return CDialog::DoModal();
return 0;
}



//void InitConfig::OnStdC() 
//{
//	m_std_c = true;	
//}

/*
	free(T);
                  free(r);
	free(log_Ts_K);
	free(log10_t_yr);
	delete rw_c;
*/


/*
void InitConfig::OnSm() 
{
m_SM =true;
OnGf();	
}

void InitConfig::OnGf() 
{
m_F_ = 0;	
//m_Gf =true;
//m_Lf =false;	
//m_Nf =false;	

}

void InitConfig::OnLf() 
{
m_F_ = 1;
//m_Gf =false;
//m_Lf =true;	
//m_Nf = false;	

}

void InitConfig::OnNf() 
{
m_F_ = 2;
//m_Gf =false;
//m_Lf =true;	
//m_Nf = false;	

}

void InitConfig::OnGLf() 
{
m_F_ = 3;
//m_Gf =false;
//m_Lf =true;	
//m_Nf = false;	

}

*/

void InitConfig::OnCsc() 
{
m_cond = true;	
}

void InitConfig::OnQStar() 
{
m_Qstar = true;	
}

void InitConfig::OnQc() 
{
rw_c.m_qcore = true;	
}

void InitConfig::OnDestroy() 
{
//	CDialog::OnDestroy();
	// delete rw_c;	
}


