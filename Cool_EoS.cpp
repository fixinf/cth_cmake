//#include "stdafx.h"
// Cool_EoS.cpp : implementation file
//
#include "stdio.h"
#include <string.h>
#include <stdlib.h>
#include "Star_MK.h"
#include "cool_eos.h"
#include "EoS_F.h"
#include "HBerg.h"
#include <sys/stat.h>
//#include <windows.h>
#include <iostream>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define  ID_TIMER 1000
#define Zero 0.000001
//#define _FORMAT_ \
//	"%lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %d"

#define _FORMAT_ \
"%g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %d\n"

#define M_N 939.56563
#define M_P 938.272
#define M_L 1115.683
#define M_Sp 1189.37
#define M_S0 1192.642 
#define M_Sm 1197.449
#define M_X0 1314.9
#define M_Xm 1321.32

#define M_ATOMIC 931.5

#ifdef _HYP_
char menu_file[] = "Menu_MKVOR_hyp17.dat";
#else
char menu_file[] = "Menu_MKVOR.dat";



//char menu_file[] = "Menu_dd2_NJL_2015.dat";

//char menu_file[] = "Menu_dd2_2017.dat";

//char menu_file[] = "Menu_TWIN17.dat";
/////////////////////////////////char menu_file[] = "Menu_TWINKsf03.dat";

//char menu_file[] = "Menu_BSk20.dat";
//char menu_file[] = "Menu_HDD_2017n.dat";

//char menu_file[] = "Menu_DD2-Vex-05-njl.dat";
//char menu_file[] = "Menu_DD2-Vex-05.dat";
//char agr_file_template[] = "EV_TEMP.agr";
//const char agr_file_template[] = "Ev-TMP.agr";
#endif // _HYP_

const char agr_file_template[] = "EV-tmp-2.agr";

char Config_file[100];
FILE * Menu;
FILE *Conffig;
FILE *agrFF;
extern bool Init();



bool readc()
{
 char k;
while (((k=fgetc(Menu))!=EOF) && (k!=':'));k= fgetc(Menu);k=fgetc(Menu);
 
		if (k=='0') return false; else
 return true;

};

char readch()
{
 char k;
while (((k=fgetc(Menu))!=EOF) && (k!=':'));k= fgetc(Menu);k=fgetc(Menu);
 
		return k;
};

void skipch()
{
 char k;
while (((k=fgetc(Menu))!=EOF) && (k!=':'));
k = fgetc(Menu);
};


    /* linear approsimation (interpolation for funtion at given N points*/
double apr(double* y,double* _x,double xc,int N)
{int k=1;  double ap;
while (xc>_x[k]&&k<N)k++;
ap=y[k-1]+(xc-_x[k-1])*(y[k]-y[k-1])/(_x[k]-_x[k-1]);
return ap;
}/*********************************************/
    /* linear approsimation (interpolation for funtion at given N points*/
double apr_op(double* y,double* _x,double xc,int N)
{int k=1;  double ap;
while (xc<_x[k]&&k<N)k++;
ap=y[k-1]+(xc-_x[k-1])*(y[k]-y[k-1])/(_x[k]-_x[k-1]);
return ap;
}/*********************************************/

CCool_EoS::CCool_EoS()
{
  if(!(Menu=fopen(menu_file, "rt")))
    {
      Menu=fopen(menu_file, "w");
      fprintf(Menu," Model Parametrs \n\n");

	  fprintf(Menu," The HOME directory is : %s \n",strS.rw_c.HOME);
	  fprintf(Menu," The EV UOTPUT directory : %s \n",strS.rw_c.HOME_EV);

      fprintf(Menu," Make EoS file : 0\n");
      m_mef  = false;
      fprintf(Menu," Make new config. file : 0\n");
//      fprintf(Menu," Make all config. file : 0\n");
      
      fprintf(Menu," Read full EoS from a file : 0 \n");
	  EK.m_file = false;
      fprintf(Menu," Read from : .\\EoS\\hyb_eos_104_06 \n");
      strcpy(EK.name, ".\\EoS\\hyb_eos_104_06");
//      fprintf(Menu," Hadronic EoS from file : 0\n");

//      Hybrid.hadron.m_file = false;

//      fprintf(Menu," Quark EoS from file : 0\n");
      
      //Hybrid.quark.m_file = false;

      fprintf(Menu," Hadronic EoS \n LWalecka (0),  NLW (1), HDD (3), BSk20 (4): 3\n");
      
      m_make_nf = false;
      m_allst = false;
      m_apply = false;

      m_msc  = false;	
      Hybrid.hadron.Hmodel = '3';
      strS.noinit = true;

      fprintf(Menu," Normal Shell : 0\n");
      
      fprintf(Menu," Quark EoS SM model (1) Bag model (0) : 0\n");
      fprintf(Menu," In case of SM  GF (0) , GL(1),  NJL (2) : 0\n");

      fprintf(Menu," with Quark core : 0\n");
      fprintf(Menu," without Mixed phase : 1\n");
      fprintf(Menu," Superconducting Quark core : 1\n");
      fprintf(Menu," Quark Star : 0\n");
      
      fprintf(Menu," Medium effects : 1\n");
      
      strS.m_std_c = false;
      strS.m_SM =	false;
	  strS.m_F_ = 0;
      strS.rw_c.m_qcore = false;
      Hybrid.m_maxwell = 1;
      strS.m_cond = true;
      strS.m_Qstar = false;
      
      strS.rw_c.normal_core = false;
      strS.rw_c.ifpicon = true;
      
      fprintf(Menu," Pion condensate : 1\n");
      
      strS.rw_c.ifPI = true;
      
      fprintf(Menu," Crust Model (Yakovlev - Y, Tsuruta - T, our - G)  : G\n");
      
      strS.rw_c._CRUST_ = 'G';
      
      fprintf(Menu," Gaps in Hadrons Model (Yakovlev - Y, AV18 - A, Schwenk - U, Armen-fit - F)  : Y\n");
      
      strS.rw_c._GAP_ = 'Y';

      strS.rw_c._FGAP_ = '8';

	  fprintf(Menu," for F-fit p-Gap \n 1-AO\n 2-BCLL\n 3-BS\n 4-CCDK\n 5-CCYms\n 6-CCYps\n 7-EEHO\n 8-EEHOr\n 9-T\n : %c\n",strS.rw_c._FGAP_);

	  strS.rw_c._FGAPn_ = 'o';

	  fprintf(Menu, " for F-fit n-Gap \n 2-AWP2\n 3 - AWP3\n 4 - CCDK\n 5 - CLS\n 6 - GIPSF\n 7 - MSH\n 8 - SCLBL\n 9 - SFB\n 0 - WAP : %c\n", strS.rw_c._FGAPn_);

#ifdef _HYP_
	  strS.rw_c._FGAPL_ = 'o';

	  fprintf(Menu, " for F-fit L-Gap \n 1-TT1\n 2 - TTGM\n 3 - TNNDS\n 4 - TNEhm\n 5 - TNFGA\n : %c\n", strS.rw_c._FGAPL_);
#endif // _HYP_

	  
      strS.rw_c._XGAP_ = 'C';
      
      fprintf(Menu," XGaps in 2SC QModel constant 0 - 0 \n  constant 0.1 MeV - 1  \n constant 0.05 MeV - 5 \n");  
      fprintf(Menu," constant 0.03 MeV - 3 \n  rising 0.03 +  MeV - A \n  incrising 0.03 - MeV - B \n");  
      fprintf(Menu,"	constant 0.03 ++ MeV - C \n  constant 0.03 -- MeV - D \n  : %c\n",strS.rw_c._XGAP_);
      
      
      fprintf(Menu," \n Gap factors in HM\n");
      
      strS.rw_c.gap_fps = 1.0;
      strS.rw_c.gap_fns = 1.0;
      strS.rw_c.gap_fnp = 0.1;
      
      fprintf(Menu," Protons  1S0p : %lg \n",strS.rw_c.gap_fps);
      fprintf(Menu," Neutrons 1S0n : %lg \n",strS.rw_c.gap_fns);
      fprintf(Menu," Neutrons 3P2n : %lg \n",strS.rw_c.gap_fnp);
      
      strS.rw_c.lgtout = 8.0;
      
      fprintf(Menu," \n End time point log10(t/yr) : %lg \n",strS.rw_c.lgtout);
      
      strS.rw_c.init_T = 0.5;
      
      fprintf(Menu," \n initial temperatur in MeV : %lg \n",strS.rw_c.init_T);
      
      strS.rw_c.min_lgT = 5.5;
      
      fprintf(Menu," \n minimal value of log Temperature : %lg \n",strS.rw_c.min_lgT);
      
      fprintf(Menu," \n Print output files for LogN-LogS : 0\n");
      
      strS.rw_c.lgnlgs = false;

	  
	  fprintf(Menu," \n Print profiles for the time points : 0\n");
      
      strS.rw_c.cptp = false;
      
      strS.rw_c.NPTP = 7;
      
     fprintf(Menu," \n Number of points : %d\n",strS.rw_c.NPTP);

  //    strS.rw_c.ptp = (double*)malloc((strS.rw_c.NPTP)*sizeof(double));
      
      for(int i=1;i<=strS.rw_c.NPTP;i++)
	{
	  strS.rw_c.ptp[i-1] = 0.0;
	  fprintf(Menu," %lg",strS.rw_c.ptp[i-1]);
	  
	}
      fprintf(Menu,"\n");
      
      /*********************************************************/
      Nmu0 = 1;
      fprintf(Menu," \n  The Masses [Mo] of Configurations to be Cooled\n");
      
      fprintf(Menu," \n Number of points : %d\n",Nmu0);
      
//      musetdat = (double*)malloc((Nmu0)*sizeof(double));
  //    Msetdat =(double*)malloc((Nmu0)*sizeof(double));
 //    int _i;
 //     for(_i=1;_i<=Nmu0;_i++)
	//{
	//  
	  Msetdat[0] = 1.4;
	  fprintf(Menu," %lg",Msetdat[0]);
	  
/*	}
 */     fprintf(Menu,"\n");
      
      /*********************************************************/
      
    }else{
 
skipch();    fscanf(Menu,"%s",&strS.rw_c.HOME);
skipch(); 	fscanf(Menu,"%s",&strS.rw_c.HOME_EV);  
      m_mef  = readc();
      m_make_nf = readc();
      m_allst = false;
      m_apply = false;
	  EK.m_file = readc();
skipch();
//	  if(EK.m_file)
		  fscanf(Menu,"%s",&EK.name);
      m_msc  = false;
	  Hybrid.hadron.Hmodel = readch();
  	  if(EK.m_file) Hybrid.hadron.Hmodel = '2';

      strS.noinit = true;
      strS.m_std_c = readc();
      strS.m_SM = readc();
	  strS.m_F_ = readc();
      strS.rw_c.m_qcore = readc();
      Hybrid.m_maxwell = readc(); //QUESTION
      strS.m_cond = readc();
      strS.m_Qstar = readc();
      
      
      strS.rw_c.normal_core = false;
      strS.rw_c.ifpicon = readc(); //QUESTION
      strS.rw_c.ifPI = readc();
            
      strS.rw_c._CRUST_ = readch();
      strS.rw_c._GAP_ = readch();
	  strS.rw_c._FGAP_ = readch();
	  strS.rw_c._FGAPn_ = readch();
#ifdef _HYP_
	  strS.rw_c._FGAPL_ = readch();
#endif // _HYP_

	  strS.rw_c._XGAP_ = readch();
      
      skipch();
      fscanf(Menu,"%lg",&strS.rw_c.gap_fps);
      skipch();
      fscanf(Menu,"%lg",&strS.rw_c.gap_fns);
      skipch();
      fscanf(Menu,"%lg",&strS.rw_c.gap_fnp);
      skipch();
      fscanf(Menu,"%lg",&strS.rw_c.lgtout); 
      
      skipch();
      fscanf(Menu,"%lg",&strS.rw_c.init_T); 
      skipch();
      fscanf(Menu,"%lg",&strS.rw_c.min_lgT); 
      
      
      strS.rw_c.normal_shell = strS.m_std_c;
      
	  strS.rw_c.lgnlgs = readc();
      strS.rw_c.cptp = readc();
      
      skipch();
      fscanf(Menu,"%ld",&strS.rw_c.NPTP);
      
  //     strS.rw_c.ptp = (double*)malloc((strS.rw_c.NPTP)*sizeof(double));

      for(int i=1;i<=strS.rw_c.NPTP;i++)
	{
	  
	  fscanf(Menu,"%lg",&strS.rw_c.ptp[i-1]);
	  
}
      /****************************************************/
      skipch();
	  int NN;
      fscanf(Menu,"%ld",&NN);
      Nmu0 = NN;
    //  musetdat = (double*)malloc((Nmu0)*sizeof(double));
      //Msetdat = (double*)malloc((Nmu0)*sizeof(double));

	  int _i;
      for(_i=1;_i<=Nmu0;_i++)
	{
	  
	  fscanf(Menu,"%lg",&Msetdat[_i-1]);
	  
	}
      /****************************************************/
      
    };
  
  fclose(Menu);
  Initsw();
}

/////////////////////////////////////////////////////////////////////////////
// CCool_EoS message handlers


int CCool_EoS::GetKind(double mu, double T)
{ 
  mu_baryon = mu;
  kT= T;
  if(EK.m_file) return GetKind(mu); else
  return Hybrid.kind_(mu_baryon,kT); 
}

double CCool_EoS::GetPressure(double mu, double T)
{  double p;


if (fabs(mu-mu_baryon)+fabs(T-kT) >= Zero) sw = GetKind(mu,T);
//return EC.p(mu);

	if(EK.m_file)p = GetPressure(mu_baryon); else p = Hybrid.pr_(mu_baryon,kT);

  return p;
}

double CCool_EoS::GetEnergy(double mu, double T)
{

	if (fabs(mu-mu_baryon)+fabs(T-kT)> Zero) s = GetEntropy(mu,T);
	//return EC.eps(mu);

	double e;
  if(EK.m_file)e = GetEnergy(mu_baryon); else e = Hybrid.en_(mu_baryon,kT);
  return e;
}

double CCool_EoS::GetEntropy(double mu, double T)
{ 

  if (fabs(mu-mu_baryon)+fabs(T-kT)> Zero) nb = GetDensity(mu,T,nn);
	  return 0;

  double s;
  if (EK.m_file)s =GetEntropy(mu_baryon); else s = Hybrid.s_(mu_baryon,kT);
  return s;
}

double CCool_EoS::GetDensity(double mu, double T, double *nn_)
{
  double n;

  if (fabs(mu-mu_baryon)+fabs(T-kT)> Zero) pr = GetPressure(mu,T);
  
// nb = n = EC.nb(mu,nn_);
//return n;
  if(EK.m_file) nb = n = GetDensity(mu_baryon,nn_); else
  nb = n = Hybrid.n_(mu_baryon,kT,nn_);
  for(int i=0;i<20;i++) nn[i] = nn_[i];
  return n;
}

int CCool_EoS::GetKind(double mu)
{ 
	if(EK.mu_crit > mu) return 0;
	else return 2;
}

double CCool_EoS::GetPressure(double mu)
{
    return apr(EK.Pf,EK.muf,mu,EK.Nf);
}

double CCool_EoS::GetEnergy(double mu)
{  
	return  apr(EK.epsf,EK.muf,mu,EK.Nf);
}

double CCool_EoS::GetEntropy(double mu)
{
	return 0;
}

double CCool_EoS::GetDensity(double mu, double *n)
{
 for(int i=2;i<13;i++) n[i]=0.;
 
	n[0]=apr(EK.n0f,EK.muf,mu,EK.Nf);
	n[1]=apr(EK.n1f,EK.muf,mu,EK.Nf);
	n[13]=apr(EK.n13f,EK.muf,mu,EK.Nf);
	n[14]=apr(EK.n14f,EK.muf,mu,EK.Nf);
	n[15]=apr(EK.n15f,EK.muf,mu,EK.Nf);
	n[16]=apr(EK.n16f,EK.muf,mu,EK.Nf);
    n[17]=apr(EK.n17f,EK.muf,mu,EK.Nf);
Hybrid.hadron.m_mtr[0]=M_N*apr(EK.m0f,EK.muf,mu,EK.Nf);
Hybrid.hadron.m_mtr[1]=M_P*apr(EK.m1f,EK.muf,mu,EK.Nf);
#ifdef _HYP_
	n[2]=apr(EK.Y_Lf,EK.muf,mu,EK.Nf);
	n[3]=apr(EK.Y_Smf,EK.muf,mu,EK.Nf);
	n[4]=apr(EK.Y_S0f,EK.muf,mu,EK.Nf);
	n[5]=apr(EK.Y_Spf,EK.muf,mu,EK.Nf);
	n[6]=apr(EK.Y_Xmf,EK.muf,mu,EK.Nf);
	n[7]=apr(EK.Y_X0f,EK.muf,mu,EK.Nf);
Hybrid.hadron.m_mtr[2]=M_L*apr(EK.m_Lf,EK.muf,mu,EK.Nf);
Hybrid.hadron.m_mtr[3]=M_Sm*apr(EK.m_Smf,EK.muf,mu,EK.Nf);
Hybrid.hadron.m_mtr[4]=M_S0*apr(EK.m_S0f,EK.muf,mu,EK.Nf);
Hybrid.hadron.m_mtr[5]=M_Sp*apr(EK.m_Spf,EK.muf,mu,EK.Nf);
Hybrid.hadron.m_mtr[6]=M_Xm*apr(EK.m_Xmf,EK.muf,mu,EK.Nf);
Hybrid.hadron.m_mtr[7]=M_X0*apr(EK.m_X0f,EK.muf,mu,EK.Nf);
#endif
	return apr(EK.rof,EK.muf,mu,EK.Nf);
}

void CCool_EoS::Make_EoS_FILE(double mu, double dmu, double T)
{
	double mu_max = 2800;
	double mu_min = 
//		M_N
		M_ATOMIC
		; 
dmu /=2;	

Star_MK msr; 
mu = mu_max;
dmu=(mu-mu_min)/(peos-1);
msr.init_EoS_WGB(&mu,T,this,EoS_file_name);
mu= mu_min;
 if(!strS.m_Qstar){
   switch(Hybrid.hadron.Hmodel){
   case '0' : strcat(EoS_file_name,"K300");break;
   case '1' :
#ifdef _new_
     strcat(EoS_file_name,"K250");
#else
     strcat(EoS_file_name,"K300");
#endif
     break;
   case '2' : strcat(EoS_file_name,"DSet"); break;
   case '3' : strcat(EoS_file_name,"-HDD"); break;
   case '4' : strcat(EoS_file_name,"-PotC"); break;

   };
 };
 strcpy(EoS_mini_file_name,EoS_file_name);
#ifndef _SYMMAT_
 strcat(EoS_file_name,"_EoS.dat");
 strcat(EoS_mini_file_name,"_s.dat");
#else
strcat(EoS_file_name,"_EoS_symmat.dat");
strcat(EoS_mini_file_name,"_s_symmat.dat");
#endif

 EoS_file_n = fopen(EoS_file_name,"w");
 EoS_mini_file = fopen(EoS_mini_file_name,"w");

 fprintf(EoS_file_n,"  #1  mu_b [MeV],  pr,   e, [MeV/fm3]    n_b , #5   n_n,    n_p, [1/fm3]  m_n/M_n,    m_p/M_n,  #9    n_e,  n_mu  n_u,  n_d,  n_s[1/fm3]  mu_e [MeV]  Y_p  \n");
 fprintf(EoS_mini_file,"  # e[MeV/fm3],   pr[MeV/fm3],  n_b[1/fm3],  mu_b[MeV]\n");
 
 fclose(EoS_file_n);
 fclose(EoS_mini_file);
 while (/* pr>1.e-9*/ mu<=mu_max){
   EoS_file_n = fopen(EoS_file_name,"a");
   EoS_mini_file = fopen(EoS_mini_file_name,"a");

 
   eps=		GetEnergy(mu,T);
   

   if(pr>zero)fprintf(EoS_file_n,"  %g  %g  %g  %g  %g  %g  %g  %g  %g  %g  %g  %g  %g  %g %g \n",
		      mu_baryon, pr, eps/* /nb */, nb, nn[0], nn[1], 
		      Hybrid.hadron.m_mtr[0]/M_N,Hybrid.hadron.m_mtr[1]/M_P,nn[13],nn[14],
		      nn[15],nn[16],nn[17],mu_electron,nn[1]/nb);

   if(pr>zero)fprintf(EoS_mini_file," %g  %g  %g  %g \n",
		       eps , pr, nb, mu_baryon);

   		
		
   fclose(EoS_file_n);
   fclose(EoS_mini_file);

   mu+=dmu;
 }
}


void CCool_EoS::OnMSr() 
{
  m_apply = true;
  Initsw();
}

void CCool_EoS::OnMsc() 
{
  m_mef = false;	
  m_msc = true;
}

void CCool_EoS::OnMef() 
{
  m_msc = false;	
  m_mef = true;	

}

void CCool_EoS::open_agr_file()
{
	if((agrFF = fopen(strS.rw_c.agr_file_,"r"))==NULL)
	{
strcpy(syscomand,"copy   ");
strcat(syscomand,agr_file_template);
strcat(syscomand,"   ");
strcat(syscomand,strS.rw_c.agr_file_);
//strcat(syscomand,strS.rw_c.HOME_EV);
system(syscomand);}else
		fclose(agrFF);

//system(syscomand);

}

void CCool_EoS::Make_Star_Config()
{
  char f[250];
  Star_MK	mks;
  if(strS.m_mu>939&&strS.m_kT>=1e-10){
    mks.init_EoS_WGB(&strS.m_mu,strS.m_kT,this,f);
    mks.star_maker();
  };
  m_make_nf = false;
}



void CCool_EoS::OnNewfile() 
{
	m_make_nf=!m_make_nf;
}

void CCool_EoS::OnRun_n() 
{
    if(strS.Cool_Star()) 
	{
if(!NOINIT)	
{strS.Reset_Profiles();	
}
else NOINIT = Init();
		return;
	}
}
void CCool_EoS::OnApply() 
{
	char c1,c2;
if(!m_apply)
{m_apply = true;

//  Initsw(); //if there are some changes in the initial data!!!
};
	if(strS.noinit) 
{

//		printf("Continue for cooling ?\n");
//scanf("%c",&c1);

//if(c1=='n') exit(1);

if(m_apply)
{
//	OnStop();	
//EOS.strS.Reset_Profiles();
 NOINIT = true;
}
}

	if (NOINIT)
	{
		if(	NOINIT=Init()) {
printf("Do you like to make a new file : y or n \n");
//scanf("%c",&c2);
//if(c2=='n') exit(1);
m_make_nf= true;
		}
	if(!NOINIT) OnRun_n();
	}
	else 
	//	OnRun_n()
		;	
}

void CCool_EoS::OnAst() 
{
	m_allst = !m_allst;	
}


void CCool_EoS::Initsw()
{
	double M_min;
strS.msc1 = m_msc  ;	
strS.mef1 = m_mef  ;
strS.mw1  = Hybrid.hadron.Hmodel ;

//Hybrid.hadron.In_file();
//Hybrid.quark.In_file();

       //Hybrid.QFF=
Hybrid.quark.m_FF=strS.m_F_;
       //Hybrid.cLW=false;
Hybrid.quark.m_nlw=true;
Hybrid.quark.m_cond =strS.m_cond;	   
	   Hybrid.quark.m_SM =strS.m_SM;
	   Hybrid.quark.m_HM =strS.mw1;
Hybrid.m_hybrid = strS.rw_c.m_qcore;
if(!strS.rw_c.m_qcore) Hybrid.m_hadron = !strS.m_Qstar; else Hybrid.m_hadron = true;
Hybrid.quark.m_new = true;
Hybrid.quark.initBfuncion();
       strS.SetFileName();
// strcat(EK.EoS_file_name,strS.rw_c.HOME);
// strcat(EK.EoS_mini_file_name,strS.rw_c.HOME);
	   strcpy(Config_file,strS.rw_c.HOME);
	   strcpy(strS.rw_c.agr_file_,strS.rw_c.HOME_EV); 
       strcat(strS.rw_c.agr_file_,".agr");

	   if(!strS.m_Qstar)
	switch(Hybrid.hadron.Hmodel) {
	case '0' :  strcat(Config_file, EoS_lw); break; 
	case '1' :	strcat(Config_file, EoS_nlw);break;
	case '2' :	strcat(Config_file, EoS_f);break;
	case '5' : strcat(Config_file, EoS_SLy);break;
		case '4' : strcat(Config_file, EoS_FPS);break;
		case '3' : strcat(Config_file, EoS_hjj);
} 
else strcat(Config_file,"/Q_");
	   strcat(Config_file,strS.rw_c.File_Name0);
	   strcat(Config_file,"-!_st.dat");
if (EK.m_file) EK.Read_EoS_FILE_();
//_mkdir("graph");

strcpy(syscomand,"md  ");
strcat(syscomand,strS.rw_c.HOME);
system(syscomand);
strcpy(syscomand,"md  ");
strcat(syscomand,strS.rw_c.HOME_EV);
system(syscomand);

open_agr_file();

if((Conffig=fopen(Config_file,"r"))==NULL)
{
if(!m_mef){
m_allst=true; Make_Star_Config();
Conffig=fopen(Config_file,"r");
};
	   }
if(!m_mef){
	   	   m_allst=false;
		   m_IIIF = false;
	   EK.Nf_M = EK.Read_Mass_Radius_relation(Conffig,EK.M_M,EK.M_R, EK.M_MB, EK.M_MU);//Higher mass branch (III Family if it exist)
	   if (M_min=EK.M_M[EK.Nf_M-1] >= 0.1) {
		   m_IIIF = true;
		   EK.Nf_M_ = EK.Read_Mass_Radius_relation(Conffig, EK.M_M_, EK.M_R_, EK.M_MB_, EK.M_MU_);// Lower mass branch (II if III Family exist)
	   }
fclose(Conffig);
for(int j=0;j<Nmu0;j++)
{
	musetdat[j] = musetdat1[j] = 0.0;
	if(Msetdat[j] <= EK.M_M[0]&&Msetdat[j]>= EK.M_M[EK.Nf_M-1])
	musetdat[j]= apr_op(EK.M_MU,EK.M_M,Msetdat[j],EK.Nf_M); //Higher mass branch (III Family if it exist) otherwise all II family 
	if (Msetdat[j] <= EK.M_M_[0] && Msetdat[j] >= EK.M_M_[EK.Nf_M_-1])
	musetdat1[j] = apr_op(EK.M_MU_, EK.M_M_, Msetdat[j], EK.Nf_M_);//Lower mass branch (if III Family exist)
};
}
//strcpy(syscomand,"rd /s /q ");
//strcat(syscomand,strS.rw_c.HOME);
//system(syscomand);

 
//	   strS.m_EoSFileName = strS.rw_c.File_Name0;

}

                                                       