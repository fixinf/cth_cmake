//#include "stdafx.h"
/*
                                    CHPT 9
                  ROOT FINDING AND NONLINEAR SETS OF EQUATIONS
                    9.3 Van Wijngaarden-Dekker-Brent Method.
*/
#include <math.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

#define ITMAX 100
//#define ITMAX 1000
#define EPS 3.0e-8
//#define EPS 3.0e-18

double zbrent(double (*func)(double),double x1,double x2,double tol)
/*float x1,x2,tol;
float (*func)(float); */	/* ANSI: float (*func)(float); */
/*                           
  Using Brent's method, find the root of the function FUNC known to lie
  between X1 and X2. The returned root will be refined until its accuracy
  is TOL.
*/
{
	int iter;
	double a=x1,b=x2,c=x2,d,e,min1,min2;
	double fa=(*func)(a),fb=(*func)(b),fc,p,q,r,s,tol1,xm;
//	void nrerror(char *);
	/*if (fb*fa > 0.0) return 1234567890;*/  /*nrerror("Root must be bracketed in ZBRENT")*/
	fc=fb;
	for (iter=1;iter<=ITMAX;iter++) {
		if (fb*fc > 0.0) {
            		c=a;  /* Rename A, B, C and adjust bounding interval D.*/
			fc=fa;
			e=d=b-a;
		}
		if (fabs(fc) < fabs(fb)) {
			a=b;
			b=c;
			c=a;
			fa=fb;
			fb=fc;
			fc=fa;
		}
        	tol1=2.0*EPS*fabs(b)+0.5*tol;  /* Convergence check.*/
		xm=0.5*(c-b);
		if (fabs(xm) <= tol1 || fb == 0.0) return b;
		if (fabs(e) >= tol1 && fabs(fa) > fabs(fb)) {
            		s=fb/fa; /* Attempt inverse quadratic interpolation.*/
			if (a == c) {
				p=2.0*xm*s;
				q=1.0-s;
			} else {
				q=fa/fc;
				r=fb/fc;
				p=s*(2.0*xm*q*(q-r)-(b-a)*(r-1.0));
				q=(q-1.0)*(r-1.0)*(s-1.0);
			}
            		if (p > 0.0)  q = -q;  /* Check whether in bounds.*/
			p=fabs(p);
			min1=3.0*xm*q-fabs(tol1*q);
			min2=fabs(e*q);
			if (2.0*p < (min1 < min2 ? min1 : min2)) {
                		e=d;  /* Accept interolation.*/
				d=p/q;
			} else {
                		d=xm;  /* Interpolation failed, use bisection.*/
				e=d;
			}
        	} else { /* Bounds decrasing too slowly, use bisection.*/
			d=xm;
			e=d;
		}
        	a=b;  /* Move last best guess to A.*/
		fa=fb;
        	if (fabs(d) > tol1)  /* Evaluate new trial root.*/
			b += d;
		else
			b += (xm > 0.0 ? fabs(tol1) : -fabs(tol1));
		fb=(*func)(b);
	}
//	nrerror("Maximum number of iterations exceeded in ZBRENT");
	return 0.0;
}
#undef ITMAX
#undef EPS
