#if !defined(AFX_COOL_EOS_H__5C208482_212F_42C3_B2BB_BD22FC048DFF__INCLUDED_)
#define AFX_COOL_EOS_H__5C208482_212F_42C3_B2BB_BD22FC048DFF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Cool_EoS.h : header file
//
//#include "EoS_crust.h"
#include "EoS_Hybrid.h"

//#include "resource.h"
#include <stdio.h>
#include "EoS_F.h"
#include "InitConfig.h"

//#include "CoolClass.h"	// Added by ClassView
/////////////////////////////////////////////////////////////////////////////
// CCool_EoS dialog

class CCool_EoS 
{
// Construction
public:

//CCool_EoS operator=( CCool_EoS &other );
	EoS_Hybrid Hybrid;
	//EoS_crust EC;
//	CEdit *p_edt;
	double eps;
	double nb;
//	double mu_b;
	double kT;
	double pr;
	double s;
	double nn[20];
	int sw; // switcher for pure and mixed phases
//	double gsig,gom,gro;
	double Xii;
//	double ms_mtr[20];
//	double mjus_mtr[20];
//	double mu_e;
	void Make_Star_Config();
//	CCool* strS;
	InitConfig strS;
	//T=0 case
	double GetDensity(double mu, double* n);
	double GetEntropy(double mu);
	double GetEnergy(double mu);
	double GetPressure(double mu);
	int GetKind(double mu);

	//finite T case in beta -equilibium
	double GetDensity(double mu, double T,double* n);
	double GetEntropy(double mu, double T);
	double GetEnergy(double mu,  double T);
	double GetPressure(double mu, double T);
	int GetKind(double mu,double T);

	void Make_EoS_FILE(double mu, double dmu, double T);
void open_agr_file();
	
	CCool_EoS(/*CWnd* pParent = NULL*/);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCool_EoS)
//	CButton	m_model;
//	CString	m_warning;
	bool	m_make_nf;
	bool	m_allst;
		bool NOINIT;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	public:      EoS_F EK;

 //               int DoModal(char*);
	protected:
   //             void DoDataExchange();    // DDX/DDV support


// Implementation
protected:
	
	
	FILE* EoS_file_n;
	FILE* EoS_mini_file;
	char EoS_file_name[100];
	char EoS_mini_file_name[100];
    char syscomand[50];
//	double Xi;
//	double Temp;
	double mu_baryon;
	double mu_electron;

 	  void OnCancel();
	  void OnMSr();
	  void OnMsc();
	  void OnMef();
	  void OnWalechka();
	  void OnHBerg();
 
 
	  void OnNewfile();
	  void OnAst();
	  void OnDestroy();
	  void OnSIV();
 

public:
	double musetdat[100], musetdat1[100];
    double Msetdat[100];
	int Nmu0;
	  void OnApply();
	  void OnRun_n(); 

	void Initsw();
//char Hmodel;
	bool m_apply;
	bool m_mef ;
	bool m_msc;
	bool m_IIIF;

};


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COOL_EOS_H__5C208482_212F_42C3_B2BB_BD22FC048DFF__INCLUDED_)
