#if !defined(AFX_EOS_BAG_H__47144C47_7532_45F7_B2F8_C8C9175E5A02__INCLUDED_) 
#define AFX_EOS_BAG_H__47144C47_7532_45F7_B2F8_C8C9175E5A02__INCLUDED_ 
 
#if _MSC_VER > 1000 
#pragma once 
#endif // _MSC_VER > 1000 
// EoS_Bag.h : header file 
// 
#include "EoS_HB.h" 
//#include "EoS_file.h"
///////////////////////////////////////////////////////////////////////////// 
// EoS_Bag window 

//#define _SM_
//#define _CSL_
//#ifdef _SM_
//#define _NOCOND_
//#define _G_F_
//#define _NJL_F_
//#define _L2_F_
//#endif


extern double mu_b_c;

const double dgap= 100.05/*MeV*/ ; 

//extern double B;
//extern double Del;
//extern double    G_s_QUARK ;
//extern double Del_dq(double mu);
extern double xi_q;

class EoS_Bag : public EoS_HB 
{ 
// Construction 
 public: 
  EoS_Bag(); 
  
  // Attributes 
 public: 
  double Del_[15];
  //	double mu_b_c;
  bool m_SM;
  int  m_FF;
  bool m_cond;
  char m_HM;
  bool m_nlw; // if true : non liener Walechka Model
  double B_[15];
  int KK;
  double mu_c;
  //	double mu_b_c;
  bool m_file_B;  

 double B;
 double Del;
 double    G_s_QUARK ;
 double Del_dq(double mu);
 double _B(double mu);
 double B_prim(double mu);
 double B_prim2(double mu);

	// Operations 
 protected: 
 public:
// EoS_file Quark_file;
 
// Overrides 
	// ClassWizard generated virtual function overrides 
	//{{AFX_VIRTUAL(EoS_Bag) 
	//}}AFX_VIRTUAL 
 
// Implementation 
public: 
	int In_file();
	double Xi_q(double mu);
	double X_gap(char type, double mu_q);
	bool m_new; // for initalisation of new Quark object
	void initBfuncion();
	double QQ[6]; 
//	={2./3.,-1./3., 
//-1./3.,2./3., 
//0.,0., 
//-1.,-1.}; 
	double B2e(double mjuB,double t); 
	EoS_HB mu_; 
	EoS_HB e_; 
	EoS_HB c_; 
	EoS_HB st_; 
	EoS_HB d_; 
	EoS_HB u_; 
	double ds_dT(double mjuB,double mjue,double t); 
	void dn_(double mjuB,double mjue,double t,double *dndmjuB,double *dndt); 
	double s_(double mjuB,double mjue,double t); 
	double n_(double mjuB,double mjue,double t,double *n); 
	double p_(double mjuB,double mjue,double t); 
	double charge_(double muB, double mue, double t);

	virtual ~EoS_Bag(); 
 
//	double chne(double x); 
}; 
 
///////////////////////////////////////////////////////////////////////////// 
 
//{{AFX_INSERT_LOCATION}} 
// Microsoft Visual C++ will insert additional declarations immediately before the previous line. 
 
#endif // !defined(AFX_EOS_BAG_H__47144C47_7532_45F7_B2F8_C8C9175E5A02__INCLUDED_)         
