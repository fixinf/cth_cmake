#include <math.h>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "RW_cool.h"
#define eps00 3.839e26
                      /*erg cm^-3 s^-1*/

#define k00   8.375e27
                       /* erg/cm/sec/K */
#define mx 0.1
				/* GeV */
#define Nx0 1e38

static double DM_kappa = 5.2e-14 / k00;
static double MU_M = 8.6e21 / eps00;
static double DM_M = 3.1e19 / eps00;
double Nx;
int RW_cool::DM_heat(int o, double T)
{
	double n_, T_h, r_h;
	double Q, Q0, Reff;
	Q0 = MU_M;
	Nx = 3e22*Nx0;

	n_ = n_b[o] / n_0;
	T_h = T*exp(-phi[o]);
	r_h = rr[o] ;
	Q_DM = 0;
	k_fac_DM = 1;
	k_DM = 0;
	double Pre_F = 1.;
	Reff = 0.6 /sqrt(n_*mx / 30.0) *sqrt(T_h*kT_0);
	double nx0 = 1e23 * (Nx/Nx0) / powf(Reff, 3);
	double rrr = r_h / Reff;

	if (log10(_time) < 3)
	{
		//Q0 = Pre_F*DM_M*n_*n_*n_*powf(T_h*kT_0, - 2.60385)/8.0;
	Q0 = Pre_F*DM_M*mx*powf(n_*mx/T_h, 3)*(Nx/Nx0)*(Nx / Nx0);
		Q = Q0* exp(-rrr*rrr);
//	Q_DM = Q;
	k_DM = DM_kappa * sqrt(T_h*(0.1/mx))/n_*nx0* exp(-rrr*rrr);
}
	return 0;
}
