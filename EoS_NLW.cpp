//#include "stdafx.h"
// EoS_NLW.cpp: implementation of the EoS_NLW class.
//
//////////////////////////////////////////////////////////////////////

//(1992, PhRevC 45,844)  K= 250


#include "EoS_NLW.h"
//#include "nrutil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


#if HYPERON
static double G2G[]={1., 1., GH2GN, GH2GN, GH2GN, GH2GN, GH2GN, GH2GN, 
#if OMEGADELTA
GH2GN, GH2GN, GH2GN, GH2GN, GH2GN,
#else
0., 0., 0., 0., 0.,//switching off omega delta++ delta+ delta0 delta-
#endif
0.,0.};
#else 
static  double G2G[]={1., 1., 0., 0.,0.,0., 0.,0., 0., 0.,0.,0.,0., 0.,0.};
#endif

//masses in MeV
// M_NEUTRON M_PROTON M_LAMBDA M_SIGMA_plus 
// M_SIGMA_0 M_SIGMA_minus M_KSI_0 M_KSI_minus 
// M_OMEGA M_DELTA_plus_2 M_DELTA_plus M_DELTA_0 M_DELTA_minus
// M_ELECTRON M_mju_MESON
#if HYPERON
static double M_[]={939.56563, 938.272, 1115.683, 1189.37,
1192.642, 1197.449, 1314.9, 1321.32,
#if OMEGADELTA
1672.45, 1232.0, 1232.0, 1232.0, 1232.0, 
#else
0., 0., 0., 0., 0.,//switching off omega delta++ delta+ delta0 delta- for G300
#endif
0.51099907, 105.658389};
#else
static double M_[]={939.56563, 938.272, 0., 0.,0.,0., 0.,0., 0., 0.,0.,0.,0., 0.51099907, 105.658389};
#endif

//Space factors g=2*J+1 J-spin
/***********************************************************/
//n p lambda sigma+ sigma0 sigma- ksi0 ksi-
//omega delta++ delta+ delta0 delta- 
//e mju
/************************************************************/

#if HYPERON
static double G_[]={2., 2., 2., 2., 2., 2., 2., 2.,
#if OMEGADELTA
4., 4., 4., 4., 4.,
#else
0., 0., 0., 0., 0.,//switching off omega delta++ delta+ delta0 delta-
#endif
2., 2.};
#else
static double G_[]={2.,2., 0., 0.,0.,0., 0.,0., 0., 0.,0.,0.,0., 2.,2.};
#endif


//Charges
#if HYPERON
double Ql_[]={0., 1., 0., 1., 0., -1., 0., -1.,
#if OMEGADELTA
-1., 2., 1., 0., -1.,
#else
0., 0., 0., 0., 0.,//switching off omega delta++ delta+ delta0 delta-
#endif
-1., -1.};
#else
double Ql_[]={0.,1., 0., 0.,0.,0., 0.,0., 0., 0.,0.,0.,0., -1.,-1.};
#endif


//Isospin projection
#if NORHO
static double I3_[]={0.,0., 0., 0.,0.,0., 0.,0., 0., 0.,0.,0.,0., 0., 0.};//switching off rho field
#else
#if HYPERON
static double I3_[]={-1./2., 1./2., 0., 1., 0., -1., 1./2., -1./2.,
#if OMEGADELTA
0., 3./2., 1./2., -1./2., -3./2.,
#else
0., 0., 0., 0., 0.,//switching off omega delta++ delta+ delta0 delta-
#endif
0., 0.};
#else
static double I3_[]={-1./2.,1./2., 0., 0.,0.,0., 0.,0., 0., 0.,0.,0.,0., 0., 0.};
#endif
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

EoS_NLW::EoS_NLW()
{
	
}

EoS_NLW::~EoS_NLW()
{
	
}



double EoS_NLW::n_(double mjuB, double mjue, double T, double gsig, double gom, double gro, double *n,double* mjus_mtr, double* ms_mtr)
{
	double nB=zero, mjus, ms;int i;
	for (i=0;i<=12;i++){
		mjus=mjuB-(Ql_[i]*mjue)-G2G[i]*gom-G2G[i]*(I3_[i])*gro;
		mjus_mtr[i]= mjus;//effective chemical potnetials
		ms=fabs(M_[i] - G2G[i]*gsig);
		ms_mtr[i]=ms;//effective masses
        mass = ms;
		g = G_[i];
		n[i]=	EoS_HB::n_(mjus,T) - EoS_HB::n_(-mjus,T)+zero*zero;
		nB+=n[i];
	}
	mass = M_[13];
	g = G_[13];
	n[13]=	EoS_HB::n_(mjue,T) - EoS_HB::n_(-mjue,T)+zero*zero;
	mass = M_[14];
	g = G_[14];
	n[14]=	EoS_HB::n_(mjue,T) - EoS_HB::n_(-mjue,T)+zero*zero;
	return nB;
}


double EoS_NLW::s_(double mjuB, double mjue, double T, double gsig, double gom, double gro)
{
	double sums = zero, mjus, ms;int i;
	for (i=0;i<=12;i++){
		mjus=mjuB-(Ql_[i]*mjue)-G2G[i]*gom-G2G[i]*(I3_[i])*gro;
		ms=M_[i]-G2G[i]*gsig;
        mass = ms;
		g = G_[i];
		sums+=	EoS_HB::s_(mjus,T) + EoS_HB::s_(-mjus,T)+zero*zero;
	}
	mass = M_[13];
	g = G_[13];
	sums+=	EoS_HB::s_(mjue,T) + EoS_HB::s_(-mjue,T)+zero*zero;
	mass = M_[14];
	g = G_[14];
	sums+=	EoS_HB::s_(mjue,T) + EoS_HB::s_(-mjue,T)+zero*zero;
	return sums;
	
}


double EoS_NLW::p_(double mjuB, double mjue, double T, double gsig, double gom, double gro)
{
	double sump = zero, mjus, ms;int i;
	for (i=0;i<=12;i++){
		mjus=mjuB-(Ql_[i]*mjue)-G2G[i]*gom-G2G[i]*(I3_[i])*gro;
		ms=M_[i]-G2G[i]*gsig;
        mass = ms;
		g = G_[i];
		sump+=	EoS_HB::p_(mjus,T) + EoS_HB::p_(-mjus,T)+zero*zero;
	}
	sump-=	gsig*gsig/SIGfac/2./hc
		
		//#if NONLIN
		+Bi/3.*M_[0]*gsig*gsig*gsig/hc3+C/4.*gsig*gsig*gsig*gsig/hc3;
	//#endif
	;
	sump+=	gom*gom/OMfac/2./hc+gro*gro/RHOfac/2./hc;
	mass = M_[13];
	g = G_[13];
	sump+=	EoS_HB::p_(mjue,T) + EoS_HB::p_(-mjue,T)+zero*zero;
	mass = M_[14];
	g = G_[14];
	sump+=	EoS_HB::p_(mjue,T) + EoS_HB::p_(-mjue,T)+zero*zero;
	return sump;
}

//E=S*T+mjuB*nB-P




void EoS_NLW::difer(double mjuB,double mjue,double T,double gsig,double gom,double gro,
					double *A,double *D,double *U,double *muX,
					double *dndmju,double *dndmass,double *dndt,
					double *dnsdmass,double *dnsdt,double *dsdt)
{
	double mjus,ms;int i;
	for (i=0;i<=12;i++){
		mjus=mjuB-(Ql_[i]*mjue)-G2G[i]*gom-G2G[i]*(I3_[i])*gro;
		ms=fabs(M_[i]-G2G[i]*gsig);
		mass = ms;
		g = G_[i];
		dndmju[i]=  EoS_HB::dn_dmu(mjus,T) + EoS_HB::dn_dmu(-mjus,T);
		dndmass[i]= EoS_HB::dn_dm(mjus,T)  - EoS_HB::dn_dm(-mjus,T);
		dndt[i]=    EoS_HB::dn_dT(mjus,T)  - EoS_HB::dn_dT(-mjus,T);
		dnsdmass[i]=EoS_HB::dns_dm(mjus,T) + EoS_HB::dns_dm(-mjus,T);
		dnsdt[i]=EoS_HB::dns_dT(mjus,T)  + EoS_HB::dns_dT(-mjus,T);
		dsdt[i]=EoS_HB::ds_dT(mjus,T)  + EoS_HB::ds_dT(-mjus,T);
	}
	for (i=13;i<=14;i++){
		mjus=mjue;
		mass=ms=M_[i];
		g = G_[i];
		dndmju[i]=  EoS_HB::dn_dmu(mjus,T) + EoS_HB::dn_dmu(-mjus,T);
		dndmass[i]= EoS_HB::dn_dm(mjus,T)  - EoS_HB::dn_dm(-mjus,T);
		dndt[i]=    EoS_HB::dn_dT(mjus,T)  - EoS_HB::dn_dT(-mjus,T);
		dnsdmass[i]=EoS_HB::dns_dm(mjus,T) + EoS_HB::dns_dm(-mjus,T);
		dnsdt[i]=EoS_HB::dns_dT(mjus,T)  + EoS_HB::dns_dT(-mjus,T);
		dsdt[i]=EoS_HB::ds_dT(mjus,T)  + EoS_HB::ds_dT(-mjus,T);
	}
	
	for (i=0;i<=5;i++) A[i]=D[i]=U[i]=muX[i]=zero;
	for (i=0;i<=12;i++) {
		muX[0]=zero; 
		muX[1]-=Ql_[i]*Ql_[i]*dndmju[i]; 
		muX[2]-=G2G[i]*Ql_[i]*dndmju[i]; 
		muX[3]-=G2G[i]*Ql_[i]*I3_[i]*dndmju[i];
		muX[4]-=G2G[i]*Ql_[i]*dndmass[i]; 
		muX[5]=zero;
		
		A[0]+=OMfac*hc*G2G[i]*dndmju[i]; 
		A[1]-=OMfac*hc*G2G[i]*Ql_[i]*dndmju[i];
		A[2]-=OMfac*hc*G2G[i]*G2G[i]*dndmju[i]; 
		A[3]-=OMfac*hc*I3_[i]*G2G[i]*G2G[i]*dndmju[i];
		A[4]-=OMfac*hc*G2G[i]*G2G[i]*dndmass[i]; 
		A[5]+=OMfac*hc*G2G[i]*dndt[i];
		
		U[0]+=RHOfac*hc*G2G[i]*I3_[i]*dndmju[i]; 
		U[1]-=RHOfac*hc*G2G[i]*I3_[i]*Ql_[i]*dndmju[i];
		U[2]-=RHOfac*hc*G2G[i]*G2G[i]*I3_[i]*dndmju[i]; 
		U[3]-=RHOfac*hc*G2G[i]*G2G[i]*I3_[i]*I3_[i]*dndmju[i];
		U[4]-=RHOfac*hc*G2G[i]*I3_[i]*G2G[i]*dndmass[i]; 
		U[5]+=RHOfac*hc*G2G[i]*I3_[i]*dndt[i];
		
		D[0]-=SIGfac*hc*G2G[i]*dndmass[i]; 
		D[1]+=SIGfac*hc*G2G[i]*Ql_[i]*dndmass[i];
		D[2]+=SIGfac*hc*G2G[i]*G2G[i]*dndmass[i]; 
		D[3]+=SIGfac*hc*I3_[i]*G2G[i]*G2G[i]*dndmass[i];
		D[4]-=SIGfac*hc*G2G[i]*G2G[i]*dnsdmass[i]; 
		D[5]+=SIGfac*hc*G2G[i]*dnsdt[i];
	}
	for (i=13;i<=14;i++) muX[1]+=Ql_[i]*dndmju[i];
	A[2]-=1;
	U[3]-=1;
	D[4]-=1
//#if NONLIN
		+2.*SIGfac/hc/hc*Bi*M_[0]*gsig+3.*SIGfac/hc/hc*C*gsig*gsig;
//#endif
	;
}


double EoS_NLW::DNDSHHwalnl(double mjuB,double mjue,double T,double gsig,double gom,double gro,
							int mix,_NQtp_ nq,_PQtp_ sq,
							int qi,double *QQ,double *DNBDMJUB,double *DNBDT,double *DSDMJUB)
{
	double *A,*D,*U,*muX,*n,*mun,*mn,*dndmju,*dndmass,*dndt,*dnsdmass,*dnsdt,*dsdt,DSDTr;
	double a_OM,a_RHO,a_SIG,b_OM,b_RHO,b_SIG,c_OM,c_RHO,c_SIG,unter;
	double dndmjub_mjue_t=zero,dndmjue_mjub_t=zero,dndt_mjub_mjue=zero;
	double dsdmjub_mjue_t=zero,dsdmjue_mjub_t=zero,dsdt_mjub_mjue=zero;
	double dxdmjub_mjue_t=zero,dxdmjue_mjub_t=zero,dxdt_mjub_mjue=zero;
	int i;
	A=(double *)malloc(6*sizeof(double));
	D=(double *)malloc(6*sizeof(double));
	U=(double *)malloc(6*sizeof(double));
	muX=(double *)malloc(6*sizeof(double));
	dndmju=(double *)malloc(15*sizeof(double)); 
	dndmass=(double *)malloc(15*sizeof(double));
	dndt=(double *)malloc(15*sizeof(double)); 
	dnsdmass=(double *)malloc(15*sizeof(double)); 
	dnsdt=(double *)malloc(15*sizeof(double)); 
	dsdt=(double *)malloc(15*sizeof(double));
	n=(double *)malloc(15*sizeof(double));
	mun=(double *)malloc(15*sizeof(double));
	mn=(double *)malloc(15*sizeof(double));
	
	difer(mjuB,mjue,T,gsig,gom,gro,A,D,U,muX,dndmju,dndmass,dndt,dnsdmass,dnsdt,dsdt);
	
	unter=A[4]*D[3]*U[2]-A[3]*D[4]*U[2]-A[4]*D[2]*U[3]+A[2]*D[4]*U[3]+A[3]*D[2]*U[4]-A[2]*D[3]*U[4];
	a_OM=-(A[4]*D[3]*U[0]-A[3]*D[4]*U[0]-A[4]*D[0]*U[3]+A[0]*D[4]*U[3]+A[3]*D[0]*U[4]-A[0]*D[3]*U[4])/unter;
	b_OM=-(A[4]*D[3]*U[1]-A[3]*D[4]*U[1]-A[4]*D[1]*U[3]+A[1]*D[4]*U[3]+A[3]*D[1]*U[4]-A[1]*D[3]*U[4])/unter;
	c_OM=-(A[5]*D[4]*U[3]-A[4]*D[5]*U[3]-A[5]*D[3]*U[4]+A[3]*D[5]*U[4]+A[4]*D[3]*U[5]-A[3]*D[4]*U[5])/unter;
	a_RHO=(A[4]*D[2]*U[0]-A[2]*D[4]*U[0]-A[4]*D[0]*U[2]+A[0]*D[4]*U[2]+A[2]*D[0]*U[4]-A[0]*D[2]*U[4])/unter;
	b_RHO=(A[4]*D[2]*U[1]-A[2]*D[4]*U[1]-A[4]*D[1]*U[2]+A[1]*D[4]*U[2]+A[2]*D[1]*U[4]-A[1]*D[2]*U[4])/unter;
	c_RHO=(A[5]*D[4]*U[2]-A[4]*D[5]*U[2]-A[5]*D[2]*U[4]+A[2]*D[5]*U[4]+A[4]*D[2]*U[5]-A[2]*D[4]*U[5])/unter;
	a_SIG=-(A[3]*D[2]*U[0]-A[2]*D[3]*U[0]-A[3]*D[0]*U[2]+A[0]*D[3]*U[2]+A[2]*D[0]*U[3]-A[0]*D[2]*U[3])/unter;
	b_SIG=-(A[3]*D[2]*U[1]-A[2]*D[3]*U[1]-A[3]*D[1]*U[2]+A[1]*D[3]*U[2]+A[2]*D[1]*U[3]-A[1]*D[2]*U[3])/unter;
	c_SIG=-(A[5]*D[3]*U[2]-A[3]*D[5]*U[2]-A[5]*D[2]*U[3]+A[2]*D[5]*U[3]+A[3]*D[2]*U[5]-A[2]*D[3]*U[5])/unter;
	
	switch (mix){
	case 1:{
		dxdmjub_mjue_t= n_(mjuB,mjue,T,gsig,gom,gro,n,mun,mn);
		for (i=0;i<=14;i++) dxdmjue_mjub_t-=Ql_[i]*n[i];//dpHdmjue_mjub_t=SUM_by_i(-qH[i]nH[i])
		dxdmjub_mjue_t-=(*nq)(mjuB,mjue,T,n);//nBH-nBQ
		for (i=0;i<=qi;i++) dxdmjue_mjub_t+=QQ[i]*n[i];//-dpQdmjue_mjub_t=-SUM_by_i(-qQ[i]nQ[i])
		dxdt_mjub_mjue= s_(mjuB,mjue,T,gsig,gom,gro)-(*sq)(mjuB,mjue,T);//sH-sQ
		   };break;
	default:{
		for (i=0;i<=12;i++) 
			dxdmjub_mjue_t+=Ql_[i]*(dndmju[i]-G2G[i]*dndmju[i]*a_OM-I3_[i]*G2G[i]*dndmju[i]*a_RHO
			-G2G[i]*dndmass[i]*a_SIG);
		for (i=0;i<=14;i++) 
			dxdmjue_mjub_t-=Ql_[i]*Ql_[i]*dndmju[i];
		for (i=0;i<=12;i++) 
			dxdmjue_mjub_t-=Ql_[i]*(G2G[i]*dndmju[i]*b_OM+I3_[i]*G2G[i]*dndmju[i]*b_RHO
			+G2G[i]*dndmass[i]*b_SIG);
		for (i=0;i<=14;i++) 
			dxdt_mjub_mjue+=Ql_[i]*dndt[i];
		for (i=0;i<=12;i++) 
			dxdt_mjub_mjue-=Ql_[i]*(G2G[i]*dndmju[i]*c_OM+I3_[i]*G2G[i]*dndmju[i]*c_RHO
			+G2G[i]*dndmass[i]*c_SIG);
			}
	}
	
	for (i=0;i<=12;i++){
		dndmjub_mjue_t+=dndmju[i]-G2G[i]*dndmju[i]*a_OM
			-I3_[i]*G2G[i]*dndmju[i]*a_RHO-G2G[i]*dndmass[i]*a_SIG;
		dndmjue_mjub_t+=-Ql_[i]*dndmju[i]-G2G[i]*dndmju[i]*b_OM
			-I3_[i]*G2G[i]*dndmju[i]*b_RHO-G2G[i]*dndmass[i]*b_SIG;
		dndt_mjub_mjue+=dndt[i]-G2G[i]*dndmju[i]*c_OM
			-I3_[i]*G2G[i]*dndmju[i]*c_RHO-G2G[i]*dndmass[i]*c_SIG;
		dsdmjub_mjue_t+=dndt[i]-G2G[i]*dndt[i]*a_OM
			-I3_[i]*G2G[i]*dndt[i]*a_RHO+G2G[i]*dnsdt[i]*a_SIG;
		dsdmjue_mjub_t+=-Ql_[i]*dndt[i]-G2G[i]*dndt[i]*b_OM
			-I3_[i]*G2G[i]*dndt[i]*b_RHO+G2G[i]*dnsdt[i]*b_SIG;
		dsdt_mjub_mjue+=dsdt[i]-G2G[i]*dndmju[i]*c_OM
			-I3_[i]*G2G[i]*dndmju[i]*c_RHO+G2G[i]*dnsdt[i]*c_SIG;
	}
	dsdmjue_mjub_t+=dndt[13]+dndt[14];
	dsdt_mjub_mjue+=dsdt[13]+dsdt[14];
	
	*DNBDMJUB=dndmjub_mjue_t-dxdmjub_mjue_t/dxdmjue_mjub_t*dndmjue_mjub_t;
	*DNBDT=dndt_mjub_mjue-dxdt_mjub_mjue/dxdmjue_mjub_t*dndmjue_mjub_t;
	*DSDMJUB=dsdmjub_mjue_t-dxdmjub_mjue_t/dxdmjue_mjub_t*dsdmjue_mjub_t;
	DSDTr=dsdt_mjub_mjue-dxdt_mjub_mjue/dxdmjue_mjub_t*dsdmjue_mjub_t;
	
	free(A);
	free(D);
	free(U);
	free(muX);
	free(dndmju);
	free(dndmass);
	free(dndt);
	free(dnsdmass);
	free(dnsdt);
	free(dsdt);
	free(n);
	return DSDTr;
}

void EoS_NLW::fields(double *gmtr,double *fmtr,double **fjacm)
{double *n,*mun,*mn; double *dndmju,*dndmass,*dndt,*dnsdmass,*dnsdt,*dsdt,*A,*U,*D,*muX,mjus,ms;
int i;


/*
for (i=1;i<=_NUMb_;i++){
int j;	 fmtr[i]=zero;
for (j=1;j<=_NUMb_;j++) 
fjacm[i][j]=zero;
}

*/

for(i=0;i<=12;i++){
	mjus=_MJUB_-(Ql_[i]*gmtr[1])-G2G[i]*gmtr[3]-G2G[i]*(I3_[i])*gmtr[4];
	if(mjus<0.0) mjus*=-1;
	ms=fabs(M_[i]-G2G[i]*gmtr[2]);
	mass=fabs(ms);
	g = G_[i];
	if(mass>3000) mass =300; 
	fmtr[2]+=G2G[i]*EoS_HB::ns_(mjus,_T_)+G2G[i]*EoS_HB::ns_(-mjus,_T_)+zero*zero;
	//else {fmtr[2] = 1.0e+120; 
	//goto A1 ; }
}

fmtr[2]=SIGfac*hc*fmtr[2]-gmtr[2]
//#if NONLIN
-SIGfac/hc/hc*Bi*M_[0]*gmtr[2]*gmtr[2]-SIGfac/hc/hc*C*gmtr[2]*gmtr[2]*gmtr[2]
//#endif
;

n=(double *)malloc(16*sizeof(double));
mun=(double *)malloc(16*sizeof(double));
mn=(double *)malloc(16*sizeof(double));
_NB_= n_(_MJUB_,gmtr[1],_T_,gmtr[2],gmtr[3],gmtr[4],n,mun,mn);

for(i=0;i<=12;i++) fmtr[3]+=G2G[i]*n[i];
fmtr[3]=OMfac*hc*fmtr[3]-gmtr[3];

for(i=0;i<=12;i++) fmtr[4]+=G2G[i]*(I3_[i])*n[i];
fmtr[4]=RHOfac*hc*fmtr[4]-gmtr[4];

A=(double *)malloc(6*sizeof(double));
D=(double *)malloc(6*sizeof(double));
U=(double *)malloc(6*sizeof(double));
muX=(double *)malloc(6*sizeof(double));
dndmju=(double *)malloc(15*sizeof(double)); dndmass=(double *)malloc(15*sizeof(double));
dndt=(double *)malloc(15*sizeof(double)); dnsdmass=(double *)malloc(15*sizeof(double)); 
dnsdt=(double *)malloc(15*sizeof(double)); dsdt=(double *)malloc(15*sizeof(double));
//TRACE("\nMalloced :--> n, mun, mn,A,D,U,muX,dndmju,dndmass,dnsdmass,dsdt,dnsdt,dndt");
difer(_MJUB_,gmtr[1],_T_,
	  gmtr[2],gmtr[3],gmtr[4],A,D,U,muX,dndmju,dndmass,dndt,dnsdmass,dnsdt,dsdt);

fjacm[2][1]=D[1];fjacm[3][1]=A[1];fjacm[4][1]=U[1];
fjacm[2][2]=D[4];fjacm[3][2]=A[4];fjacm[4][2]=U[4];
fjacm[2][3]=D[2];fjacm[3][3]=A[2];fjacm[4][3]=U[2];
fjacm[2][4]=D[3];fjacm[3][4]=A[3];fjacm[4][4]=U[3];

switch (_MIX_){
case 1:{
	fmtr[1]=p_(_MJUB_,gmtr[1],_T_,gmtr[2],gmtr[3],gmtr[4])
		-(*_PQ_)(_MJUB_,gmtr[1],_T_);
	_NB_=n_(_MJUB_,gmtr[1],_T_,gmtr[2],gmtr[3],gmtr[4],n,mun,mn);
	for(i=0;i<=14;i++) fjacm[1][1]-=(Ql_[i]*n[i]);
	_NB_=(*_NQ_)(_MJUB_,gmtr[1],_T_,n);
	for(i=0;i<=_qi_;i++) 
		fjacm[1][1]+=(_QQ_[i]*n[i]);
	fjacm[1][2]=fmtr[2]/SIGfac/hc;
	fjacm[1][3]=-fmtr[3]/OMfac/hc;
	fjacm[1][4]=-fmtr[4]/RHOfac/hc;
	   }break;
default:{
	_NB_=n_(_MJUB_,gmtr[1],_T_,gmtr[2],gmtr[3],gmtr[4],n,mun,mn);
	for(i=0;i<=14;i++) fmtr[1]+=(Ql_[i]*n[i]);
	fjacm[1][1]=muX[1];
	fjacm[1][2]=muX[4];
	fjacm[1][3]=muX[2];
	fjacm[1][4]=muX[3];
		}
}
free(n);free(A);free(D);free(U);free(muX);
free(dndmju);free(dndmass);free(dndt);free(dnsdmass);free(dnsdt);free(dsdt);
free(mun);free(mn);
//TRACE("\n Free :--> n, mun, mn,A,D,U,muX,dndmju,dndmass,dnsdmass,dsdt,dnsdt,dndt");
}
/***********************************************************************/
EoS_NLW *m_pNLW;

void fields_(double *gmtr0,double *fmtr0,double **fjacmtr0)
{
	
	int i,n;
	n = 	m_pNLW->_NUMb_;
	for (i=1;i<= n;i++){
		int j;fmtr0[i]=zero;
		for (j=1;j<= n;j++) fjacmtr0[i][j]=zero;
	}
	m_pNLW->fields( gmtr0, fmtr0, fjacmtr0);
}

double EoS_NLW::musigomro(double mjuB,double t,int mix,_NQtp_ nq,_PQtp_ pq,int qi,double *QQ,double *gsig,double *gom,double *gro)
{double mjue,*gmtr,*shur,**shurjac;int ntrial=100; 
// m_pNLW = new EoS_NLW; 
m_pNLW = this;
_MIX_=mix;
_MJUB_=mjuB;
_T_=t;
_NQ_=nq;
_PQ_=pq;
_qi_=qi;
_QQ_=QQ;
_NUMb_=4;

gmtr=dvector(1,_NUMb_);
shurjac=dmatrix(1,_NUMb_,1,_NUMb_);
shur=dvector(1,_NUMb_);

gmtr[1]= 
//130;
-mjuB;
gmtr[2]= 
//20;
-mjuB;
gmtr[3]= 
//0.1;
-mjuB;
gmtr[4]= 
//-0.02;
mjuB;

again: mnewt(fields_,ntrial,gmtr,_NUMb_,zero,zero);
fields_(gmtr,shur,shurjac);
double acc= fabs(shur[1]*shur[2]*shur[3]*shur[4]);
if ((ntrial<1000)&&(acc>zero*zero*zero*zero))
{ntrial*=10; goto again;}

mjue=gmtr[1];
*gsig=gmtr[2];
*gom=gmtr[3];
*gro=gmtr[4];

//printf("\nmjuB=%g mjue=%g \t X=%g D=%g A=%g U=%g\n",mjuB,mjue,shur[1],shur[2],shur[3],shur[4]);

free_dvector(gmtr,1,_NUMb_);
free_dmatrix(shurjac,1,_NUMb_,1,_NUMb_);
free_dvector(shur,1,_NUMb_);
//delete m_pNLW;
return mjue;
}

double EoS_NLW::musigomro(double mjuB,double t,double *gsig,double *gom,double *gro)
{double mjue,*gmtr,*shur,**shurjac;int ntrial=100; 
// m_pNLW = new EoS_NLW; 
m_pNLW = this;
_MIX_= 0;
_MJUB_=mjuB;
_T_=t;
_NQ_=0;
_PQ_=0;
_qi_=5;
//_QQ_=QQ;
_NUMb_=4;

gmtr=dvector(1,_NUMb_);
shurjac=dmatrix(1,_NUMb_,1,_NUMb_);
shur=dvector(1,_NUMb_);

gmtr[1]= 
//130;
-mjuB;
gmtr[2]= 
//20;
-mjuB;
gmtr[3]= 
//0.1;
-mjuB;
gmtr[4]= 
//-0.02;
mjuB;

again: mnewt(fields_,ntrial,gmtr,_NUMb_,zero,zero);
fields_(gmtr,shur,shurjac);
double acc= fabs(shur[1]*shur[2]*shur[3]*shur[4]);
if ((ntrial<1000)&&(acc>zero*zero*zero*zero))
{ntrial*=10; goto again;}

mjue=gmtr[1];
*gsig=gmtr[2];
*gom=gmtr[3];
*gro=gmtr[4];

//printf("\nmjuB=%g mjue=%g \t X=%g D=%g A=%g U=%g\n",mjuB,mjue,shur[1],shur[2],shur[3],shur[4]);

free_dvector(gmtr,1,_NUMb_);
free_dmatrix(shurjac,1,_NUMb_,1,_NUMb_);
free_dvector(shur,1,_NUMb_);
//delete m_pNLW;
return mjue;
}


void EoS_NLW::ffields(double *gmtr,double *fmtr,double **fjacmtr)
{double *n,*mun,*mn; double *dndmju,*dndmass,*dndt,*dnsdmass,*dnsdt,*dsdt,*A,*U,*D,*muX,mjus,ms;
int i;

for (i=1;i<=_NUMb_;i++){int j;fmtr[i]=zero;for (j=1;j<=_NUMb_;j++) fjacmtr[i][j]=zero;}

for(i=0;i<=12;i++){
	mjus=_MJUB_ - (Ql_[i]*_MJUE_) - G2G[i]*gmtr[2] - G2G[i]*(I3_[i])*gmtr[3];
	ms=fabs(M_[i] - G2G[i]*gmtr[1]);
	mass = ms;
	g = G_[i];
	fmtr[1]+= G2G[i]*EoS_HB::ns_(mjus,_T_) + G2G[i]*EoS_HB::ns_(-mjus,_T_)+zero*zero;
}
fmtr[1]=SIGfac*hc*fmtr[1]-gmtr[1]
//#if NONLIN
- SIGfac/hc/hc*Bi*M_[0]*gmtr[1]*gmtr[1]-SIGfac/hc/hc*C*gmtr[1]*gmtr[1]*gmtr[1]
//#endif
;

n=(double *)malloc(16*sizeof(double));
mun=(double *)malloc(16*sizeof(double));
mn=(double *)malloc(16*sizeof(double));

_NB_= n_(_MJUB_,_MJUE_,_T_,gmtr[1],gmtr[2],gmtr[3],n,mun,mn);

for(i=0;i<=12;i++) fmtr[2]+=G2G[i]*n[i];
fmtr[2]=OMfac*hc*fmtr[2]-gmtr[2];

for(i=0;i<=12;i++) fmtr[3]+=G2G[i]*(I3_[i])*n[i];
fmtr[3]=RHOfac*hc*fmtr[3]-gmtr[3];

A=(double *)malloc(6*sizeof(double));
D=(double *)malloc(6*sizeof(double));
U=(double *)malloc(6*sizeof(double));
muX=(double *)malloc(6*sizeof(double));
dndmju=(double *)malloc(15*sizeof(double)); 
dndmass=(double *)malloc(15*sizeof(double));
dndt=(double *)malloc(15*sizeof(double)); 
dnsdmass=(double *)malloc(15*sizeof(double)); 
dnsdt=(double *)malloc(15*sizeof(double)); 
dsdt=(double *)malloc(15*sizeof(double));

difer(_MJUB_,_MJUE_,_T_,gmtr[1],gmtr[2],gmtr[3],A,D,U,muX,dndmju,dndmass,dndt,dnsdmass,dnsdt,dsdt);

fjacmtr[1][1]=D[4];fjacmtr[2][1]=A[4];fjacmtr[3][1]=U[4];
fjacmtr[1][2]=D[2];fjacmtr[2][2]=A[2];fjacmtr[3][2]=U[2];
fjacmtr[1][3]=D[3];fjacmtr[2][3]=A[3];fjacmtr[3][3]=U[3];

free(n);
free(A);
free(D);
free(U);
free(muX);
free(dndmju);
free(dndmass);
free(dndt);
free(dnsdmass);
free(dnsdt);
free(dsdt);
}



void ffields_(double *gmtr,double *fmtr,double **fjacmtr)
{
	int i;
	
	for (i=1;i<=m_pNLW->_NUMb_;i++){
		int j;fmtr[i]=zero;
		for (j=1;j<=m_pNLW->_NUMb_;j++) fjacmtr[i][j]=zero;
	}
	
	
	m_pNLW->ffields( gmtr, fmtr, fjacmtr);
}


void EoS_NLW::sigomro(double mjuB,double mjue,double t,double *gsig,double *gom,double *gro)
{

	double *gmtr,*shur,**shurjac;int ntrial=100;
//m_pNLW = new EoS_NLW;
m_pNLW  = this;
_MJUE_=mjue;_MJUB_=mjuB;_T_=t;
_NUMb_=3;
gmtr=dvector(1,_NUMb_);
shurjac=dmatrix(1,_NUMb_,1,_NUMb_);
shur=dvector(1,_NUMb_);
gmtr[1]=-mjuB;
gmtr[2]=-mjuB;
gmtr[3]=mjuB;
again:mnewt(ffields_,ntrial,gmtr,_NUMb_,zero,zero);
	  ffields(gmtr,shur,shurjac);
	  
	  if ((ntrial<10000)&&(fabs(shur[1]*shur[2]*shur[3])>zero*zero*zero)){ntrial*=10; goto again;}
	  
	  *gsig=gmtr[1];
	  *gom=gmtr[2];
	  *gro=gmtr[3];
	  //printf("\nmjuB=%g mjue=%g \t X=%g D=%g A=%g U=%g\n",mjuB,mjue,shur[1],shur[2],shur[3],shur[4]);
	  
	  free_dvector(gmtr,1,_NUMb_);
	  free_dmatrix(shurjac,1,_NUMb_,1,_NUMb_);
	  free_dvector(shur,1,_NUMb_);
	  //delete  m_pNLW;
}


double EoS_NLW::Charge(double mu_B, double mu_E, double T)
{
//	double gsig, gom, gro;
	double HQ=0;
	double *n,*mun,*mn;
sigomro(mu_B,mu_E, T, &_GSIG_, &_GOM_, &_GRO_);
n=(double *)malloc(16*sizeof(double));
mun=(double *)malloc(16*sizeof(double));
mn=(double *)malloc(16*sizeof(double));

_NB_= n_(mu_B,mu_E,T, _GSIG_, _GOM_, _GRO_,n,mun,mn);

	for(int i=0;i<=14;i++) HQ+=(Ql_[i]*n[i]);

	free(n);
	free(mun);
	free(mun);
return HQ;
}


#undef zer0
#undef db
#undef OMfac
#undef SIGfac
#undef RHOfac
#undef B 
#undef C 
#undef GH2GN
#undef hc3
#undef hc
#undef NONLIN


