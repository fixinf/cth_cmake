//#include "stdafx.h"
#include "EoS_crust.h"
#include <math.h>
#include "Zbrent.h"

#define _mu_  1.66e-24
#define cs  (2.99e10)
#define cs2  (cs*cs)
#define n0  0.16
//#define Ktr 1.5940457572502688e33
#define Ktr 1.5931901234567905e33

#define nd  2.62673e-4
#define ncc 0.0854
//#define ncc 0.000854
//without crust BSk20
#define Zp 40.0


double BSk20a[] = {0,4.078, 7.587, 0.00839, 0.21695, 3.614, 11.942, 13.751, 
   1.3373, 3.606, -22.996, 1.6229, 4.88, 14.274, 23.56, -1.5564, 
   2.095, 15.294, 0.084, 6.36, 11.67, -0.042, 14.8, 14.18};
double BSk20b[] = {0,0.632, 2.71, 0.0352, 0.383, 1.087e-5, 3.51, 0.6167,3.4815};
double BSk20c[] = {0,0.152, 1.02, 10.26, 3.691, 2.586, 1.067e-5, 3.255, 
   0.6123, 12.0570};
double BSk20d[] = {0,81.631, 31.583, 15.310, 0.594, 58.890, 56.74, 0.449, 4.5, 
   0.58, 7.5};
double BSk20e[] = {0,-0.0078, 0.745, 0.508, 22.888, 0.449, 0.00323};
double BSk20m[] = {0,-0.0364, 0.2748, 0.2603, 12.99, 0.0767, 0.00413};

double BSk20p[] = {9.30, 92.8, 12.95, 1.493, 0.354, 7.57};
double BSk20ps[] = {0,134.7, 188.2, 275.6, 0.4346, 0.00163, 0.00149};
double BSk20pss[] = {20, 19};

double dz(double *a,double x)
{
	return (a[1] + x*a[2] + pow(x,3)*a[3])/
    ((1 + exp(a[5]*(x - a[6])))*(1 + x*a[4]))
     + (a[7] + x*a[8])/
    (1 + exp((-x + a[6])*a[9])) + 
   (a[10] + x*a[11])/
    (1 + exp(a[12]*(-x + a[13]))) + 
   (a[14] + x*a[15])/
    (1 + exp(a[16]*(-x + a[17]))) + 
   a[18]/(1 + pow(a[19],2)*
       pow(x - a[20],2)) + 
   a[21]/(1 + pow(a[22],2)*pow(x - a[23],2));
}
double f1(double b8,double n)
  {return  1.0/(exp(1.1* log10(n) + b8) + 1);}

double f2(double c9,double rot)
{
double xsi = log10(rot*_mu_)+39;	
	return  1.0/(exp(xsi - c9) + 1);
}


double Deltap(double *b, double n){
  return  (1 - f1(b[8],n)) *(b[1]* pow(n,b[2]) + b[3]* sqrt(n))/pow(1 + b[4]*n,2) +
	  f1(b[8],n)* n/(b[5] + b[6]*pow( n,b[7]));
}

double roovn(double *c,double rot){  
return   1 + (1 - f2(c[9],rot))* (c[1] *pow(rot,c[2]) + c[3]*pow( rot,c[4]))/pow(1 + c[5]* rot,3) + 
f2(c[9],rot)* rot/(c[6] + c[7]*pow( rot,c[8]));
}

double ro_mass(double n)
{
return n*_mu_*1e39*(1 + Deltap(BSk20b,n));
}

double n_baryon(double x)
{
	double rot = exp(log(10.0)*x)/(_mu_*1e39);
	return rot/roovn(BSk20c,rot);
}

double miu(double n) // in MeV
{double u = ro_mass(n); 
double pp = exp(log(10.0)*dz(BSk20a,log10(u)));
return (u*cs2 + pp)/(n+1e-30)/Ktr;
}
double _mu;
double miuEQ(double n)
{ 
	return miu(n)-_mu;
}

double n_b(double mu)
{
_mu = mu;
return zbrent(miuEQ,0.00000001, 20*n0,1e-7);
}

/*
double depsdn(double *b,double n){
  double eb8 = exp(b[8]);
	
return	1e39*cs2*_mu_*
   (1 + eb8*
      ((-2*pow(n,1.477723930093577 + b[2])*
           b[1]*b[4])/
         ((1 + eb8*
              pow(n,0.477723930093577))*
           pow(1 + n*b[4],3)) - 
        (2*pow(n,1.977723930093577)*b[3]*b[4])/
         ((1 + eb8*
              pow(n,0.477723930093577))*
           pow(1 + n*b[4],3)) + 
        (pow(n,b[2])*b[1]*
           (eb8*
              pow(n,0.955447860187154)*
              (1 + b[2]) + 
             pow(n,0.477723930093577)*
              (1.477723930093577 + 1.*b[2])))/
         (pow(1. + 
             eb8*
              pow(n,0.477723930093577),2)*
           pow(1. + 1.*n*b[4],2)) + 
        ((1.977723930093577*
              pow(n,0.9777239300935769) + 
             1.5*eb8*
              pow(n,1.4554478601871539))*b[3])/
         (pow(1. + 
             eb8*
              pow(n,0.477723930093577),2)*
           pow(1. + 1.*n*b[4],2)) - 
        (0.477723930093577*
           pow(n,1.477723930093577))/
         (pow(1. + 
             1.*eb8*
              pow(n,0.477723930093577),2)*
           (1.*b[5] + 1.*pow(n,b[7])*b[6]))) + 
     (n*(2*b[5] - pow(n,b[7])*b[6]*
           (-2 + b[7])))/
      ((1 + eb8*
           pow(n,0.477723930093577))*
        pow(b[5] + pow(n,b[7])*b[6],2)));
}
*/
double Yem(double *q,double n){
	return (q[1] + q[2]* n + q[3]* n*n*n*n)/(1 + q[4]*sqrt(n*n*n) + 
    q[5]* n*n*n*n) *exp(-q[6]* n*n*n*n*n);
}

double AmZ(double *p,double n){
	double xx = log10(n/nd); 
return 
(p[1] + p[2]* xx + pow(p[3]*xx,3.5))/(1 + pow(p[4]*xx,p[5]))*(1 - pow(n/ncc,p[0]));
}

double Z(double *p,double n){return  Zp* exp(-pow(n/ncc,p[1]))*(1 - pow(n/ncc,p[0]));}

double Ap(double *p,double n)
{
	double x = n/nd;
	double lx =log10(x);
return (p[1] + p[2]*lx + p[3]*lx*lx)/(1 + pow(p[4]*lx,4.0)) *(1 - x*p[5])*(1 - pow(p[6]*x,2.0));
}

EoS_crust::EoS_crust(void)
{
//	double s=ro_mass(5*n0)*1e-14;
//	double u = n_baryon(log10(s)+14);
//double nn[5];
//double nnn=nb(miu(u),nn);
//double eee = eps(miu(u));
//double ppp = p(miu(u));
//double checking = miu(u)*nnn-ppp-eee; 
//	printf("%15.15g %30.15g %0.20g\n%0.5g %5.20g %1.20g\n",nnn,ppp,eee,checking,u,depsdn(BSk20b,u)/Ktr);
}

EoS_crust::~EoS_crust(void)
{
}

double EoS_crust::eps(double mu)
{ double nn= n_b(mu);
	double eps_ =ro_mass(nn)*cs2; 
	return eps_/Ktr;
}

double EoS_crust::p(double mu)
{
	double u = ro_mass(n_b(mu)); 
//	if(u<0.00001) return 0; 
double pp = exp(log(10.0)*dz(BSk20a,log10(u)));
return pp/Ktr;
	
}

double EoS_crust::ro(double mu)
{  double nn =n_b(mu);
	return ro_mass(nn)/Ktr;
}

double  EoS_crust::n_(double mu,double mue,double *nn)
{double n = n_b(mu);
	double Zpp,A,Yp;

 for(int i=2;i<20;i++) nn[i]=0.;
 if(n>ncc){ //core  npe-phase
nn[13] = Yem(BSk20e,n)*n;
nn[14] = Yem(BSk20m,n)*n;
if(nn[14]<=0.0) nn[14] = 0.0;
nn[1] = nn[13] + nn[14];
nn[0] = n - nn[1];
if(nn[1]<0) {
exit(1);
}
 }else
	 if(n>nd){ //inner crust Aen-phase
	Zpp = Z(BSk20pss,n);
    A = AmZ(BSk20p,n)+Zpp;

	Yp = Zpp/Ap(BSk20ps,n);
nn[13] = nn[1] = Yp*n;
nn[0]  = n - nn[1];
nn[1] = Zpp/A*n;
if(nn[1]<0) {
exit(1);
}
//nn[15] = A;
//nn[16] = Zpp;
//nn[17]= Ap(BSk20ps,n);
	 }
	 else{  //outer crust  Ae-phase
	 double Z0 = Z(BSk20pss,nd);
	 double A0 = Ap(BSk20ps,nd);
	 Zpp = (Z0 - 30)/(log10(nd) + 5)*(log10(n) + 5) + 30;
     A = (A0 - 95)/(log10(nd) + 5)*(log10(n) + 5) + 95;
     Yp = Zpp/A;
nn[13] = nn[1] = Yp*n;
nn[0]  = n - nn[1];
//nn[1] = Zpp/A*n;
if(nn[1]<0) {
exit(1);
}
//nn[17]=nn[15] = A;
//nn[16] = Zpp;
	 }
	return n;
}
